<?php

error_reporting(E_ALL);

use Phalcon\Mvc\Application;
use Phalcon\Config\Adapter\Ini as ConfigIni;
include '../app/library/simple_html_dom.php';
include '../app/library/xlsxwriter.class.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);

try{

	define('APP_PATH', realpath('..') . '/');

	/**
	 * Read the configuration
	 */
	$config = new ConfigIni(APP_PATH . 'app/config/config.ini');

	/**
	 * Auto-loader configuration
	 */
	require APP_PATH . 'app/config/loader.php';

	/**
	 * Load application services
	 */
	require APP_PATH . 'app/config/services.php';

	$application = new Application($di);

    //set error handler
    set_error_handler(
        function ($errno, $errstr, $errfile, $errline) use ($di) {
            $di->getLogger()->error("$errno, $errstr, $errfile, $errline");
        }
    );

    //Set fatal error logging
    register_shutdown_function(function () use ($di) {
        $error = error_get_last();
        if($error['type']!=null) {
            $di->getLogger()->error("{$error['type']} {$error['message']} {$error['file']} {$error['line']}");
        }
    });


	echo $application->handle()->getContent();

} catch (Exception $e){
	echo $e->getMessage();
    $di->getLogger()->error(get_class($e). ": ". $e->getMessage(). "\n");
    $di->getLogger()->error(" File=". $e->getFile(). "\n");
    $di->getLogger()->error(" Line=". $e->getLine(). "\n");
    $di->getLogger()->error($e->getTraceAsString());
}
