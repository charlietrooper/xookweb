<?php

class BookInfoHelper
{
     function index()
    {
        // ...
    }

    public static function getCurrentPrice($book)
    {
        $precioActual=0;
        $fechaActual=date("Y-m-d");
        // $fechaActual=date('Y-m-d', strtotime($fechaActual));

        //si el conteo es más de 5, significa que ha habido o hay alguna promoción
        //Un libro que solo ha tenido un precio, solo viene con 5 campos
        //Si ha habido algun cambio de precio se le agrega un array que a su vez contiene más datos con los precios.
        if(!(count($book['activeRevision']['prices']['price'])>5))
        {
            $from=$book['activeRevision']['prices']['price']['@attributes']['from'];
            if ($fechaActual>$from)
            {
                $precioActual=$book['activeRevision']['prices']['price']['sellingprice']['@value'];

            }
        }

        else{
            //recorremos lo que traiga 'price'
            foreach($book['activeRevision']['prices']['price'] as $precios)
            {
                if (isset($precios['@attributes']['currency']))
                {
                    $from=$precios['@attributes']['from'];
                    $to=$precios['@attributes']['to'];
                    //Si el JSON viene con los campos 'from' y 'to' vamos a comparar esas dos fechas con la fecha actual
                    //si la fecha actual está entre el FROM y el TO, entonces aquí se encuentra el precio vigente
                    if (isset($to))
                    {
                        if(($from > $fechaActual) && ($fechaActual < $to))
                        {
                            $precioActual=$precios['sellingprice']['@value'];
                        }
                    }
                    //si solo hay FROM hay que checar que el Precio Actual sea después de la fecha del FROM para...
                    //... así verificar que el precio sea vigente
                    else{
                        if ($fechaActual>$from)
                        {
                            $precioActual=$precios['sellingprice']['@value'];
                        }
                    }

                }
            }
        }

        return $precioActual;

    }
}
?>