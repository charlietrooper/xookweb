<?php

use Phalcon\Mvc\User\Component;

/**
 * Elements
 *
 *
 *
 *
 *
 * Helps to build UI elements for the application
 */

class Elements extends Component
{


    private $_headerMenu = array(
        'navbar-left' => array(
            'index' => array(
                'caption' => 'Inicio',
                'action' => 'index'
            ),
            'partners'=>array(
                'caption'=> 'Socios',
                'action'=> 'index'
            ),
            'affiliates'=>array(
                'caption'=>'Afiliados',
                'action'=> 'index'
            ),
            'sales-reports' =>array(
                'caption'   => 'Reportes',
                'action'    => 'index'
            ),
            'catalog-feed-file' => array(
                'caption'   => 'Catalog Feed File',
                'action'    => 'index'
            ),
            'authentication' => array(
                'caption'   => 'API',
                'action' => 'index'
            ),
            'users' => array(
                'caption'   => 'Usuarios del sistema',
                'action'    => 'index'
            ),
            'session' => array(
                'caption' => 'Entrar',
                'action' => 'index'
            )
        ),

    );


    private $_headerMenu2 = array(
        'navbar-left' => array(
            'index' => array(
                'caption' => 'Inicio',
                'action' => 'index'
            ),
            'partners'=>array(
                'caption'=> 'Socios',
                'action'=> 'index'
            ),
            'affiliates'=>array(
                'caption'=>'Afiliados',
                'action'=> 'index'
            ),
            'sales-reports' =>array(
                'caption'   => 'Reportes',
                'action'    => 'index'
            ),
            'catalog-feed-file' => array(
                'caption'   => 'Catalog Feed File',
                'action'    => 'index'
            ),
            'library' => array(
                'caption'   => 'API',
                'action' => 'index'
            ),
            'session' => array(
                'caption' => 'Entrar',
                'action' => 'index'
            )
        ),

    );

    private $_tabs = array(
        'Ventas' => array(
            'controller' => 'sales-reports',
            'action' => 'index',
            'any' => true
        ),
        'Detalle de ventas' => array(
            'controller' => 'purchases-line',
            'action' => 'index',
            'any' => true
        ),
        'Ventas por unidad' => array(
            'controller'    => 'ventas-unidades',
            'action'        => 'index',
            'any'           => true
        ),
        'Libros más vendidos' => array(
            'controller'    => 'mas-vendidos',
            'action'        => 'actual',
            'any'           => true
        ),
        'Clientes' => array(
            'controller' => 'customers',
            'action' => 'index',
            'any' => true
        ),
        'Editoriales' => array(
            'controller' => 'editoriales',
            'action' => 'index',
            'any' => true
        ),
        'Sellos' => array(
            'controller' => 'sellos',
            'action' => 'index',
            'any' => true
        ),
        'Distribuidores digitales' => array(
            'controller' => 'titular-cuenta',
            'action' => 'index',
            'any' => true
        ),
        'Precio actual' => array(
            'controller' => 'precio-actual',
            'action' => 'index',
            'any' => true
        ),
        'Precio futuro' => array(
            'controller' => 'precio-futuro',
            'action' => 'index',
            'any' => true
        ),
        'Comparador' => array(
            'controller' => 'comparador',
            'action' => 'index',
            'any' => true
        ),
        'Libros Activos' => array(
            'controller' => 'libros-activos',
            'action' => 'index',
            'any' => true
        ),
        'Libros Gratuitos' => array(
            'controller' => 'libros-gratuitos',
            'action' => 'index',
            'any' => true
        ),
        'Información de Ventas' => array(
            'controller' => 'info-ventas',
            'action' => 'index',
            'any' => true
        ),




        /*,
        'Catálogos de actualización' => array(
            'controller' => 'notification-catalogues',
            'action' => 'index',
            'any' => true
        )*/
    );


    private $_tabsapi = array(
        'Autenticación' => array(
            'controller'    => 'authentication',
            'action'        => 'index',
            'any'           => true
        ),
        'Compras' => array(
            'controller'    => 'purchases',
            'action'        => 'index',
            'any'           => true
        ),
        'Pre Órdenes' => array(
            'controller'    => 'pre-order',
            'action'        => 'index',
            'any'           => true
        ),
        'Lista de pre órdenes' => array(
            'controller'    => 'pre-ordenes',
            'action'        => 'index',
            'any'           => true
        ),
        'Biblioteca' => array(
            'controller'    => 'library',
            'action'        => 'index',
            'any'           => true
        ),
        'Libros' => array(
            'controller'    => 'libros',
            'action'        => 'index',
            'any'           => true
        ),
        'Busqueda de ISBN\'s' => array(
            'controller'    => 'isbn',
            'action'        => 'index',
            'any'           => true
        ),
        'Unión de cuentas' => array(
            'controller'    => 'merge',
            'action'        => 'index',
            'any'           =>  true
        ),
        'Configuración de url' => array(
            'controller'    => 'config',
            'action'        => 'index',
            'any'           => true
        ),
        'Libros en Pre orden' => array(
            'controller'    => 'listas',
            'action'        => 'index',
            'any'           => true
        ),
        'Web Links' => array(
            'controller' => 'weblinks',
            'action' => 'index',
            'any' => true
        ),

    );

    /**
     * Builds header menu with left and right items
     *
     * @return string
     */


    public function getMenu()
    {

        $auth = $this->session->get('auth');
        if ($auth) {

            if ($auth['role_name']=='admin')
            {
                $this->_headerMenu['navbar-left']['session'] = array(
                    'caption' => 'Cerrar sesión',
                    'action' => 'end'
                );
                $guardar='';
                $controllerName = $this->view->getControllerName();
                foreach ($this->_headerMenu as $position => $menu) {
                    echo '<div class="collapse navbar-collapse" id="navbar-collapse">';
                    echo '<ul class="nav navbar-nav ', $position, '">';
                    foreach ($menu as $controller => $option) {
                            if ($controllerName == $controller) {
                                if($option['caption']=="Reportes")
                                {
                                    echo '<li class="active barraTop">';

                                }
                                else if($option['caption']=="Cerrar sesión"){
                                    echo '<li class="active barraSesion">';

                                }

                                else{
                                    echo '<li class="active">';

                                }

                            } else {
                                if($option['caption']=="Reportes")
                                {
                                    echo '<li class="barraTop">';
                                }

                                else if($option['caption']=="Cerrar sesión"){
                                    echo '<li class="barraSesion">';
                                }
                                else{
                                    echo '<li>';
                                }

                            }
                        echo $this->tag->linkTo($controller . '/' . $option['action'], $option['caption']);
                        echo '</li>';
                    }
                    echo '</ul>';
                    echo '</div>';

                }
            }
            else
            {
                if($auth['role_name'] == 'affiliate')
                {
                    unset($this->_headerMenu['navbar-left']['users']);
                    unset($this->_headerMenu['navbar-left']['partners']);
                    unset($this->_headerMenu['navbar-left']['catalog-feed-file']);
                    unset($this->_headerMenu['navbar-left']['partners']);
                    unset($this->_headerMenu['navbar-left']['notification-catalogues']);
                    unset($this->_headerMenu['navbar-left']['sales-reports']);
                    unset($this->_headerMenu['navbar-left']['customers']);
                    unset($this->_headerMenu['navbar-left']['catalog-feed-file']);
                    unset($this->_headerMenu['navbar-left']['authentication']);
                    unset($this->_headerMenu['navbar-left']['ventas-unidades']);
                    $this->_headerMenu['navbar-left']['session'] = array(
                        'caption' => 'Cerrar sesión',
                        'action' => 'end'
                    );

                    $controllerName = $this->view->getControllerName();
                    foreach ($this->_headerMenu as $position => $menu) {
                        echo '<div class="collapse navbar-collapse" id="navbar-collapse">';
                        echo '<ul class="nav navbar-nav ', $position, '">';
                        foreach ($menu as $controller => $option) {
                            if ($controllerName == $controller)
                            {
                                if($option['caption']=="Reportes")
                                {
                                    echo '<li class="active barraTop">';
                                }
                                else if($option['caption']=="Cerrar sesión"){
                                    echo '<li class="active barraSesion">';
                                }

                                else{
                                    echo '<li class="active">';
                                }

                            }

                            else
                            {
                                if($option['caption']=="Reportes")
                                {
                                    echo '<li class="barraTop">' ;
                                }

                                else if($option['caption']=="Cerrar sesión"){
                                    echo '<li class="barraSesion">';
                                }
                                else{
                                    echo '<li>';
                                }

                            }
                            echo $this->tag->linkTo($controller . '/' . $option['action'], $option['caption']);
                            echo '</li>';
                        }
                        echo '</ul>';
                        echo '</div>';
                    }
                }
                elseif($auth['role_name'] == 'partner' || $auth['role_name'] == 'AdminSoloLectura'){
                    unset($this->_headerMenu2['navbar-left']['affiliates']);
                    unset($this->_headerMenu2['navbar-left']['users']);
                    unset($this->_headerMenu2['navbar-left']['partners']);
                    unset($this->_headerMenu2['navbar-left']['catalog-feed-file']);
                    unset($this->_headerMenu2['navbar-left']['authentication']);
                    $this->_headerMenu2['navbar-left']['session'] = array(
                        'caption' => 'Cerrar sesión',
                        'action' => 'end'
                    );

                    $controllerName = $this->view->getControllerName();
                    foreach ($this->_headerMenu2 as $position => $menu) {
                        echo '<div class="collapse navbar-collapse" id="navbar-collapse">';
                        echo '<ul class="nav navbar-nav ', $position, '">';
                        foreach ($menu as    $controller => $option) {
                            if ($controllerName == $controller)
                            {
                                if($option['caption']=="Reportes")
                                {
                                    echo '<li class="active barraTop">';
                                }
                                else if($option['caption']=="Cerrar sesión"){
                                    echo '<li class="active barraSesion">';
                                }

                                else{
                                    echo '<li class="active">';
                                }

                            }

                            else
                            {
                                if($option['caption']=="Reportes")
                                {
                                    echo '<li class="barraTop">';
                                }

                                else if($option['caption']=="Cerrar sesión"){
                                    echo '<li class="barraSesion">';
                                }
                                else{
                                    echo '<li>';
                                }

                            }
                            echo $this->tag->linkTo($controller . '/' . $option['action'], $option['caption']);
                            echo '</li>';
                        }
                        echo '</ul>';
                        echo '</div>';
                    }
                }

            }
        } else {
            unset($this->_headerMenu['navbar-left']['affiliates']);
            unset($this->_headerMenu['navbar-left']['users']);
            unset($this->_headerMenu['navbar-left']['partners']);
            unset($this->_headerMenu['navbar-left']['notification-catalogues']);
            unset($this->_headerMenu['navbar-left']['sales-reports']);
            unset($this->_headerMenu['navbar-left']['customers']);
            unset($this->_headerMenu['navbar-left']['catalog-feed-file']);
            unset($this->_headerMenu['navbar-left']['authentication']);

            $controllerName = $this->view->getControllerName();
            foreach ($this->_headerMenu as $position => $menu) {
                echo '<div class="collapse navbar-collapse" id="navbar-collapse">';
                echo '<ul class="nav navbar-nav ', $position, '">';
                foreach ($menu as $controller => $option) {
                    if ($controllerName == $controller) {
                        echo '<li class="active">';
                    } else {
                        echo '<li>';
                    }
                    echo $this->tag->linkTo($controller . '/' . $option['action'], $option['caption']);
                    echo '</li>';
                }
                echo '</ul>';
                echo '</div>';
            }
        }
    }

    /**
     * Returns menu tabs
     */
    public function getTabs()
    {
        $controllerName = $this->view->getControllerName();
        $actionName = $this->view->getActionName();
        $guardar='';
        echo '<ul class="nav nav-tabs fontDown">';
        foreach ($this->_tabs as $caption => $option) {
            if ($option['controller'] == $controllerName && ($option['action'] == $actionName || $option['any'])) {
                echo '<li class="active">';
                $guardar=$caption;

            } else {
                echo '<li>';
            }
            echo $this->tag->linkTo($option['controller'] . '/' . $option['action'], $caption), '<li>';

        }
        echo '</ul>';

        echo $this->regresarTitulo($guardar);
    }

    /*
     * Returns menu secondtabs
     */
    public function getTabsAPI()
    {
        $auth = $this->session->get('auth');

        if($auth['role_name'] == 'partner' || $auth['role_name'] == 'AdminSoloLectura')
        {
            unset($this->_tabsapi['Autenticación']);
            unset($this->_tabsapi['Compras']);
            unset($this->_tabsapi['Pre Ordenes']);
            unset($this->_tabsapi['Activar pre ordenes']);
            unset($this->_tabsapi['Configuración de url']);
            unset($this->_tabsapi['Unión de cuentas']);
            if($auth['role_name'] == 'partner')
            {
                unset($this->_tabsapi['Busqueda de ISBN\'s']);
            }
          //  unset($this->_tabsapi['Busqueda de ISBN\'s']);
        }


        $controllerName = $this->view->getControllerName();
        $actionName = $this->view->getActionName();
        $guardar='';
        echo '<ul class="nav nav-tabs">';
        foreach ($this->_tabsapi as $caption => $option) {
            if ($option['controller'] == $controllerName && ($option['action'] == $actionName || $option['any'])) {
                echo '<li class="active">';
                $guardar=$caption;
            } else {
                echo '<li>';
            }
            echo $this->tag->linkTo($option['controller'] . '/' . $option['action'], $caption), '<li>';
        }
        echo '</ul>';

        echo $this->regresarTitulo($guardar);
    }

    public function regresarTitulo($titulo)
    {
        if ($titulo=="Compras")
        {
            $titulo="";
        }

        else if ($titulo=="Libros")
        {
            $titulo="Ver detalles de libros";
        }

        else if ($titulo=="Configuración de url")
        {
            $titulo="Configuración";
        }
        $print='';
        $print.='<div class="titulo">';
        $print.='<h1 class="retroshadow">'.$titulo.'</h1>';
        $print.='</div>';
        return $print;

    }

   /* public function returnTitulo()
    {
        $print='';
        $print.='<div class="titulo">';
        $print.='<h1 class="retroshadow">'.$guardar.'</h1>';
        $print.='</div>';
        echo $print;
    }*/
}