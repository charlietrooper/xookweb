<?php

use Phalcon\Acl;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;
use Phalcon\Events\Event;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Acl\Adapter\Memory as AclList;

/**
 * SecurityPlugin
 *
 * This is the security plugin which controls that users only have access to the modules they're assigned to
 */
class SecurityPlugin extends Plugin
{

	/**
	 * Returns an existing or new access control list
	 *
	 * @returns AclList
	 */
	public function getAcl()
	{
		//throw new \Exception("something");
		if (!isset($this->persistent->acl)) {

			$acl = new AclList();

			$acl->setDefaultAction(Acl::DENY);

			//Register roles
			$roles = array(
				'admin'     => new Role('admin'),
                'AdminSoloLectura'    => new Role('AdminSoloLectura'),
                'partner'   => new Role('partner'),
                'affiliate' => new Role('affiliate'),
				'guests'    => new Role('guests')
			);

			foreach ($roles as $role) {
				$acl->addRole($role);
			}

			//Recursos para el Admin
			$privateResources = array(
                'partners'              => array('index','create','delete','edit','savepartner','update','eliminar', 'details'),
                'users'                 => array('index','create','register','edit','update', 'eliminar', 'delete'),
                'affiliates'            => array('index', 'create','delete', 'eliminar', 'register', 'update','edit'),
                'authentication'        => array('index', 'authenticate'),
                'purchases'             => array('index', 'verification', 'purchase', 'detalles', 'prueba', 'agr', 'agregardatos', 'agregargenero'),
                'library'               => array('index', 'downloadbook' ,'getlibrary', 'setlibrary','getbookisbn', 'getbookrev', 'deletebook'),
                'config'                => array('index', 'edit', 'configura'),
                'libros'                => array('index','enviarlibro','searchbookisbn', 'detalle'),
                'merge'                 => array('index','merge'),
                'activar-preordenes'    => array('act', 'activar'),
                'catalog-feed-file'     => array('index', 'search','excel','descargarxml'),
                'sales-metadata'         => array('index', 'updateMetadata'),

               // 'isbn'                      => array('index','buscamasivaisbn','excel')

			);
			foreach ($privateResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

            //Recursos para el orbile
            $AdminSoloLecturaResources = array(
                'purchases-line'            => array('searchadmin'),
                'notification-catalogues'   => array('search'),
                'customers'                 => array('search'),
                'isbn'                      => array('index','buscamasivaisbn','excel'),
                'pre-ordenes'               => array('index', 'buscar'),
                'pre-order'                   => array('index', 'list', 'cancel', 'act', 'activar'),
                'listas'                    => array('index','busqueda', 'excel'),
                'weblinks'                  => array('index','buscalinks','excel')

            );
            foreach($AdminSoloLecturaResources as $resource => $actions){
                $acl->addResource(new Resource($resource), $actions);
            }

            //Recursos para el partner
            $registeredResources = array(
                'sales-reports'             => array('index','showCsv','search', 'pais', 'estado', 'ciudad', 'details', 'selectgrafica','graficas', 'anioventas', 'mes', 'semana', 'dias', 'total'),
               // 'catalog-feed-file'         => array('index', 'search','excel','descargarxml'),
                'purchases-line'            => array('index', 'searchpartner', 'reporteexcel'),
                'ventas-unidades'           =>array('index', 'adminbusca', 'excel'),
                'notification-catalogues'   => array('index','excel','searchforpartner'),
                'customers'                 => array('index', 'showCsv','details', 'reportecompras', 'detalles', 'searchforpartner','prueba'),
                'library'                   => array('index','downloadbook' ,'getlibrary', 'setlibrary','getbookisbn', 'getbookrev', 'deletebook'),
                'libros'                    => array('index','enviarlibro','searchbookisbn', 'detalle'),
                'mas-vendidos'              => array('index', 'busqueda', 'excel', 'actual','pieData'),
                'editoriales'               => array('index','busqueda'),
                'sellos'                    => array('index','busqueda'),
                'titular-cuenta'            => array('index','busqueda'),
                'precio-actual'             => array('index','busqueda'),
                'precio-futuro'             => array('index','busqueda'),
                'comparador'                => array('index','busqueda'),
                'libros-activos'            => array('index','busqueda','excel'),
                'libros-gratuitos'          => array('index','busqueda','excel'),
                'info-ventas'               => array('index','buscaventas','excel'),


            );

            foreach ($registeredResources as $resource => $actions){
                $acl->addResource(new Resource($resource), $actions);
            }

            //Affiliate's areas resources
            $affiliateResources = array(
                'affiliates' => array('index', 'datos')
            );

            foreach ($affiliateResources as $resource => $actions){
                $acl->addResource(new Resource($resource), $actions);
            }

			//Public area resources
			$publicResources = array(
                'index'      => array('index'),
			    'errors'     => array('show401', 'show404', 'show500'),
				'session'    => array('index', 'register', 'start', 'end')
			);
			foreach ($publicResources as $resource => $actions) {
				$acl->addResource(new Resource($resource), $actions);
			}

			//Dar acceso a las areas publicas a todos los usuarios
			foreach ($roles as $role) {
				foreach ($publicResources as $resource => $actions) {
					foreach ($actions as $action){
						$acl->allow($role->getName(), $resource, $action);
					}
				}
			}

			//Dar acceso al admin, a las áreas privadas
			foreach ($privateResources as $resource => $actions) {
				foreach ($actions as $action){
					$acl->allow('admin', $resource, $action);
				}
			}

            //Dar acceso al orbile a las áreas orbile
            foreach($AdminSoloLecturaResources as $resource => $actions){
                //Acceso al orbile
                foreach($actions as $action){
                    $acl->allow('AdminSoloLectura', $resource, $action);
                }
                //Acceso al admin
                foreach ($actions as $action){
                    $acl->allow('admin', $resource, $action);
                }
            }

            //Dar acceso al área de partner al admin, al orbile y al partner
            foreach ($registeredResources as $resource=>$actions)
            {
                //Acceso al partner
                foreach($actions as $action)
                {
                    $acl->allow('partner', $resource, $action);
                }
                //Acceso al admin
                foreach($actions as $action)
                {
                    $acl->allow('admin', $resource, $action);
                }
                //Acceso al AdminSoloLectura
                foreach($actions as $action)
                {
                    $acl->allow('AdminSoloLectura', $resource, $action);
                }
            }

            //Dar acceso a los affiliate y a los admin, a las areas affiliates
            foreach ($affiliateResources as $resource => $actions)
            {
                //Acceso al affiliate
                foreach($actions as $action)
                {
                    $acl->allow('admin', $resource, $action);
                }

                //Acceso al admin
                foreach($actions as $action)
                {
                    $acl->allow('affiliate', $resource, $action);
                }
            }

			//The acl is stored in session, APC would be useful here too
			$this->persistent->acl = $acl;
		}

		return $this->persistent->acl;
	}
	/**
	 * This action is executed before execute any action in the application
	 *
	 * @param Event $event
	 * @param Dispatcher $dispatcher
	 */
    public function beforeDispatch(Event $event, Dispatcher $dispatcher)
    {
        $auth = $this->session->get('auth');
        if (!$auth){
            $role = 'guests';
        }else{
                $role = $auth ['role_name'];
        }

		$controller = $dispatcher->getControllerName();
		$action = $dispatcher->getActionName();

		$acl = $this->getAcl();

		$allowed = $acl->isAllowed($role, $controller, $action);

		if ($allowed != Acl::ALLOW) {
			$dispatcher->forward(array(
				'controller' => 'errors',
				'action'     => 'show401'
			));
            return false;
		}
	}
}