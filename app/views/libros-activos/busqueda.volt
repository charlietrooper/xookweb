{{ content() }}
<div class="row" xmlns="https://www.w3.org/1999/html">
    <div class="col-md-12" style="padding-bottom: 10px;">
        {{ link_to("libros-activos/index", "class":"btn btn-primary","&larr; Regresar") }}
    </div>
</div>

<?php if(!isset($libactivos)) { ?>
    <h3>Su búsqueda no regresó resultados.</h3>
<?php } else { ?>
    <div style="padding:10px;">
        <td colspan="4"><a href="/libros-activos/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a></td>
    </div>
    <?php if(isset($msg)) { ?>
<div style="margin-top:10px;">
   <p> {{ msg }}</p>
</div>
<?php } ?>

    <div width="100%" style="display: block; overflow: auto; margin-top:20px;">
        <table class="table table-bordered table-striped" align="center" id="tablita">
            <tr>
                <th>Título</th>
                <th>ISBN</th>
                <th>Active Revision ID</th>
                <th>Autores</th>
                <th>Lenguaje</th>
            </tr>
            {% for i in libactivos %}
                <tr>
                    <td>
                            {% if i['title']==null %}
                            <strong> {{ "No Disponible" }}</strong>
                            {% else %}
                                {{ link_to("library/getbookrev/" ~ i['active_revision_id'], i['title'],'target': '_blank') }}
                            {% endif %}
                    </td>
                    <td>
                            {% if i['isbn']==null %}
                              <strong>{{ "No Disponible" }} </strong>
                            {% else %}
                                {{ i['isbn'] }}
                            {% endif %}
                    </td>
                    <td>
                        {% if i['active_revision_id']==null %}
                            <strong>{{ "No Disponible" }} </strong>
                        {% else %}
                            {{ i['active_revision_id'] }}
                        {% endif %}
                    </td>
                    <td>
                        {% if i['autores']==null %}
                            <strong>{{ "No Disponible" }} </strong>
                        {% else %}
                            {{ i['autores'] }}
                        {% endif %}
                    </td>
                    <td>
                        {% if i['leng']==null %}
                            <strong>{{ "No Disponible" }} </strong>
                        {% else %}
                            {{ i['leng'] }}
                        {% endif %}
                    </td>
                </tr>


            {% endfor %}

        </table>
    </div>
<?php } ?>

