{{ content() }}
<div class="row" style="padding-bottom: 30px">
    {% if se == 1 or se == 4 %}
        <div class="col-md-   1"></div>
        <div class="col-md-3">
            <label>Año: </label>
            <select id="anio">
                <option value="0">Todos los años</option>
                {% for a in anios %}
                    <option value="{{ a }}">{{ a }}</option>
                {% endfor %}
                <option selected="selected"></option>
            </select>
        </div>
        <div class="col-md-4">
            <label>Mes: </label>
            <select id="mes">
                <option value="0">Todos los meses</option>
                <option value="1">Enero</option>
                <option value="2">Febrero</option>
                <option value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
                <option selected="selected"></option>
            </select>
        </div>
        <div class="col-md-3">
            <label>Asociado: </label>
            <select id="partner">
                <option value="0">Todos los asociados</option>
                {% for p in partners %}
                    <option value="{{ p.partner_id }}">{{ p.name }}</option>
                {% endfor %}
                <option selected="selected"></option>
            </select>
        </div>
        <div class="col-md-1"></div>
    {% else %}
        <div class="col-md-3"></div>
        <div class="col-md-3">
            <label>Año: </label>
            <select id="anio">
                <option value="0">Todos los años</option>
                {% for a in anios %}
                <option value="{{ a }}">{{ a }}</option>
                {% endfor %}
                <option selected="selected"></option>
            </select>
        </div>
        <div class="col-md-3">
            <label>Mes: </label>
            <select id="mes">
                <option value="0">Todos los meses</option>
                <option value="1">Enero</option>
                <option value="2">Febrero</option>
                <option value="3">Marzo</option>
                <option value="4">Abril</option>
                <option value="5">Mayo</option>
                <option value="6">Junio</option>
                <option value="7">Julio</option>
                <option value="8">Agosto</option>
                <option value="9">Septiembre</option>
                <option value="10">Octubre</option>
                <option value="11">Noviembre</option>
                <option value="12">Diciembre</option>
                <option selected="selected"></option>
            </select>
        </div>
        <div class="col-md-3"></div>
    {% endif %}
</div>

<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="busqueda" >
        <tr><th colspan="12">Registros por página/Total de registros: {{ page.total_items/ page.total_items * page.limit }}/{{ page.total_items }} </th></tr>
        <tr>
            <th style="text-align: center">Día</th>
            <th style="text-align: center">Mes</th>
            <th style="text-align: center">Año</th>
            <th style="text-align: center">Unidades</th>
            <th style="text-align: center">Precio antes de tax</th>
            <th style="text-align: center">COGS reportado</th>
        </tr>

        {% for obj in page.items %}
            <tr>
                <td align="center">{{ obj.day }}</td>
                <td align="center">
                    {% if  obj.month == "January" %}
                        Enero
                    {% elseif obj.month == "February" %}
                        Febrero
                    {% elseif obj.month == "March" %}
                        Marzo
                    {% elseif obj.month == "April" %}
                        Abril
                    {% elseif obj.month == "May" %}
                        Mayo
                    {% elseif obj.month == "June" %}
                        Junio
                    {% elseif obj.month == "July" %}
                        Julio
                    {% elseif obj.month == "August" %}
                        Agosto
                    {% elseif obj.month == "September" %}
                        Septiembre
                    {% elseif obj.month == "October" %}
                        Octubre
                    {% elseif obj.month == "November" %}
                        Noviembre
                    {% elseif obj.month == "December" %}
                        Diciembre
                    {% elseif obj.month == "" %}
                        Nulo
                    {% endif %}
                    </td>
                <td align="center">{{ obj.year }}</td>
                <td align="center">{{ obj.units }}</td>
                <td align="center">${{ obj.total_ptax }} </td>
                <td align="center">${{ obj.total_cogs }}</td>
            </tr>
        {% endfor %}
        <tr>
            {# <td align="center">{{ t_day }}</td>#}
            {# <td align="center">{{ t_month }}</td>#}
            <td align="center"></td>
            <td align="center"></td>
            <td style="font-weight: bold" align="center">Totales</td>
            <th style="text-align: center">{{ t_units }}</th>
            <th style="text-align: center">${{ t_total_ptax }}</th>
            <th style="text-align: center">${{ t_total_cogs }}</th>
        </tr>
        <td colspan="9" align="left">
            <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
            <div class="btn-group">
                {{ link_to("ventas-unidades/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                {{ link_to("ventas-unidades/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                {{ link_to("ventas-unidades/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                {{ link_to("ventas-unidades/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
            </div>
        </td>
    </table>
</div>

<div id="link" align="right"></div>

<script type="text/javascript">

    $(document).ready(function(){

        function traducirMes(mesIngles) //Función para traducir meses
        {
            var mesTraducido='';
            switch(mesIngles)
            {
                case ('January'):mesTraducido='Enero';
                    break;
                case ('February'):mesTraducido='Febrero';
                    break;
                case ('March'):mesTraducido='Marzo';
                    break;
                case ('April'):mesTraducido='Abril';
                    break;
                case ('May'):mesTraducido='Mayo';
                    break;
                case ('June'):mesTraducido='Junio';
                    break;
                case ('July'):mesTraducido='Julio';
                    break;
                case ('August'):mesTraducido='Agosto';
                    break;
                case ('September'):mesTraducido='Septiembre';
                    break;
                case ('October'):mesTraducido='Octubre';
                    break;
                case ('November'):mesTraducido='Noviembre';
                    break;
                case ('December'):mesTraducido='Diciembre';
                    break;
                default:mesTraducido='';
            }
            return mesTraducido;
        }


        //función para poner comas de separacion de miles
        function comas(x)
        {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        }

        $('#anio').on('change', function () {
            $ajax_page = '';
            Busca($ajax_page);
        });


        $('#mes').on('change', function () {

            $ajax_page = '';
            Busca($ajax_page);
        });

        $('#partner').on('change', function () {

            $ajax_page = '';
            Busca($ajax_page);
        });


        function Busca($ajax_page)
        {
            $page = $ajax_page;
            $anio = $('#anio').val();
            $mes = $('#mes').val();
            $partner = $('#partner').val();
            $.get("<?php echo $this->url->get('ventas-unidades/adminbusca')?>",{'anio':$anio, 'mes':$mes, 'page':$page, 'partner':$partner},function($data)
            {
                var unidades = $data;

                var tab = "<tr><th colspan='13'>Total de registros: " + ~~(unidades.total_items/unidades.total_items*unidades.limit) +"/" + unidades.total_items+ "</th></tr>"+
                        "<th style='text-align: center'>Día</th>"+
                        "<th style='text-align: center'>Mes</th>"+
                        "<th style='text-align: center'>Año</th>"+
                        "<th style='text-align: center'>Unidades</th>"+
                        "<th style='text-align: center'>Precio antes de tax</th>"+
                        "<th style='text-align: center'>COGS reportado</th>";

                $.each(unidades.items, function(index, u){

                    tab += '<tr>'+
                    '<td align="center">'+u.day+'</td>'+
                    '<td align="center">'+traducirMes(u.month)+'</td>'+
                    '<td align="center">'+u.year+'</td>'+
                    '<td align="center">'+u.units+'</td>'+
                    '<td align="center">$'+u.total_ptax+'</td>'+
                    '<td align="center">$'+u.total_cogs+'</td>'+
                    '</tr>';
                });
                var elementos = unidades.items.length;
                console.log(unidades);
                if(elementos > 0 && unidades.total_items < 32)
                {
                    var unidad = 0;
                    var costo = 0.00;
                    var costo_tax = 0.00;
                    for($i = 0; $i < elementos; $i ++)
                    {
                        unidad += parseFloat(unidades.items[$i].units);
                        costo += parseFloat(unidades.items[$i].r_total_ptax);
                        costo_tax += parseFloat(unidades.items[$i].r_total_cogs);
                        console.log(costo);
                        console.log(costo_tax);
                    }

                    var t_ptax = comas(costo.toFixed(2));
                    var costo_tax = comas(costo_tax.toFixed(2));

                    tab += '<tr>'+
                    //'<td align="center">'+unidades.items[0].day+'</td>'+
                    //'<td align="center">'+unidades.items[0].month+'</td>'+
                    '<td align="center"></td>'+
                    '<td align="center"></td>'+
                    '<td style="font-weight: bold"align="center">Totales</td>'+
                    '<th style="text-align: center">'+unidad+'</th>'+
                    '<th style="text-align: center">$'+t_ptax+'</th>'+
                    '<th style="text-align: center">$'+costo_tax+'</th>'+
                    '</tr>';
                }

                tab += "<tr>"+
                            "<td colspan='6' align='left'>"+
                                "<span class='help-inline'>"+unidades.current+"/"+unidades.total_pages+"</span>"+
                                "<div class='btn-group'>"+
                                    "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                                    "<a data-before='"+unidades.before+"' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                                    "<a data-next='"+unidades.next+"' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                                    "<a data-last='"+unidades.last+"' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                                "</div>"+
                            "</td>"+
                        "</tr>";
                document.getElementById("busqueda").innerHTML = tab;
                //$('#busqueda').html(tab);
                $('#link').html(' <a href="/ventas-unidades/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
            });
        }


        $(document).on('click', '#next', function(){
            $ajax_page=$('#next').data('next');

            Busca($ajax_page);
        });
        $(document).on('click', '#first', function(){
            $ajax_page=$('#first').data('first');

            Busca($ajax_page);
        });
        $(document).on('click', '#last', function(){
            $ajax_page=$('#last').data('last');

            Busca($ajax_page);
        });
        $(document).on('click', '#before', function(){
            $ajax_page=$('#before').data('before');

            Busca($ajax_page);
        });


    });

</script>