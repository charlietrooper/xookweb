{{ content() }}

<div align="center" style="padding: 10px;">
    <label>Búsqueda:</label><br/>
    {{ text_field('titulares', 'id':'titulares') }}
</div>

<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="tablita">
        <tr>
            <th>Nombre del distribuidor</th>
            <th>Conteo de distribuidores</th>
        </tr>
        {% for e in titulares.items['Items'] %}
            <tr>
                <td>{{ e['_id'] }}</td>
                <td>{{ e['count'] }}</td>
            </tr>
        {% endfor %}
        <tr>
            <td colspan="2" align="left">
                <span class="help-inline">{{ titulares.actual }}/{{ titulares.paginasTotales }}</span>
                <div class="btn-group">
                    {{ link_to("titular-cuenta/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                    {{ link_to("titular-cuenta/index?page=" ~ titulares.anterior, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("titular-cuenta/index?page=" ~ titulares.siguiente, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("titular-cuenta/index?page=" ~ titulares.ultima, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                </div>
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('#titulares').on('change', function () {
            $titulares = $('#titulares').val();
            if($titulares == '')
            {
                var pagina = "/titular-cuenta/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            Busqueda($pagina_ajax);
        });


        function Busqueda($pagina_ajax){
            $titulares = $('#titulares').val();

            $.get("<?php echo $this->url->get('titular-cuenta/busqueda')?>",{'titulares':$titulares, 'pagina':$pagina_ajax},function($data)
            {
                if($data)
                {
                    var tab = "<tr><th>Nombre del distribuidor</th><th>Conteo de distribuidores</th></tr>";

                    $.each($data.items.Items, function (index, datos) {
                        tab += "<tr>"+
                        "<td>"+datos._id+"</td>"+
                        "<td>"+datos.count+"</td>"+
                        "</tr>";
                    });

                    tab += "<tr>"+
                    "<td colspan='2' align='left'>"+
                    "<span class='help-inline'>"+$data.actual +"/"+ $data.paginasTotales +"</span>"+
                    "<div class='btn-group'>"+
                    "<a data-first='1' id='primera' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                    "<a data-before='" + $data.anterior + "' id='anterior' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                    "<a data-next='" + $data.siguiente + "' id='siguiente' class= 'btn'><i class='icon-step-backward'></i> Siguiente</a>"+
                    "<a data-last='" + $data.ultima + "' id='ultima' class= 'btn'><i class='icon-step-backward'></i> Última</a>"+
                    "</div>"+
                    "</td>"+
                    "</tr>";
                    $('table#tablita').html(tab);
                }
            });
        }

        $(document).on('click','#siguiente', function(){
            $pagina_ajax = $('#siguiente').data('next');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#primera', function(){
            $pagina_ajax = $('#primera').data('first');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#ultima', function(){
            $pagina_ajax = $('#ultima').data('last');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#anterior', function(){
            $pagina_ajax = $('#anterior').data('before');
            Busqueda($pagina_ajax);
        });
    });
</script>