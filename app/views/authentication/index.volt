{{ content() }}

{{ form("authentication/authenticate") }}
<p>{{ submit_button("Autocompletar", "class": "btn btn-primary", "id":"auto-llenar") }}</p>


<table align="center" id="answer-admin" class="detalle table-bordered table-striped">

        <tr>
            <td><label>ID de socio:</label></td>
            <td><select name='partner' id="partner">
                    <option value='0'>Please, choose one...</option>
                    {% for p in partner %}
                        <option value={{ p.partner_id }}>{{ p.name }}</option>
                    {% endfor %}
                </select></td>
        </tr>
        <tr colspan="3"><td>&nbsp;</td></tr>
        <tr>
            <td><label for="partner_user_id">ID socio usuario:</label></td>
            <td>{{ text_field("partner_user_id", "id":"id") }}</td>

        </tr>
        <tr colspan="3"><td>&nbsp;</td></tr>
        <tr>
            <td><label>Correo:</label></td>
            <td>{{ text_field('email', 'id':'email') }}</td>
        </tr>
</table>
    <table align="center">
        <tr>
            <td colspan="3" align="right"><ul class="pager">
                    <li class="pull-right">
                        {{ submit_button("Llamar", "class": "btn btn-primary", "id":"ok") }}
                    </li>
                </ul></td>
        </tr>
        </p>
</table>
</form>

{#------------------------------INICIA JAVASCRIPT--------------------------------------------------#}
<script type="text/javascript">
    $(document).ready(function(){
        $('#ok').on('click', function(){

            var mensaje = [];
            var mens = [];

            mensaje.push('Los siguientes campos estan vacios o son incorrectos:\n');
            mens.push('Los siguientes campos estan vacios o son incorrectos:\n');

            $correo = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+/;

            $partner = $('#partner').val();
            $id = $('#id').val();
            $email = $('#email').val();

            if($partner.trim()=='0')
            {
                mensaje.push('-Seleccione un PARTNER_ID \n');
            }
            if($id.trim()=='')
            {
                mensaje.push('-El campo PARTER USER ID esta vacio \n');
            }
            if(!($correo.test($email)))
            {
                mensaje.push('-El EMAIL es incorrecto\n');
            }
            if($email.trim()=='')
            {
                mensaje.push('-El campo EMAIL esta vacio\n');
            }
            if(mensaje.length != mens.length)
            {
                event.preventDefault();
                alert(mensaje);
            }
        });

        //
        $('#auto-llenar').on('click', function(){

            Autollenado();
        })
        //Funcion para autollenar campos
        function Autollenado()
        {
            event.preventDefault();
            $('#partner').val('28');
            $('#id').val('129662');
            $('#email').val('prueba@gmail.com');

        }
    });
</script>


