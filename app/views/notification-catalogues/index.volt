{{content()}}
<div class="titulo" >
    <h1 class="retroshadow">Catálogos de actualización</h1>'
</div>
{#--------------------------------------------TABLA PARA ADMINISTRADOR-------------------------------------#}
<div class="row" style="padding-bottom: 30px">
    <div class="col-md-12">
        <label style="text-align: center;">Buscar por:</label>
    </div>
    {% if se == '1' or se == '4' %}
    <div class="col-sm-12 col-md-3" style="margin-top: 12px;">
        <label>Socio:</label>

        <select name="partners" id="partners">

            {% if partnergraf!='' %}
                <option></option>
                <option value="1">Todos</option>
                <option selected="selected" value="{{ partnergraf }}">{{ nombre }}</option>
                {% for p in partners %}
                    <option value="{{ p.partner_id }}">{{ p.name }}</option>
                {% endfor %}
            {% else %}
                <option selected="selected"></option>
                <option value="1">Todos</option>
                {% for p in partners %}
                    <option value="{{ p.partner_id }}">{{ p.name }}</option>
                {% endfor %}
            {% endif %}
        </select>
    </div>
    <div class="col-sm-12 col-md-9">
        <label>Fecha: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>de:&nbsp;
            {{date_field('date', 'id':'fecha_ini')}}

        <label for="dates">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>a:&nbsp;
        {{date_field('dates', 'id':'fecha_fin')}}
        {{submit_button("buscar", 'class':'search-admin')}}
    </div>
</div>
<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="answer-admin" >
        <tr>
            <th>ID de notificación</th>
            <th>Fecha de notificación</th>
            <th>Correo del usuario</th>
            <th>ID del socio</th>
            <th>ID del catálogo</th>
        </tr>
            {% for obj in page.items %}
        <tr>
            <td>{{obj.notification_id}}</td>
            <td>{{obj.date_notification}}</td>
            <td>{{obj.user_email}}</td>
            <td>{{ link_to("partners/details/" ~ obj.partner_id, obj.partner_id) }}</td>
            <td>{{obj.catalogue_id}}</td>
        </tr>
            {% endfor %}
        <tr>
            <td colspan="9" align="left">
                <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                <div class="btn-group">
                    {{ link_to("notification-catalogues/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                    {{ link_to("notification-catalogues/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("notification-catalogues/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("notification-catalogues/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                </div>
            </td>
        </tr>

    </table>
</div>
{#-------------------------------------------TABLA PARA PARTNER----------------------------------------------------#}
    {% else %}
    <div>
        <label>Fecha de:</label>
        {{date_field('date', 'id':'fecha_ini')}}
        <label>a:</label>
        {{date_field('dates', 'id':'fecha_fin')}}
        {{submit_button("buscar", 'class':'search-partner')}}
    </div>
</div>
<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="answer-partner" >
        <tr>
            <th>ID de notificación</th>
            <th>Fecha de notificación</th>
            <th>Correo del usuario</th>
            <th>ID del catálogo</th>
        </tr>
        {% for obj in page.items %}
        <tr>
            <td>{{obj.notification_id}}</td>
            <td>{{obj.date_notification}}</td>
            <td>{{obj.user_email}}</td>
            <td>{{obj.catalogue_id}}</td>
        </tr>
        {% endfor %}
        <tr>
            <td colspan="9" align="left">
                <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                <div class="btn-group">
                    {{ link_to("notification-catalogues/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                    {{ link_to("notification-catalogues/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("notification-catalogues/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("notification-catalogues/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                </div>
            </td>
        </tr>
    </table>
</div>
{% endif %}
<div align="right" id="link">

</div>

{#---------------------CODIGO JAVASCRIPT---------------------------------------#}
<script type="text/javascript">
$(document).ready(function(){

{#----------------------------------------------------------------------------------------------------------------------------#}
{#----------------------------------------------------------------------------------------------------------------------------#}
{#----------------------------------------------------------------------------------------------------------------------------#}
{#-----------------------------------------------------------PARTE PARA ADMINISTRADOR-----------------------------------------#}
{#----------------------------------------------------------------------------------------------------------------------------#}
{#----------------------------------------------------------------------------------------------------------------------------#}
    $('.search-admin').on('click',function(){

        $id = document.getElementById("partners").value;
        $fecha_ini=$('#fecha_ini').val();
        $fecha_fin=$('#fecha_fin').val();

        if($id==1)
        {
            var pagina = "/notification-catalogues/index";
            location.href = location.origin + pagina;
        }
        else {
            if ($id.trim() != 0 && $fecha_ini.trim() == 0 && $fecha_fin.trim() == 0) {
                adminAjax($id, $fecha_ini, $fecha_fin);
            }
            else if ($id.trim() == 0 && $fecha_ini.trim() != 0 && $fecha_fin.trim() != 0) {
                adminAjax($id, $fecha_ini, $fecha_fin);
            }
            else if ($id.trim() != 0 && $fecha_ini.trim() != 0 && $fecha_fin.trim() != 0) {
                adminAjax($id, $fecha_ini, $fecha_fin);
            }
            else {
                alert('Las consultas se hacen de la siguiente manera: \n -Solo por partner_id \n -Solo fecha inicion y fin \n -O llenando los tres campos');
            }
        }
 });
function adminAjax($id,$fecha_ini,$fecha_fin){
$id=$id;
$fecha_ini=$fecha_ini;
$fecha_fin=$fecha_fin;

 $.get("<?php echo $this->url->get('notification-catalogues/search')?>",{'id':$id, 'fecha_ini':$fecha_ini, 'fecha_fin':$fecha_fin},function($data)
                    {
                          {#-------------parseamos el json y recorremos-----------#}
                          {#debugger;#}
                          var pur="<tr>"+
                                     "<th>ID de notificación</th>"+
                                     "<th>Fecha de notificación</th>"+
                                     "<th>Correo del usuario</th>"+
                                     "<th>ID del socio</th>"+
                                     "<th>ID del catálogo</th>"+
                                  "</tr>";
                          var purchase=JSON.parse($data);
                           $.each(purchase.items, function(index, datos) {
                           pur +='<tr>'+
                                      '<td>'+datos.notification_id+'</td>'+
                                      '<td>'+datos.date_notification+'</td>'+
                                      '<td>'+datos.user_email+'</td>'+
                                      '<td>'+'<a href="/partners/details/'+datos.partner_id+'">'+datos.partner_id+'</a></td>'+
                                      '<td>'+datos.catalogue_id+'</td>'+
                                 '</tr>';
                          });

                           pur +="<tr>"+
                             "<td colspan='9' align='left'>"+
                           "<span class='help-inline'>"+purchase.current+"/"+purchase.total_pages+"</span>"+
                              "<div class='btn-group'>"+
                                  "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                                  "<a data-before='"+purchase.before+"' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                                  "<a data-next='"+purchase.next+"' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                                  "<a data-last='"+purchase.last+"' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                              "</div>"+
                              "</td>"+
                              "</tr>";
                          {#------------------populamos el desplegable purchase con sus respectivos datos obtenidos---------#}
                          $('table#answer-admin').html(pur);
                          $('#link').html(' <a href="/notification-catalogues/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
                    });
}
{#--------------------------------------------------------------------------------------------------------------------------------#}
{#--------------------------------------------------------------------------------------------------------------------------------#}
{#--------------------------------------------------------------------------------------------------------------------------------#}
{#-------------------------------------------------------------PARTE PARA PARTNER-------------------------------------------------#}
{#--------------------------------------------------------------------------------------------------------------------------------#}
{#--------------------------------------------------------------------------------------------------------------------------------#}
$('.search-partner').on('click',function(){
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();

             if($fecha_ini.trim()!=0 && $fecha_fin.trim()!=0)
             {
                partner_ajax($fecha_ini,$fecha_fin);
             }
             else
             {
                alert('INGRESE LAS DOS FECHAS');
             }
          });
   function partner_ajax($fecha_ini,$fecha_fin){

   $fecha_ini=$fecha_ini;
   $fecha_fin=$fecha_fin;

      $.get("<?php echo $this->url->get('notification-catalogues/searchforpartner')?>",{'fecha_ini':$fecha_ini,'fecha_fin':$fecha_fin},function($data)
           {
             {#-------------parseamos el json y recorremos-----------#}
              {#debugger;#}
              var pur="<tr>"+
              "<th>ID de notificación</th>"+
              "<th>Fecha de notificación</th>"+
              "<th>Correo del usuario</th>"+
              "<th>ID del catálogo</th>"+
              "</tr>";
              var purchase=JSON.parse($data);
              $.each(purchase.items, function(index, datos) {
              pur +='<tr>'+
                '<td>'+datos.notification_id+'</td>'+
                '<td>'+datos.date_notification+'</td>'+
                '<td>'+datos.user_email+'</td>'+
                '<td>'+datos.catalogue_id+'</td>'+
              '</tr>';
              });

               pur +="<tr>"+
                   "<td colspan='9' align='left'>"+
                       "<span class='help-inline'>"+purchase.current+"/"+purchase.total_pages+"</span>"+
                       "<div class='btn-group'>"+
                           "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                           "<a data-before='"+purchase.before+"' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                           "<a data-next='"+purchase.next+"' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                           "<a data-last='"+purchase.last+"' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                       "</div>"+
                   "</td>"+
                   "</tr>";
              {#------------------populamos el desplegable purchase con sus respectivos datos obtenidos---------#}
              $('table#answer-partner').html(pur);
              $('#link').html(' <a href="/notification-catalogues/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
           });

   }
   {#----------------------------------------------------------------------------TERMINA PARTE PARA PARTNER---------------------------#}
    {#--------------------------------------------------------------------------------------------------------------------------------#}
    {#--------------------------------------------------------------------------------------------------------------------------------#}
   {#-------------------------------------------------------------AJAX PAGINACION PARA ADMIN-------------------------------------------------------------------#}
    {#--------------------------------------------------------------------------------------------------------------------------------#}
   {#---------------------------------------------------------------------------------------------------------------------------------#}

$(document).on('click', '#next', function(){
  $ajax_page=$('#next').data('next');

  admin_page($ajax_page);
});
$(document).on('click', '#first', function(){
  $ajax_page=$('#first').data('next');

  admin_page($ajax_page);
});
$(document).on('click', '#last', function(){
  $ajax_page=$('#last').data('last');

     admin_page($ajax_page);
});
$(document).on('click', '#before', function(){
  $ajax_page=$('#before').data('before');

     admin_page($ajax_page);
});
{#--------------------------------------------------------------------------------------------------------------------------------#}
{#---------------------------------------------------------------------------------------------------------------------------------#}
{#--------------------------------------------------------------------------------------------------------------------------------#}
{#---------------------------------------------------------------------------------------------------------------------------------#}
{#--------------------------------------------------------------------------------------------------------------------------------#}
{#------------------------------INICIA FUNCION PARA PETICION AJAX PAGINATION-------------------------------------------------------#}
function admin_page($ajax_page){
$page=$ajax_page;
$id = document.getElementById("partners").value;
   $fecha_ini=$('#fecha_ini').val();
   $fecha_fin=$('#fecha_fin').val();

{#----------------HACEMOS LA PETICION AJAX PARA AVANZAR UNA PAGINA----------------#}
$.get("<?php echo $this->url->get('notification-catalogues/search')?>", {'page_ajax':$page,'id':$id,'fecha_ini':$fecha_ini,'fecha_fin':$fecha_fin},function($data){
 {#-------------parseamos el json y recorremos-----------#}
                           {#debugger;#}
                           var pur="<tr>"+
                                      "<th>ID de notificación</th>"+
                                      "<th>Fecha de notificación</th>"+
                                      "<th>Correo del usuario</th>"+
                                      "<th>ID del socio</th>"+
                                      "<th>ID del catálogo</th>"+
                                   "</tr>";
                           var purchase=JSON.parse($data);
                            $.each(purchase.items, function(index, datos) {
                            pur +='<tr>'+
                                       '<td>'+datos.notification_id+'</td>'+
                                       '<td>'+datos.date_notification+'</td>'+
                                       '<td>'+datos.user_email+'</td>'+
                                       '<td>'+'<a href="/partners/details/'+datos.partner_id+'">'+datos.partner_id+'</a></td>'+
                                       '<td>'+datos.catalogue_id+'</td>'+
                                  '</tr>';
                           });

         pur +="<tr>"+
                    "<td colspan='9' align='left'>"+
                        "<span class='help-inline'>"+purchase.current+"/"+purchase.total_pages+"</span>"+
                        "<div class='btn-group'>"+
                            "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                            "<a data-before='"+purchase.before+"' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                            "<a data-next='"+purchase.next+"' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                            "<a data-last='"+purchase.last+"' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                        "</div>"+
                    "</td>"+
               "</tr>";
         {#------------------populamos el desplegable purchase con sus respectivos datos obtenidos---------#}
         $('table#answer-admin').html(pur);
         $('#link').html(' <a href="/notification-catalogues/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
});
}

{#-------------------------------------------------TERMINA LA FUNCION PARA TABLE ADMIN-------------------------------------------------------------------------------#}
{#-------------------------------------------------TERMINA PAGINACION AJAX PARA ADMIN--------------------------------------------------------------------------------#}
{#--------------------------------------------------------------------------------------------------------------------------------#}
{#---------------------------------------------------------------------------------------------------------------------------------#}
{#-------------------------------------------------INICIA PAGINACION AJAX PARA PARTNER--------------------------------------------------------------------------------#}
$(document).on('click', '#next_p', function(){
   $ajax_page=$('#next_p').data('next');

   partner_page($ajax_page);
});
$(document).on('click', '#first_p', function(){
  $ajax_page=$('#first_p').data('first');

   partner_page($ajax_page);
});
$(document).on('click', '#last_p', function(){
  $ajax_page=$('#last_p').data('last');

   partner_page($ajax_page);
});
$(document).on('click', '#before_p', function(){
  $ajax_page=$('#before_p').data('before');

   partner_page($ajax_page);
});

function partner_page($ajax_page){
$page=$ajax_page;
 $fecha_ini=$('#fecha_ini').val();
 $fecha_fin=$('#fecha_fin').val();
$.get("<?php echo $this->url->get('notification-catalogues/searchforpartner')?>", {'page_ajax':$page,'fecha_ini':$fecha_ini,'fecha_fin':$fecha_fin},function($data)
     {
          {#-------------parseamos el json y recorremos-----------#}
                       {#debugger;#}
                       var pur="<tr>"+
                       "<th>ID de notificación</th>"+
                       "<th>Fecha de notificación</th>"+
                       "<th>Correo del usuario</th>"+
                       "<th>ID del catálogo</th>"+
                       "</tr>";
                       var purchase=JSON.parse($data);
                       $.each(purchase.items, function(index, datos) {
                       pur +='<tr>'+
                         '<td>'+datos.notification_id+'</td>'+
                         '<td>'+datos.date_notification+'</td>'+
                         '<td>'+datos.user_email+'</td>'+
                         '<td>'+datos.catalogue_id+'</td>'+
                       '</tr>';
                       });
                    pur +="<tr>"+
                                       "<td colspan='9' align='left'>"+
                                           "<span class='help-inline'>"+purchase.current+"/"+purchase.total_pages+"</span>"+
                                           "<div class='btn-group'>"+
                                               "<a data-first='1' id='first_p' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                                               "<a data-before='"+purchase.before+"' id='before_p' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                                               "<a data-next='"+purchase.next+"' id='next_p' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                                               "<a data-last='"+purchase.last+"' id='last_p' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                                           "</div>"+
                                       "</td>"+
                                  "</tr>";
                            {#------------------populamos el desplegable purchase con sus respectivos datos obtenidos---------#}
                            $('table#answer-partner').html(pur);
                            $('#link').html(' <a href="/notification-catalogues/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
     });

}
});
</script>