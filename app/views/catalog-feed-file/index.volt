{{ content() }}
<div class="titulo" >
    <h1 class="retroshadow">Catalog Feed File</h1>'
</div>
<div class="row" style="padding-bottom: 30px">
    <div class="col-md-12" align="center">
        <label style="text-align: center;">Buscar por:</label>
    </div>
    <div class="col-md-4" align="center" style="margin-top: 10px;">
        <label>ID de archivo</label>
        {{text_field('file_id', 'id':'file_id')}}
    </div>
    <div class="col-md-8" align="center">
        <label>&nbsp;&nbsp;&nbsp; FECHA DE INICIO:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>de:
        {{date_field('fecha', 'id':'fecha_ini')}}
        <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>a:
        {{date_field('fecha', 'id':'fecha_fin')}}
       {# {{submit_button("BUSCAR", 'class':'search-admin')}}#}
    </div>
</div>
<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="answer-admin">
        <tr><th colspan="9">Registros por página/Total de registros: {{ page.total_items/ page.total_items * page.limit }}/{{ page.total_items }}</th></tr>
        <tr>
            <th>ID de archivo</th>
            <th>Ruta del archivo</th>
            <th>Ruta del servidor</th>
            <th>Hora de inicio</th>
            <th>Hora de término</th>
            <th>Número de ebooks</th>
            <th>Ebooks activos</th>
            <th>Ebooks inactivos</th>
            <th></th>
        </tr>
     {% for obj in page.items %}
     <tr>

        <td>{{ obj.file_id }}</td>
        <td>
            <div class="emergente">
                <div class="ocultar"><?php echo substr($obj->file_path,0,5).'...'; ?></div>
                <div class="mostrar mostrarAPI">{{ obj.file_path }}</div>
            </div>
        </td>
        <td>
           <div class="emergente">
               <div class="ocultar"><?php echo substr($obj->server_path,0,5).'...'; ?></div>
               <div class="mostrar mostrarAPI">{{ obj.server_path }}</div>
           </div>
        </td>

        <td>{{ obj.start_time }}</td>
        <td>{{ obj.end_time }}</td>
        <td>{{ obj.num_ebooks }}</td>
         <td>{{ obj.active_ebooks }}</td>
         <td>{{ obj.inactive_ebooks }}</td>
        <td width="7%">{{ link_to("catalog-feed-file/descargarxml/" ~ obj.file_id, '<i class="glyphicon glyphicon-download"></i> DESCARGAR XML', "class": "btn btn-default", "target":"_blank") }}</td>
     </tr>

    {% endfor %}
    {#% if loop.last %#}

          <tr>
              <td colspan="9" align="right">
                  <div class="btn-group">
                      {{ link_to("catalog-feed-file/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                      {{ link_to("catalog-feed-file/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                      {{ link_to("catalog-feed-file/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                      {{ link_to("catalog-feed-file/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                      <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                  </div>
              </td>
          </tr>
    </table>
</div>

<div align="right" id="link">

</div>
{#---------------------CODIGO JAVASCRIPT PARA LAS LLAMADAS DE BUSQUEDA A LA BD---------------------------------------#}
<script type="text/javascript">
    $(document).ready(function(){
{#-------------------------------------------------------------PARTE PARA ADMINISTRADOR-------------------------------------------#}

        $('#fecha_ini').on('change', function () {
            $id=$('#file_id').val();
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();
            $ajax_page = '';
            searchAdmin($id,$fecha_ini,$fecha_fin);
        });

        $('#fecha_fin').on('change', function () {
            $id=$('#file_id').val();
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();
            $ajax_page = '';
            searchAdmin($id,$fecha_ini,$fecha_fin);
        });

        $('#file_id').on('change', function () {
            $id=$('#file_id').val();
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();
            $ajax_page = '';
            searchAdmin($id,$fecha_ini,$fecha_fin);
        });

/*
        $('.search-admin').on('click',function(){
   {#----Obtenemos el id de la caja de texto-----#}
            $id=$('#file_id').val();
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();

            if($id.trim()!=0 && $fecha_ini.trim()==0 && $fecha_fin.trim()==0)
            {
            {#---------hacer consulta solo por id---------#}
                searchAdmin($id,$fecha_ini,$fecha_fin);
            }
            else if($id==0 && $fecha_ini.trim()!=0 && $fecha_fin.trim()!=0)
            {
            {#---------hacer consulta por las dos fechas---------#}
                searchAdmin($id,$fecha_ini,$fecha_fin);
            }
            else if($id.trim()!=0 && $fecha_ini.trim()!=0 && $fecha_fin.trim()!=0)
            {
            {#---------hacer consulta por las tres variables-------#}
                searchAdmin($id,$fecha_ini,$fecha_fin);
            }
            else
            {
            {#---------mandar mensaje de error con los siguientes pasos-------#}
                alert('Las consultas se hacen de la siguiente manera: \n -Solo por partner_id \n -Solo fecha inicio y fin \n -O llenando los tres campos');
            }
        });

        */



        function searchAdmin($id,$fecha_ini,$fecha_fin)
        {
            $id=$id;
            $fecha_ini=$fecha_ini;
            $fecha_fin=$fecha_fin;
            {#-----------Hacemos la petición vía post o get contra sales-reports/search pasando el id, la fecha inicial y fecha final----------#}

            $.get("<?php echo $this->url->get('catalog-feed-file/search')?>",{'id':$id, 'fecha_ini':$fecha_ini, 'fecha_fin':$fecha_fin},function($data)
            {
            {#-------------parseamos el json y recorremos-----------#}
            {#debugger;#}
                var catalogfeedfile = $data;

                var pur = "<tr><th colspan='13'>Registros por página/Total de registros: " + ~~(catalogfeedfile.total_items/catalogfeedfile.total_items*catalogfeedfile.limit) +"/" + catalogfeedfile.total_items+ "</th></tr>"+
                "<tr>"+
                "<th>FILE_ID</th>"+
                "<th>FILE PATH</th>"+
                "<th>SERVER PATH</th>"+
                "<th>START TIME</th>"+
                "<th>END TIME</th>"+
                "<th>NUM EBOOKS</th>"+
                "<th>ACTIVE EBOOKS</th>"+
                "<th>INACTIVE EBOOKS</th>"+
                "<th></th>"+
                "</tr>";

                $.each(catalogfeedfile.items, function(index, datos) {
                    pur +='<tr>'+
                    '<td>'+datos.file_id+'</td>'+
                    '<td><div class="emergente"><div class="ocultar">'+(datos.file_path || " ").substring(0,5)+'...</div><div class="mostrar mostrarAPI">'+datos.file_path+'</div></div>'+
                    '<td><div class="emergente"><div class="ocultar">'+(datos.server_path || " ").substring(0,5)+'...</div><div class="mostrar mostrarAPI">'+datos.server_path+'</div></div>'+
                    '<td>'+datos.start_time+'</td>'+
                    '<td>'+datos.end_time+'</td>'+
                    '<td>'+datos.num_ebooks+'</td>'+
                    '<td>'+datos.ative_ebooks+'</td>'+
                    '<td>'+datos.inactive_ebooks+'</td>'+
                    '<td width="7%"><a href="catalog-feed-file/descargarxml?file_id="'+datos.file_id+'" class= "btn btn-default"><i class="glyphicon glyphicon-download"></i> DESCARGAR XML</a></td>'+

                    '</tr>';
                });
                pur += "<tr>"+
                        "<td colspan='9' align='right'>"+
                        "<div class='btn-group'>"+
                        "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                        "<a data-before='"+catalogfeedfile.before+"' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                        "<a data-next='"+catalogfeedfile.next+"' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                        "<a data-last='"+catalogfeedfile.last+"' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                        "<span class='help-inline'>"+catalogfeedfile.current+"/"+catalogfeedfile.total_pages+"</span>"+
                        "</div>"+
                        "</td>"+
                        "</tr>";

                        {#------------------populamos el desplegable purchase con sus respectivos datos obtenidos---------#}
                $('table#answer-admin').html(pur);
                $('#link').html(' <a href="/catalog-feed-file/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
            });
        }

        {#---------------------------AJAX PAGINACIÓN---------------------------#}

        $(document).on('click', '#next', function(){
            $ajax_page=$('#next').data('next');
            page_ajax($ajax_page);
        });
        $(document).on('click', '#first', function(){
            $ajax_page=$('#first').data('first');
            page_ajax($ajax_page);
        });
        $(document).on('click', '#last', function(){
            $ajax_page=$('#last').data('last');
            page_ajax($ajax_page);
        });
        $(document).on('click', '#before', function(){
            $ajax_page=$('#before').data('before');
            page_ajax($ajax_page);
        });
        {#------------------------------INICIA FUNCION PARA PETICION AJAX PAGINATION------------------------------#}

        function page_ajax($ajax_page)
        {
            $page=$ajax_page;
            $id=$('#file_id').val();
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();
            {#----------------HACEMOS LA PETICION AJAX PARA AVANZAR UNA PAGINA----------------#}

            $.get("<?php echo $this->url->get('catalog-feed-file/search')?>", {'page_ajax':$page,'id':$id,'fecha_ini':$fecha_ini,'fecha_fin':$fecha_fin},function($data)
            {

                var catalogfeedfile = $data;

                var pur = "<tr><th colspan='13'>Registros por página/Total de registros: " + ~~(catalogfeedfile.total_items/catalogfeedfile.total_items*catalogfeedfile.limit) +"/" + catalogfeedfile.total_items+ "</th></tr>"+
                        "<tr>"+
                        "<th>FILE_ID</th>"+
                        "<th>FILE PATH</th>"+
                        "<th>SERVER PATH</th>"+
                        "<th>START TIME</th>"+
                        "<th>END TIME</th>"+
                        "<th>NUM EBOOKS</th>"+
                        "<th>ACTIVE EBOOKS</th>"+
                        "<th>INACTIVE EBOOKS</th>"+
                        "<th></th>"+
                        "</tr>";

                $.each(catalogfeedfile.items, function(index, datos) {
                    pur += '<tr>'+
                            '<td>'+datos.file_id+'</td>'+
                            '<td><div class="emergente"><div class="ocultar">'+(datos.file_path || " ").substring(0,5)+'...</div><div class="mostrar mostrarAPI">'+datos.file_path+'</div></div>'+
                            '<td><div class="emergente"><div class="ocultar">'+(datos.server_path || " ").substring(0,5)+'...</div><div class="mostrar mostrarAPI">'+datos.server_path+'</div></div>'+
                            '<td>'+datos.start_time+'</td>'+
                            '<td>'+datos.end_time+'</td>'+
                            '<td>'+datos.num_ebooks+'</td>'+
                            '<td>'+datos.ative_ebooks+'</td>'+
                            '<td>'+datos.inactive_ebooks+'</td>'+
                            '<td width="7%"><a href="catalog-feed-file/descargarxml?file_id="'+datos.file_id+'" class= "btn btn-default"><i class="glyphicon glyphicon-download"></i> DESCARGAR XML</a></td>'+
                            '</tr>';
                });

                pur += "<tr>"+
                        "<td colspan='9' align='right'>"+
                        "<div class='btn-group'>"+
                        "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                        "<a data-before='"+catalogfeedfile.before+"' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                        "<a data-next='"+catalogfeedfile.next+"' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                        "<a data-last='"+catalogfeedfile.last+"' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                        "<span class='help-inline'>"+catalogfeedfile.current+"/"+catalogfeedfile.total_pages+"</span>"+
                        "</div>"+
                        "</td>"+
                        "</tr>";
                        {#------------------populamos el desplegable purchase con sus respectivos datos obtenidos------------------#}
                $('table#answer-admin').html(pur);
                $('#link').html(' <a href="/catalog-feed-file/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
            });
        }
    });
</script>
