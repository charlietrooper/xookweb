{{ content() }}
<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    window.onload = function () {
        var atras = document.getElementById('irAtras');
        atras.addEventListener("click", irAtras);
        function irAtras() { this.style.color = 'red'; window.history.back();}
    }
</script>
<ul class="pager">
    <li id="irAtras">
        {{ link_to("sales-reports/selectgrafica","&larr; Go Back") }}
    </li>
</ul>


<div class="row">
    <div class="col-md-12">
        <label style="text-align: center;">Graficar ventas y customers registrados<br/>
            para {{ partner }}:</label>
    </div>
    <div class="col-md-12">
        <p><select name="partnerid" id="seleccionGrafica" onchange="window.location.href=this.value">
                <option value="" >seleciona una opcion...</option>
                <option value="\sales-reports/anioventas/{{ id_partner }}" >por año</option>
                <option value="\sales-reports/mes/{{ id_partner }}">por mes</option>
                <option value="\sales-reports/semana/{{ id_partner }}">por semana</option>
                <option value="\sales-reports/dias/{{ id_partner }}">por día</option>
                <option value="\sales-reports/total/{{ id_partner }}">Totales</option>
            </select></p>
        <p>&nbsp;</p>
    </div>

</div>
