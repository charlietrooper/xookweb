{{ content() }}

{{ hidden_field('rol', 'value': se, 'id':'rol') }}
{{ hidden_field('id_del_partner', 'value': id_del_partner, 'id':'id_del_partner') }}
<div class="row"  style="padding-bottom: 30px; vertical-align: middle;">
    <div class="col-md-12">
        <label style="text-align: center;">Buscar por:</label>
    </div>

    {% if se == '1' or se == '4' %}
        {# Selectores de busqueda para administrador INICIA #}
        <div class="col-sm-12 col-md-2" style="/*border-style: solid; border-width: 1px; border-color: #ddd; height: 127px;*/">
            <label>Socio:</label><br/>
            <select name="partners" id="partners">
                {% if partnergraf is defined %}
                    <option value="">Todos</option>
                    <option selected="selected" value="{{ partnergraf }}">{{ nombre }}</option>
                    {% for p in partners %}
                        <option value="{{ p.partner_id }}">{{ p.name }}</option>
                    {% endfor %}
                {% else %}
                    <option value="" selected="selected">Todos</option>
                    {% for p in partners %}
                        <option value="{{ p.partner_id }}">{{ p.name }}</option>
                    {% endfor %}
                {% endif %}
            </select>
        </div>
        <div class="col-sm-12 col-md-4" style="/*border-style: solid; border-width: 1px; border-color: #ddd; height: 127px;*/">
            <label>Ubicación:</label><br/>
            <div class="row">
                <div class="col-xs-3" align="left" style="margin-top: 5px; margin-bottom: 5px;">
                    País:
                </div>
                <div class="col-xs-9" style="margin-top: 5px; margin-bottom: 5px;">
                    <select name="pais" id="pais" style="width: 80%;">
                        <option value=""  selected="selected">Todos</option>
                        <option value="MX">México</option>
                        {% for p in paises %}
                            <option value="{{ p.sortname }}">{{ p.name }}</option>
                        {%  endfor %}
                    </select>
                </div>
                <div class="col-xs-3" align="left" style="margin-top: 5px; margin-bottom: 5px;">
                    Estado/provincia:
                </div>
                <div class="col-xs-9" style="margin-top: 5px; margin-bottom: 5px;" id="est">
                    <select name="estado" id="estado" style="width: 80%;">
                        <option value="">--</option>
                    </select>
                </div>
                <div class="col-xs-3" align="left" style="margin-top: 5px; margin-bottom: 5px;">
                    Ciudad:
                </div>
                <div class="col-xs-9" style="margin-top: 5px; margin-bottom: 5px;">
                    <select name="ciudad" id="ciudad" style="width: 80%;">
                        <option value="">--</option>
                        {#if....#}
                        <option value="todos">Todos</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="col-sm-12 col-md-3" style="/*border-style: solid; border-width: 1px; border-color: #ddd; height: 127px;*/">
            <label>Fecha: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><br>
            <p>de:&nbsp;{{date_field('date', 'id':'fecha_ini')}}</p>
            <p>&nbsp;&nbsp;a:&nbsp;{{date_field('dates', 'id':'fecha_fin')}}</p>
        </div>
        <div class="col-sm-12 col-md-3" style="/*border-style: solid; border-width: 1px; border-color: #ddd; height: 127px;*/">
            <label>ID compra socio:</label><br/>
            {{ text_field('purchase_partner_id', 'value':idSocio, 'id':'id_cs') }}
        </div>
        {# Selectores de busqueda para administrador FIN #}
    {% else %}
        {# Selectores de busqueda para partner INICIA #}
        <div class="col-sm-12 col-md-4" style="margin-top: 9px;">
            <label>ID compra socio:</label>
            <p>{{ text_field('purchase_partner_id', 'id':'id_cs', 'style':'width:300px;') }}</p>
        </div>
        <div class="col-sm-12 col-md-4" style="/*border-style: solid; border-width: 1px; border-color: #ddd; height: 127px;*/">
            <label>Ubicación:</label><br/>
            <div class="row">
                <div class="col-xs-3" align="left" style="margin-top: 5px; margin-bottom: 5px;">
                    País:
                </div>
                <div class="col-xs-9" style="margin-top: 5px; margin-bottom: 5px;">
                    <select name="pais" id="pais" style="width: 80%;">
                        <option value=""  selected="selected">Todos</option>
                        <option value="MX">México</option>
                        {% for p in paises %}
                            <option value="{{ p.sortname }}">{{ p.name }}</option>
                        {%  endfor %}
                    </select>
                </div>
                <div class="col-xs-3" align="left" style="margin-top: 5px; margin-bottom: 5px;">
                    Estado/provincia:
                </div>
                <div class="col-xs-9" style="margin-top: 5px; margin-bottom: 5px;" id="est">
                    <select name="estado" id="estado" style="width: 80%;">
                        <option value="">--</option>
                    </select>
                </div>
                 <div class="col-xs-3" align="left" style="margin-top: 5px; margin-bottom: 5px;">
                     Ciudad:
                 </div>
                 <div class="col-xs-9" style="margin-top: 5px; margin-bottom: 5px;">
                     <select name="ciudad" id="ciudad" style="width: 80%;">
                         <option value="">--</option>
                         {#if....#}
                        <option value="todos">Todos</option>
                    </select>
                </div>
            </div>

        </div>
        <div class="col-sm-12 col-md-4" >
            <label>Fecha:</label>
            <p>de: {{date_field('date', 'id':'fecha_ini')}} a: {{date_field('dates', 'id':'fecha_fin')}}</p>
        </div>
        {# Selectores de busqueda para partner FIN #}
{% endif %}
</div>

{# Tabla de contenido principal INICIO #}
<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="answer-admin">
        <tr><th colspan="11">Registros por página/Total de registros: {{ page.total_items/ page.total_items * page.limit }}/{{ page.total_items }}</th></tr>
        <tr>
            <th>ID de compra</th>
            {% if se=='1' or se =='4' %}<th>ID del socio</th>{% endif %}
            <th>ID kobo</th>
            <th>Fecha</th>
            <th>ID compra socio</th>
            <th>Ciudad</th>
            <th>Estado/Provincia</th>
            <th>País</th>
            <th>Código Postal</th>
            <th>ID cliente orbile</th>
            <th>Detalles</th>
        </tr>
        {% for val,obj in page.items %}
            <tr>
                <td>{{obj.purchase_id}}</td>
                {% if se == '1' or se == '4' %}<td>{{ link_to("partners/details/" ~ obj.partner_id, obj.partner_id) }}</td>{% endif %}
                <td><div class="emergente">
                        <div class="ocultar"><?php echo substr($obj->user_id,0,5).'...'; ?></div>
                        <div class="mostrar mostrarPID">{{ obj.user_id }}</div>
                    </div>
                </td>
                <td><div class="emergente">
                        <div class="ocultar"><?php echo substr($obj->date,0,5).'...'; ?></div>
                        <div class="mostrar mostrarDate">{{obj.date}}</div>
                    </div>
                </td>
                <td>{{obj.purchase_partner_id}}</td>
                <td>
                    {{obj.address_city}}
                </td>
                <td>
                    {{obj.address_state_provincy}}
                </td>
                <td>{{obj.address_country}}</td>
                <td>{{obj.address_zip_postal_code}}</td>
                <td>
                    <div class="emergente">
                        <div class="ocultar" style="color: #428bca;"><?php echo substr($obj->orbile_customer_id,0,5).'...'; ?></div>
                        <div class="mostrar mostrarIDorb">{{ link_to ("customers/details/" ~ obj.orbile_customer_id, obj.orbile_customer_id) }}</div>
                    </div>
                </td>
                <td width="7%">{{ link_to("sales-reports/details/" ~ obj.purchase_id, '<i class="glyphicon glyphicon-list-alt"></i> DETALLES', "class": "btn btn-default") }}</td>
            </tr>
        {% if loop.last %}
            <tr>
                <td colspan="8" align="left">
                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                    <div class="btn-group">
                        {{ link_to("sales-reports/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                        {{ link_to("sales-reports/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                        {{ link_to("sales-reports/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                        {{ link_to("sales-reports/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                    </div>
                </td>
                {% if se == '1' or se == '4' %}
                {% if partnergraf is defined %}
                    <td colspan="3" align="right">{{ link_to("sales-reports/mes?partnerid=" ~ partnergraf, '<i class="glyphicon glyphicon-stats"></i> Gráficas', "class": "btn btn-default") }} </td>
                {% else %}
                    <td colspan="3" align="right">{{ link_to("sales-reports/selectgrafica/", '<i class="glyphicon glyphicon-stats"></i> Gráficas', "class": "btn btn-default") }} </td>
                {% endif %}
                {% else %}
                    <td colspan="2" align="right">{{ link_to("sales-reports/mes/" ~ obj.partner_id, '<i class="glyphicon glyphicon-stats"></i> Gráficas', "class": "btn btn-default") }} </td>
                {% endif %}
            </tr>
        {% endif %}
        {% endfor %}
    </table>
</div>
{# Tabla de contenido principal FIN #}

{# Sección para que aparezca el botón para imprimir el reporte #}
<div align="right" id="link">

</div>


{# Código javaScript para las llamadas de búsqueda en la BD #}
<script type="text/javascript">




    $(document).ready(function(){
        window.onload=linkBuscar;

        function linkBuscar()
        {

            if(document.getElementById('id_cs').value !== '')
            {
                // alert(document.getElementById('id_cs').value);
                $pagina_ajax = '';
                Busqueda($pagina_ajax);
            }

        }

        $('#partners').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
        });

        $('#id_cs').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
        });

        $('#fecha_ini').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
        });

        $('#fecha_fin').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
        });

        $('#pais').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
            Cambio_de_estado();
        });
        $('#estado').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
            Cambio_de_ciudad();
        });
        $('#ciudad').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
        });

        function decode_utf8(s) {
            return decodeURIComponent(escape(s));
        }

        function Busqueda($pagina_ajax)
        {
            $pagina = $pagina_ajax;
            $id_partner = $('#partners').val();
            $id_cs = $('#id_cs').val();
            $fecha_ini = $('#fecha_ini').val();
            $fecha_fin = $('#fecha_fin').val();
            $pais = $('#pais').val();
            $estado = $('#estado').val();
            $ciudad = $('#ciudad').val();
            $rol = $('#rol').val();
            $id_del_partner = $('#id_del_partner').val();

            {# Se hace la peticion via get hacia sales-reports/search pasando los parámetros obtenidos #}
            $.get("<?php echo $this->url->get('sales-reports/search')?>",{'pagina':$pagina, 'id_partner':$id_partner, 'id_cs':$id_cs, 'fecha_ini':$fecha_ini, 'fecha_fin':$fecha_fin, 'pais':$pais, 'estado':$estado, 'ciudad':$ciudad},function($data)
            {
                if($data) {
                    {#debugger;#}
                    //console.log($data);
                    var purchase = $data

                    if ($rol == 1 || $rol == 4) {
                        var pur = "<tr><th colspan='12'>Registros por página/Total de registros: " + ~~(purchase.total_items / purchase.total_items * purchase.limit) + "/" + purchase.total_items + "</th></tr>";
                    }
                    else {
                        var pur = "<tr><th colspan='13'>Registros por página/Total de registros: " + ~~(purchase.total_items / purchase.total_items * purchase.limit) + "/" + purchase.total_items + "</th></tr>";
                    }
                    pur += "<tr>" +
                    "<th>ID de compra</th>";

                    if ($rol == 1 || $rol == 4) {
                        pur += "<th>ID del socio</th>";
                    }

                    pur += "<th>ID kobo</th>" +
                    "<th>Fecha</th>" +
                    "<th>ID compra socio</th>" +
                    "<th>Ciudad</th>" +
                    "<th>Estado/Provincia</th>" +
                    "<th>País</th>" +
                    "<th>Código Postal</th>" +
                    "<th>ID cliente orbile</th>" +
                    "<th>Detalles</th>" +
                    "</tr>";
                    $.each(purchase.items, function (index, datos) {
                        pur += '<tr>' +
                        '<td>' + datos.purchase_id + '</td>';

                        if ($rol == 1 || $rol == 4) {
                            pur += '<td><a href="/partners/details/' + datos.partner_id + '">' + datos.partner_id + '</a></td>';
                        }

                        pur += '<td><div class="emergente"><div class="ocultar">' + (datos.user_id || " ").substring(0, 5) + '...</div><div class="mostrar mostrarPID">' + datos.user_id + '</div></div></td>' +
                        '<td><div class="emergente"><div class="ocultar">' + (datos.date || " ").substring(0, 5) + '...</div><div class="mostrar mostrarDate">' + datos.date + '</div></div></td>' +
                        '<td>' + datos.purchase_partner_id + '</td>' +
                        '<td>' + datos.address_city + '</td>' +
                        '<td>' + datos.address_state_provincy + '</td>' +
                        '<td>' + datos.address_country + '</td>' +
                        '<td>' + datos.address_zip_postal_code + '</td>' +
                        '<td><div class="emergente"><div class="ocultar" style="color: #428bca;">' + (datos.orbile_customer_id || " ").substring(0, 5) + '...</div><div class="mostrar mostrarIDorb">' + '<a href="/customers/details/' + datos.orbile_customer_id + '">' + datos.orbile_customer_id + '</div></div></td>' +
                        '<td width="7%"><a href="/sales-reports/details/' + datos.purchase_id + '" class= "btn btn-default"> <i class="glyphicon glyphicon-list-alt"></i> DETALLES</a></td>' +
                        '</tr>';

                    });

                    pur += "<tr>" +
                    "<td colspan='9' align='left'>" +
                    "<div class='btn-group'>" +
                    "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>" +
                    "<a data-before='" + purchase.before + "' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>" +
                    "<a data-next='" + purchase.next + "' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>" +
                    "<a data-last='" + purchase.last + "' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>" +
                    "<span class='help-inline'>" + purchase.current + "/" + purchase.total_pages + "</span>" +
                    "</div>" +
                    "</td>";
                    if ($rol == 1 || $rol == 4) {
                        pur += "<td colspan='2' align='right'><a href='/sales-reports/selectgrafica' class='btn btn-default'><i class='glyphicon glyphicon-stats'></i> Gráficas</a></td>";
                    }
                    else {
                        pur += "<td align='right'><a href='/sales-reports/mes/" + $id_del_partner + "' class='btn btn-default'><i class='glyphicon glyphicon-stats'></i> Gráficas</a></td>";
                    }
                    pur += "</tr>";
                    {# Populamos el desplegable purchase con sus respectivos datos obtenidos #}
                    $('table#answer-admin').html(pur);
                    $('#link').html(' <a href="/sales-reports/showCsv" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
                }
            });

        }

        function Cambio_de_estado()
        {
            $pais = $('#pais').val();

            {# Se hace la peticion via get hacia sales-reports/estado pasando los parámetros obtenidos #}
            $.get("<?php echo $this->url->get('sales-reports/estado')?>",{'pais':$pais},function($data) {
                //alert($data[0]);
                if($data){
                    var nom_estados = "<option value=''>Todos</option>";
                    for($i = 0 ; $i < $data.length ; $i++)
                    {
                        nom_estados += "<option value='"+ $data[$i]+"'>"+$data[$i]+"</option>";
                    }
                    nom_estados += "</select>";
                    $('#estado').html(nom_estados);
                }
            });
        }

        function Cambio_de_ciudad()
        {
            $estado = $('#estado').val();

            {# Se hace la peticion via get hacia sales-reports/ciudad pasando los parámetros obtenidos #}
            $.get("<?php echo $this->url->get('sales-reports/ciudad')?>",{'estado':$estado},function($data) {
                if($data) {
                    var nom_ciudades = "<option value=''>Todos</option>";
                    for ($i = 0; $i < $data.length; $i++) {
                        nom_ciudades += "<option value='" + $data[$i] + "'>" + $data[$i] + "</option>";
                    }
                    nom_ciudades += "</select>";
                    $('#ciudad').html(nom_ciudades);
                }
            });
        }


        {# Se realiza la paginación que redirecciona a la busqueda nuevamente con la variable de la página a obtener #}
        $(document).on('click','#next', function(){
            $ajax_page = $('#next').data('next');
            Busqueda($ajax_page);
        });
        $(document).on('click','#first', function(){
            $ajax_page=$('#first').data('first');
            Busqueda($ajax_page);
        });
        $(document).on('click','#last', function(){
            $ajax_page=$('#last').data('last');
            Busqueda($ajax_page);
        });
        $(document).on('click','#before', function(){
            $ajax_page=$('#before').data('before');
            Busqueda($ajax_page);
        });


    });
</script>
