{{ content() }}
{# comentario para quitar error#}
<script type="text/javascript">
window.onload = function () {
var atras = document.getElementById('irAtras');
atras.addEventListener("click", irAtras);
function irAtras() { this.style.color = 'red'; window.history.back();}
}
</script>

<p style="text-align: left; padding: 10px;">{{ link_to("sales-reports", "class":"btn btn-primary","&larr; Regresar", "id":"irAtras") }}</p>

<div width="100%" style="display: block; overflow: auto; padding-bottom: 20px;">
    <table align="center" class="table-striped">
        <tr>
            <th>ID de compra:</th>
            <td>{{ purchase_id }}</td>
        </tr>
        {% if se == 1 or se == 4 %}
            <tr>
                <th>ID del socio:</th>
                <td>{{ link_to("partners/details/" ~ partner_id, partner_id) }}</td>
            </tr>
        {% endif %}
        <tr>
            <th>ID kobo:</th>
            <td>{{ user_id }}</td>
        </tr>
        <tr>
            <th>Fecha:</th>
            <td>{{ date }}</td>
        </tr>
        <tr>
            <th>ID compra socio:</th>
            <td>{{ purchase_partner_id }}</td>
        </tr>
        <tr>
            <th>Ciudad:</th>
            <td>{{ address_city }}</td>
        </tr>
        <tr>
            <th>Provincia/Estado:</th>
            <td>{{ address_state_provincy }}</td>
        </tr>
        <tr>
            <th>País:</th>
            <td>{{ address_country }}</td>
        </tr>
        <tr>
            <th>Código Postal:</th>
            <td>{{ address_zip_postal_code }}</td>
        </tr>
        <tr>
            <th>ID cliente orbile: &nbsp;</th>
            <td>{{ link_to ("customers/details/" ~ orbile_customer_id, orbile_customer_id) }}</td>
        </tr>
        <tr>
            <th>Correo:</th>
            <td>{{ link_to("customers/details/" ~ email, email,'target': '_blank') }}</td>
        </tr>
    </table>
</div>


    <div width="100%" style="display: block; overflow: auto;">
        <table class="table table-bordered table-striped" align="center">
            <tr>
                <th>ID linea de compra</th>
                <th>ID de compra</th>
                <th>Resultado</th>
                <th>ID del producto</th>
                <th>Importe</th>
                <th>Código de moneda</th>
                <th>Precio antes de impuestos</th>
                <th>ID artículo</th>
                <th>Url de descarga</th>
            </tr>
            {% for obj in page.items %}
            <tr>
                <td>{{obj.line_id}}</td>
                <td>{{obj.purchase_id}}</td>
                <td>{{obj.result}}</td>
                <td><div class="emergente">
                        <div class="ocultar" style="color: #428bca;"><?php echo substr($obj->product_id,0,5).'...'; ?></div>
                        <div class="mostrar mostrarIDorb">{{ link_to("/library/getbookrev/" ~ obj.product_id, obj.product_id) }}</div>
                    </div>
                </td>
                <td style="white-space:nowrap;">{{"$ "~ number_format(obj.cogs_amount,2)}}</td>
                <td>{{obj.cogs_currency_code}}</td>
                <td>{{"$ "~ number_format(obj.price_before_tax,2)}}</td>
                <td><div class="emergente">
                        <div class="ocultar" style="color: #428bca;"><?php echo substr($obj->item_id,0,5).'...'; ?></div>
                        <div class="mostrar mostrarIDorb">{{ link_to("/library/getbookrev/" ~ obj.item_id, obj.item_id) }}</div>
                    </div>
                </td>
                <td width="250px"><div class="emergente">
                        <div class="ocultar"><?php echo substr($obj->download_url,0,30).'...'; ?></div>
                        <div class="mostrar mostrarURLDetail">{{  obj.download_url }}</div>
                    </div>
                </td>

            </tr>
            {% endfor %}
                <tr>
                    <td colspan="12" align="left">
                        <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                        <div class="btn-group">
                            {{ link_to("sales-reports/details/" ~ obj.purchase_id, '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                            {{ link_to("sales-reports/details/" ~ obj.purchase_id ~ "?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                            {{ link_to("sales-reports/details/" ~ obj.purchase_id ~ "?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                            {{ link_to("sales-reports/details/" ~ obj.purchase_id ~ "?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                        </div>
                    </td>
                </tr>
        </table>

</div>