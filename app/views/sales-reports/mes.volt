{{ content() }}
{% if rol==1 or rol==4 %}

    <p style="text-align: left; padding: 10px;">{{ link_to("sales-reports/selectgrafica", "class":"btn btn-primary","&larr; Go Back") }}</p>
{% else %}
    <p style="text-align: left; padding: 10px;">{{ link_to("sales-reports", "class":"btn btn-primary","&larr; Go Back") }}</p>
{% endif %}
<div class="row">
    <div class="col-md-12">
        <label style="text-align: center;">Graficar ventas y customers registrados<br/>
            para {{ partner }}:</label>
    </div>
    <div class="col-md-12">
        <p><select name="partnerid" id="seleccionGrafica" onchange="window.location.href=this.value">
                <option value="\sales-reports/anioventas/{{ id_partner }}">por año</option>
                <option value="\sales-reports/mes/{{ id_partner }}" selected="selected">por mes</option>
                <option value="\sales-reports/semana/{{ id_partner }}">por semana</option>
                <option value="\sales-reports/dias/{{ id_partner }}">por día</option>
                <option value="\sales-reports/total/{{ id_partner }}">Totales</option>
            </select></p>
    </div>
</div>
<div id="content">
    <!-- renglón(class:row) -->
    <div class="row">
        <!-- columna(class:col) -->
        <div class="col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <!-- título -->
                <i class="fa fa-fw fa-bar-chart-o"></i>
                Gráficas {{ partner }}
            </h1>
        </div>
        <!-- fin de columna -->
    </div>
    <!-- fin de renglón -->

    <!-- Selector del año y mes -->
    <p>
        <label>en: &nbsp;</label>
        <select name="mes" id="selectMes">
            {% for val,mes in meses %}
                {% if val == estemes %}
                    <option value="{{ val }}" selected="selected">{{ mes }}</option>
                {% else %}
                    <option value="{{ val }}">{{ mes }}</option>
                {% endif %}
            {% endfor %}
        </select>
        <label>&nbsp;de:&nbsp;</label>
        <select name="anio" id="selectAnio">
            {% if esteanio is defined %}
                <option value="{{ esteanio }}" selected="selected">{{ esteanio }}</option>
            {%  endif %}
            {% for an in anios %}
                {% if a == an.anio %}
                    <option value="{{ an.anio }}" selected="selected">{{ an.anio }}</option>
                {%  else %}
                <option value="{{ an.anio }}">{{ an.anio }}</option>
                {% endif %}
            {% endfor %}
        </select>
    </p>
    <!-- Fin del selector del año y mes -->

    <!--=====================    GRÁFICAS DE VENTAS    ===================== -->
    <!-- widget grid -->
    <section id="widget-grid" class="">
        <!-- renglón(class:row) -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div id="contenido" class="jarviswidget" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
                    <!-- Contenido de la gráfica -->
                </div>
                <!-- end widget -->
            </article>
            <!-- WIDGET END -->
        </div>
        <!-- fin de renglón -->

    </section>
    <!-- end widget grid -->
    <!--=====================  FIN DE GRÁFICAS DE VENTAS  ===================== -->

    <!--=====================     GRÁFICAS DE USUARIOS    ===================== -->
    <!-- widget grid -->
    <section id="widget-grid" class="">
        <!-- renglón(class:row) -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div id="contenidoUsuarios" class="jarviswidget" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
                    <!-- Contenido de la gráfica -->
                </div>
                <!-- end widget -->
            </article>
            <!-- WIDGET END -->
        </div>
        <!-- fin de renglón -->
    </section>
    <!-- end widget grid -->
    <!--===================== FIN DE GRÁFICAS DE USUARIOS ===================== -->

</div>

<!--=====================     CONTENIDO JAVASCRIPT    ===================== -->


<script type="text/javascript">

    $(document).ready(function() {
        window.onload = function(){

            var meses = new Array ("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre", "Diciembre");
            var fecha_actual = new Date();
            var EsteMes = meses[fecha_actual.getMonth()];
            var EsteAnio = fecha_actual.getFullYear();
            var graf = '<header><h2>Ventas en '+EsteMes+' de '+EsteAnio+': <img src="/../img/linea_azul_graficas.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
                    'Clientes en '+EsteMes+' de '+EsteAnio+': <img src="/../img/linea_roja_graficas.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                    'Miles de pesos vendidos en '+EsteMes+' de '+EsteAnio+': <img src="/../img/linea_verde_graficas.png"></h2></header>'+
                    '<div>'+
                    '<div class="widget-body">'+
                    '<canvas id="ventasMes" height="120"></canvas>'+
                    '</div>'+
                    '</div>';
            $('#contenido').html(graf);

            pageSetUp();

            // GRÁFICA DE VENTAS POR MES
            // ref: http://www.chartjs.org/docs/#line-chart-introduction

            var lineOptions = {
                ///Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines : true,
                //String - Colour of the grid lines
                scaleGridLineColor : "rgba(0,0,0,0.12)",
                //Number - Width of the grid lines
                scaleGridLineWidth : 1,
                //Boolean - Whether the line is curved between points
                bezierCurve : true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension : 0.4,
                //Boolean - Whether to show a dot for each point
                pointDot : true,
                //Number - Radius of each point dot in pixels
                pointDotRadius : 3,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth : 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius : 10,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke : true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth : 2,
                //Boolean - Whether to fill the dataset with a colour
                datasetFill : true,
                //Boolean - Re-draw chart on page resize
                responsive: true,
                //String - A legend template
                legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){ %><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){ %><%=datasets[i].label%><%}%></li><%}%></ul>"
            };

            //Se obtienen los valores del controlador
            var ventas_dias =<?php echo json_encode($dias); ?>;
            var ventas_por_mes = <?php echo json_encode($ventas_por_mes); ?>;
            var usuarios_por_mes = <?php echo json_encode($usuarios_por_mes); ?>;
            var dineros_por_mes = <?php echo json_encode($dineros_por_mes); ?>;

            var lineData = { labels: ventas_dias,
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(255,0,0,0.1)",
                        strokeColor: "rgba(255,0,0,0.6)",
                        pointColor: "rgba(255,0,0,0.3)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(255,0,0,0.5)",
                        data: usuarios_por_mes
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(0, 0, 255, 0.1)",
                        strokeColor: "rgba(0, 0, 255, 0.6)",
                        pointColor: "rgba(0, 0, 255, 0.3)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(0, 0, 255,0.5)",
                        data: ventas_por_mes
                    },
                    {
                        label: "My third dataset",
                        fillColor: "rgba(0,128,0,0.1)",
                        strokeColor: "rgba(0,128,0,0.7)",
                        pointColor: "rgba(0,128,0,3)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(0,128,0,0.5)",
                        data: dineros_por_mes
                    }
                ]
            };

            // render chart
            var ctx = document.getElementById("ventasMes").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);
        };

        $('#selectAnio').on('change', function () {
            GraficaNueva();
        });
        $('#selectMes').on('change', function () {
            GraficaNueva();
        });

        function GraficaNueva() {
            $anio = $('#selectAnio').val();
            $mes = $('#selectMes').val();
            $id_partner = <?php echo $id_partner ?>;

            $.get("<?php echo $this->url->get('sales-reports/mes')?>",{'anio':$anio, 'mes':$mes, 'id_partner':$id_partner},function($data){
                var otra_fecha = $data;
                var meses = new Array ("","Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre", "Diciembre");
                var EsteMes = meses[$mes];
                var EsteAnio = $anio;
                var graf = '<header><h2>Ventas en '+EsteMes+' de '+EsteAnio+': <img src="/../img/linea_azul_graficas.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
                        'Clientes en '+EsteMes+' de '+EsteAnio+': <img src="/../img/linea_roja_graficas.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                        'Miles de pesos vendidos en '+EsteMes+' de '+EsteAnio+': <img src="/../img/linea_verde_graficas.png"></h2></header>'+
                        '<div>'+
                        '<div class="widget-body">'+
                        '<canvas id="ventasMes" height="120"></canvas>'+
                        '</div>'+
                        '</div>';
                $('#contenido').html(graf);

                pageSetUp();

                // GRÁFICA DE VENTAS POR MES
                // ref: http://www.chartjs.org/docs/#line-chart-introduction

                var lineOptions = {
                    ///Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines : true,
                    //String - Colour of the grid lines
                    scaleGridLineColor : "rgba(0,0,0,0.12)",
                    //Number - Width of the grid lines
                    scaleGridLineWidth : 1,
                    //Boolean - Whether the line is curved between points
                    bezierCurve : true,
                    //Number - Tension of the bezier curve between points
                    bezierCurveTension : 0.4,
                    //Boolean - Whether to show a dot for each point
                    pointDot : true,
                    //Number - Radius of each point dot in pixels
                    pointDotRadius : 3,
                    //Number - Pixel width of point dot stroke
                    pointDotStrokeWidth : 1,
                    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                    pointHitDetectionRadius : 10,
                    //Boolean - Whether to show a stroke for datasets
                    datasetStroke : true,
                    //Number - Pixel width of dataset stroke
                    datasetStrokeWidth : 2,
                    //Boolean - Whether to fill the dataset with a colour
                    datasetFill : true,
                    //Boolean - Re-draw chart on page resize
                    responsive: true,
                    //String - A legend template
                    legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){ %><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){ %><%=datasets[i].label%><%}%></li><%}%></ul>"
                };

                var lineData = { labels: otra_fecha.dias,
                    datasets: [
                        {
                            label: "My First dataset",
                            fillColor: "rgba(255,0,0,0.1)",
                            strokeColor: "rgba(255,0,0,0.6)",
                            pointColor: "rgba(255,0,0,0.3)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(255,0,0,0.5)",
                            data: otra_fecha.customers_por_mes
                        },
                        {
                            label: "My Second dataset",
                            fillColor: "rgba(0, 0, 255, 0.1)",
                            strokeColor: "rgba(0, 0, 255, 0.6)",
                            pointColor: "rgba(0, 0, 255, 0.3)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(0, 0, 255,0.5)",
                            data: otra_fecha.ventas_por_mes
                        },
                        {
                            label: "My third dataset",
                            fillColor: "rgba(0,128,0,0.1)",
                            strokeColor: "rgba(0,128,0,0.7)",
                            pointColor: "rgba(0,128,0,3)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(0,128,0,0.5)",
                            data: otra_fecha.dineros_por_mes
                        }
                    ]
                };

                // render chart
                var ctx = document.getElementById("ventasMes").getContext("2d");
                var myNewChart = new Chart(ctx).Line(lineData, lineOptions);
            });
        }
    })

</script>


<!-- Your GOOGLE ANALYTICS CODE Below -->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>