{{ content() }}

{% if rol==1 or rol==4 %}
    <p style="text-align: left; padding: 10px;">{{ link_to("sales-reports/selectgrafica", "class":"btn btn-primary","&larr; Go Back", "id":"irAtras") }}</p>
{% else %}
    <p style="text-align: left; padding: 10px;">{{ link_to("sales-reports", "class":"btn btn-primary","&larr; Go Back", "id":"irAtras") }}</p>
{% endif %}

<div class="row">
    <div class="col-md-12">
        <label style="text-align: center;">Graficar ventas y customers registrados<br/>
            para {{ partner }}:</label>
    </div>
    <div class="col-md-12">
        <p><select name="partnerid" id="seleccionGrafica" onchange="window.location.href=this.value">
                <option value="\sales-reports/anioventas/{{ id_partner }}">por año</option>
                <option value="\sales-reports/mes/{{ id_partner }}">por mes</option>
                <option value="\sales-reports/semana/{{ id_partner }}" selected="selected">por semana</option>
                <option value="\sales-reports/dias/{{ id_partner }}">por día</option>
                <option value="\sales-reports/total/{{ id_partner }}">Totales</option>
            </select></p>
    </div>
</div>
<div id="content">
    <!-- renglón(class:row) -->
    <div class="row">
        <!-- columna(class:col) -->
        <div class="col-lg-12">
            <h1 class="page-title txt-color-blueDark">
                <!-- título -->
                <i class="fa fa-fw fa-bar-chart-o"></i>
                Gráficas {{ partner }}
            </h1>
        </div>
        <!-- fin de columna -->
    </div>
    <!-- fin de renglón -->

    <!--=====================    GRÁFICAS DE VENTAS    ===================== -->
    <!-- widget grid -->
    <section id="widget-grid" class="">
        <!-- renglón(class:row) -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div id="contenido" class="jarviswidget" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
                    <!-- Contenido de la gráfica -->
                </div>
                <!-- end widget -->
            </article>
            <!-- WIDGET END -->
        </div>
        <!-- fin de renglón -->

    </section>
    <!-- end widget grid -->
    <!--=====================  FIN DE GRÁFICAS DE VENTAS  ===================== -->

    <!--=====================     GRÁFICAS DE USUARIOS    ===================== -->
    <!-- widget grid -->
    <section id="widget-grid" class="">
        <!-- renglón(class:row) -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div id="contenidoUsuarios" class="jarviswidget" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
                    <!-- Contenido de la gráfica -->
                </div>
                <!-- end widget -->
            </article>
            <!-- WIDGET END -->
        </div>
        <!-- fin de renglón -->
    </section>
    <!-- end widget grid -->
    <!--===================== FIN DE GRÁFICAS DE USUARIOS ===================== -->

</div>


<!--=====================     CONTENIDO JAVASCRIPT    ===================== -->

<script type="text/javascript">

    $(document).ready(function() {
        window.onload = function(){

            var graf = '<header><h2>Ventas de la semana en curso: <img src="/../img/linea_azul_graficas.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
            'Clientes de la semana en curso: <img src="/../img/linea_roja_graficas.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+
            'Miles de pesos vendidos en la semana en curso: <img src="/../img/linea_verde_graficas.png"></h2></header>'+
                    '<div>'+
                    '<div class="widget-body">'+
                    '<canvas id="ventasSemana" height="120"></canvas>'+
                    '</div>'+
                    '</div>';
            $('#contenido').html(graf);

            pageSetUp();

            var lineOptions = {
                ///Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines : true,
                //String - Colour of the grid lines
                scaleGridLineColor : "rgba(0,0,0,0.12)",
                //Number - Width of the grid lines
                scaleGridLineWidth : 1,
                //Boolean - Whether the line is curved between points
                bezierCurve : true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension : 0.4,
                //Boolean - Whether to show a dot for each point
                pointDot : true,
                //Number - Radius of each point dot in pixels
                pointDotRadius : 3,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth : 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius : 10,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke : true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth : 2,
                //Boolean - Whether to fill the dataset with a colour
                datasetFill : true,
                //Boolean - Re-draw chart on page resize
                responsive: true,
                //String - A legend template
                legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){ %><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){ %><%=datasets[i].label%><%}%></li><%}%></ul>"
            };

            //Se obtienen los valores del controlador
            var ventas_por_semana = <?php echo json_encode($ventas_por_semana) ?>;
            var usuarios_por_semana = <?php echo json_encode($usuarios_por_semana) ?>;
            var dineros_por_semana = <?php echo json_encode($dineros_por_semana) ?>;

            var lineData = { labels: ["Domingo","Lunes", "Martes","Miércoles","Jueves","Viernes","Sábado"],
                datasets: [
                    {
                        label: "My First dataset",
                        fillColor: "rgba(255,0,0,0.1)",
                        strokeColor: "rgba(255,0,0,0.6)",
                        pointColor: "rgba(255,0,0,0.3)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(255,0,0,0.5)",
                        data: usuarios_por_semana
                    },
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(0, 0, 255, 0.1)",
                        strokeColor: "rgba(0, 0, 255, 0.6)",
                        pointColor: "rgba(0, 0, 255, 0.3)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(0, 0, 255,0.5)",
                        data: ventas_por_semana
                    },
                    {
                        label: "My third dataset",
                        fillColor: "rgba(0,128,0,0.1)",
                        strokeColor: "rgba(0,128,0,0.7)",
                        pointColor: "rgba(0,128,0,3)",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(0,128,0,0.5)",
                        data: dineros_por_semana
                    }
                ]
            };

            // render chart
            var ctx = document.getElementById("ventasSemana").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);

            // TERMINA GRÁFICA DE VENTAS Y CUSTOMERS POR SEMANA





            /*

            var graf = '<header><h2>Customers registrados en la semana en curso</h2></header>'+
                    '<div>'+
                    '<div class="widget-body">'+
                    '<canvas id="usuariosSemana" height="120"></canvas>'+
                    '</div>'+
                    '</div>';
            $('#contenidoUsuarios').html(graf);

            //GRÉFICA DE USUARIOS POR SEMANA
            pageSetUp();

            var lineOptions = {
                ///Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines : true,
                //String - Colour of the grid lines
                scaleGridLineColor : "rgba(0,0,0,0.12)",
                //Number - Width of the grid lines
                scaleGridLineWidth : 1,
                //Boolean - Whether the line is curved between points
                bezierCurve : true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension : 0.4,
                //Boolean - Whether to show a dot for each point
                pointDot : true,
                //Number - Radius of each point dot in pixels
                pointDotRadius : 3,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth : 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius : 10,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke : true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth : 2,
                //Boolean - Whether to fill the dataset with a colour
                datasetFill : true,
                //Boolean - Re-draw chart on page resize
                responsive: true,
                //String - A legend template
                legendTemplate : "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){ %><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){ %><%=datasets[i].label%><%}%></li><%}%></ul>"
            };


            // Se obtienen los valores desde el controlador

            var usuarios_por_semana = <?php echo json_encode($usuarios_por_semana) ?>;


            var lineData = { labels: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
                datasets: [
                    {
                        label: "My Second dataset",
                        fillColor: "rgba(90, 166, 220, 0.3)",
                        strokeColor: "rgba(90, 166, 220, 1)",
                        pointColor: "rgba(39, 86, 200, 0.6)",
                        pointStrokeColor: "#A9A9F5",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(156,0,0,0.3)",
                        data: usuarios_por_semana

                    }
                ]
            };

            // render chart
            var ctx = document.getElementById("usuariosSemana").getContext("2d");
            var myNewChart = new Chart(ctx).Line(lineData, lineOptions);
            */
        };
    })

</script>


<!-- Your GOOGLE ANALYTICS CODE Below -->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>