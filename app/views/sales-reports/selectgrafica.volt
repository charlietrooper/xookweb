{{ content() }}
<script type="text/javascript">
    window.onload = function () {
        var atras = document.getElementById('irAtras');
        atras.addEventListener("click", irAtras);
        function irAtras() { this.style.color = 'red'; window.history.back();}
    }
</script>
<p style="text-align: left; padding: 10px;">{{ link_to("sales-reports", "class":"btn btn-primary","&larr; Go Back", "id":"irAtras") }}</p>

<div class="col-lg-12" xmlns="http://www.w3.org/1999/html">

    <!------------------------------------Parte para admin------------------------------------>
    {% if se == '1' or se == '4' %}
    {{ form("sales-reports/mes", "method":"get") }}
    <select name="partnerid" id="partnerid" style="width: 200px;">
        <option value="0">Elige el partner a graficar:</option>
        {% for p in partner %}
            <option value={{ p.partner_id }}>{{ p.name }}</option>
        {% endfor %}
    </select>
    <div align="center" style="padding-top: 20px; padding-bottom: 20px">
        <ul class="pager">
            <li>
                {{ submit_button("id":"graf", "Ver graficas", "class": "btn btn-primary") }}
            </li>
        </ul>
    </div>

    {#
    <div class="row" style="padding-bottom: 30px">
        <div class="col-md-12">
            <label>Año:</label><br/>
            <select name="anio" id="selectAnio">
                <option selected="selected" value="2016">2016</option>
                <option value="2015">2015</option>
            </select>
        </div>
    </div>#}
    </form>
</div>

<!---------------------NUEVO--------------------------->

<div id="content">

    <!-- Selector del año -->
    <p>


        <label>Año:</label><br/>
        <select name="anio" id="selectAnio">
            {% for an in selectAnios %}
                {% if thisyear == an.anio %}
                    <option value="{{ an.anio }}" selected="selected">{{ an.anio }}</option>
                {% else %}
                    <option value="{{ an.anio }}">{{ an.anio }}</option>
                {% endif %}
        <!--    <option selected="selected" value="2017">2017</option>
            <option value="2016">2016</option>
            <option value="2015">2015</option>-->
            {% endfor %}
        </select>
</div>
    </p>
    <!-- Fin del selector del año -->
<!--=====================    GRÁFICAS DE VENTAS    ===================== -->
    <!-- widget grid -->
    <section id="widget-grid" class="">
        <!-- renglón(class:row) -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div id="contenido" class="jarviswidget" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
                    <!-- Contenido de la gráfica -->
                </div>
                <!-- end widget -->
            </article>
            <!-- WIDGET END -->
        </div>
        <!-- fin de renglón -->

    </section>
    <!-- end widget grid -->
    <!--=====================  FIN DE GRÁFICAS DE VENTAS  ===================== -->

    <!--=====================     GRÁFICAS DE USUARIOS    ===================== -->
    <!-- widget grid -->
    <section id="widget-grid" class="">
        <!-- renglón(class:row) -->
        <div class="row">
            <!-- NEW WIDGET START -->
            <article class="col-lg-12">
                <!-- Widget ID (each widget will need unique ID)-->
                <div id="contenidoUsuarios" class="jarviswidget" data-widget-colorbutton="false" data-widget-fullscreenbutton="false" data-widget-editbutton="false" data-widget-sortable="false">
                    <!-- Contenido de la gráfica -->
                </div>
                <!-- end widget -->
            </article>
            <!-- WIDGET END -->
        </div>
        <!-- fin de renglón -->
    </section>
    <!-- end widget grid -->
    <!--===================== FIN DE GRÁFICAS DE USUARIOS ===================== -->

</div>


{% endif %}
<!--------------------------------Termina parte para admin-------------------------------->

<!--================================================== -->

{#<script src="../../../public/js/jquery-1.11.1.min.js"></script>#}

<script type="text/javascript">

    $(document).ready(function() {
        window.onload = function(){
            GraficaNueva();
        };

        //AL CAMBIAR EL AÑO

        $('#selectAnio').on('change', function () {
            GraficaNueva();
        });

        function GraficaNueva() {
            $anio = $('#selectAnio').val();

            $.get("<?php echo $this->url->get('sales-reports/selectgrafica')?>", {'anio': $anio}, function ($data) {
                var otro_anio = $data;
               // var otro_anio=$data;
                var EsteAnio = $anio;
                var graf = '<header><h2>Ventas en ' + EsteAnio + ': <img src="/../img/linea_azul_graficas.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                        'Clientes en ' + EsteAnio + ': <img src="/../img/linea_roja_graficas.png"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' +
                        'Miles de pesos vendidos en ' + EsteAnio + ': <img src="/../img/linea_verde_graficas.png"></h2></header>' +
                        '<div>' +
                        '<div class="widget-body">' +
                        '<canvas id="ventasAnio" height="120"></canvas>' +
                        '</div>' +
                        '</div>';
                $('#contenido').html(graf);

                //GRÁFICA DE VENTAS Y CUSTOMERS POR AÑO
                pageSetUp();

                var lineOptions = {
                    ///Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines: true,
                    //String - Colour of the grid lines
                    scaleGridLineColor: "rgba(0,0,0,0.12)",
                    //Number - Width of the grid lines
                    scaleGridLineWidth: 1,
                    //Boolean - Whether the line is curved between points
                    bezierCurve: true,
                    //Number - Tension of the bezier curve between points
                    bezierCurveTension: 0.4,
                    //Boolean - Whether to show a dot for each point
                    pointDot: true,
                    //Number - Radius of each point dot in pixels
                    pointDotRadius: 3,
                    //Number - Pixel width of point dot stroke
                    pointDotStrokeWidth: 1,
                    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                    pointHitDetectionRadius: 10,
                    //Boolean - Whether to show a stroke for datasets
                    datasetStroke: true,
                    //Number - Pixel width of dataset stroke
                    datasetStrokeWidth: 2,
                    //Boolean - Whether to fill the dataset with a colour
                    datasetFill: true,
                    //Boolean - Re-draw chart on page resize
                    responsive: true,
                    //String - A legend template
                    legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){ %><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){ %><%=datasets[i].label%><%}%></li><%}%></ul>"
                };

                var lineData = {
                    labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
                    datasets: [

                        {
                            label: "My First dataset",
                            fillColor: "rgba(255,0,0,0.1)",
                            strokeColor: "rgba(255,0,0,0.6)",
                            pointColor: "rgba(255,0,0,0.3)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(255,0,0,0.5)",
                            data: otro_anio.customers_por_anio
                        },
                        {
                            label: "My Second dataset",
                            fillColor: "rgba(0, 0, 255, 0.1)",
                            strokeColor: "rgba(0, 0, 255, 0.6)",
                            pointColor: "rgba(0, 0, 255, 0.3)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(0, 0, 255,0.5)",
                            data: otro_anio.ventas_por_anio
                        },
                        {
                            label: "My third dataset",
                            fillColor: "rgba(0,128,0,0.1)",
                            strokeColor: "rgba(0,128,0,0.7)",
                            pointColor: "rgba(0,128,0,3)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(0,128,0,0.5)",
                            data: otro_anio.dineros_por_anio
                        }
                    ]
                };

                // render chart
                var ctx = document.getElementById("ventasAnio").getContext("2d");
                var myNewChart = new Chart(ctx).Line(lineData, lineOptions);
            });
        }

        <!-- Validación -->
        $('#graf').on('click', function() {
            $partnerid = $('#partnerid').val();
            var mensaje = 'Debe seleccionar un partner';
            if($partnerid == 0)
            {
                alert(mensaje);
                return false;
            }
        })
    })

</script>

<!-- Your GOOGLE ANALYTICS CODE Below -->
<script type="text/javascript">
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script');
        ga.type = 'text/javascript';
        ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(ga, s);
    })();

</script>