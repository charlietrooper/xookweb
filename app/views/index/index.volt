
{{ content() }}
<div class="row" style="display: block; padding-bottom: 25%">
    <div class="col-md-12" align="center" style="vertical-align: middle">
        {{ image("img/home_libros.jpg", "alt": "Orbile logo", "class" : "imghome img-responsive"  ) }}


        <img style="padding-top: 20%" class="logohome img-responsive" src="../img/LogoOrbile_home.png">

        <img style="padding-top: 10%" class="logohome img-responsive" src="../img/Enamorate_home.png">



    </div>
</div>

<div class="row" align="center">
    <div class="col-md-12">
        <span><a href="\session/index" class="entrar"><img src="../img/entrar_home.png"></a></span>
    </div>
</div>

{#-------Script para eliminar la imagen de pie de página, pues en este caso contiene la imagen como fondo -------#}
<script type="text/javascript">
    window.onload = function() {
        $('#imagen_ocultar').hide();
    }
</script>
