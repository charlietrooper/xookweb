{{ content() }}

  {% if fecha1==null %}
      <p> Ventas totales </p>
  {% else %}
      <p>De: {{ fecha1 }}  &nbsp;a {{ fecha2 }}</p>
  {% endif %}


<div style="padding:10px;">
    <td colspan="4"><a href="/info-ventas/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a></td>
</div>

<div width="100%" style="display: block; overflow: auto; margin-top:20px;">
    <table class="table table-bordered table-striped" align="center" id="tablita">
        <tr>
            <th>Título</th>
            <th>ISBN</th>
            <th>Ventas</th>
            <th>Idioma</th>
        </tr>
        {% for i in ventasInfo %}

            <tr>
                <td>
                    {% if i['titulo']==null %}
                        {{ "No Disponible" }}
                    {% else %}
                        {{ link_to("library/getbookrev/" ~ i['product_id'], i['titulo'],'target': '_blank') }}
                    {% endif %}
                </td>
                <td>
                    {% if i['isbn']==null %}
                       {{ "No Disponible" }}
                    {% else %}
                        {{ i['isbn'] }}
                    {% endif %}
                </td>
                <td>
                    {% if i['veces_vendido']==null %}
                       {{ i['veces_vendido'] }}
                    {% else %}
                        {{ i['veces_vendido'] }}
                    {% endif %}
                </td>
                <td>
                    {% if i['idioma']==null %}
                        {{ "No Disponible" }}
                    {% else %}
                        {{ i['idioma'] }}
                    {% endif %}
                </td>
            </tr>


        {% endfor %}


    </table>
</div>