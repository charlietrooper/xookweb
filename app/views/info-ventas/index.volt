{{ form ('info-ventas/buscaventas', 'method':'post') }}
<fieldset>
    <div width="100%" style="display: block; overflow: auto;">
        <table align="center" class="detalle table-bordered table-striped">
            <tr><th colspan="4  " style="text-align: center">Busqueda de IBSN:</th></tr>
            <tr>
                <td><label>Ingresar ISBN:</label><br>
                    <div class="emergente">
                        {# <div align="center" class="ocultar"><label>?</label></div>#}
                          <div>(Separados por comas, sin espacios)</div>
                    </div>
                </td>
                <td>
                    {#{{ text_field('isbn', 'id':'isbn') }}#}
                    {{ text_area('isbn', 'cols': 30, 'rows': 3, 'id':'isbn', 'style':'margin-top:5px;') }}
                </td>
                <td><label>Fecha:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de:&nbsp;
                    {{date_field('fecha1', 'id':'fecha_ini')}}
                    </td>
                <td>a:&nbsp;{{date_field('fecha2', 'id':'fecha_fin')}}</td>

            </tr>
        </table>
        <table align="center">
            <tr>
                <td colspan="3" style="text-align: right">
                    <ul class="pager">
                        <li class="pull-right">
                            {{ submit_button("Obtener reporte", "class": "btn btn-primary", "id":"ok") }}
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
</fieldset>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $('#ok').on('click', function(){
            $isbn = $('#isbn').val();
            $fecha_ini = $('#fecha_ini').val();
            $fecha_fin =$('#fecha_fin').val();

            if($isbn.trim() == '')
            {
                event.preventDefault();
                alert("Requiere ingresar al menos un ISBN");
            }
            
            if($fecha_ini.trim()=='' && $fecha_fin.trim()!=='')
            {
                alert('-Debe seleccionar una fecha de inicio \n');
            }

            if($fecha_ini.trim()!=='' && $fecha_fin.trim()=='')
            {
                alert('-Debe seleccionar una fecha final \n');
            }
        });

    });

</script>

