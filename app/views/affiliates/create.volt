{{ content() }}
{{ link_to("affiliates", "class": "btn btn-primary","&larr; Regresar") }}
{{ form("affiliates/register") }}
   <fieldset>
       <div width="100%" style="display: block; overflow: auto;">
           <table align="center" class="detalle table-bordered table-striped">
               <tr>
                   <th><label for="name">Nombre:</label></th>
                   <td>{{ text_field("name", "id":"name") }}</td>
               </tr>
               <tr>
                   <th><label for="email">Correo:</label></th>
                   <td>{{ text_field("email", "id":"email") }}</td>
               </tr>
               <tr>
                   <th><label for="big_logo">Logotipo grande:</label></th>
                   <td>{{ text_field("big_logo", "id":"big") }}</td>
               </tr>
               <tr>
                   <th><label for="small_logo">Logotipo pequeño:</label></th>
                   <td>{{ text_field("small_logo", "id":"small") }}</td>
               </tr>
               <tr>
                   <th><label for="domain_url">Dominio url:</label></th>
                   <td>{{ text_field("domain_url", "id":"url") }}</td>
               </tr>
               <tr>
                   <th><label for="layout">Diseño:</label></th>
                   <td>{{ text_field("layout", "id":"layout") }}</td>
               </tr>
               <tr>
                   <th><label for="configuration">Configuración:&nbsp;</label></th>
                   <td>{{ text_field("configuration", "id":"config") }}</td>
               </tr>
           </table>
       </div>
   </fieldset>
<ul class="pager">
    <li class="pull-center">
        {{ submit_button("Guardar", "class": "btn btn-primary", "id":"ok") }}
    </li>
</ul>
</form>

{#---------------------CODIGO JAVASCRIPT---------------------------------------#}
<script type="text/javascript">
    $(document).ready(function(){

        $('#ok').on('click', function(){

            $correo = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+/;
            var mensaje = [];
            var mens = [];

            mensaje.push('Los siguientes campos son incorrectos o estan vacios:\n');
            mens.push('Los siguientes campos son incorrectos o estan vacios:\n');

            $name = $('#name').val();
            $email = $('#email').val();
            $big = $('#big').val();
            $small = $('#small').val();
            $url = $('#url').val();
            $layout = $('#layout').val();
            $config = $('#layout').val();

            if($name.trim()=='')
            {
                mensaje.push('-El campo NOMBRE esta vacio \n');
            }
            if($email.trim()=='')
            {
                mensaje.push('-El campo CORREO esta vacio \n');
            }
            if(!($correo.test($email)))
            {
                mensaje.push('El CORREO es invalido \n');
            }
            if($big.trim()=='')
            {
                mensaje.push('-El campo BIG LOGO esta vacio \n');
            }
            if($small.trim()=='')
            {
                mensaje.push('-El campo SMALL LOGO esta vacio \n');
            }
            if($url.trim()=='')
            {
                mensaje.push('-El campo DOMAIN URL esta vacio\n');
            }
            if($layout.trim()=='')
            {
                mensaje.push('-El campo LAYOUT esta vacio\n');
            }
            if($config.trim()=='')
            {
                mensaje.push('-El campo CONFIGURATION esta vacio \n');
            }
            if(mensaje.length != mens.length)
            {
                event.preventDefault();
                alert(mensaje);
            }

        })

    });
</script>