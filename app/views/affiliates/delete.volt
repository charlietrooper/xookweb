{{ content() }}
{{ link_to("affiliates", "class": "btn btn-primary","&larr; Regresar") }}

{{ form("affiliates/eliminar") }}
<h3 align="center">¿Estas seguro que deseas eliminar estos datos?</h3>
<br>
<br>
<fieldset>
       {% for b in a %}
    <div width="100%" style="display: block; overflow: auto;" >
        <table align="center" class="detalle table-bordered table-striped">
            <tr>
                <th><label>ID:</label></th>
                <td>{{ text_field('id', 'value':b.affiliate_id, 'readonly':'true') }}</td>
            </tr>
            <tr>
                <th><label>Nombre:</label></th>
                <td>{{ setDefault('name', b.name) }}
                    {{ text_field('name', 'readonly':'true') }}</td>
            </tr>
            <tr>
                <th><label>Correo:</label></th>
                <td>{{ setDefault('email', b.name) }}
                    {{ text_field('email', 'readonly':'true') }}</td>
            </tr>
            <tr>
                <th><label>Logotipo grande:</label></th>
                <td>{{ setDefault('big_logo', b.big_logo) }}
                    {{ text_field('big_logo', 'readonly':'true') }}</td>
            </tr>
            <tr>
                <th><label>Logotipo pequeño:</label></th>
                <td>{{ setDefault('small_logo', b.small_logo) }}
                    {{ text_field('small_logo', 'readonly':'true') }}</td>
            </tr>
            <tr>
                <th><label>Dominio url:</label></th>
                <td>{{ setDefault('domain_url', b.domain_url) }}
                    {{ text_field('domain_url', 'readonly':'true') }}</td>
            </tr>
            <tr>
                <th><label>Diseño:</label></th>
                <td>{{ setDefault('layout', b.layout) }}
                    {{ text_field('layout', 'readonly':'true') }}</td>
            </tr>
            <tr>
                <th><label>Configuración:&nbsp;</label></th>
                <td>{{ setDefault('configuration', b.configuration) }}
                    {{ text_field('configuration', 'readonly':'true') }}</td>
            </tr>
        </table>
    </div>
       {% endfor %}
    <div style="text-align: center;">
        <ul class="pager">
            <li>
                {{ submit_button("Eliminar", "class": "btn btn-primary") }}
            </li>
        </ul>
    </div>
</fieldset>
</form>