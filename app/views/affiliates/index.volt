{{ content() }}
<div class="titulo" >
    <h1 class="retroshadow">Afiliados</h1>
</div>
<div align="right" style="margin-bottom: 15px;">
    {{ link_to("affiliates/create", "CREAR AFILIADO", "class":"btn btn-primary") }}
</div>
<div width="100%" style="display: block; overflow: auto;">
<table class="table table-bordered table-striped" align="center">
    <tr>
        <th>AFILIADO ID</th>
        <th>NOMBRE</th>
        <th>EMAIL</th>
        <th>LOGOTIPO GRANDE</th>
        <th>LOGOTIPO PEQUEÑO</th>
        <th>DOMINIO URL</th>
        <th>DISEÑO</th>
        <th>CONFIGURACION</th>
        <th></th>
        <th></th>
    </tr>
    {% for a in page.items %}
    <tr>
        <td>{{ a.affiliate_id }}</td>
        <td>{{ a.name }}</td>
        <td>{{ a.email }}</td>
        <td
            <div class="emergente">
                <div class="ocultar"><?php echo substr($a->big_logo,0,5).'...'; ?></div>
                <div class="mostrar mostrarAPI">{{ a.big_logo }}</div>
            </div>
        </td>
        <td>
            <div class="emergente">
                <div class="ocultar"><?php echo substr($a->small_logo,0,5).'...'; ?></div>
                <div class="mostrar mostrarAPI">{{ a.small_logo }}</div>
            </div>
        </td>
        <td>{{ a.domain_url }}</td>
        <td>{{ a.layout }}</td>
        <td>{{ a.configuration }}</td>
        <td width="7%">{{ link_to("affiliates/edit/" ~ a.affiliate_id, '<i class="glyphicon glyphicon-edit"></i> EDITAR', "class": "btn btn-default") }}</td>
        <td width="7%">{{ link_to("affiliates/delete/" ~ a.affiliate_id, '<i class="glyphicon glyphicon-remove"></i> ELIMINAR', "class": "btn btn-default") }}</td>
    </tr>
    {% endfor %}

    <tr>
        <td colspan="10" align="left">
            <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
           <div class="btn-group">
                {{ link_to("affiliates/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                {{ link_to("affiliates/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                {{ link_to("affiliates/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                {{ link_to("affiliates/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
           </div>
        </td>
    </tr>
</table>
</div>