{{ content() }}
{{ link_to("affiliates", "class": "btn btn-primary", "&larr; Regresar") }}

{{ form("affiliates/update") }}
    <fieldset>
        {% for a in affi %}
        <div width="100%" style="display: block; overflow: auto;">
            <table align="center" class="detalle table-bordered table-striped">
                <tr>
                    <th><label>ID:</label></th>
                    <td>{{ text_field('id', 'value':a.affiliate_id, 'readonly':'true') }}</td>
                </tr>
                <tr>
                    <th><label>Nombre:</label></th>
                    <td>{{ setDefault('name', a.name) }}
                        {{ text_field('name') }}</td>
                </tr>
                <tr>
                    <th><label>Correo:</label></th>
                    <td>{{ setDefault('email', a.email) }}
                        {{ text_field('email') }}</td>
                </tr>
                <tr>
                    <th><label>Logotipo grande:</label></th>
                    <td>{{ setDefault('big', a.big_logo) }}
                        {{ text_field('big') }}</td>
                </tr>
                <tr>
                    <th><label>Logotipo pequeño:</label></th>
                    <td>{{ setDefault('small', a.small_logo) }}
                        {{ text_field('small') }}</td>
                </tr>
                <tr>
                    <th><label>Dominio url:</label></th>
                    <td>{{ setDefault('url', a.domain_url) }}
                        {{ text_field('url') }}</td>
                </tr>
                <tr>
                    <th><label>Diseño:</label></th>
                    <td>{{ setDefault('layout', a.layout) }}
                        {{ text_field('layout') }}</td>
                </tr>
                <tr>
                    <th><label>Configuración:&nbsp;&nbsp;</label></th>
                    <td>{{ setDefault('config', a.configuration) }}
                        {{ text_field('config') }}</td>
                </tr>
            </table>
        </div>
        {% endfor %}
    </fieldset>
<ul class="pager">
    <li class="pull-center">
        {{ submit_button("Update", "class":"btn btn-primary") }}
    </li>
</ul>
</form>