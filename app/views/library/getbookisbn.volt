{#----COMENTARIO PARA QUE LO PUEDA DESCARGAR MIKEASESINO----#}
{{ content() }}
<ul>
    <li class="previous pull-left">
        {{ link_to("library/getlibrary","&larr; Go Back") }}
    </li>
</ul>
<fieldset>
    <div class="row">

        <div class="col-md-12">
            <h2>TITULO: {{ book.activeRevision.title }}</h2>
            <hr>
        </div>
        <div class="col-md-4">
            <img style="margin: 20px; margin-left: 5px;" src="{{  book.activeRevision.links.coverImage }}">
            <h4>IMPRENTA: </h4>{{ book.activeRevision.imprint }}
        </div>
        <div class="col-md-8" style="text-align: justify;">
            <h2 align="center">DESCRIPCIÓN:</h2> {{ book.activeRevision.description }}
        </div>
    </div>

</fieldset>