{{ content() }}
<?php //var_dump($test); ?>
<?php //var_dump(date("Y-m-d"));?>
<script type="text/javascript" xmlns="https://www.w3.org/1999/html">
    window.onload = function () {
        var atras = document.getElementById('irAtras');
        atras.addEventListener("click", irAtras);
        function irAtras() { this.style.color = 'red'; window.history.back();}
    }
</script>
<div class="row" xmlns="https://www.w3.org/1999/html">
    <div class="col-md-12">
        <p style="text-align: left; padding-top: 10px;">{{ link_to("biblioteca", "class":"btn btn-primary", "&larr; Regresar", "id":"irAtras") }}</p>
    </div>
</div>
{# DIV PARA MANTENER EL CONTENIDO O EL RESULTADO DEL JSON #}
<div style="display: none" id="mostrar">
    {{ rest }}
</div>
{{ submit_button("ver json", "id":"show", "class": "btn btn-primary") }}
{{ submit_button("ocultar json", "id":"hidden", "class": "btn btn-primary", "style":"display: none") }}
{# TERMINA DIV PARA MANTENER EL CONTENIDO O EL RESULTADO DEL JSON #}

{#Seccion para ver detalles de consultas de top mas vendidos
<table class="table table-bordered">
    <tr>
        <td>{{ book['activeRevision']['title'] }}</td>
        <td>{{ book['isbn'] }}</td>
        <td>{{ book['activeRevision']['contibutors']['contributor']['@value'] }}</td>
    </tr>
</table>
#}

{#El siguiente código php es para desplegar correctamente si hay más de un autor#}
<?php
 //var_dump( $book['activeRevision']['contibutors']['contributor']['@value']);
 $prueba=$book['activeRevision']['contibutors']['contributor']['@value'];
$autorCount=count($book['activeRevision']['contibutors']['contributor']);
    if(empty($prueba))
    {
        for ($i=0;$i<$autorCount;$i++)
        {
            $aut=$book['activeRevision']['contibutors']['contributor'][$i]['@value'];
            if($i==0)
            {
                $autor.=$aut;
            }
            else
            {
              $autor.=', '. $aut;
            }

        }

    }
    else{
        $autor=$book['activeRevision']['contibutors']['contributor']['@value'];
        }
?>
{# Div con clase renglon #}
<div class="row" style="margin-top: 20px">
    <div class="col-md-8">

        {#Div que contiene la imagen del libro y la imprenta#}
        <div class="col-md-4">
            <img style="margin: 20px; margin-left: 0px;" src="{{  book['activeRevision']['links']['coverImage'] }}">
        </div>

        {#Div que contiene la descripción#}
        <div class="col-md-8" style="text-align: justify;">
            <h3 class="detalleTitulo"> {{ book['activeRevision']['title'] }}</h3>
            <p>Por: <span class="detalle"> {{ autor }}</span> </p>
            <p>Número de ventas: <span class="detalle"> {{ veces_vendido }}</span> | Precio actual:<span class="detalle"> ${{  pActual }} </span></p>
            <span style=" font-size: 93%; color:#111111;"><i> {{ book['activeRevision']['description'] }}</i></span>
        </div>
        <div class="col-md-12" align="left">
            <div hidden="hidden">
                <table>
                    <tr>
                        <td>
                            <textarea id="input">{{ resultado }}</textarea>
                        </td>
                        <td>
                            <div style="padding-left: 5px; padding-top: 10px;">
                                Show Array Indices : <input id="opt_show_array_index" type="checkbox" checked> <br />
                                <div style="margin: 10px 0px; font-weight: bold">Hyperlinks</div><hr>
                                <table>
                                    <tr>
                                        <td>Enable Hyperlinks</td>
                                        <td><input id="opt_enable_hyperlinks" type="checkbox" checked></td>
                                    </tr>
                                    <tr>
                                        <td> Keys </td>
                                        <td><input id="opt_hyper_keys" type="text" placeholder="Ex: url, main" value="url, contributors"></td>
                                    </tr>
                                    <tr>
                                        <td> Hyperlink Target </td>
                                        <td><input id="opt_hyper_target" type="text" placeholder="Ex: _blank" value="_blank"></td>
                                    </tr>
                                </table>
                                <div  style="margin: 10px 0px; font-weight: bold">Boolean Options</div><hr>
                                <table>
                                    <tr>
                                        <td>Show Text ? </td>
                                        <td><input id="opt_bool_show_text" type="checkbox" checked></td>
                                    </tr>
                                    <tr>
                                        <td> Label for "true" </td>
                                        <td><input id="opt_bool_text_true" type="text" placeholder="Ex: Yes" value="Yes"></td>
                                    </tr>
                                    <tr>
                                        <td> Label for "false" </td>
                                        <td><input id="opt_bool_text_false" type="text" placeholder="Ex: No" value="No"></td>
                                    </tr>
                                    <tr>
                                        <td>Show Image ? </td>
                                        <td><input id="opt_bool_show_img" type="checkbox" checked></td>
                                    </tr>
                                    <tr>
                                        <td> Url for "true" </td>
                                        <td><input id="opt_bool_img_true" type="text" placeholder="Ex: css/true.png" value="css/true.png"></td>
                                    </tr>
                                    <tr>
                                        <td> Url for "false" </td>
                                        <td><input id="opt_bool_img_false" type="text" placeholder="Ex: css/false.png" value="css/false.png"></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <button id="convert" class="btn btn-primary">Ver detalles</button>
            <div id="output"></div>

            <textarea id="output-raw" hidden="hidden" style="width: 100%; height: 20em;"></textarea>
        </div>
    </div>
    <div class="col-md-4" style="margin-top: 20px">
        {#Div para consultar mas libros#}
        <div>
            {{ form ('libros/enviarlibro', 'method':'get') }}
            <div width="100%" style="display: block; overflow: auto;">
                <table align="center" class="detalle table-bordered table-striped">
                    <tr><th colspan="2" style="text-align: center">Ingresa el IBSN o el active revision<br>ID para buscar otro libro:</th></tr>
                    <tr>
                        <th><label for="items_per_page">ISBN:</label></th>
                        <td>{{ text_field("isbn", "size":"11", "id":"isbn") }}</td>
                    </tr>
                    <tr>
                        <th><label for="items_per_page">Active revision ID:</label></th>
                        <td>{{ text_field("id_libro", "size":"11", "id":"id_libro") }}</td>
                    </tr>
                </table>
                <table align="center">
                    <tr>
                        <td colspan="3" style="text-align: right">
                            <ul class="pager">
                                <li class="pull-right">
                                    {{ submit_button("Ver detalles", "class": "btn btn-primary", "id":"ok") }}
                                </li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div>
            </form>
        </div>
    </div>
</div>
{#------------------------------INICIA JAVASCRIPT----------------------------#}
<script type="text/javascript">

    $(document).ready(function(){
        $('#ok').on('click', function(){
            $isbn = $('#isbn').val();
            $id_libro = $('#id_libro').val();

            if($isbn.trim() == '' && $id_libro.trim() == '')
            {
                event.preventDefault();
                alert("Requiere ingresar valores en uno de los dos campos: \n 'ISBN' ó el 'ID del libro'");
            }
            if($isbn.trim() != '' && $id_libro.trim() != '')
            {
                event.preventDefault();
                alert("Solo debe de ingresar valores en uno de los dos campos, no en ambos");
            }
        });

        //PARTE PARA OCULTAR O MOSTRAR EL JSON
        $('#show').click(function(){

            $('#mostrar').show();
            $('#hidden').show();
            $('#show').hide();

        });

        $('#hidden').click(function(){
            $('#mostrar').hide();
            $('#hidden').hide();
            $('#show').show();

        });

        window.options = {
            showArrayIndex: false,
            hyperlinks : {
                enable : true,
                keys: [],
                target : '_blank'
            },

            bool : {
                showText : true,
                text : {
                    true : "true",
                    false : "false"
                },
                showImage : true,
                img : {
                    true : 'css/true.png',
                    false : 'css/false.png'
                }
            }
        };

        /*global document, JSON, window, alert*/
        (function (JsonHuman, crel, CodeMirror) {
            "use strict";



            var textarea = document.getElementById("input"),
                    output = document.getElementById("output"),
                    raw = document.getElementById("output-raw"),
                    button = document.getElementById("convert"),
                    editor = CodeMirror.fromTextArea(textarea, {
                        mode: "application/json",
                        json: true
                    });

            function convert(input, output) {
                var i;
                var aOptions = window.options.hyperlinks;
                var boolOptions = window.options.bool;

                // Parse options
                window.options.showArrayIndex = document.getElementById('opt_show_array_index').checked;

                aOptions.enable = document.getElementById('opt_enable_hyperlinks').checked;
                aOptions.keys = document.getElementById('opt_hyper_keys').value.split(',');
                for(i =0; i < aOptions.keys.length; i++){
                    aOptions.keys[i] = aOptions.keys[i].trim();
                }
                aOptions.target = document.getElementById('opt_hyper_target').value;

                boolOptions.showText = document.getElementById('opt_bool_show_text').checked;
                boolOptions.showImage = document.getElementById('opt_bool_show_img').checked;
                boolOptions.text.true = document.getElementById('opt_bool_text_true').value;
                boolOptions.text.false = document.getElementById('opt_bool_text_false').value;
                boolOptions.img.true = document.getElementById('opt_bool_img_true').value;
                boolOptions.img.false = document.getElementById('opt_bool_img_false').value;

                var node = JsonHuman.format(input, window.options);
                output.innerHTML = "";
                output.appendChild(node);
                raw.textContent = output.innerHTML;
            }

            function doConvert() {
                var json;
                try {
                    json = JSON.parse(editor.getValue());
                } catch (error) {
                    alert("Error parsing json:\n" + error.stack);
                    return;
                }

                convert(json, output);
            }

            $('#convert').on('click', function(){
                doConvert();
            })
            //button.addEventListener("click", doConvert);

        }(window.JsonHuman, window.crel, window.CodeMirror));
    });


</script>