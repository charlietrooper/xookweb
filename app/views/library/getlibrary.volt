{{ content() }}
{# DIV PARA MANTENER EL CONTENIDO O EL RESULTADO DEL JSON #}
<div style="display: none" id="mostrar">
    {{ rest }}
</div>
<p style="padding-bottom: 10px; padding-top: 10px;">
    {{ submit_button("ver json", "id":"show", "class": "btn btn-primary") }}
    {{ submit_button("ocultar json", "id":"hidden", "class": "btn btn-primary", "style":"display: none") }}
</p>
{# TERMINA DIV PARA MANTENER EL CONTENIDO O EL RESULTADO DEL JSON #}
<table class="table table-bordered table-striped" align="center">
    <tr>
        <th style='text-align:center; background-color:#080808; color:#ffffff;'>Título</th>
        <th style='text-align:center; background-color:#080808; color:#ffffff;'>Tipo de contenido</th>
        <th style='text-align:center; background-color:#080808; color:#ffffff;'>Editorial</th>
        <th style='text-align:center; background-color:#080808; color:#ffffff;'>ISBN</th>
        <th style='text-align:center; background-color:#080808; color:#ffffff;'>Active revision ID</th>
        <th style='text-align:center; background-color:#080808; color:#ffffff;'>Fecha de compra</th>
    </tr>
    {% for obj in page.items %}
        <tr>
            <td>{{ obj['Title'] }}</td>
            <td>{{ obj['ContentType'] }}</td>
            <td>{{ obj['Imprint'] }}</td>
            <td>{{ link_to("libros/searchbookisbn/" ~ obj['Isbn'], obj['Isbn']) }}</td>
            <td>{{ link_to("library/getbookrev/" ~ obj['ProductId'], obj['ProductId']) }}</td>
            <td>{{ obj['PurchaseDate'] }}</td>
            <td width="7%">{{ link_to("library/deletebook/" ~ obj['ProductId'], '<i class="glyphicon glyphicon-remove"></i> ELIMINAR', "class": "btn btn-default") }}</td>
            <td width="7%">{{ link_to("library/downloadbook/" ~ obj['ProductId'], '<i class="glyphicon glyphicon-download"></i> DESCARGAR', "class": "btn btn-default") }}</td>
        </tr>
    {% endfor %}
    <tr>
        <td colspan="8" align="left">
            <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
            <div class="btn-group">
                {{ link_to("library/setlibrary?user_id=" ~ user_id, '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                {{ link_to("library/setlibrary?page=" ~ page.before ~ "&user_id=" ~ user_id, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                {{ link_to("library/setlibrary?page=" ~ page.next ~ "&user_id=" ~ user_id, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                {{ link_to("library/setlibrary?page=" ~ page.last ~ "&user_id=" ~ user_id, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
            </div>
        </td>
    </tr>
</table>

{#------------------------------INICIA JAVASCRIPT----------------------------#}
<script type="text/javascript">

    $(document).ready(function(){
        $('#ok').on('click', function(){
            $isbn = $('#isbn').val();
            $id_libro = $('#id_libro').val();

            if($isbn.trim() == '' && $id_libro.trim() == '')
            {
                event.preventDefault();
                alert("Requiere ingresar valores en uno de los dos campos: \n 'ISBN' ó el 'ID del libro'");
            }
            if($isbn.trim() != '' && $id_libro.trim() != '')
            {
                event.preventDefault();
                alert("Solo debe de ingresar valores en uno de los dos campos, no en ambos");
            }
        });

        //PARTE PARA OCULTAR O MOSTRAR EL JSON
        $('#show').click(function(){

            $('#mostrar').show();
            $('#hidden').show();
            $('#show').hide();

        });

        $('#hidden').click(function(){
            $('#mostrar').hide();
            $('#hidden').hide();
            $('#show').show();

        });
    });
</script>