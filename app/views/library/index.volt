{{ content() }}


{{ form ('library/setlibrary', 'method':'get') }}
    <fieldset>
        <div width="100%" style="display: block; overflow: auto;">
            <table align="center" class="detalle table-bordered table-striped">
                <tr>
                    <th><label for="items_per_page">ID kobo:</label></th>
                    <td>{{ text_field("user_id", "value":kobo, "id":"user_id") }}</td>
                </tr>
                <tr>
                    <th><label for="items_per_page">Elementos por página:</label></th>
                    <td>{{ text_field("items_per_page", "id":"items", "value": 10) }}</td>
                </tr>
                <tr>
                    <th><label for="page">Página:</label></th>
                    <td>{{ text_field("page", "id":"page", "value":1) }}</td>
                </tr>
            </table>
            <table align="center">
                <tr>
                    <td colspan="3" style="text-align: right">
                        <ul class="pager">
                            <li class="pull-right">
                                {{ submit_button("Obtener biblioteca", "class": "btn btn-primary", "id":"ok") }}
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </fieldset>
</form>
{#------------------------------INICIA JAVASCRIPT--------------------------------------------------#}
<script type="text/javascript">

    $(document).ready(function(){
        $('#ok').on('click', function(){
            $kobo = $('#kobo').val();
            $items = $('#items').val();
            $page = $('#page').val();

            if($kobo.trim() == '' || $items.trim() == '' || $page.trim() == '')
            {
                event.preventDefault();
                alert('Ninguno de los campos debe de estar vacio');
            }
        });
    });
</script>