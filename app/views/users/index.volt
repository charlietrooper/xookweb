{{ content() }}
<div class="titulo" >
    <h1 class="retroshadow">Usuarios</h1>'
</div>
<div align="right" style="padding-bottom: 20px;">
  {{ link_to("users/create", "CREAR USUARIO", "class":"btn btn-primary") }}
</div>
<div width="100%" style="display: block; overflow: auto;">
<table  class="table table-bordered table-striped" align="center">
    <tr>
        <th>ID de usuario</th>
        <th>Nombre de usuario</th>
        <th>Contraseña</th>
        <th>Nombre</th>
        <th>Email</th>
        <th>Fecha de creación</th>
        <th>ID de rol</th>
        <th>ID de Socio</th>
        <th>Activo</th>
        <th></th>
        <th></th>
    </tr>
    {% for u in page.items %}
    <tr style="height: 30px;">
        <td>{{ u.user_id }}</td>
        <td>{{ u.username }}</td>
        <td>
            <div class="emergente">
                <div class="ocultar"><?php echo substr($u->pass,0,5).'...'; ?></div>
                <div class="mostrar mostrarAPI">{{ u.pass }}</div>
            </div>
        </td>
        <td>{{ u.name }}</td>
        <td>{{ u.email }}</td>
        <td>{{ u.created_at }}</td>
        <td>{{ u.role_id }}</td>
        {% if u.partner_id == 0 %}
            <td>{{ u.partner_id }}</td>
        {% else %}
        <td>{{ link_to("partners/details/" ~ u.partner_id, u.partner_id) }}</td>
        {% endif %}
        <td>{{ u.active }}</td>
        <td width="7%">{{ link_to("users/edit/" ~ u.user_id, '<i class="glyphicon glyphicon-edit"></i> EDITAR', "class": "btn btn-default") }}</td>
        <td width="7%">{{ link_to("users/delete/" ~ u.user_id, '<i class="glyphicon glyphicon-remove"></i> ELIMINAR', "class": "btn btn-default") }}</td>
    </tr>
    {% endfor %}
    <tr>
        <td colspan="11" align="right">
            <div class="btn-group">
            {{ link_to("users/index", '<i class="icon-fast-backward"></i> Primera', "class":"btn") }}
            {{ link_to("users/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class":"btn") }}
            {{ link_to("users/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class":"btn") }}
            {{ link_to("users/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Ultima', "class":"btn") }}
            <span class="help-inline">{{ page.current}}/{{ page.total_pages }}</span>
            </div>
        </td>
    </tr>
</table>
</div>

