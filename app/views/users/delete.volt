{{ content() }}
<p style="text-align: left; padding: 10px;">{{ link_to("users", "class":"btn btn-primary","&larr; Regresar") }}</p>

{{ form("users/eliminar") }}
<h3 align="center">¿Está seguro que desea eliminar estos datos?</h3>
<br>
<br>
<fieldset>
    <div width="100%" style="display: block; overflow: auto;">
        <table align="center" class="detalle table-striped">
            {% for a in u %}
            <tr>
                <td>
                    <label>ID:</label>
                </td>
                <td>
                    {{ text_field('user_id', 'value':a.user_id, 'readonly':'true') }}
                </td>
            </tr>
                <tr>
                    <td>
                        <label>Nombre de usuario:</label>
                    </td>
                    <td>
                        {{ text_field('username', 'value':a.username, 'readonly':'true') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Contraseña:</label>
                    </td>
                    <td>
                        {{ password_field('pass', 'value':a.pass, 'readonly':'true') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Nombre:</label>
                    </td>
                    <td>
                        {{ text_field('name', 'value':a.name, 'readonly':'true') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Correo:</label>
                    </td>
                    <td>
                        {{ text_field('email', 'value':a.email, 'readonly':'true') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Creado en:</label>:</label>
                    </td>
                    <td>
                        {{ text_field('create', 'value':a.created_at, 'readonly':'true') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Activos:</label>
                    </td>
                    <td>
                        {{ text_field('active', 'value':a.active, 'readonly':'true') }}
                    </td>
                </tr>
            {% endfor %}
        </table>
    </div>
</fieldset>
<ul class="pager">
    <li class="pull-center">
        {{ submit_button("Eliminar", "class": "btn btn-primary") }}
    </li>
</ul>
{{ end_form() }}