{{ content() }}
<p style="text-align: left; padding: 10px;">{{ link_to("users", "class":"btn btn-primary","&larr; Regresar") }}</p>

{{ form('users/update') }}
<fieldset>
    {% for u in user %}
    <div width="100%" style="display: block; overflow: auto;">
        <table align="center" class="detalle table-striped">
            <tr>
                <th><label>ID:</label></th>
                <td>{{ text_field('id', 'value':u.user_id, 'readonly':'true') }}</td>
            </tr>
            <tr>
                <th><label>Nombre de usuario:</label></th>
                <td>{{ setDefault('username', u.username) }}
                    {{ text_field('username') }}</td>
            </tr>
            <tr>
                <th><label>Contraseña:&nbsp;</label></th>
                <td>{{ setDefault('pass', u.pass) }}
                    {{ password_field('pass', 'id':'pass') }}</td>
            </tr>
            <tr>
                <td>
                    <label>Confirmar contraseña:</label>
                </td>
                <td>
                    {{ setDefault('confirmpass', u.pass) }}
                    {{ password_field('confirmpass', 'id':'confirm-pass') }}
                </td>
            </tr>
            <tr>
                <th><label>Nombre:</label></th>
                <td>{{ setDefault('name', u.name) }}
                    {{ text_field('name') }}</td>
            </tr>
            <tr>
                <th><label>Correo:</label></th>
                <td>{{ setDefault('email', u.email) }}
                    {{ text_field('email') }}</td>
            </tr>
            <tr>
                <th><label>Rol:</label></th>
                <td><select name='role'>
                        <option value='0'>Por favor, elija uno...</option>
                        {% for r in rol %}
                            {% if u.role_id == r.role_id %}
                                <option value={{ r.role_id }} selected>{{ r.role_name }}</option>
                            {% else %}
                                <option value={{ r.role_id }}>{{ r.role_name }}</option>
                            {% endif %}
                        {% endfor %}
                    </select></td>
            </tr>
            <tr>
                <th><label>Socio:</label></th>
                <td><select name='partner'>
                        <option value='0'>Por favor, elija uno...</option>
                        {% for p in partner %}
                            {% if u.partner_id == p.partner_id %}
                                <option value={{ p.partner_id }} selected>{{ p.name }}</option>
                            {% else %}
                                <option value={{ p.partner_id }}>{{ p.name }}</option>
                            {% endif %}
                        {% endfor %}
                    </select></td>
            </tr>
            <tr>
                <th><label>Activo:</label></th>
                <td><select name='active'>
                        <option value='0'>Contraseña</option>
                        {% if u.active == 's' %}
                        <option value='s' selected>Si</option>
                        <option value='n'>No</option>
                        {% else %}
                        <option value='s'>Si</option>
                        <option value='n' selected>No</option>
                        {% endif %}
                    </select></td>
            </tr>
        </table>
    </div>
    {% endfor %}
</fieldset>
<ul class="pager">
    <li class='pull-center'>
        {{ submit_button("Actualizar", "class":"btn btn-primary", "id":"ok") }}
    </li>
</ul>
{{ end_form() }}

{#----------------PARTE DE JAVASCRIPT-----------------#}
<script type="text/javascript">
    $(document).ready(function(){

        $('#ok').on('click', function(){

            $pass = $('#pass').val();
            $confirm_pass = $('#confirm-pass').val();

            if($pass.trim() != $confirm_pass.trim())
            {
                event.preventDefault();
                alert('El password y el confirmar password no son iguales');
            }

        });
    });

</script>