{{ content() }}
<p style="text-align: left; padding: 10px;">{{ link_to("users", "class":"btn btn-primary","&larr; Regresar") }}</p>

{{ form("users/register") }}
<fieldset>
    <div width="100%" style="display: block; overflow: auto;">
        <table align="center" class="detalle table-bordered table-striped">
            <tr>
                <th><label>Nombre de usuario:</label></th>
                <td>{{ text_field('username', 'id':'user') }}</td>
            </tr>
            <tr>
                <th><label>Contraseña:</label></th>
                <td>{{ password_field('pass', 'id':'pass') }}</td>
            </tr>
            <tr>
                <th><label>Confirmar contraseña:&nbsp;</label></th>
                <td>{{ password_field('password', 'id':'password') }}</td>
            </tr>
            <tr>
                <th><label>Nombre:</label></th>
                <td>{{ text_field('name', 'id':'name') }}</td>
            </tr>
            <tr>
                <th><label>Correo:</label></th>
                <td>{{ text_field('email', 'id':'email') }}</td>
            </tr>
            <tr>
                <th><label>Rol:</label></th>
                <td><select name='role', id="role">
                        <option value='0'>Por favor, elija uno...</option>
                        {% for r in rol %}
                            <option value='{{ r.role_id }}'>{{ r.role_name }}</option>
                        {% endfor %}
                    </select></td>
            </tr>
            <tr>
                <th><label>Socio:</label></th>
                <td><select name='partner' id="partner">
                        <option value='0'>Por favor, elija uno...</option>
                        {% for p in partner %}
                            <option value={{ p.partner_id }}>{{ p.name }}</option>
                        {% endfor %}
                    </select></td>
            </tr>
            <tr>
                <th><label>Activo:</label></th>
                <td><select name='active' id="activo">
                        <option value='0'>Por favor, elija uno...</option>
                        <option value='s'>Si</option>
                        <option value='n'>No</option>
                    </select></td>
            </tr>
        </table>
    </div>
</fieldset>
<ul class="pager">
    <li class="pull-center">
        {{ submit_button("Guardar", "class":"btn btn-primary", "id":"ok") }}
    </li>
</ul>
</form>
{#------------------------------INICIA JAVASCRIPT--------------------------------------------------#}
<script type="text/javascript">
    $(document).ready(function(){
        $('#ok').on('click', function(){
            // event.preventDefault();

            var mensaje = [];
            var mens = [];
            $email = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+/;
            $contra =/(?=^.{7,21}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;

            mens.push('Los siguientes campos son incorrectos o estan vacios:\n');
            mensaje.push('Los siguientes campos son incorrectos o estan vacios:\n');

            $user = $('#user').val();
            $pass = $('#pass').val();
            $password=$('#password').val();
            $name = $('#name').val();
            $email_user = $('#email').val();
            $role = $('#role').val();
            $partner = $('#partner').val();
            $activo = $('#activo').val();

            if($user.trim()=='')
            {
                mensaje.push('-El campo USERNAME esta vacio \n');
            }
            if(!($contra.test($pass)))
            {
                mensaje.push('-La contraseña debe contener:\n' +
                '      al menos una letra mayúscula\n' +
                '      al menos una letra minúscula\n' +
                '      al menos un número o caracter especial\n' +
                '      longitud mínima de 7 caracteres\n' +
                '      longitud máxima de 21 caracteres\n');
            }
            if($password!=$pass)
            {
                mensaje.push('-Las contraseñas no coinciden');
            }
            if($name.trim()=='')
            {
                mensaje.push('-El campo NOMBRE esta vacio \n');
            }
            if(!($email.test($email_user)))
            {
                mensaje.push('-El email es invalido en el campo EMAIL\n');
            }
            if($email_user.trim()=='')
            {
                mensaje.push('-El campo EMAIL esta vacio \n');
            }
            if($role.trim()=='0')
            {
                mensaje.push('-No se a seleccionado ningun ROLE_ID \n');
            }
            /*if($partner.trim()=='0')
            {
                mensaje.push('-No se a seleccionado ningun PARTNER_ID \n');
            }*/
            if($activo.trim()=='0')
            {
                mensaje.push('-No se a seleccionado ningun ACTIVO \n');
            }
            if(mensaje.length != mens.length)
            {
                event.preventDefault()
                alert(mensaje);
            }
        })
    });
</script>