{{ content() }}

{{ form ('libros/enviarlibro', 'method':'get') }}
    <fieldset>
        <div width="100%" style="display: block; overflow: auto;">
            <table align="center" class="detalle table-bordered table-striped">
                <tr><th colspan="2" style="text-align: center">Ingresa el IBSN ó el Active revison ID del producto:</th></tr>
                <tr>
                    <th><label for="items_per_page">ISBN:</label></th>
                    <td>{{ text_field("isbn", "id":"isbn") }}</td>
                </tr>
                <tr>
                    <th><label for="items_per_page">Active revision  ID:</label></th>
                    <td>{{ text_field("id_libro", "id":"id_libro") }}</td>
                </tr>
            </table>
            <table align="center">
                <tr>
                    <td colspan="3" style="text-align: right">
                        <ul class="pager">
                            <li class="pull-right">
                                {{ submit_button("Ver detalles", "class": "btn btn-primary", "id":"ok") }}
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </fieldset>
</form>

{#------------------------------INICIA JAVASCRIPT----------------------------#}
<script type="text/javascript">

    $(document).ready(function(){
        $('#ok').on('click', function(){
            $isbn = $('#isbn').val();
            $id_libro = $('#id_libro').val();

            if($isbn.trim() == '' && $id_libro.trim() == '')
            {
                event.preventDefault();
                alert("Requiere ingresar valores en uno de los dos campos: \n 'ISBN' o el 'ID del libro'");
            }
            if($isbn.trim() != '' && $id_libro.trim() != '')
            {
                event.preventDefault();
                alert("Solo debe de ingresar valores en uno de los dos campos, no en ambos");
            }
        });
    });
</script>