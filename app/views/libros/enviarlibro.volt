{{ content() }}
{% if isbn!='' %}
<fieldset>
    <div class="row">

        <div class="col-md-12">
                <h2>TITULO: {{ book_isbn.activeRevision.title }}</h2>
            <hr>
        </div>
        <div class="col-md-4">
            <img style="margin: 20px; margin-left: 5px;" src="{{  book_isbn.activeRevision.links.coverImage }}">
            <h4>IMPRENTA: </h4>{{ book_isbn.activeRevision.imprint }}
        </div>
        <div class="col-md-8" style="text-align: justify;">
            <h2 align="center">DESCRIPCIÓN:</h2> {{ book_isbn.activeRevision.description }}
        </div>
    </div>
</fieldset>
<br><br>
{% else %}
<fieldset>
    <div class="row">

        <div class="col-md-12">
            <h2>TITULO: {{ book_id.activeRevision.title }}</h2>
            <hr>
        </div>
        <div class="col-md-4">
            <img style="margin: 20px; margin-left: 5px;" src="{{  book_id.activeRevision.links.coverImage }}">
            <h4>IMPRENTA: </h4>{{ book_id.activeRevision.imprint }}
        </div>
        <div class="col-md-8" style="text-align: justify;">
            <h2 align="center">DESCRIPCIÓN:</h2> {{ book_id.activeRevision.description }}
        </div>
    </div>

</fieldset>
{% endif %}