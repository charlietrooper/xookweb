{{ content() }}
<script type="text/javascript" xmlns="https://www.w3.org/1999/html">
    window.onload = function () {
        var atras = document.getElementById('irAtras');
        atras.addEventListener("click", irAtras);
        function irAtras() { this.style.color = 'red'; window.history.back();}
    }
</script>
<div class="row" xmlns="https://www.w3.org/1999/html">
    <div class="col-md-12">
        <p style="text-align: left; padding-top: 10px;">{{ link_to("libros", "class":"btn btn-primary", "&larr; Regresar", "id":"irAtras") }}</p>
    </div>
</div>
<table class="table table-bordered table-striped" align="center">
    <tr>
        <th style='text-align:center; background-color:#080808; color:#ffffff;'>ISBN</th>
        <th style='text-align:center; background-color:#080808; color:#ffffff;'>Activo</th>
        <th style='text-align:center; background-color:#080808; color:#ffffff;'>Título</th>
        <th style='text-align:center; background-color:#080808; color:#ffffff;'>Active revision ID / Deprecated revision ID</th>
    </tr>
    {% for obj in page.items %}

    <tr>
        {% if  obj['activeRevision']['@attributes']['id'] != '' %}
            <td>{{ obj['isbn'] }}</td>
            <td>Si</td>
            <td>{{ obj['activeRevision']['title'] }}</td>
            <td>{{ link_to("library/getbookrev/" ~obj['activeRevision']['@attributes']['id'], obj['activeRevision']['@attributes']['id']) }}</td>
        {% else %}
            <td>{{ obj['isbn'] }}</td>
            <td>No</td>
            <td></td>
            <td>{{ obj['deprecatedRevisions']['revision'] }}</td>
        {% endif %}
    </tr>
    {% endfor %}
    <tr>
        <td colspan="8" align="right">
            <div class="btn-group">
                {{ link_to("libros/searchbookisbn",'<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                {{ link_to("libros/searchbookisbn?page=" ~ page.before ,'<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                {{ link_to("libros/searchbookisbn?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                {{ link_to("libros/searchbookisbn?page=" ~ page.last ,'<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
            </div>
        </td>
    </tr>
</table>