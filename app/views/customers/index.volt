{{ content() }}
<?php //var_dump($test); ?>
      {#  <p>-----</p>#}
<?php //var_dump($test2); ?>
{#--------------------------------------------TABLA PARA ADMINISTRADOR-------------------------------------#}
<div class="row" style="padding-bottom: 30px">
    <div class="col-md-12">
        <label style="text-align: center;">Buscar por:</label>
    </div>
    {% if se == '1' or se == '4' %}
    <div class="col-sm-12 col-md-3" style="margin-top: 12px;">
        <label>Socio:</label>

        {#----------------------------------------------------------------------------------#}

        <select name="partners" id="partners">

            {% if partnergrafic !='' %}
                <option value="1">Todos</option>
                <option selected="selected" value="{{ partnergrafic }}">{{ nombre }}</option>
                {% for p in partners %}
                    <option value="{{ p.partner_id }}">{{ p.name }}</option>
                {% endfor %}
            {% else %}
                <option selected="selected"></option>
                <option value="1">Todos</option>
                {% for p in partners %}
                    <option value="{{ p.partner_id }}">{{ p.name }}</option>
                {% endfor %}
            {% endif %}
        </select>
    </div>
    <div class="col-md-3" style="margin-top: 9px;">
        <label>Correo:</label>
        {{ text_field('mail', 'id':'mail') }}
    </div>
    <div class="col-sm-12 col-md-6">
        <label>Fecha: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>de:&nbsp;
        {{date_field('date', 'id':'fecha_ini')}}

        <label for="dates">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>a:&nbsp;
        {{date_field('dates', 'id':'fecha_fin')}}
        {# {{submit_button("buscar", 'class':'search-admin')}}#}
    </div>
</div>
<div width="100%" style="display: block; overflow: auto;">
<table class="table table-bordered table-striped" align="center" id="answer-admin">
    <tr><th colspan="12">Registros por página/Total de registros: {{ page.total_items/ page.total_items * page.limit }}/{{ page.total_items }} </th></tr>
    <tr>
        <th>ID del cliente</th>
        <th>ID del socio</th>
        <th>ID kobo</th>
        <th>Correo</th>
        <th>Fecha de registro</th>
        <th>ID socio cliente</th>
        <th>Promedio de gasto</th>
        <th>Promedio de libros por compra</th>
        <th>Total de libros</th>
        <th>Última compra</th>
        {# <th>Resultado</th>#}
    </tr>
    <?php $conteo=0; ?>
    {% for obj in page.items %}
        <?php //var_dump($obj.customer_id); ?>
        <tr>
            <td>{{ link_to("customers/details/" ~ obj.customer_id, obj.customer_id) }}</td>
            <td>{{ link_to("partners/details/" ~ obj.partner_id, obj.partner_id) }}</td>
            <td>
                <div class="emergente">
                    <div class="ocultar"><?php echo substr($obj->kobo_id,0,5).'...'; ?></div>
                    <div class="mostrar mostrarPID">{{ obj.kobo_id }}</div>
                </div>
            </td>
            <td>{{ link_to("customers/details/" ~ obj.email, obj.email) }}</td>
            <td>{{obj.register_date}}</td>
            <td>{{obj.partner_customer_id}}</td>
            <td>
                {% if obj.gasto_promedio==NULL %}
                    {{ "-" }}
                {% else %}
                    <?php $gastoFormat = number_format($obj[gasto_promedio],2);
                     ?>
                    {{ gastoFormat }}
                {% endif %}
            </td>
            <td>
                {% if obj.promedio_libros_compra==NULL %}
                    {{ "-" }}
                {% else %}
                    <?php $promFormat = number_format($obj[promedio_libros_compra],2);
                     // number_format($obj.promedio_libros_compra, 2, '.', ''); ?>
                    {{ promFormat }}
                {% endif %}
            </td>
            <td>
                {% if obj.libros_total==NULL %}
                    {{ "-" }}
                {% else %}
                    {{ obj.libros_total }}
                {% endif %}

            </td>
           {# <td>{{ arrLibrosTotal[conteo] }}</td>#}
            <td style="white-space:nowrap;">
                {% if obj.lastdate==NULL %}
                    {{ "-" }}
                {% else %}
                    {{ obj.lastdate }}
                {% endif %}
            </td>
            {#<td>{{obj.result}}</td>#}
        </tr>
        <?php $conteo++; ?>
    {% endfor %}
    <tr>
        <td colspan="5" align="left">
            <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
            <div class="btn-group">
                {{ link_to("customers/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                {{ link_to("customers/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                {{ link_to("customers/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                {{ link_to("customers/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
            </div>
        </td>
        {% if partnergrafic == '' %}
            <td colspan="6" align="right">{{ link_to("sales-reports/selectgrafica/", '<i class="glyphicon glyphicon-stats"></i> Gráficas', "class": "btn btn-default") }} </td>
        {% else %}
            <p>
            <td colspan="6" align="right">{{ link_to("sales-reports/graficas?partnerid=" ~ partnergrafic, '<i class="glyphicon glyphicon-stats"></i> Gráficas', "class": "btn btn-default") }} </td>
        {% endif %}
    </tr>

</table>

{#-------------------------------------------TABLA PARA PARTNER----------------------------------------------------#}
    {% else %}
<div class="col-md-4" style="margin-top: 12px;">
    <label>Correo:</label>
    {{ text_field('mail', 'id':'mail') }}
</div>
<div class="col-md-8">
    <label>Fecha de:</label>
    {{date_field('date', 'id':'fecha_ini')}}
    <label>a:</label>
    {{date_field('dates', 'id':'fecha_fin')}}
    {# {{submit_button("buscar", 'class':'search-partner')}}#}
</div>
</div>
<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="answer-partner">
        <tr><th colspan="12">Registros por página/Total de registros: {{ page.total_items/ page.total_items * page.limit }}/{{ page.total_items }} </th></tr>
        <tr>
            <th>ID del cliente</th>
            <th>ID kobo</th>
            <th>Correo</th>
            <th>Fecha de registro</th>
            <th>ID socio cliente</th>
            <th>Promedio de gasto</th>
            <th>Promedio de libros por compra</th>
             <th>Total de libros</th>
             <th>última compra</th>

        </tr>
        <?php $conteo=0; ?>
        {% for obj in page.items %}
            <tr>
                <td>{{ link_to("customers/details/" ~ obj.customer_id, obj.customer_id) }}</td>
                <td>
                    <div class="emergente">
                        <div class="ocultar"><?php echo substr($obj->kobo_id,0,5).'...'; ?></div>
                        <div class="mostrar mostrarPID">{{ obj.kobo_id }}</div>
                    </div>
                </td>
                <td>{{ link_to("customers/details/" ~ obj.email, obj.email) }}</td>
                <td>{{obj.register_date}}</td>
                <td>{{obj.partner_customer_id}}</td>
                <td>
                    {% if obj.gasto_promedio==NULL %}
                        {{ "-" }}
                    {% else %}
                        <?php $gastoFormat = number_format($obj[gasto_promedio],2);
                     ?>
                        {{ gastoFormat }}
                    {% endif %}
                </td>
                <td>
                    {% if obj.promedio_libros_compra==NULL %}
                        {{ "-" }}
                    {% else %}
                        <?php $promFormat = number_format($obj[promedio_libros_compra],2);
                     // number_format($obj.promedio_libros_compra, 2, '.', ''); ?>
                        {{ promFormat }}
                    {% endif %}
                </td>
                <td>
                    {% if obj.libros_total==NULL %}
                        {{ "-" }}
                    {% else %}
                        {{ obj.libros_total }}
                    {% endif %}

                </td>
                {# <td>{{ arrLibrosTotal[conteo] }}</td>#}
                <td style="white-space:nowrap;">
                    {% if obj.lastdate==NULL %}
                        {{ "-" }}
                    {% else %}
                        {{ obj.lastdate }}
                    {% endif %}
                </td>
                {#<td>{{obj.result}}</td>#}
            </tr>
            <?php $conteo++; ?>
        {% endfor %}
        <tr>
            <td colspan="3" align="left">
                <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                <div class="btn-group">
                    {{ link_to("customers/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                    {{ link_to("customers/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("customers/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("customers/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                </div>
            </td>
            <td colspan="6" align="right">{{ link_to("sales-reports/mes/" ~ obj.partner_id, '<i class="glyphicon glyphicon-stats"></i> Gráficas', "class": "btn btn-default") }}</td>
        </tr>
    </table>

    {% endif %}
</div>
<div align="right" id="link">

</div>
{#---------------------CODIGO JAVASCRIPT PARA LAS LLAMADAS DE BUSQUEDA A LA BD---------------------------------------#}
<script type="text/javascript">

    $(document).ready(function(){

        window.onload=function(){
            var elementExists = document.getElementById("partners");
            if (elementExists)
            {
                if(document.getElementById("partners").value!=="")
                {
                    $mail = document.getElementById("mail").value;
                    $fecha_ini=$('#fecha_ini').val();
                    $fecha_fin=$('#fecha_fin').val();
                    $ajax_page = '';
                    if(!document.getElementById("partners") )
                    {
                        searchFunc('X',$mail,$fecha_ini,$fecha_fin);
                    }

                    else{

                        $id = document.getElementById("partners").value;
                        if($id==1)
                        {
                            var pagina = "/customers/index";
                            location.href = location.origin + pagina;
                        }
                        searchFunc($id,$mail,$fecha_ini,$fecha_fin);
                    }
                 }

            }
        }
        {#--------------------------------------------------------------------------------------------------------------------------------#}
        {#--------------------------------------------------------------------------------------------------------------------------------#}
        {#--------------------------------------------------------------------------------------------------------------------------------#}
        {#--------------------------------------------------PARTE PARA ADMINISTRADOR-------------------------------------------------------#}
        {#--------------------------------------------------BÚSQUEDA------------------------------------------------------------------------#}
        $('#fecha_ini').on('change', function () {
            $mail = document.getElementById("mail").value;
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();
            $ajax_page = '';
            if(!document.getElementById("partners") )
            {
                searchFunc('X',$mail,$fecha_ini,$fecha_fin);
            }

            else{

                $id = document.getElementById("partners").value;
                searchFunc($id,$mail,$fecha_ini,$fecha_fin);
            }

        });


        $('#fecha_fin').on('change', function () {
            $mail = document.getElementById("mail").value;
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();
            $ajax_page = '';
            if(!document.getElementById("partners") )
            {
                searchFunc('X',$mail,$fecha_ini,$fecha_fin);
            }

            else{

                $id = document.getElementById("partners").value;
                searchFunc($id,$mail,$fecha_ini,$fecha_fin);
            }
        });

        $('#partners').on('change', function () {
            $mail = document.getElementById("mail").value;
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();
            $ajax_page = '';
            if(!document.getElementById("partners") )
            {
                searchFunc('X',$mail,$fecha_ini,$fecha_fin);
            }

            else{

                $id = document.getElementById("partners").value;
                if($id==1)
                {
                    var pagina = "/customers/index";
                    location.href = location.origin + pagina;
                }
                searchFunc($id,$mail,$fecha_ini,$fecha_fin);
            }
        });

        $('#mail').on('change', function () {
            $mail = document.getElementById("mail").value;
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();
            $ajax_page = '';
            if(!document.getElementById("partners") )
            {
                searchFunc('X',$mail,$fecha_ini,$fecha_fin);
            }

            else{

                $id = document.getElementById("partners").value;
                searchFunc($id,$mail,$fecha_ini,$fecha_fin);
            }
        });

        function checarDato($datoChecar)
        {
            $dato='';
            if(!$datoChecar)
            {
                $dato='-'
            }
            else{
                $dato=$datoChecar;
                $dato=parseFloat($dato).toFixed(2);
            }
            return $dato;
        }

        function checarFecha($datoChecar)
        {
            $dato='';
            if(!$datoChecar)
            {
                $dato='-'
            }
            else{
                $dato=$datoChecar;
            }
            return $dato;
        }


        {#--------------------------------------------------------------------------------------------------------------------------------#}
        {#--------------------------------------------------------------------------------------------------------------------------------#}
        {#--------------------------------------------------------------------------------------------------------------------------------#}
        function searchFunc($id,$mail,$fecha_ini,$fecha_fin){
            {#-----------Hacemos la peticion via post o get contra sales-reports/search pasando el id----------#}

            $id=$id;
            $mail=$mail;
            $fecha_ini=$fecha_ini;
            $fecha_fin=$fecha_fin;
            if ($id!=='X')
            {

                $.get("<?php echo $this->url->get('customers/search')?>",{'id':$id, 'mail':$mail, 'fecha_ini':$fecha_ini, 'fecha_fin':$fecha_fin},function($data)
                {
                    {#-------------parseamos el json y recorremos-----------#}
                    {#debugger;#}
                    var purchase = $data;
                  //  var datosClientes=$data[1];
                    var count=0;

                    var pur = "<tr><th colspan='13'>Registros por página/Total de registros: " + ~~(purchase.total_items/purchase.total_items*purchase.limit) +"/" + purchase.total_items+ "</th></tr>"+
                            "<tr>"+
                            "<th>ID del cliente</th>"+
                            "<th>ID del socio</th>"+
                            "<th>ID kobo</th>"+
                            "<th>Correo</th>"+
                            "<th>Fecha de registro</th>"+
                            "<th>ID SOCIO CLIENTE</th>"+
                            "<th>Promedio de gasto</th>"+
                            "<th>Promedio de libros por compra</th>"+
                            "<th>Total de libros</th>"+
                            "<th>Última compra</th>"+
                                // "<th>RESULTADO</th>"+
                            "</tr>";

                    $.each(purchase.items, function(index, datos) {
                        pur +='<tr>'+
                        '<td>'+'<a href="/customers/details/'+datos.customer_id+'">'+datos.customer_id+'</a></td>'+
                        '<td>'+'<a href="/partners/details/'+datos.partner_id+'">'+datos.partner_id+'</a></td>'+
                        '<td><div class="emergente"><div class="ocultar">'+(datos.kobo_id || " ").substring(0,5)+'...</div><div class="mostrar mostrarPID">'+datos.kobo_id+'</div></div></td>'+
                        '<td><a href="details/'+datos.email+'">'+datos.email+'</a></td>'+
                        '<td>'+datos.register_date+'</td>'+
                        '<td>'+datos.partner_customer_id+'</td>'+
                        '<td> $ '+checarDato(datos.gasto_promedio)+'</td>'+
                        '<td>'+checarDato(datos.promedio_libros_compra)+'</td>'+
                        '<td>'+checarDato(datos.libros_total)+'</td>'+
                        '<td>'+checarFecha(datos.lastdate)+'</td>'+
                            // '<td>'+datos.result+'</td>'+
                        '</tr>';
                        count++;
                    });
                    pur +="<tr>"+
                    "<td colspan='5' align='left'>"+
                    "<span class='help-inline'>"+purchase.current+"/"+purchase.total_pages+"</span>"+
                    "<div class='btn-group'>"+
                    "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                    "<a data-before='"+purchase.before+"' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                    "<a data-next='"+purchase.next+"' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                    "<a data-last='"+purchase.last+"' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                    "</div>"+
                    "</td>"+
                    "<td colspan='5' align='right'><a href='/sales-reports/selectgrafica' class='btn btn-default'><i class='glyphicon glyphicon-stats'></i> Gráficas</a></td>"+
                    "</tr>";
                    {#------------------populamos el desplegable purchase con sus respectivos datos obtenidos---------#}
                    $('table#answer-admin').html(pur);
                    $('#link').html(' <a href="/customers/showCsv" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
                });
            }

            else{
                $.get("<?php echo $this->url->get('customers/searchforpartner')?>", {'mail':$mail,'fecha_ini':$fecha_ini, 'fecha_fin':$fecha_fin},function($data)
                {
                    var purchase = $data;
                    //  var datosClientes=$data[1];
                    var count=0;

                    var pur = "<tr><th colspan='13'>Registros por página/Total de registros: " + ~~(purchase.total_items/purchase.total_items*purchase.limit) +"/" + purchase.total_items+ "</th></tr>"+
                            "<tr>"+
                            "<th>ID del cliente</th>"+
                            "<th>ID kobo</th>"+
                            "<th>Correo</th>"+
                            "<th>Fecha de registro</th>"+
                            "<th>ID socio cliente</th>"+
                            "<th>Promedio de gasto</th>"+
                            "<th>Promedio de libros por compra</th>"+
                            "<th>Total de libros</th>"+
                            "<th>Última compra</th>"+
                            "</tr>";

                    $.each(purchase.items, function(index, datos) {
                        pur +='<tr>'+
                        '<td>'+'<a href="/customers/details/'+datos.customer_id+'">'+datos.customer_id+'</a></td>'+
                        '<td><div class="emergente"><div class="ocultar">'+(datos.kobo_id || " ").substring(0,5)+'...</div><div class="mostrar mostrarPID">'+datos.kobo_id+'</div></div></td>'+
                        '<td><a href="details/'+datos.email+'">'+datos.email+'</a></td>'+
                        '<td>'+datos.register_date+'</td>'+
                        '<td>'+datos.partner_customer_id+'</td>'+
                        '<td> $ '+checarDato(datos.gasto_promedio)+'</td>'+
                        '<td>'+checarDato(datos.promedio_libros_compra)+'</td>'+
                        '<td>'+checarDato(datos.libros_total)+'</td>'+
                        '<td>'+checarFecha(datos.lastdate)+'</td>'+
                        '</tr>';
                        count++;
                    });

                    $.each(purchase.items, function(index, datos) {
                        pur +="<tr>"+
                        "<td colspan='3' align='left'>"+
                        "<span class='help-inline'>"+purchase.current+"/"+purchase.total_pages+"</span>"+
                        "<div class='btn-group'>"+
                        "<a data-first='1' id='first_p' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                        "<a data-before='"+purchase.before+"' id='before_p' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                        "<a data-next='"+purchase.next+"' id='next_p' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                        "<a data-last='"+purchase.last+"' id='last_p' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                        "</div>"+
                        "</td>"+
                        "<td colspan='5' align='right'><a href='/sales-reports/mes/" + datos.partner_id + "' class='btn btn-default'><i class='glyphicon glyphicon-stats'></i> Gráficas</a></td>" +
                        "</tr>";
                        return ( index == 1 );
                    });
                    {#------------------populamos el desplegable purchase con sus respectivos datos obtenidos---------#}
                    $('table#answer-partner').html(pur);
                    $('#link').html(' <a href="/customers/showCsv" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
                });
            }


        }



        {#-------------------------------------------------------TERMINA PARTE PARA PARTNER----------------------------------------------#}
        {#-------------------------------------------------------------------------------------------------------------------------------#}
        {#-------------------------------------------------------------------------------------------------------------------------------#}
        {#-------------------------------------------------------------------------------------------------------------------------------#}
        {#-------------------------------------------------------------------------------------------------------------------------------#}
        {#--------------------------------------------------------INICIA PARTE PAGINACION AJAX ADMIN-------------------------------------#}
        $(document).on('click','#next', function(){
            $ajax_page=$('#next').data('next');

            page_ajax($ajax_page);
        });
        $(document).on('click', '#first', function(){
            $ajax_page=$('#first').data('first');

            page_ajax($ajax_page);
        });
        $(document).on('click', '#last', function(){
            $ajax_page=$('#last').data('last');

            page_ajax($ajax_page);
        });
        $(document).on('click', '#before', function(){
            $ajax_page=$('#before').data('before');

            page_ajax($ajax_page);
        });
        {#--------------------------------------------------------------------------------------------------------------------------------#}
        {#---------------------------------------------------------------------------------------------------------------------------------#}
        {#--------------------------------------------------------------------------------------------------------------------------------#}
        {#---------------------------------------------------------------------------------------------------------------------------------#}
        {#--------------------------------------------------------------------------------------------------------------------------------#}
        {#------------------------------INICIA FUNCION PARA PETICION AJAX PAGINATION ADMIN-------------------------------------------------#}
        function page_ajax($ajax_page)
        {
            $page=$ajax_page;
            $id = document.getElementById("partners").value;
            $mail = document.getElementById("mail").value;
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();

            {#----------------HACEMOS LA PETICION AJAX PARA AVANZAR UNA PAGINA----------------#}

            $.get("<?php echo $this->url->get('customers/search')?>", {'page_ajax':$page,'id':$id, 'mail':$mail, 'fecha_ini':$fecha_ini,'fecha_fin':$fecha_fin},function($data)
            {
                var purchase = $data;
               // var datosClientes=$data[1];
                var count=0;

                var pur = "<tr><th colspan='13'>Registros por página/Total de registros: " + ~~(purchase.total_items/purchase.total_items*purchase.limit) +"/" + purchase.total_items+ "</th></tr>"+
                        "<tr>"+
                        "<th>ID del cliente</th>"+
                        "<th>ID del socio</th>"+
                        "<th>ID kobo</th>"+
                        "<th>Correo</th>"+
                        "<th>Fecha de registro</th>"+
                        "<th>ID socio cliente</th>"+
                        "<th>Promedio de gasto</th>"+
                        "<th>Promedio de libros por compra</th>"+
                        "<th>Total de libros</th>"+
                        "<th>Última compra</th>"+
                            //"<th>Resultado</th>"+
                        "</tr>";

                $.each(purchase.items, function(index, datos) {
                    pur +='<tr>'+
                    '<td>'+'<a href="/customers/details/'+datos.customer_id+'">'+datos.customer_id+'</a></td>'+
                    '<td>'+'<a href="/partners/details/'+datos.partner_id+'">'+datos.partner_id+'</a></td>'+
                    '<td><div class="emergente"><div class="ocultar">'+(datos.kobo_id || " ").substring(0,5)+'...</div><div class="mostrar mostrarPID">'+datos.kobo_id+'</div></div></td>'+
                    '<td><a href="details/'+datos.email+'">'+datos.email+'</a></td>'+
                    '<td>'+datos.register_date+'</td>'+
                    '<td>'+datos.partner_customer_id+'</td>'+
                    '<td> $ '+checarDato(datos.gasto_promedio)+'</td>'+
                    '<td>'+checarDato(datos.promedio_libros_compra)+'</td>'+
                    '<td>'+checarDato(datos.libros_total)+'</td>'+
                    '<td>'+checarFecha(datos.lastdate)+'</td>'+
                        //'<td>'+datos.result+'</td>'+
                    '</tr>';
                    count++;
                });
                pur +="<tr>"+
                "<td colspan='9' align='left'>"+
                "<span class='help-inline'>"+purchase.current+"/"+purchase.total_pages+"</span>"+
                "<div class='btn-group'>"+
                "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                "<a data-before='"+purchase.before+"' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                "<a data-next='"+purchase.next+"' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                "<a data-last='"+purchase.last+"' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                "</div>"+
                "</td>"+
                "<td colspan='2' align='right'><a href='/sales-reports/selectgrafica' class='btn btn-default'><i class='glyphicon glyphicon-stats'></i> Gráficas</a></td>"+
                "</tr>";
                {#------------------populamos el desplegable purchase con sus respectivos datos obtenidos---------#}
                $('table#answer-admin').html(pur);
                $('#link').html(' <a href="/customers/showCsv" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');

            });

        }
        {#-------------------------------------------------TERMINA LA FUNCION PARA TABLE ADMIN-------------------------------------------------------------------------------#}
        {#-------------------------------------------------TERMINA PAGINACION AJAX PARA ADMIN--------------------------------------------------------------------------------#}
        {#--------------------------------------------------------------------------------------------------------------------------------#}
        {#---------------------------------------------------------------------------------------------------------------------------------#}
        {#-------------------------------------------------INICIA PAGINACION AJAX PARA PARTNER--------------------------------------------------------------------------------#}
        $(document).on('click', '#next_p', function(){
            $ajax_page=$('#next_p').data('next');

            ajaxPartner($ajax_page);
        });
        $(document).on('click', '#first_p', function(){
            $ajax_page=$('#first_p').data('first');

            ajaxPartner($ajax_page);
        });
        $(document).on('click', '#last_p', function(){
            $ajax_page=$('#last_p').data('last');

            ajaxPartner($ajax_page);
        });
        $(document).on('click', '#before_p', function(){
            $ajax_page=$('#before_p').data('before');

            ajaxPartner($ajax_page);
        });

        function ajaxPartner($ajax_page)
        {
            $page=$ajax_page;
            $mail = document.getElementById("mail").value;
            $fecha_ini=$('#fecha_ini').val();
            $fecha_fin=$('#fecha_fin').val();

            $.get("<?php echo $this->url->get('customers/searchforpartner')?>", {'page_ajax':$page,'mail':$mail, 'fecha_ini':$fecha_ini,'fecha_fin':$fecha_fin},function($data)
            {
                var purchase = $data;
              //  var datosClientes=$data[1];
                var count=0;

                var pur = "<tr><th colspan='13'>Registros por página/Total de registros: " + ~~(purchase.total_items/purchase.total_items*purchase.limit) +"/" + purchase.total_items+ "</th></tr>"+
                        "<tr>"+
                        "<th>ID del cliente</th>"+
                        "<th>ID kobo</th>"+
                        "<th>Correo</th>"+
                        "<th>Fecha de registro</th>"+
                        "<th>ID socio cliente</th>"+
                        "<th>Promedio de gasto</th>"+
                        "<th>Promedio de libros por compra</th>"+
                        "<th>Total de libros</th>"+
                        "<th>Última compra</th>"+
                        "</tr>";

                $.each(purchase.items, function(index, datos) {
                    pur +='<tr>'+
                    '<td>'+'<a href="/customers/details/'+datos.customer_id+'">'+datos.customer_id+'</a></td>'+
                    '<td><div class="emergente"><div class="ocultar">'+(datos.kobo_id || " ").substring(0,5)+'...</div><div class="mostrar" style="width: 300px;">'+datos.kobo_id+'</div></div></td>'+
                    '<td><a href="details/'+datos.email+'">'+datos.email+'</a></td>'+
                    '<td>'+datos.register_date+'</td>'+
                    '<td>'+datos.partner_customer_id+'</td>'+
                    '<td> $ '+checarDato(datos.gasto_promedio)+'</td>'+
                    '<td>'+checarDato(datos.promedio_libros_compra)+'</td>'+
                    '<td>'+checarDato(datos.libros_total)+'</td>'+
                    '<td>'+checarFecha(datos.lastdate)+'</td>'+
                    '</tr>';
                    count++;
                });
                $.each(purchase.items, function(index, datos) {
                    pur +="<tr>"+
                    "<td colspan='9' align='left'>"+
                    "<span class='help-inline'>"+purchase.current+"/"+purchase.total_pages+"</span>"+
                    "<div class='btn-group'>"+
                    "<a data-first='1' id='first_p' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                    "<a data-before='"+purchase.before+"' id='before_p' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                    "<a data-next='"+purchase.next+"' id='next_p' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                    "<a data-last='"+purchase.last+"' id='last_p' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                    "</div>"+
                    "</td>"+
                    "<td colspan='2' align='right'><a href='/sales-reports/mes/" + datos.partner_id + "' class='btn btn-default'><i class='glyphicon glyphicon-stats'></i> Gráficas</a></td>" +
                    "</tr>";
                    return ( index == 1 );
                });

                {#------------------populamos el desplegable purchase con sus respectivos datos obtenidos---------#}
                $('table#answer-partner').html(pur);
                $('#link').html(' <a href="/customers/showCsv" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
            });
        }
    });
</script>
