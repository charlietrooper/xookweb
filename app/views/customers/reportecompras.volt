{{ content() }}
<script type="text/javascript">
    window.onload = function () {
        var atras = document.getElementById('irAtras');
        atras.addEventListener("click", irAtras);
        function irAtras() { this.style.color = 'red'; window.history.back();}
    }
</script>
<p style="text-align: left; padding-top: 10px;">{{ link_to("customers", "class":"btn btn-primary", "&larr; Regresar", "id":"irAtras") }}</p>

<div width="100%" style="display: block; overflow: auto;">
    <table  class="table table-bordered table-striped" align="center">
        <tr>
            <th>ID de compra</th>
            <th>ID del cliente</th>
            <th>ID de usuario</th>
            <th>Fecha de registro</th>
            <th>ID compra kobo</th>
            <th>ID compra socio</th>
            <th>Ciudad</th>
            <th>País</th>
            <th>Provincia del estado</th>
            <th>Código postal</th>
            <th>Detalles</th>
        </tr>
        {% for obj in page.items %}
            <tr>
                <td>{{obj.purchase_id}}</td>
                <td>{{ link_to("partners/details/" ~ obj.partner_id, obj.partner_id) }}</td>
                <td>{{obj.user_id}}</td>
                <td>{{obj.date}}</td>
                <td>{{obj.purchase_kobo_id}}</td>
                <td>{{obj.purchase_partner_id}}</td>
                <td>{{obj.address_city}}</td>
                <td>{{obj.address_country}}</td>
                <td>{{obj.address_state_provincy}}</td>
                <td>{{obj.address_zip_postal_code}}</td>
                <td width="7%">{{ link_to("sales-reports/details/" ~ obj.purchase_id, '<i class="glyphicon glyphicon-list-alt"></i> DETALLES', "class": "btn btn-default") }}</td>
            </tr>
        {% endfor %}
        <tr>
            <td colspan="11" align="right">
                <div class="btn-group">
                    {{ link_to("customers/reportecompras/"~ page.items[0].orbile_customer_id, '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                    {{ link_to("customers/reportecompras/"~ page.items[0].orbile_customer_id ~"?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("customers/reportecompras/"~ page.items[0].orbile_customer_id ~"?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("customers/reportecompras/"~ page.items[0].orbile_customer_id ~"?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                </div>
            </td>
        </tr>
    </table>
</div>