{{ content() }}
<script type="text/javascript" xmlns="http://www.w3.org/1999/html">
    window.onload = function () {
        var atras = document.getElementById('irAtras');
        atras.addEventListener("click", irAtras);
        function irAtras() { this.style.color = 'red'; window.history.back();}
    }
</script>

<p style="text-align: left; padding-top: 10px;">{{ link_to("customers", "class":"btn btn-primary", "&larr; Regresar", "id":"irAtras") }}</p>


<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="answer-admin">
        <tr>
            <th>ID del cliente</th>
            <th>ID del socio</th>
            <th>ID kobo</th>
            <th>Correo</th>
            <th>Resultado</th>
            <th>Día de registro</th>
            <th>ID socio cliente</th>
            <th>ID cliente orbile</th>
            <th>Tipo de cuenta</th>
        </tr>
    {% for b in c %}
        <tr>
            <td>{{ b.customer_id }}</td>
            <td>{{ b.partner_id }}</td>
            <td>{{ b.kobo_id }}</td>
            <td>{{ b.email }}</td>
            <td>{{ b.result }}</td>
            <td>{{ b.register_date}}</td>
            <td>{{ b.partner_customer_id }}</td>
            <td>{{ b.orbile_customer_id }}</td>
            <td>
                {% if  b.account_type == "P" %}
                    Primaria
                {% elseif b.account_type == "S" %}
                    Secundaria
                {% endif %}

            </td>
        </tr>
    {% endfor %}
    </table>
</div>

{{ link_to("library/getlibrary/" ~ b.kobo_id,"Obtener librería", "class":"btn btn-primary btn-lg") }}
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 {{ link_to("customers/reportecompras/"~ b.orbile_customer_id,"Reporte de compras", "class":"btn btn-primary btn-lg") }}

