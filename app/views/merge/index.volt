{{ content() }}


{{ form ('merge/merge') }}
    <fieldset>
        <div width="100%" style="display: block; overflow: auto;">
            <table align="center" class="detalle table-bordered table-striped">
                <tr>
                    <th><label for="items_per_page">Ingresa cuenta:</label></th>
                    <td>{{ text_field("cuenta", "id":"cuenta") }}</td>
                </tr>
                <tr>
                    <th><label for="items_per_page">Password:</label></th>
                    <td>{{ text_field("contra", "id":"contra") }}</td>
                </tr>
            </table>
            <table align="center">
                <tr>
                    <td colspan="3" style="text-align: right">
                        <ul class="pager">
                            <li class="pull-right">
                                {{ submit_button("Enviar", "class": "btn btn-primary", "id":"ok") }}
                            </li>
                        </ul>
                    </td>
                </tr>
            </table>
        </div>
    </fieldset>
</form>

{#------------------------------INICIA JAVASCRIPT----------------------------#}
<script type="text/javascript">

    $(document).ready(function(){
        $('#ok').on('click', function(){
            $cuenta = $('#cuenta').val();
            $contra = $('#contra').val();

            if($cuenta.trim() == '' || $contra.trim() == '')
            {
                event.preventDefault();
                alert("Requiere ingresar valores en ambos campos: \n 'ISBN' ó el 'ID del libro'");
            }
        });
    });
</script>