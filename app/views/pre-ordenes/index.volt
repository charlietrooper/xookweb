{{ content() }}

<div class="row" align="center">
    <div class="col-sm-12 col-md-12"><label>Buscar por:</label></div>
    <div class="col-sm-12 col-md-4" style="margin-top: 30px;">
        <label>Estatus:</label>
        <select name="estatus" id="estatus">
            <option value="pendiente">Pendiente</option>
            <option value="cancelado">Cancelado</option>
            <option selected="selected" value="todos">Todos</option>
        </select>
    </div>
    <div class="col-sm-12 col-md-4" style="/*border-style: solid; border-width: 1px; border-color: #ddd; height: 127px;*/">
        <label>Fecha de activación por el API: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label><br>
        <p>de:&nbsp;{{date_field('date', 'id':'fecha_ini')}}&nbsp;&nbsp;a:&nbsp;{{date_field('dates', 'id':'fecha_fin')}}</p>
    </div>
    <div class="col-sm-12 col-md-4" style="margin-top: 30px;">
        <label>Correo:</label>
        {{ text_field('mail', 'id':'mail') }}
    </div>
</div>
<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="reemplazar-busqueda">
        <tr><th colspan="12">Registros por página/Total de registros: {{ page.total_items/ page.total_items * page.limit }}/{{ page.total_items }}</th></tr>
        <tr>
            <th>ID de pre-orden</th>
            <th>ID del socio</th>
            <th>ID de compra</th>
            <th>ID del producto</th>
            <th>E-mail del cliente</th>
            <th>Estatus de la preventa</th>
            <th>Fecha de activación de pre-orden</th>
            <th>Fecha en que se compró la pre-orden</th>
            <th>Fecha de activación por el API</th>
            <th>Título del libro</th>
            <th>ISBN</th>
            <th>ID de kobo del cliente</th>
        </tr>
        {% for p in page.items %}
            <tr>
                <td>{{ p.clv_pre_order }}</td>
                <td>{{ p.partner_id }}</td>
                <td>{{ p.purchase_id }}</td>
                <td>
                    <div class="emergente">
                        <div class="ocultar"><?php echo substr($p->product_id,0,5).'...'; ?></div>
                        <div class="mostrar mostrarPID">{{ p.product_id }}</div>
                    </div>
                </td>
                <td>
                    <div class="emergente">
                        <div class="ocultar"><?php echo substr($p->email_customer,0,5).'...'; ?></div>
                        <div class="mostrar mostrarNormal">{{ p.email_customer }}</div>
                    </div>
                </td>
                <td>{{ p.status }}</td>
                <td>
                    <div class="emergente">
                        <div class="ocultar"><?php echo substr($p->fch_order,0,5).'...'; ?></div>
                        <div class="mostrar mostrarDate">{{ p.fch_order }}</div>
                    </div>
                </td>
                <td>
                    <div class="emergente">
                        <div class="ocultar"><?php echo substr($p->fch_pedido,0,5).'...'; ?></div>
                        <div class="mostrar mostrarDate">{{ p.fch_pedido }}</div>
                    </div>
                </td>
                <td>
                    <div class="emergente">
                        <div class="ocultar"><?php echo substr($p->fch_entrega,0,5).'...'; ?></div>
                        <div class="mostrar mostrarDate">{{ p.fch_entrega }}</div>
                    </div>
                </td>
                <td>{{ p.title_product }}</td>
                <td>{{ p.isbn }}</td>
                <td>
                    <div class="emergente">
                        <div class="ocultar"><?php echo substr($p->kobo_id,0,5).'...'; ?></div>
                        <div class="mostrar mostrarKobo">{{ p.kobo_id }}</div>
                    </div>
                </td>
            </tr>
        {% if loop.last %}
            <tr>
                <td colspan="12" align="left">
                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                    <div class="btn-group">
                        {{ link_to("pre-ordenes/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                        {{ link_to("pre-ordenes/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                        {{ link_to("pre-ordenes/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                        {{ link_to("pre-ordenes/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                    </div>
                </td>
            </tr>
        {% endif %}

        {% endfor %}
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('#estatus').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
        });

        $('#mail').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
        });

        $('#fecha_ini').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
        });

        $('#fecha_fin').on('change', function () {
            $pagina_ajax = '';
            Busqueda($pagina_ajax);
        });

        function Busqueda($pagina_ajax) {
            $pagina = $pagina_ajax;
            $estatus = $('#estatus').val();
            $mail = $('#mail').val();
            $fecha_ini = $('#fecha_ini').val();
            $fecha_fin = $('#fecha_fin').val();

            $.get("<?php echo $this->url->get('pre-ordenes/buscar')?>",{'pagina':$pagina, 'estatus':$estatus, 'mail':$mail, 'fecha_ini':$fecha_ini, 'fecha_fin':$fecha_fin},function($data)
            {
                if($data){
                    var html = "<tr><th colspan='12'>Registros por página/Total de registros: " + ~~($data.total_items / $data.total_items * $data.limit) + "/" + $data.total_items + "</th></tr>";
                    html += "<tr>" +
                    "<th>ID de pre-orden</th>" +
                    "<th>ID del socio</th>" +
                    "<th>ID de compra</th>" +
                    "<th>ID del producto</th>" +
                    "<th>E-mail del cliente</th>" +
                    "<th>Estatus de la preventa</th>" +
                    "<th>Fecha de activación de pre-orden</th>" +
                    "<th>Fecha en que se compró la pre-orden</th>" +
                    "<th>Fecha de activación por el API</th>" +
                    "<th>Título del libro</th>" +
                    "<th>ISBN</th>" +
                    "<th>ID de kobo del cliente</th>" +
                    "</tr>";

                    $.each($data.items, function (index, datos){
                        html += '<tr>'+
                        '<td>' + datos.clv_pre_order + '</td>' +
                        '<td>' + datos.partner_id + '</td>' +
                        '<td>' + datos.purchase_id + '</td>' +
                        '<td><div class="emergente"><div class="ocultar">' + (datos.product_id || " ").substring(0, 5) + '...</div><div class="mostrar mostrarPID">' + datos.product_id + '</div></div></td>' +
                        '<td><div class="emergente"><div class="ocultar">' + (datos.email_customer || " ").substring(0, 5) + '...</div><div class="mostrar mostrarNormal">' + datos.email_customer + '</div></div></td>' +
                        '<td>' + datos.status + '</td>' +
                        '<td><div class="emergente"><div class="ocultar">' + (datos.fch_order || " ").substring(0, 5) + '...</div><div class="mostrar mostrarDate">' + datos.fch_order + '</div></div></td>' +
                        '<td><div class="emergente"><div class="ocultar">' + (datos.fch_pedido || " ").substring(0, 5) + '...</div><div class="mostrar mostrarDate">' + datos.fch_pedido + '</div></div></td>' +
                        '<td><div class="emergente"><div class="ocultar">' + (datos.fch_entrega || " ").substring(0, 5) + '...</div><div class="mostrar mostrarDate">' + datos.fch_entrega + '</div></div></td>' +
                        '<td>' + datos.title_product + '</td>' +
                        '<td>' + datos.isbn + '</td>' +
                        '<td><div class="emergente"><div class="ocultar">' + (datos.kobo_id || " ").substring(0, 5) + '...</div><div class="mostrar mostrarKobo">' + datos.kobo_id + '</div></div></td>' +
                        '</tr>';
                    });

                    html += "<tr>" +
                    "<td colspan='12' align='left'>" +
                    "<div class='btn-group'>" +
                    "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>" +
                    "<a data-before='" + $data.before + "' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>" +
                    "<a data-next='" + $data.next + "' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>" +
                    "<a data-last='" + $data.last + "' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>" +
                    "<span class='help-inline'>" + $data.current + "/" + $data.total_pages + "</span>" +
                    "</div>" +
                    "</td>" +
                    "</tr>";
                    $('#reemplazar-busqueda').html(html);
                }
            });
        }

        $(document).on('click','#next', function(){
            $ajax_page = $('#next').data('next');
            Busqueda($ajax_page);
        });
        $(document).on('click','#first', function(){
            $ajax_page = $('#first').data('first');
            Busqueda($ajax_page);
        });
        $(document).on('click','#last', function(){
            $ajax_page = $('#last').data('last');
            Busqueda($ajax_page);
        });
        $(document).on('click','#before', function(){
            $ajax_page = $('#before').data('before');
            Busqueda($ajax_page);
        });
    });
</script>