{{ content() }}
<p style="text-align: left; padding-top: 10px;">{{ link_to("pre-order/index", "class":"btn btn-primary", "&larr; Regresar") }}</p>

<h1>Lista de PreOrder</h1>
{{ form("pre-order/list") }}


<table align="center" id="answer-admin" class="detalle table-bordered table-striped">

    <tr>
        <th>
            <label>ID PREORDER</label>
        </th>
        <th>
            <label>PRODUCTO ID</label>
        </th>
        <th>
            <label>ESTATUS</label>
        </th>
        <th>
            <label>FECHA ORDER</label>
        </th>
        <th>
            <label>FECHA PEDIDO</label>
        </th>
        <th>
            <label>TITULO</label>
        </th>
        <th>
            <label>ISBN</label>
        </th>
        <th>
            <label>ACTIVE REVISION</label>
        </th>
    </tr>
    {% for r in res %}

        <tr>
            <td>
                {{ r.id }}
            </td>
            <td>
                {{ r.product_id }}
            </td>
            <td>
                {{ r.status }}
            </td>
            <td>
                {{ r.fch_order }}
            </td>
            <td>
                {{ r.fch_pedido }}
            </td>
            <td>
                {{ r.title }}
            </td>
            <td>
                {{ r.isbn }}
            </td>
            <td>
                {{ r.act_rev_product }}
            </td>
            <td width="7%">
                {{ link_to("pre-order/cancel/" ~ r.product_id, '<i class="glyphicon glyphicon-remove"></i> CANCELAR', "class": "btn btn-default") }}
            </td>
        </tr>
    {% endfor %}

</table>

<table align="center">
    <tr>
        <td colspan="3" align="right">
            <ul class="pager">

            </ul>
        </td>
    </tr>
    </p>
</table>
</form>