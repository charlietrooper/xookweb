{{ content() }}

{{ form("pre-order/list") }}


<table align="center" id="answer-admin" class="detalle table-bordered table-striped">
    <tr>
        <td>
            <label>PARTNER:</label>
        </td>
        <td>
            <select name="partner">
                <option value="0">-Selecciona un partner-</option>
                {% for p in partner %}
                    <option value="{{ p.partner_id }}">{{ p.name }}</option>
                {% endfor %}
            </select>
        </td>
    </tr>
    <tr>
        <td>
            <label>EMAIL:</label>
        </td>
        <td>
            {{ text_field('email', 'id':'email') }}
        </td>
    </tr>
</table>
<table align="center">
    <tr>
        <td colspan="3" align="right">
            <ul class="pager">
                <li class="pull-right">
                    {{ submit_button("llamar", "class": "btn btn-primary", "id":"ok") }}
                </li>
            </ul>
        </td>
    </tr>
    </p>
</table>
</form>

{#-----------------------------------JAVASCRIPT----------------------------------------------------#}
<script type="text/javascript">
    $(document).ready(function(){

        $('#ok').on('click', function(){
            $email = $('#email').val();

            if($email.trim() == '')
            {
                event.preventDefault();

                alert('EL campo email no puede estar vacio');
            }
        });

    });
</script>