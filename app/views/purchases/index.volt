{{ content() }}
<div class="titulo" >
    <h1 class="retroshadow">Verificar compra</h1>'
</div>
{{ form('purchases/verification') }}
<p>{{ submit_button("Autocompletar", "class": "btn btn-primary", "id":"auto-llenar") }}</p>
</li>
<div width="100%" style="display: block; overflow: auto; margin-top: 30px; " align="center">
    <table align="center" class="detalle table-bordered table-striped">
        <tr>
            <th><label>ID de usuario:</label></th>
            <td>{{ text_field('user_id', 'id':'user_id', 'value':k) }}</td>
        </tr>
        <tr>
            <th><label>Ciudad:</label></th>
            <td>{{ text_field('city', 'id':'city') }}</td>
        </tr>
        <tr>
            <th><label>País:</label></th>
            <td>{{ text_field('country', 'id':'country') }}</td>
        </tr>
        <tr>
            <th><label>Provincia del estado:</label></th>
            <td>{{ text_field('state', 'id':'state') }}</td>
        </tr>
        <tr>
            <th><label>Código postal:</label></th>
            <td>{{ text_field('postal', 'id':'postal') }}</td>
        </tr>
        <tr>
            <th><label>ID del producto:</label><br></th>
            <td>{{ text_area('product_id', 'cols': 40, 'rows': 1, 'id':'product') }}</td>
        </tr>
        <tr>
            <td colspan="3" style="text-align: right;">{{ submit_button('Enviar', 'class': 'btn btn-primary', 'id':'ok') }}</td>
        </tr>
    </table>
</div>
</form>
<br>
<p>
    <button id="libros" type="button" class="btn btn-info btn-xs">
        <span class="glyphicon glyphicon-eye-open"> Ver libros</span>
    </button>
</p>
<div>
    <table id="mostrar" align="center" class="detalle table-bordered table-striped"></table>
</div>

{#------------------------------INICIA JAVASCRIPT--------------------------------------------------#}
<script type="text/javascript">
    $(document).ready(function()
    {
        $('#ok').on('click', function(){
            $id = $('#user_id').val();
            $product = $('#product').val();
            $city = $('#city').val();
            $country = $('#country').val();
            $state = $('#state').val();
            $postal = $('#postal').val();

            $letter = /^[a-zA-Z]{1,}$/;
            $NUM = /^[0-9]{1,}$/;
            var mensaje = [];
            var mens = [];

            mensaje.push('Los siguientes campos estan vacios o son incorrectos:\n');
            mens.push('Los siguientes campos estan vacios o son incorrectos:\n');

            if($id.trim()=='')
            {
                mensaje.push('-El campo USER_ID esta vacio \n');
            }
            if($product.trim()=='')
            {
                mensaje.push('-El campo PRODUCT_ID esta vacio \n');
            }
            if($city.trim()=='')
            {
                mensaje.push('-El campo CITY esta vacio \n');
            }
            if(!($letter.test($city)))
            {
                mensaje.push('-El campo CITY solo permite letras\n')
            }
            if($country.trim()=='')
            {
                mensaje.push('-El campo COUNTRY esta vacio\n');
            }
            if(!($letter.test($country)))
            {
                mensaje.push('-El campo COUNTRY solo permite letras\n')
            }
            if($state.trim()=='')
            {
                mensaje.push('-El campo STATE PROVINCE esta vacio\n');
            }
            if(!($letter.test($state)))
            {
                mensaje.push('-El campo STATE PROVINCE solo permite letras\n');
            }
            if($postal.trim()=='')
            {
                mensaje.push('-El campo ZIP CODE POSTAL no puede estar vacia\n');
            }
            if(!($NUM.test($postal)))
            {
                mensaje.push('-El campo ZIP CODE POSTAL solo permite numeros \n');
            }
            if(mensaje.length != mens.length)
            {
                event.preventDefault();
                alert(mensaje);
            }
        });

        //
        $('#auto-llenar').on('click', function(){

            Autollenado();
        })
        //Funcion para autollenar campos
        function Autollenado()
        {
            event.preventDefault();

            //$('#product').val('C86DA6BA-2513-4037-BC2C-01BA5213AEBB');
            $('#city').val('MX');
            $('#country').val('MX');
            $('#state').val('MX');
            $('#postal').val('06100');

        }

        $('#libros').on('click', function(){

            var product = $('#product').val();
            var productos = product.split(",");
            var num_elementos = productos.length;
            //alert(productos[0]);
            //alert(num_elementos);

            $.get("<?php echo $this->url->get('purchases/detalles')?>", {
                'productos': productos,
                'num_elementos': num_elementos
            }, function ($data) {

                var tabla = '<tr>'+
                        '<td>ID del producto</td>'+
                        '<td>Isbn</td>'+
                        '<td>Título</td>'+
                        '<td>Autor</td>'+
                        '</tr>';
                var lib = $data;

               $.each(lib, function(index, datos) {
                   tabla += '<tr>'+
                   '<td>'+datos['id_actrev']+'</td>'+
                   '<td>'+datos['isbn']+'</td>'+
                   '<td>'+datos['titulo']+'</td>'+
                   '<td>'+datos['autor']+'</td>'+
                   '</tr>';

                });
                $('table#mostrar').html(tabla);
            });
        });
    });
</script>