{{ content() }}
<div class="titulo" >
    <h1 class="retroshadow">Compra</h1>'
</div>
{{ form('purchases/purchase') }}
        <label>ID de compra:</label>
        {{ text_field('purchase', 'id':'purchase') }}
        <br>
        <label>ID kobo usuario:</label>
        {{ text_field('user_id', 'value':kobo, 'id':'user') }}

        {#---tabla del listado de los libros seleccioados---#}
<div width="100%" style="display: block; overflow: auto; font-size: small">
    <table class="table table-bordered table-striped">
        <tr>
            <th></th>
            <th style="text-align: center">{{ submit_button("Autocompletar", "class": "btn btn-primary", "id":"auto-llenar1") }}</th>
            <th></th>
            <th></th>
            <th></th>
            <th style="text-align: center">Precio</th>
            <th colspan="3" style="text-align: center">Descuentos</th>
            <th colspan="2" style="text-align: center">Tax</th>
            <th></th>
        </tr>
        <tr>
            <th></th>
            <th style="text-align: center">Product id</th>
            <th style="text-align: center">Isbn</th>
            <th style="text-align: center">Titulo</th>
            <th style="text-align: center">Autor</th>
            <th style="text-align: center">Código de moneda</th>
            <th style="text-align: center">Código de moneda</th>
            <th style="text-align: center">Descuento aplicado</th>
            <th style="text-align: center">Precio antes del tax</th>
            <th style="text-align: center">Cantidad de tax</th>
            <th style="text-align: center">Nombre del tax</th>
            <th style="text-align: center">Descartar libro</th>
        </tr>
        {% for name, value in libros  %}
                <tr>
                    <td style="text-align: center; width: 10px;">{{ name + 1 }}</td>
                    <td style="text-align: center; width: 160px;"><input type="text" name="id{{ name }}" id="id" style="width: 150px;" value="{{ value['id_actrev'] }}"></td>
                    <td>{{ value['isbn'] }}</td>
                    <td>{{ value['titulo'] }}</td>
                    <td>{{ value['autor'] }}</td>
                    <td style="text-align: center; width: 60px;"><input type="text" name="currency_code{{ name}}" id="currency_code{{ name }}" style="width: 50px;"></td>
                    <td style="text-align: center; width: 60px;"><input type="text" name="currency2_code{{ name}}" id="currency2_code{{ name }}" style="width: 50px;"></td>
                    {{ hidden_field('descuento', 'id':'d'~name, 'value': value['descuento']) }}<td style="text-align: center; width: 60px;"><input type="text" name="discount_applied{{ name}}" id="discount_applied{{ name }}" style="width: 50px;"></td>
                    {{ hidden_field('precio', 'id':'h'~name, 'value': value['precio']) }}<td style="text-align: center; width: 90px;"><input type="text" name="price{{ name}}" id="price{{ name }}" style="width: 50px;"></td>
                    {{ hidden_field('tax', 'id':'t'~name, 'value': value['tax']) }}<td style="text-align: center; width: 60px;"><input type="text" name="amount_tax{{ name}}" id="amount_tax{{ name }}" style="width: 50px;"></td>
                    <td style="text-align: center; width: 60px;"><input type="text" name="name_tax{{ name }}" id="name_tax{{ name }}" style="width: 50px;"></td>
                    <td><input type="button" value="Descartar" onclick="Eliminar(this.parentNode.parentNode.rowIndex)"/></td>
                </tr>
        {% endfor %}
    </table>
    <table class="table table-bordered table-striped">
        <tr>
            <th width="50%" style="text-align: right"> Método de pago</th>
            <td>{{  text_field('method', 'id':'method') }}</td>
        </tr>
    </table>
    <table class="table table-bordered table-striped">
        <tr><th colspan="2" style="text-align: center">Dirección</th></tr>
        <tr>
            <th  width="50%" style="text-align: right">Ciudad:</th>
            <td>{{ text_field('city', 'value':city, 'readonly':'true') }}</td>
        </tr>
        <tr>
            <th  width="50%" style="text-align: right">País:</th>
            <td>{{ text_field('country', 'value':country, 'readonly':'true') }}</td>
        </tr>
        <tr>
            <th  width="50%" style="text-align: right">Provincia de estado:</th>
            <td>{{ text_field('state', 'value':state, 'readonly':'true') }}</td>
        </tr>
        <tr>
            <th  width="50%" style="text-align: right">Código postal:</th>
            <td>{{ text_field('zip', 'value':zip, 'readonly':'true') }}</td>
        </tr>

    </table>
</div>
        {{ submit_button('Comprar', 'class': 'btn btn-primary', 'id':'ok') }}
</form>


{#------------------------------INICIA JAVASCRIPT--------------------------------------------------#}
<script type="text/javascript">
    function Eliminar (i) {
        document.getElementsByTagName("table")[0].setAttribute("id","tableid");
        document.getElementById("tableid").deleteRow(i);
    }
    $(document).ready(function(){

        $('#ok').on('click', function(){

            $GUID = /^[0-9a-zA-Z]{8}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{4}-[0-9a-zA-Z]{12}$/;

            $product_uno = $('#id').val();

            var mensaje = [];
            var mens = [];

            mensaje.push('Los siguientes campos tienen errores:\n');
            mens.push('Los siguientes campos tienen errores:\n');

           if(!($GUID.test($product_uno)))
           {
            mensaje.push('-Formato es xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx');
           }
            if(mensaje.length != mens.length)
            {
                event.preventDefault();
                alert(mensaje);
            }

        });

        //
        $('#auto-llenar1').on('click', function(){

            Autollenado1();
        })

        //Funcion para autollenar campos
        function Autollenado1()
        {
            var num_elementos = <?php echo json_encode($num_elementos) ?>;
            $i = 0;
            for($i = 0; $i < num_elementos; $i++)
            {
                $precio = $('#h'+$i).val();
                $descuento = $('#d'+$i).val();
                $tax = $('#t'+$i).val();

                event.preventDefault();
                $('#currency_code'+$i).val('MXN');
                $('#currency2_code'+$i).val('MXN');
                $('#discount_applied'+$i).val($descuento);
                $('#price'+$i).val($precio);
                $('#amount_tax'+$i).val($tax);
                $('#method').val('CREDITO');
            }
        }
    });
</script>