{{ content() }}
{{ titulo }}
{# DIV PARA MANTENER EL CONTENIDO O EL RESULTADO DEL JSON #}

{#
<div align="center" style="padding: 10px;">
    <label>ID compra socio:</label><br/>
    {{ text_field('pactual', 'id':'pactual') }}
</div>
#}
<div class="col-md-12">
    <label style="text-align: center; margin-bottom:15px;">Buscar por:</label>
</div>
<div class="row" style="padding-bottom: 30px">


    <div class="col-md-6">
        <label>Título:</label>
        {{ text_field('actualTitulo', 'id':'actualTitulo') }}
        {{submit_button("Buscar", 'class':'search-titulo')}}
    </div>
    <div class="col-md-6">
        <label>ISBN:</label>
        {{ text_field('actualISBN', 'id':'actualISBN') }}
        {{submit_button("Buscar", 'class':'search-isbn')}}
    </div>

</div>

<div class="col-md-12">
    <label style="text-align:center; margin-bottom:15px;">Filtrar por precio:</label>
</div>
<div class="row" style="padding-bottom: 30px">
    <div class="col-md-6">
        <label style="color:#666">Precio de lista:</label>
        <br>
        <select id="slc_actualLista">
            <option value="" selected>--</option>
            <option value="1">$0 - $50</option>
            <option value="2">$50 - $100</option>
            <option value="3">$100 - $200</option>
            <option value="4">$200 - $500</option>
            <option value="5">$500 - </option>
        </select>
        <br><div style="margin-top:10px;">{{submit_button("Buscar", 'class':'search-PAL')}}</div>
    </div>
    <div class="col-md-6">
        <label style="color:#666">Precio de venta:</label>
        <br>
        <select id="slc_actualVenta">
            <option value="" selected>--</option>
            <option value="1">$0 - $50</option>
            <option value="2">$50 - $100</option>
            <option value="3">$100 - $200</option>
            <option value="4">$200 - $500</option>
            <option value="5">$500 - </option>
        </select>
        <br><div style="margin-top:10px;">{{submit_button("Buscar", 'class':'search-PAV')}}</div>
    </div>
</div>

<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="tablita">
        <tr>
            <th>Título</th>
            <th>ISBN</th>
            <th>Precio de lista</th>
            <th>Precio de venta</th>
            <th>COGS</th>
            <th>Pre Orden</th>

        </tr>
        <?php
        $cuenta=0;
        ?>
        {% for e in pactual.items['Items'] %}
            <tr>
                <td style="width:50%;">{{ e['title'] }}</td>
                <td class="fitCelda">{{ link_to("libros/enviarlibro?isbn=" ~ e['isbn'], e['isbn'],'target': '_blank') }}</td>
                <td>{{ "$ "~ e['listprice'] }}</td>
                <td>{{ "$ "~ e['sellingprice'] }}</td>
                <td class="fitCelda">{{ "$ "~ e['cogs'] }}</td>
                <td>
                    {% if  e['preorder'] == "false" %}
                    No Disponible
                    {% elseif  e['preorder'] == null %}
                        No Disponible
                    {% elseif e['preorder'] == "true" %}
                    Disponible
                    {% endif %}
                </td>

            </tr>
            <?php
                 $cuenta++;
            ?>
        {% endfor %}
        <tr>
            <td colspan="2" align="left">
                <span class="help-inline">{{ pactual.actual }}/{{ pactual.paginasTotales }}</span>
                <div class="btn-group">
                    {{ link_to("precio-actual/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                    {{ link_to("precio-actual/index?page=" ~ pactual.anterior, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("precio-actual/index?page=" ~ pactual.siguiente, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("precio-actual/index?page=" ~ pactual.ultima, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                </div>
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function(){

        $('.search-titulo').on('click',function(){
            $pactual = $('#actualTitulo').val();
            if($pactual == '')
            {
                var pagina = "/precio-actual/index";
                location.href = location.origin + pagina;
            }
            document.getElementById('actualISBN').value = "";
            document.getElementById('slc_actualLista').value = "";
            document.getElementById('slc_actualVenta').value = "";
            $pagina_ajax = 1;
            Busqueda($pagina_ajax);
        });

        $('.search-isbn').on('click',function(){
            $pactual = $('#actualISBN').val();
            if($pactual == '')
            {
                var pagina = "/precio-actual/index";
                location.href = location.origin + pagina;
            }
            document.getElementById('actualTitulo').value = "";
            document.getElementById('slc_actualLista').value = "";
            document.getElementById('slc_actualVenta').value = "";
            $pagina_ajax = 1;
            Busqueda($pagina_ajax);
        });

        //BUSQUEDA POR PRECIO ACTUAL DE LISTA
        $('.search-PAL').on('click',function(){
            $actLis = $('#slc_actualLista').val();
            if($actLis== 0)
            {
                var pagina = "/precio-actuak/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            document.getElementById('actualTitulo').value = "";
            document.getElementById('actualISBN').value = "";
            document.getElementById('slc_actualVenta').value = "";
            Busqueda($pagina_ajax);
        });

        //BUSQUEDA POR PRECIO ACTUAL DE VENTA
        $('.search-PAV').on('click',function(){
            $actVen = $('#slc_actualVenta').val();
            if($actVen== 0)
            {
                var pagina = "/precio-actuak/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            document.getElementById('actualTitulo').value = "";
            document.getElementById('actualISBN').value = "";
            document.getElementById('slc_actualLista').value = "";
            Busqueda($pagina_ajax);
        });

        function traducir(preorden)
        {
            $traduccion='';
            if (preorden=="true")
            {
                $traduccion='Disponible';
            }

            else if(preorden=="false"|| preorden==null)
            {
                $traduccion='No Disponible';
            }

            return $traduccion;
        }

        function obtenerBusquedaID()
        {
            $ID=0;
            $actualTitulo = $('#actualTitulo').val();
            $actualISBN = $('#actualISBN').val();
            $pActualLista = $('#slc_actualLista').val();
            $pActualVenta = $('#slc_actualVenta').val();

            if ($actualTitulo!=="")
            {
                $ID=1;
            }

            else if ($actualISBN!=="")
            {
                $ID=2;
            }
            else if ($pActualLista!=="")
            {
                $ID=3;
            }
            else if ($pActualVenta!=="")
            {
                $ID=4;
            }

            return $ID;
        }

        function Busqueda($pagina_ajax){
            $actualTitulo = $('#actualTitulo').val();
            $actualISBN = $('#actualISBN').val();
            $pActualLista = $('#slc_actualLista').val();
            $pActualVenta = $('#slc_actualVenta').val();
            $busquedaID=obtenerBusquedaID();

            $.get("<?php echo $this->url->get('precio-actual/busqueda')?>",
                    {
                        'actualTitulo':$actualTitulo,
                        'actualISBN':$actualISBN,
                        'pActualLista':$pActualLista,
                        'pActualVenta':$pActualVenta,
                        'busquedaID':$busquedaID,
                        'pagina':$pagina_ajax},function($data)
            {

                if($data)
                {

                    var tab = "<tr><th>Título</th><th>ISBN</th><th>Precio de lista</th><th>Precio de venta</th><th>COGS</th><th>Pre Orden</th></tr>";
                    if($actualISBN!=='')
                    {
                        $.each($data.items.Items.Item, function (index, datos) {
                            tab += "<tr>"+
                            "<td style='width:50%;'>"+datos.title+"</td>"+
                            '<td><a target="_blank" href="../libros/enviarlibro?isbn='+datos.isbn+'">'+datos.isbn+'</a></td>'+
                            "<td style='white-space:nowrap;'> $ "+datos.listprice+"</td>"+
                            "<td style='white-space:nowrap;'> $ "+datos.sellingprice+"</td>"+
                            "<td> $"+datos.cogs+"</td>"+
                            "<td>"+traducir(datos.preorder)+"</td>"+
                            "</tr>";
                        });
                    }

                    else
                    {
                        $.each($data.items.Items, function (index, datos) {
                            tab += "<tr>"+
                            "<td style='width:50%;'>"+datos.title+"</td>"+
                            '<td><a target="_blank" href="../libros/enviarlibro?isbn='+datos.isbn+'">'+datos.isbn+'</a></td>'+
                            "<td style='white-space:nowrap;'> $ "+datos.listprice+"</td>"+
                            "<td style='white-space:nowrap;'> $ "+datos.sellingprice+"</td>"+
                            "<td> $"+datos.cogs+"</td>"+
                            "<td>"+traducir(datos.preorder)+"</td>"+
                            "</tr>";
                        });
                    }

                    tab += "<tr>"+
                    "<td colspan='2' align='left'>"+
                    "<span class='help-inline'>"+$data.actual +"/"+ $data.paginasTotales +"</span>"+
                    "<div class='btn-group'>"+
                    "<a data-first='1' id='primera' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                    "<a data-before='" + $data.anterior + "' id='anterior' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                    "<a data-next='" + $data.siguiente + "' id='siguiente' class= 'btn'><i class='icon-step-backward'></i> Siguiente</a>"+
                    "<a data-last='" + $data.ultima + "' id='ultima' class= 'btn'><i class='icon-step-backward'></i> Última</a>"+
                    "</div>"+
                    "</td>"+
                    "</tr>";
                    $('table#tablita').html(tab);
                }
            });
        }

        $(document).on('click','#siguiente', function(){
            $pagina_ajax = $('#siguiente').data('next');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#primera', function(){
            $pagina_ajax = $('#primera').data('first');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#ultima', function(){
            $pagina_ajax = $('#ultima').data('last');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#anterior', function(){
            $pagina_ajax = $('#anterior').data('before');
            Busqueda($pagina_ajax);
        });

        //PARTE PARA OCULTAR O MOSTRAR EL JSON
        $('#show').click(function(){

            $('#mostrar').show();
            $('#hidden').show();
            $('#show').hide();

        });

        $('#hidden').click(function(){
            $('#mostrar').hide();
            $('#hidden').hide();
            $('#show').show()


        });
    });
</script>