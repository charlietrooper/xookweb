{{ content() }}

<?php

     $arrGenesp=[];
     foreach ($generos as $gen)
     {
      $test2=mb_convert_encoding($gen['genero'], "UTF-8");
      array_push($arrGenesp,$test2);

     }

?>

{{ form('libros-gratuitos/busqueda', 'method': 'get') }}
<div class="row" style="margin-top:40px; padding-bottom: 30px">

    <div class="col-md-4">
        <label for="elementos">Selecciona la cantidad de libros a mostrar:</label>&nbsp;
        {{ text_field('elementos', 'id':'elementos', 'style':'width: 50px') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </div>

    <div class="col-md-4">
        <label>Género:</label>
        <select name="genero" id="genero" style="width: 80%;">
            <option value="TODOS"  selected="selected">Todos</option>
            <?php $conteo=0;?>
            {% for g in arrGenesp %}
                {% if g!='' %}
                    <option value="{{ g }}">{{ generosPrint[conteo] }}</option>
                {% else %}
                    <option value="{{ 'NULL' }}">{{ "No definido" }}</option>
                {% endif %}
                <?php $conteo++;?>
            {%  endfor %}
        </select>
    </div>
    <div class="col-md-4">
        <label>Idioma:</label>
        <select name="language" id="language" style="width: 80%;">
            <option value="TODOS"  selected="selected">Todos</option>
            <option value="en"  selected="selected">Inglés</option>
            <option value="es"  selected="selected">Español</option>
        </select>
    </div>

</div>


</div>

<div align="center">
    <p><br/>{{ submit_button('Listar', 'id':'ok', 'class':'btn btn-primary') }}</p>
</div>
{{ end_form() }}

<script type="text/javascript">
    $(document).ready(function(){
        $('#ok').on('click', function(){
            // event.preventDefault();

            var mensaje = [];
            var mens = [];

            mens.push('Los siguientes campos son incorrectos o estan vacios:\n');
            mensaje.push('Los siguientes campos son incorrectos o estan vacios:\n');

            $elementos = $('#elementos').val();
            $generos=$('#genero').val();

            $numeros = /^([0-9])*$/;

            if($elementos.trim()=='')
            {
                mensaje.push('-Debe elegir el numero de elementos a mostrar\n');
            }
            if(!($numeros.test($elementos)))
            {
                mensaje.push('-Solo se deben ingresar números en el primer campo\n');
            }

            if(mensaje.length != mens.length)
            {
                event.preventDefault()
                alert(mensaje);
            }
        })
    });
</script>
