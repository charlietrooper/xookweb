{{ content() }}


<table class="table table-bordered table-striped" align="center">
    <tr>
        <th>ID</th>
        <th>Llave</th>
        <th>Valor</th>
        <th>Edit</th>
    </tr>
{% for obj in url %}
    <tr>
        <td>{{obj.id_url}}</td>
        <td>{{obj.key}}</td>
        <td>{{obj.value}}</td>
        <td width="7%">{{ link_to("config/edit/" ~ obj.id_url, '<i class="glyphicon glyphicon-edit"></i> Editar', "class": "btn btn-default") }}</td>
    </tr>
{% endfor %}
</table>