{{ content() }}
<p style="text-align: left; padding-top: 10px;">{{ link_to("config", "class":"btn btn-primary", "&larr; Regresar") }}</p>

<h1 align="center">Editar el valor</h1>

{{ form("config/configura") }}

    <fieldset>
        {% for c in conf %}
        <p><label>ID:&nbsp&nbsp</label>{{ c.id_url }}</p>
            {{ text_field('id_url', 'value':c.id_url, 'hidden':true) }}
        <p><label>Llave:&nbsp&nbsp</label>{{ c.key }}</p>
            {{ text_field('key', 'value':c.key, 'hidden':true) }}
        <p><label>Valor:&nbsp</label>
        {{ text_field('value', 'value':c.value) }}</p>
        {% endfor %}
    </fieldset>
<ul class="pager">
    <li class="pull-right">
        {{ submit_button("Update", "class": "btn btn-primary") }}
    </li>
</ul>
</form>


