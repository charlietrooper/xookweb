
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        {{ get_title() }}
        {{ stylesheet_link('css/bootstrap.min.css') }}
        {{ stylesheet_link('css/smartadmin-production-plugins.min.css') }}
        {{ stylesheet_link('css/estilo.css?v=24022016') }}


        {# css para jsonHuman #}
        {{ stylesheet_link('css/demo.css') }}
        {{ stylesheet_link('css/json.human.css') }}
        {{ stylesheet_link('css/codemirror.css') }}
        {# terminan css para jsonHuman #}

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Xook Web">
        <meta name="author" content="Xook Team">



        {{ javascript_include('js/jquery-ajax.min.js') }}

        <!-- IMPORTANT: APP CONFIG -->
        {{ javascript_include('js/app.config.js') }}

        <!-- BOOTSTRAP JS -->
        {{ javascript_include('js/bootstrap.min.js') }}

        <!-- MAIN APP JS FILE -->
        {{ javascript_include('js/app.min.js') }}

        {{ javascript_include('js/utils.js') }}

        <!-- SmartChat UI : plugin -->
        {{ javascript_include('js/smart-chat-ui/smart.chat.manager.min.js') }}

        <!-- PAGE RELATED PLUGIN(S)-->
        {{ javascript_include('js/plugin/chartjs/chart.min.js') }}


        <!--------------incluidas pero pueden cambiar------------------>

        {# scripts para jsonHuman #}
        {{ javascript_include('js/codemirror.min.js', true) }}
        {{ javascript_include('js/json.human.js', true) }}
        {# terminan scripts para jsonHuman #}

    </head>
    <body>

        {{ content() }}


    </body>
</html></html>

