
{{ content() }}

<div class="jumbotron">
    <h1>Pagina no encontrada</h1>
    <p>Disculpe, ha accedido a una página que ya no existe o ha sido movida</p>
    <p>{{ link_to('index', 'Inicio', 'class': 'btn btn-primary') }}</p>
</div>