
{{ content() }}

<div class="jumbotron">
    <h1>No autorizado</h1>
    <p>Usted no tiene acceso a esta opción. Contacte al administrador</p>
    <p>{{ link_to('index', 'Inicio', 'class': 'btn btn-primary') }}</p>
</div>