
{{ content() }}

<div class="jumbotron">
    <h1>Error interno</h1>
    <p>Algo salió mal, si continúa el error por favor en contáctenos</p>
    <p>{{ link_to('index', 'Inicio', 'class': 'btn btn-primary') }}</p>
</div>