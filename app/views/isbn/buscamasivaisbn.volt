{{ content() }}

<div style="padding:10px;">
<td colspan="4"><a href="/isbn/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a></td>
</div>

<div width="100%" style="display: block; overflow: auto; margin-top:20px;">
    <table class="table table-bordered table-striped" align="center" id="tablita">
        <tr>
            <th>Título</th>
            <th>ISBN</th>
            <th>Autor</th>
            <th>ID</th>
        </tr>
        {# <?php var_dump($isbnBusq);?>#}
        <?php $arrayBusqueda=[];?>
         {% for i in 0..(isbnBusq|length-1) %}
            {% if isbnBusq[i]['Items']==null %}
                <tr>
                    <td><strong>{{ "No Encontrado" }}</strong></td>
                    <td><strong>{{ isbnsArray[i] }}</strong></td>
                    <td><strong>{{ "No Encontrado" }}</strong></td>
                    <td><strong>{{ "No Encontrado" }}</strong></td>
                </tr>
            {% endif %}

                 {% for e in isbnBusq[i]['Items'] %}
                     {#<?php var_dump($e['isbn']); ?>#}
                     <tr>
                         <td>
                             {% if e['activeRevision']['title']==null  %}
                             <strong style="color:red;">ISBN no encontrado</strong>
                             {% else %}
                                 {{  e['activeRevision']['title']}}
                             {% endif %}

                         </td>
                         <td>
                             {% if e['isbn']==null  %}
                                 <strong style="color:red;"> {{  e['isbn']}}</strong>
                             {% else %}
                                 {{  e['isbn']}}
                             {% endif %}
                         </td>
                         <td>
                             {% if e['activeRevision']['contibutors']['contributor']['@value']==null AND e['activeRevision']['title']==null %}
                                 <strong style="color:red;">ISBN no encontrado</strong>
                             {% elseif e['activeRevision']['contibutors']['contributor']['@value']==null %}
                                 <strong>-</strong>
                             {% else %}
                                 {{  e['activeRevision']['contibutors']['contributor']['@value']}}
                             {% endif %}
                         </td>
                         <td>
                             {% if e['activeRevision']['@attributes']['id']==null  %}
                                 <strong style="color:red;">ISBN no encontrado</strong>
                             {% else %}
                                 {{  e['activeRevision']['@attributes']['id']}}
                             {% endif %}
                         </td>
                     </tr>
                 {% endfor %}



        {% endfor %}
       {# <tr>
            <td colspan="2" align="left">
                <span class="help-inline">{{ busq.actual }}/{{ busq.paginasTotales }}</span>
                <div class="btn-group">
                    {{ link_to("isbn/buscamasivaisbn", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                    {{ link_to("isbn/buscamasivaisbn?page=" ~ busq.anterior, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("isbn/buscamasivaisbn?page=" ~ busq.siguiente, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("isbn/buscamasivaisbn?page=" ~ busq.ultima, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                </div>
            </td>
        </tr>#}

    </table>
</div>
{#
<script type="text/javascript">
    $(document).ready(function(){
        window.onload = function(){
            Busqueda();
        };

        function Busqueda(){
            $isbn="9780730312703";
            $.get("<?php echo $this->url->get('isbn/busqueda')?>",{'isbn':$isbn},function($data)
            {

                if($data)
                {

                    var tab = "<tr><th>Título</th><th>ISBN</th></tr>";

                        $.each($data.items.Items, function (index, datos) {
                            tab += "<tr>"+
                            "<td>"+datos.title+"</td>"+
                            "<td>"+datos.isbn+"</td>"+
                            "</tr>";
                        });


                    $('table#tablita').html(tab);
                }
            });
        }


    });
</script>#}