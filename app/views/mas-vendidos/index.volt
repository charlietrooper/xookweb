{{ content() }}
<?php //var_dump($arrGeneros); ?>

<?php
     $arrGenesp=[];
     foreach ($generos as $gen)
     {
      $test2=mb_convert_encoding($gen['genero'], "UTF-8");
      array_push($arrGenesp,$test2);

     }

?>
{{ form('mas-vendidos/busqueda', 'method': 'get') }}
    <div class="row" style="padding-bottom: 30px">
        <div class="col-md-6">
            <label for="elementos">Selecciona el top de libros a mostrar:</label>&nbsp;
            {{ text_field('elementos', 'id':'elementos', 'style':'width: 50px') }}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <div class="col-md-6">
            <label>Fecha:</label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;de:&nbsp;
            {{date_field('fecha1', 'id':'fecha_ini')}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            a:&nbsp;{{date_field('fecha2', 'id':'fecha_fin')}}

        </div>
    </div>


        <div class="row" style="padding-bottom: 30px">
            <div class="col-md-2">

            </div>

            <div class="col-md-3">
                <label>Género:</label>
                <select name="genero" id="genero" style="width: 80%;">
                    <option value="TODOS"  selected="selected">Todos</option>
                    <?php $conteo=0;?>
                    {% for g in arrGenesp %}
                    {% if g!='' %}
                            <option value="{{ g }}">{{ generosPrint[conteo] }}</option>
                        {% else %}
                            <option value="{{ 'NULL' }}">{{ "No definido" }}</option>
                        {% endif %}
                        <?php $conteo++;?>
                    {%  endfor %}
                </select>
            </div>

            <div class="col-md-2">

            </div>

            <div class="col-md-3">
                <label>Idioma:</label>
                <select name="idioma" id="idioma" style="width: 80%;">
                    <option value="TODOS"  selected="selected">Todos</option>
                    {% for var in idiomas %}
                        {% if var.idioma !== NULL %}
                        <option value="{{ var.idioma }}">{{ var.idioma }}</option>
                    {% endif %}
                    {%  endfor %}
                </select>
            </div>

            <div class="col-md-2">

            </div>

        </div>


    </div>

    <div align="center">
    <p><br/>{{ submit_button('Listar', 'id':'ok', 'class':'btn btn-primary') }}</p>
    </div>
{{ end_form() }}

<script type="text/javascript">
    $(document).ready(function(){
        $('#ok').on('click', function(){
            // event.preventDefault();

            var mensaje = [];
            var mens = [];

            mens.push('Los siguientes campos son incorrectos o estan vacios:\n');
            mensaje.push('Los siguientes campos son incorrectos o estan vacios:\n');

            $elementos = $('#elementos').val();
            $fecha_ini = $('#fecha_ini').val();
            $fecha_fin =$('#fecha_fin').val();
            $generos=$('#genero').val();
            $idioma=$('#idioma').val();

            $numeros = /^([0-9])*$/;

            if($elementos.trim()=='')
            {
                mensaje.push('-Debe elegir el numero de elementos a mostrar\n');
            }
            if(!($numeros.test($elementos)))
            {
                mensaje.push('-Solo se deben ingresar números en el primer campo\n');
            }
            if($fecha_ini.trim()=='')
            {
                mensaje.push('-Debe seleccionar una fecha de inicio \n');
            }
            if($fecha_fin.trim()=='')
            {
                mensaje.push('-Debe seleccionar una fecha final \n');
            }

            if(mensaje.length != mens.length)
            {
                event.preventDefault()
                alert(mensaje);
            }
        })
    });
</script>