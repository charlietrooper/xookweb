{{ content() }}
<?php //var_dump($test); ?>
{#<p>--------</p>#}
<?php //var_dump($test2); ?>
<button style="margin-bottom:12px;" id="hideChart" class="btn btn-primary">Ver Gráfica</button>
<div id="chart_div" style="display:none;">
</div>
<div class="row" xmlns="https://www.w3.org/1999/html">
    <div class="col-md-12" style="padding-bottom: 10px;">

        <p>De: {{ fecha1 }}  &nbsp;a: {{ fecha2 }}</p>

                {{ link_to("mas-vendidos/index", "class":"btn btn-primary","&larr; Regresar") }}
    </div>
</div>
<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="busqueda" >
        <tr><th colspan="12">Registros por página/Total de registros: {{ page.total_items/ page.total_items * page.limit }}/{{ page.total_items }} </th></tr>
        <tr>

        </tr>
        <tr>
            <th style="text-align: center">Unidades vendidas</th>
            <th style="text-align: center">Título del libro</th>
            <th style="text-align: center">ISBN</th>
            <th style="text-align: center">Active Revision ID</th>
            <th style="text-align: center">Género</th>
            <th style="text-align: center">Idioma</th>
        </tr>

        {% for lib in page.items %}
        <tr>
            <td align="center">{{ lib['veces_vendido'] }}</td>
            <td align="center">
                {% if lib['titulo']!== NULL %}
                    {{ link_to("library/getbookrev/" ~ lib['product_id'], lib['titulo'],'target': '_blank') }}
                {% else %}
                    {{ "Libro inactivo" }}
                {% endif %}
            </td>
            <td align="center">
                {% if lib['isbn']!== NULL %}
                    {{ lib['isbn'] }}
                {% else %}
                    {{ "-" }}
                {% endif %}

            </td>
            <td align="center">{{ lib['product_id'] }}</td>
            <td align="center">
                {% if lib['genero']!== NULL %}
                    {{ lib['genero'] }}
                {% else %}
                    {{ "-" }}
                {% endif %}
            </td>
            <td align="center">
                <?php //$idLC=strtolower($lib['idioma']); ?>
                {% if lib['idioma']!== NULL %}
                    {{ lib['idioma'] }}
                {% else %}
                    {{ "-" }}
                {% endif %}
            </td>
        </tr>
        {% endfor %}
        <tr>
            <td colspan="6" align="left">
                <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                <div class="btn-group">
                    {{ link_to("mas-vendidos/busqueda?elementos="~ elementos ~"&fecha1=" ~ fecha1 ~ "&fecha2=" ~ fecha2 ~ "&genero=" ~ genero ~ "&idioma=" ~ idiomas, '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                    {{ link_to("mas-vendidos/busqueda?page=" ~ page.before ~ "&elementos="~ elementos ~"&fecha1=" ~ fecha1 ~ "&fecha2=" ~ fecha2 ~ "&genero=" ~ genero ~ "&idioma=" ~ idiomas, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("mas-vendidos/busqueda?page=" ~ page.next ~ "&elementos="~ elementos ~"&fecha1=" ~ fecha1 ~ "&fecha2=" ~ fecha2 ~ "&genero=" ~ genero ~ "&idioma=" ~ idiomas, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("mas-vendidos/busqueda?page=" ~ page.last ~ "&elementos="~ elementos ~"&fecha1=" ~ fecha1 ~ "&fecha2=" ~ fecha2 ~ "&genero=" ~ genero ~ "&idioma=" ~ idiomas, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                </div>
            </td>
        </tr>
    </table>
    <td colspan="4"><a href="/mas-vendidos/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a></td>


</div>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script>
    $(document).ready(function(){


        $("#hideChart").click(function(){
            google.load('visualization','1',{'packages':['corechart']});
            google.setOnLoadCallback(drawChart);
            $.ajax({
                data:  '',
                url:  "<?php echo $this->url->get('mas-vendidos/pieData')?>",
                type:  'get',
                beforeSend: function () {
                    $("#chart_div").html("Procesando, espere por favor...");
                },
                success:  function (response) {
                    drawChart(response);
                }
            });
            $("#chart_div").toggle();
        });
    });
</script>
<script type="text/javascript">
    google.load('visualization','1',{'packages':['corechart']});
    google.setOnLoadCallback(drawChart);
    var parametros = "bbb";
    $.ajax({
        data:  '',
        url:  "<?php echo $this->url->get('mas-vendidos/pieData')?>",
        type:  'get',
        beforeSend: function () {
            $("#chart_div").html("Procesando, espere por favor...");
        },
        success:  function (response) {
            drawChart(response);
        }
    });


    function drawChart(arrayLibros)
    {
        var data = new google.visualization.DataTable();
        data.addColumn('string','Libro');
        data.addColumn('number','Vendido');
        for(var i=0; i<arrayLibros.length;i++)
        {
            data.addRows([
                [arrayLibros[i]['titulo'],parseInt(arrayLibros[i]['veces_vendido'])]
            ]);
        }

        var options = {'title':'Búsqueda',
            'width':700,
            'height':600,
            'labels':'none'
           };
        var chart= new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data,options);
    }
</script>
