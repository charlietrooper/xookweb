<nav class="navbar-default {#navbar navbar-inverse#}" role="navigation">
    {{ image("img/LogoOrbile_header.png", "class":"img-responsive orbileheader", "alt": "-------") }}
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Orbile {% if username is defined %}({{username}}){% endif %}</a>
        </div>
        {{ elements.getMenu() }}
    </div>
    {{ image("img/linea_header.png", "class":"img-responsive lineaimg", "alt": "-------") }}
</nav>

<div class="container">
    {{ flash.output() }}
    {{ content() }}
    <hr>
    <footer class="centrarFooter">
        <span id="imagen_ocultar">
            <script type="text/javascript">
                var total_images = 18;
                var random_number = Math.floor((Math.random()*total_images));
                var random_img = new Array();
                random_img[0] = '{{ image("img/1a.png", "class":"img-responsive") }}';
                random_img[1] = '{{ image("img/1b.png", "class":"img-responsive") }}';
                random_img[2] = '{{ image("img/1c.png", "class":"img-responsive") }}';
                random_img[3] = '{{ image("img/1d.png", "class":"img-responsive") }}';
                random_img[4] = '{{ image("img/1e.png", "class":"img-responsive") }}';
                random_img[5] = '{{ image("img/1f.png", "class":"img-responsive") }}';
                random_img[6] = '{{ image("img/2a.png", "class":"img-responsive") }}';
                random_img[7] = '{{ image("img/2b.png", "class":"img-responsive") }}';
                random_img[8] = '{{ image("img/2c.png", "class":"img-responsive") }}';
                random_img[9] = '{{ image("img/2d.png", "class":"img-responsive") }}';
                random_img[10] = '{{ image("img/2e.png", "class":"img-responsive") }}';
                random_img[11] = '{{ image("img/2f.png", "class":"img-responsive") }}';
                random_img[12] = '{{ image("img/3a.png", "class":"img-responsive") }}';
                random_img[13] = '{{ image("img/3b.png", "class":"img-responsive") }}';
                random_img[14] = '{{ image("img/3c.png", "class":"img-responsive") }}';
                random_img[15] = '{{ image("img/3d.png", "class":"img-responsive") }}';
                random_img[16] = '{{ image("img/3e.png", "class":"img-responsive") }}';
                random_img[17] = '{{ image("img/3f.jpg", "class":"img-responsive") }}';
                document.write(random_img[random_number]);
            </script>
        </span>
        <p>{{ image("img/Orbile2015_footer.png", "alt": "Orbile pie de página") }}{#&copy; Orbile 2015#}</p>
    </footer>
</div>
