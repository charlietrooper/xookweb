{{ content() }}
{#
<div align="center" style="padding: 10px;">
    <label>ID compra socio:</label><br/>
    {{ text_field('pfuturo', 'id':'pfuturo') }}
</div>
#}
<div class="col-md-12">
    <label style="text-align: center; margin-bottom:15px;">Buscar por:</label>
</div>
<div class="row" style="padding-bottom: 30px">


    <div class="col-md-4">
        <label style="color:#666">Título:</label>
        {{ text_field('futuroTitulo', 'id':'futuroTitulo') }}
        {{submit_button("Buscar", 'class':'search-titulo')}}
    </div>
    <div class="col-md-4">
        <label style="color:#666">ISBN:</label>
        {{ text_field('futuroISBN', 'id':'futuroISBN') }}
        {{submit_button("Buscar", 'class':'search-isbn')}}
    </div>
    <div class="col-md-4">
        <label style="color:#666">Variación:</label>
        <select id="slc_variacion">
            <option value="" selected>--</option>
            <option value="1">Aumento</option>
            <option value="2">Disminución</option>
            <option value="3">Igual</option>
        </select>
       {{submit_button("Buscar", 'class':'search-VAR')}}
    </div>

</div>
<div class="col-md-12">
    <label style="text-align:center; margin-bottom:15px;">Filtrar por precio:</label>
</div>
<div class="row" style="padding-bottom: 30px">
    <div class="col-md-3"   >
        <label style="color:#666">Precio actual de lista:</label>
        <br>
        <select id="slc_actualLista">
            <option value="" selected>--</option>
            <option value="1">$0 - $50</option>
            <option value="2">$50 - $100</option>
            <option value="3">$100 - $200</option>
            <option value="4">$200 - $500</option>
            <option value="5">$500 - </option>
        </select>
        <br><div style="margin-top:10px;">{{submit_button("Buscar", 'class':'search-PAL')}}</div>
    </div>
    <div class="col-md-3">
        <label style="color:#666">Precio actual de venta:</label>
        <br>
        <select id="slc_actualVenta">
            <option value="" selected>--</option>
            <option value="1">$0 - $50</option>
            <option value="2">$50 - $100</option>
            <option value="3">$100 - $200</option>
            <option value="4">$200 - $500</option>
            <option value="5">$500 - </option>
        </select>
        <br><div style="margin-top:10px;">{{submit_button("Buscar", 'class':'search-PAV')}}</div>
    </div>
    <div class="col-md-3">
        <label style="color:#666">Precio futuro de lista:</label>
        <br>
        <select id="slc_futuroLista">
            <option value="" selected>--</option>
            <option value="1">$0 - $50</option>
            <option value="2">$50 - $100</option>
            <option value="3">$100 - $200</option>
            <option value="4">$200 - $500</option>
            <option value="5">$500 - </option>
        </select>
        <br><div style="margin-top:10px;">{{submit_button("Buscar", 'class':'search-PFL')}}</div>
    </div>
    <div class="col-md-3">
        <label style="color:#666">Precio futuro de venta:</label>
        <br>
        <select id="slc_futuroVenta">
            <option value="" selected>--</option>
            <option value="1">$0 - $50</option>
            <option value="2">$50 - $100</option>
            <option value="3">$100 - $200</option>
            <option value="4">$200 - $500</option>
            <option value="5">$500 - </option>
        </select>
        <br><div style="margin-top:10px;">{{submit_button("Buscar", 'class':'search-PFV')}}</div>
    </div>
</div>


<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="tablita">
        <tr>
            <th>Título</th>
            <th>ISBN</th>
            <th>Precio actual de lista</th>
            <th>Precio actual de venta</th>
            <th>Precio futuro de lista</th>
            <th>Precio futuro de venta</th>
            <th>Pre Orden</th>
            <th>Fecha</th>
            <th>Variación</th>
        </tr>
        {% for e in pfuturo.items['Items'] %}
            <tr>
                <td>{{ e['title'] }}</td>
                <td class="fitCelda">{{ link_to("libros/enviarlibro?isbn=" ~ e['isbn'], e['isbn'],'target': '_blank') }}</td>
                <td class="fitCelda">{{ "$ "~e['today_list_price'] }}</td>
                <td class="fitCelda">{{ "$ "~e['today_selling_price'] }}</td>
                <td class="fitCelda">{{ "$ "~e['future_list_price'] }}</td>
                <td class="fitCelda">{{ "$ "~e['future_selling_price'] }}</td>
                <td>

                    {% if  e['future_ispreorder'] == "false" %}
                        No Disponible
                    {% elseif  e['future_ispreorder'] == null %}
                        No Disponible
                    {% elseif e['future_ispreorder'] == "true" %}
                        Disponible
                    {% endif %}
                </td>
                <td>
                    {{ e['future_price_from_date'] }}
                </td>
                 <td>
                    {% if e['today_selling_price'] > e['future_selling_price'] %}
                       <div class="arrow-down"></div>
                    {% elseif e['today_selling_price'] == e['future_selling_price'] %}
                        <div class="igual"></div>
                    {% elseif e['today_selling_price'] < e['future_selling_price'] %}
                        <div class="arrow-up"></div>
                    {% endif %}
                </td>

            </tr>
        {% endfor %}
        <tr>
            <td colspan="2" align="left">
                <span class="help-inline">{{ pfuturo.actual }}/{{ pfuturo.paginasTotales }}</span>
                <div class="btn-group">
                    {{ link_to("precio-futuro/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                    {{ link_to("precio-futuro/index?page=" ~ pfuturo.anterior, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("precio-futuro/index?page=" ~ pfuturo.siguiente, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("precio-futuro/index?page=" ~ pfuturo.ultima, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                </div>
            </td>
        </tr>
    </table>
</div>



<script type="text/javascript">
    $(document).ready(function(){

        //BUSQUEDA POR TITULO
        $('.search-titulo').on('click',function(){
            $futuroTitulo = $('#futuroTitulo').val();
            if($futuroTitulo == '')
            {
                var pagina = "/precio-futuro/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            document.getElementById('futuroISBN').value = "";
            document.getElementById('slc_actualLista').value = "";
            document.getElementById('slc_actualVenta').value = "";
            document.getElementById('slc_futuroLista').value = "";
            document.getElementById('slc_futuroVenta').value = "";
            document.getElementById('slc_variacion').value = "";
            Busqueda($pagina_ajax);
        });

  //BUSQUEDA POR ISBN
        $('.search-isbn').on('click',function(){
            $futuroISBN = $('#futuroISBN').val();
            if($futuroISBN == '')
            {
                var pagina = "/precio-futuro/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            document.getElementById('futuroTitulo').value = "";
            document.getElementById('slc_actualLista').value = "";
            document.getElementById('slc_actualVenta').value = "";
            document.getElementById('slc_futuroLista').value = "";
            document.getElementById('slc_futuroVenta').value = "";
            document.getElementById('slc_variacion').value = "";
            Busqueda($pagina_ajax);
        });

        //BUSQUEDA POR PRECIO ACTUAL DE LISTA
        $('.search-PAL').on('click',function(){
            $actLis = $('#slc_actualLista').val();
            if($actLis== 0)
            {
                var pagina = "/precio-futuro/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            document.getElementById('futuroTitulo').value = "";
            document.getElementById('futuroISBN').value = "";
            document.getElementById('slc_actualVenta').value = "";
            document.getElementById('slc_futuroLista').value = "";
            document.getElementById('slc_futuroVenta').value = "";
            document.getElementById('slc_variacion').value = "";
            Busqueda($pagina_ajax);
        });

        //BUSQUEDA POR PRECIO ACTUAL DE VENTA
        $('.search-PAV').on('click',function(){
            $actVen = $('#slc_actualVenta').val();
            if($actVen== 0)
            {
                var pagina = "/precio-futuro/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            document.getElementById('futuroTitulo').value = "";
            document.getElementById('futuroISBN').value = "";
            document.getElementById('slc_actualLista').value = "";
            document.getElementById('slc_futuroLista').value = "";
            document.getElementById('slc_futuroVenta').value = "";
            document.getElementById('slc_variacion').value = "";
            Busqueda($pagina_ajax);
        });

        //BUSQUEDA POR variacion
        $('.search-VAR').on('click',function(){
            $actVen = $('#slc_variacion').val();
            if($actVen== 0)
            {
                var pagina = "/precio-futuro/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            document.getElementById('futuroTitulo').value = "";
            document.getElementById('futuroISBN').value = "";
            document.getElementById('slc_actualLista').value = "";
            document.getElementById('slc_actualVenta').value = "";
            document.getElementById('slc_futuroLista').value = "";
            document.getElementById('slc_futuroVenta').value = "";
            Busqueda($pagina_ajax);
        });

        //BUSQUEDA POR PRECIO FUTURO DE LISTA
        $('.search-PFL').on('click',function(){
            $futLis = $('#slc_futuroLista').val();
            if($futLis== 0)
            {
                var pagina = "/precio-futuro/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            document.getElementById('futuroTitulo').value = "";
            document.getElementById('futuroISBN').value = "";
            document.getElementById('slc_actualLista').value = "";
            document.getElementById('slc_actualVenta').value = "";
            document.getElementById('slc_futuroVenta').value = "";
            document.getElementById('slc_variacion').value = "";
            Busqueda($pagina_ajax);
        });

        //BUSQUEDA POR PRECIO FUTURO DE VENTA
        $('.search-PFV').on('click',function(){
            $futVen = $('#slc_futuroVenta').val();
            if($futVen== 0)
            {
                var pagina = "/precio-futuro/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            document.getElementById('futuroTitulo').value = "";
            document.getElementById('futuroISBN').value = "";
            document.getElementById('slc_actualLista').value = "";
            document.getElementById('slc_actualVenta').value = "";
            document.getElementById('slc_futuroLista').value = "";
            document.getElementById('slc_variacion').value = "";
            Busqueda($pagina_ajax);
        });

        function traducir(preorden)
        {
            $traduccion='';
            if (preorden=="true")
            {
                $traduccion='Disponible';
            }

            else if(preorden=="false"|| preorden==null)
            {
                $traduccion='No Disponible';
            }

            return $traduccion;
        }

        function variacion($valorActual, $valorFuturo)
        {
            $var="";
            if ($valorActual>$valorFuturo)
            {
                $var="<div class='arrow-down'></div>";
            }
            else if($valorActual==$valorFuturo)
            {
                $var="<div class='igual'></div>";
            }
            else if ($valorActual<$valorFuturo)
            {
                $var="<div class='arrow-up'></div>";
            }
            return $var;
        }

        function obtenerBusquedaID()
        {
            $ID=0;
            $futuroTitulo = $('#futuroTitulo').val();
            $futuroISBN = $('#futuroISBN').val();
            $pActualLista = $('#slc_actualLista').val();
            $pActualVenta = $('#slc_actualVenta').val();
            $pFuturoLista = $('#slc_futuroLista').val();
            $pFuturoVenta = $('#slc_futuroVenta').val();
            $variacion = $('#slc_variacion').val();

            if ($futuroTitulo!=="")
            {
                $ID=1;
            }

            else if ($futuroISBN!=="")
            {
                $ID=2;
            }
            else if ($pActualLista!=="")
            {
                $ID=3;
            }
            else if ($pActualVenta!=="")
            {
                $ID=4;
            }
            else if ($pFuturoLista!=="")
            {
                $ID=5;
            }
            else if ($pFuturoVenta!=="")
            {
                $ID=6;
            }

            else if ($variacion!=="")
            {
                $ID=7;
            }


            return $ID;
        }

        function Busqueda($pagina_ajax){
            $futuroTitulo = $('#futuroTitulo').val();
            $futuroISBN = $('#futuroISBN').val();
            $pActualLista = $('#slc_actualLista').val();
            $pActualVenta = $('#slc_actualVenta').val();
            $pFuturoLista = $('#slc_futuroLista').val();
            $pFuturoVenta = $('#slc_futuroVenta').val();
            $variacion = $('#slc_variacion').val();
           // alert($porPActual);
            $busquedaID=obtenerBusquedaID();
            $.get("<?php echo $this->url->get('precio-futuro/busqueda')?>",
                    {
                        'futuroTitulo':$futuroTitulo,
                        'futuroISBN':$futuroISBN,
                        'pActualLista':$pActualLista,
                        'pActualVenta':$pActualVenta,
                        'pFuturoLista':$pFuturoLista,
                        'pFuturoVenta':$pFuturoVenta,
                        'variacion':$variacion,
                        'busquedaID':$busquedaID,
                        'pagina':$pagina_ajax},function($data)
            {
                if($data)
                {

                    var tab = "<tr><th>Título</th><th>ISBN</th>";
                    tab +="<th>Precio actual de lista</th> <th>Precio actual de venta</th>";
                    tab += "<th>Precio futuro de lista</th>";
                    tab +="<th>Precio futuro de venta</th> <th>Pre Orden</th>";
                    tab +="<th>Fecha</th><th>Variación</th>";


                    if($futuroISBN!=='')
                    {
                        $.each($data.items.Items.Item, function (index, datos) {
                            tab += "<tr>"+
                            "<td>"+datos.title+"</td>"+
                            '<td><a target="_blank" href="../libros/enviarlibro?isbn='+datos.isbn+'">'+datos.isbn+'</a></td>'+
                            "<td class='fitCelda'>$ "+datos.today_list_price+"</td>"+
                            "<td class='fitCelda'>$ "+datos.today_selling_price+"</td>"+
                            "<td class='fitCelda'>$ "+datos.future_list_price+"</td>"+
                            "<td class='fitCelda'>$ "+datos.future_selling_price+"</td>"+
                            "<td>"+traducir(datos.future_ispreorder)+"</td>"+
                            "<td>"+datos.future_price_from_date+"</td>"+
                            "<td>"+variacion(datos.today_selling_price,datos.future_selling_price)+"</td>"+
                            "</tr>";
                        });
                    }

                    else //if($futuroTitulo!=='' || $porPActual!=''|| $porPFuturo!='')
                    {
                        $.each($data.items.Items, function (index, datos) {
                            tab += "<tr>"+
                            "<td>"+datos.title+"</td>"+
                            '<td><a target="_blank" href="../libros/enviarlibro?isbn='+datos.isbn+'">'+datos.isbn+'</a></td>'+
                            "<td class='fitCelda'>$ "+datos.today_list_price+"</td>"+
                            "<td class='fitCelda'>$ "+datos.today_selling_price+"</td>"+
                            "<td class='fitCelda'>$ "+datos.future_list_price+"</td>"+
                            "<td class='fitCelda'>$ "+datos.future_selling_price+"</td>"+
                            "<td>"+traducir(datos.future_ispreorder)+"</td>"+
                            "<td>"+datos.future_price_from_date+"</td>"+
                            "<td>"+variacion(datos.today_selling_price,datos.future_selling_price)+"</td>"+
                            "</tr>";
                        });
                    }


                    tab += "<tr>"+
                    "<td colspan='2' align='left'>"+
                    "<span class='help-inline'>"+$data.actual +"/"+ $data.paginasTotales +"</span>"+
                    "<div class='btn-group'>"+
                    "<a data-first='1' id='primera' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                    "<a data-before='" + $data.anterior + "' id='anterior' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                    "<a data-next='" + $data.siguiente + "' id='siguiente' class= 'btn'><i class='icon-step-backward'></i> Siguiente</a>"+
                    "<a data-last='" + $data.ultima + "' id='ultima' class= 'btn'><i class='icon-step-backward'></i> Última</a>"+
                    "</div>"+
                    "</td>"+
                    "</tr>";
                    $('table#tablita').html(tab);
                }
            });
        }

        $(document).on('click','#siguiente', function(){
            $pagina_ajax = $('#siguiente').data('next');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#primera', function(){
            $pagina_ajax = $('#primera').data('first');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#ultima', function(){
            $pagina_ajax = $('#ultima').data('last');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#anterior', function(){
            $pagina_ajax = $('#anterior').data('before');
            Busqueda($pagina_ajax);
        });
    });
</script>

