
{{ content() }}

<div class="row" align="center">

    <div class="col-md-12" align="center">
        <div class="titulo">
            <h1 class="retroshadow">Log in</h1>'
        </div>
        {{ form('session/start', 'role': 'form') }}
            <fieldset>
                <div align="left" class="form-group" style="width: 300px;">
                    <label for="username">Nombre de usuario</label>
                    <div class="controls">
                        {{ text_field('email', 'class': " col-sm form-control") }}
                    </div>
                </div>
                <div align="left" class="form-group" style="width: 300px;">
                    <label for="pass">Contraseña</label>
                    <div class="controls">
                        {{ password_field('password', 'class': "form-control") }}
                    </div>
                </div>
                <div class="form-group">
                    {{ submit_button('Iniciar sesión', 'class': 'btn btn-primary btn-large') }}
                </div>
            </fieldset>
        </form>
    </div>

    <!--<div class="col-md-6">

        <div class="page-header">
            <h2>¿Aún no tienes una cuenta?</h2>
        </div>

        <p>Create an account offers the following advantages:</p>
        <ul>
            <li>Create, track and export your invoices online</li>
            <li>Gain critical insights into how your business is doing</li>
            <li>Stay informed about promotions and special packages</li>
        </ul>

        <div class="clearfix center">
            {{ link_to('register', 'Registrate', 'class': 'btn btn-primary btn-large btn-success') }}
        </div>
    </div>-->
</div>
