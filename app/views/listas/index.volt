{{ content() }}
{{ titulo }}

<!--<?php $var= '2015-10-20T21:00:00';
        $split=explode('T',$var);
        $split2=explode('-', $split[0]);
        $dateFormat=$split2[2].'-'.$split2[1].'-'.$split2[0];
        var_dump($dateFormat);
?>-->
{# DIV PARA MANTENER EL CONTENIDO O EL RESULTADO DEL JSON #}

{#
<div align="center" style="padding: 10px;">
    <label>ID compra socio:</label><br/>
    {{ text_field('pactual', 'id':'pactual') }}
</div>
#}
<div class="col-md-12">
    <label style="text-align:center; margin-bottom:15px;">Filtrar:</label>
</div>
<div class="row" style="padding-bottom: 30px">
    <div class="col-md-12">
        <label style="color:#666">Por idioma:</label>
        <br>
        <select id="slc_language">
            <option value="" selected>--</option>
            <option value="1">Espa&ntilde;ol</option>
            <option value="2">Ingl&eacute;s</option>
        </select>
        <br><div style="margin-top:10px;">{{submit_button("Buscar", 'class':'search-lang')}}</div>
    </div>
</div>
<!--
<div style="padding:10px;">
    <td colspan="4"><a href="/listas/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a></td>
</div>-->

<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="tablita">
        <tr>
            <th>T&iacute;tulo</th>
            <th>ISBN</th>
            <th>Active Revision ID</th>
            <th>Inicio Preorden</th>
            <th>Fin Preorden</th>
            <th>Precio de Lista</th>
            <th>Precio de Venta</th>
            <th>Idioma</th>

        </tr>
        <?php
        $cuenta=0;
        ?>
        {% for e in preorderBooks.items['Items'] %}
            <?php $formatDate=($preorderBooks->items['Items'][$cuenta]['preorder_initial_date']); ?>
            <tr>
                <td style="width:50%;">{{ e['title'] }}</td>
                <td class="fitCelda">{{ link_to("libros/enviarlibro?isbn=" ~ e['isbn'], e['isbn'],'target': '_blank') }}</td>
                <td style='white-space:nowrap;'>{{ e['active_revision_id'] }}</td>
                <td style='white-space:nowrap;'> <?php echo date("d-m-Y", strtotime($formatDate)); ?> </td>
                <td style='white-space:nowrap;' class="fitCelda">
                    {% if  e['preorder_end_date'] == null %}
                        No Disponible
                    {% else %}
                        <?php echo date("d-m-Y", strtotime($preorderBooks->items['Items'][$cuenta]['preorder_end_date'])); ?>
                    {% endif %}
                </td>
                <td style='white-space:nowrap;' class="fitCelda">{{ "$ "~ e['preorder_listprice'] }}</td>
                <td style='white-space:nowrap;' class="fitCelda">{{ "$ "~ e['preorder_sellingprice'] }}</td>
                <td>
                    {% if  e['preorder_language'] == "en" OR e['preorder_language'] == "EN" %}
                        Ingl&eacute;s
                    {% elseif  e['preorder_language'] == null %}
                        No Disponible
                    {% elseif e['preorder_language'] == "es" OR e['preorder_language'] == "ES" %}
                        Espa&ntilde;ol
                    {% endif %}
                </td>

            </tr>
            <?php
                 $cuenta++;
            ?>
        {% endfor %}
        <tr>
            <td colspan="8" align="left">
                <span class="help-inline">{{ preorderBooks.actual }}/{{ preorderBooks.paginasTotales }}</span>
                <div class="btn-group">
                    {{ link_to("listas/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                    {{ link_to("listas/index?page=" ~ preorderBooks.anterior, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                    {{ link_to("listas/index?page=" ~ preorderBooks.siguiente, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                    {{ link_to("listas/index?page=" ~ preorderBooks.ultima, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                </div>
            </td>
        </tr>
    </table>
</div>

<script type="text/javascript">
    $(document).ready(function(){


        //BUSQUEDA POR IDIOMA
        $('.search-lang').on('click',function(){
            $poLis = $('#slc_language').val();
            if($poLis == 0)
            {
                var pagina = "/listas/index";
                location.href = location.origin + pagina;
            }
            $pagina_ajax = 1;
            Busqueda($pagina_ajax);
        });

        function traducir(preorden)
        {
            $traduccion='';
            if (preorden=="en" || preorden=="EN")
            {
                $traduccion='Ingl&eacute;s';
            }

            else if(preorden=="es" || preorden=="ES")
            {
                $traduccion='Espa&ntilde;ol';
            }
            else{
                $traduccion='No Disponible';
            }

            return $traduccion;
        }

        function preorderMSG(date)
        {
            $msg='';
            if (date!==null)
            {
                $msg=date;
            }

            else
            {
                $msg='No Disponible';
            }

            return $msg;
        }


        function Busqueda($pagina_ajax){

            $preorderBooksLista = $('#slc_language').val();

            $.get("<?php echo $this->url->get('listas/busqueda')?>",
                    {

                        'preorderBooksLista':$preorderBooksLista,
                        'pagina':$pagina_ajax},function($data)
                    {

                        if($data)
                        {

                            var tab = "<tr><th>T&iacute;tulo</th><th>ISBN</th><th>Active Revision ID</th><th>Inicio Preorden</th><th>Fin Preorden</th><th>Precio de Lista</th><th>Precio de Venta</th> <th>Idioma</th></tr>";

                            $.each($data.items.Items, function (index, datos) {
                                tab += "<tr>"+
                                "<td style='width:50%;'>"+datos.title+"</td>"+
                                '<td><a target="_blank" href="../libros/enviarlibro?isbn='+datos.isbn+'">'+datos.isbn+'</a></td>'+
                                "<td style='white-space:nowrap;'>"+datos.active_revision_id+"</td>"+
                                "<td style='white-space:nowrap;'>"+datos.preorder_initial_date+"</td>"+
                                "<td style='white-space:nowrap;'>"+preorderMSG(datos.preorder_end_date)+"</td>"+
                                "<td style='white-space:nowrap;'> $ "+datos.preorder_listprice+"</td>"+
                                "<td style='white-space:nowrap;'> $ "+datos.preorder_sellingprice+"</td>"+
                                "<td>"+traducir(datos.preorder_language)+"</td>"+
                                "</tr>";
                            });

                            tab += "<tr>"+
                            "<td colspan='8' align='left'>"+
                            "<span class='help-inline'>"+$data.actual +"/"+ $data.paginasTotales +"</span>"+
                            "<div class='btn-group'>"+
                            "<a data-first='1' id='primera' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                            "<a data-before='" + $data.anterior + "' id='anterior' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                            "<a data-next='" + $data.siguiente + "' id='siguiente' class= 'btn'><i class='icon-step-backward'></i> Siguiente</a>"+
                            "<a data-last='" + $data.ultima + "' id='ultima' class= 'btn'><i class='icon-step-backward'></i> Última</a>"+
                            "</div>"+
                            "</td>"+
                            "</tr>";
                            $('table#tablita').html(tab);
                        }
                    });
        }

        $(document).on('click','#siguiente', function(){
            $pagina_ajax = $('#siguiente').data('next');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#primera', function(){
            $pagina_ajax = $('#primera').data('first');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#ultima', function(){
            $pagina_ajax = $('#ultima').data('last');
            Busqueda($pagina_ajax);
        });
        $(document).on('click','#anterior', function(){
            $pagina_ajax = $('#anterior').data('before');
            Busqueda($pagina_ajax);
        });

        //PARTE PARA OCULTAR O MOSTRAR EL JSON
        $('#show').click(function(){

            $('#mostrar').show();
            $('#hidden').show();
            $('#show').hide();

        });

        $('#hidden').click(function(){
            $('#mostrar').hide();
            $('#hidden').hide();
            $('#show').show()


        });
    });
</script>