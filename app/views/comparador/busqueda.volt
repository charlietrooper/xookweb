{{ content() }}

<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="tablita">
        <tr>
            <th>Título</th>
            <th>ISBN</th>
            <th>Precio de Orbile</th>
            <th>Precio de Gandhi</th>
            <th>Precio de Porrúa</th>
        </tr>
        <?php
        $cuenta=0;
        ?>
        {% for e in arrayISBN %}
            <tr>
                <td>
                    {% if precioOrbile[cuenta]=="NO" %}
                        No encontrado
                    {% else %}
                        {{ precioOrbile[cuenta]['Items']['Item'][0]['title'] }}
                    {% endif %}
                </td>
                <td>{{ e }}</td>
                <td>
                    {% if precioOrbile[cuenta]=="NO" %}
                        -
                    {% else %}
                         <a target="_blank" href="https://web.orbile.com/libros/enviarlibro?isbn={{ e }}&id_libro=">{{"$ "~ precioOrbile[cuenta]['Items']['Item'][0]['sellingprice'] }}</a>

                    {% endif %}

                </td>
                <td>
                    <a target="_blank" href="{{precioGandhi[cuenta]['url']}}">{{ precioGandhi[cuenta]['precio'] }}</a>
                </td>
               <td>
                   <a target="_blank" href="{{precioPorrua[cuenta]['url']}}">{{ precioPorrua[cuenta]['precio'] }}</a>
               </td>
            </tr>
            <?php
                 $cuenta++;
            ?>
        {% endfor %}
    </table>
</div>