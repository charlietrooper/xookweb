{{ content() }}
<div class="titulo" >
    <h1 class="retroshadow">Socios</h1>
</div>
<div align="right" style="margin-bottom: 15px">
   {{ link_to("partners/create", "CREAR PARTNER", "class" : "btn btn-primary")}}
</div>
<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center">
        <tr>
            <th>ID del socio</th>
            <th>Nombre</th>
            <th>Correo de soporte</th>
            <th>Correo para notificaciones</th>
            <th>Api username</th>
            <th>Orden del prefijo</th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        {% for obj in page.items %}
            <tr>
                <td>{{obj.partner_id}}</td>
                <td>{{obj.name}}</td>
                <td>{{obj.email_support}}</td>
                <td>{{obj.email_notification}}</td>
                <td>{{obj.api_username}}</td>
                <td>{{ obj.order_prefix }}</td>
                <td width="7%">{{ link_to("partners/edit/" ~ obj.partner_id, '<i class="glyphicon glyphicon-edit"></i> EDITAR', "class": "btn btn-default") }}</td>
                <td width="7%">{{ link_to("partners/delete/" ~ obj.partner_id, '<i class="glyphicon glyphicon-remove"></i> ELIMINAR', "class": "btn btn-default") }}</td>
                <td width="7%">{{ link_to("partners/details/" ~ obj.partner_id, '<i class="glyphicon glyphicon-list-alt"></i> DETALLES', "class": "btn btn-default") }}</td>
            </tr>
            {% if loop.last %}

                <tr>
                    <td colspan="9" align="left">
                        <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                        <div class="btn-group">
                            {{ link_to("partners/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                            {{ link_to("partners/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                            {{ link_to("partners/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                            {{ link_to("partners/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                        </div>
                    </td>
                </tr>
            {% endif %}
        {% endfor %}
    </table>
</div>