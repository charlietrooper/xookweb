{{ content() }}
<p style="text-align: left; padding-top: 10px;">{{ link_to("partners", "class":"btn btn-primary", "&larr; Regresar") }}</p>

{{ form("partners/update") }}
<fieldset>
    <div width="100%" style="display: block; overflow: auto;">
        <table align="center" class="detalle table-bordered table-striped">
            {% for b in a %}
                <tr>
                    <td>
                        <label>ID:</label>
                    </td>
                    <td>
                        {{ text_field('partner_id', 'value':b.partner_id, 'readonly':'true') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Nombre:</label>
                    </td>
                    <td>
                        {{ setDefault('name', b.name) }}
                        {{ text_field('name', 'id':'name') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Correo de apoyo:&nbsp;&nbsp;</label>
                    </td>
                    <td>
                        {{ setDefault('email_support', b.email_support) }}
                        {{ text_field('email_support', 'id':'email_supo') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Correo para<br>notificaciones:&nbsp;</label>
                    </td>
                    <td>
                        {{ setDefault('email_notification', b.email_notification) }}
                        {{ text_field('email_notification', 'id':'email_noti') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Api username:</label>
                    </td>
                    <td>
                        {{ setDefault('username', b.api_username) }}
                        {{ text_field('username', 'id':'user') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Contraseña:</label>
                    </td>
                    <td>
                        {{ setDefault('pass', b.api_pass) }}
                        {{ password_field('pass', 'id':'pass') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Confirmar contraseña:</label>
                    </td>
                    <td>
                        {{ setDefault('confirmpass', b.api_pass) }}
                        {{ password_field('confirmpass', 'id':'confirmpass') }}
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Orden del prefijo:</label>
                    </td>
                    <td>
                        {{ setDefault('prefix', b.order_prefix) }}
                        {{ text_field('prefix', 'id':'prefix') }}
                    </td>
                </tr>

            {% endfor %}
        </table>
    </div>
</fieldset>
<ul class="pager">
    <li class="pull-center">
        {{ submit_button("Actualizar", "class": "btn btn-primary", "id":"ok") }}
    </li>
</ul>
</form>
{#------------------------------INICIA JAVASCRIPT--------------------------------------------------#}
<script type="text/javascript">
    $(document).ready(function () {
        $('#ok').on('click', function () {
            // event.preventDefault();

            var mensaje = [];
            var mens = [];
            $email = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+/;

            mens.push('Los siguientes campos son incorrectos o estan vacios:\n');
            mensaje.push('Los siguientes campos son incorrectos o estan vacios:\n');

            $name = $('#name').val();
            $email_supo = $('#email_supo').val();
            $email_noti = $('#email_noti').val();
            $user = $('#user').val();
            $prefix = $('#prefix').val();

            $pass = $('#pass').val();
            $confirm_pass = $('#confirmpass').val();

            if ($name.trim() == '') {
                mensaje.push('-El campo NOMBRE esta vacio \n');
            }
            if (!($email.test($email_supo))) {
                mensaje.push('-El email es invalido en el campo CORREO DE APOYO \n');
            }
            if ($email_supo.trim() == '') {
                mensaje.push('-El campo CORREO DE APOYO esta vacio \n');
            }
            if (!($email.test($email_noti))) {
                mensaje.push('-El email es invalido en el campo CORREO PARA NOTIFICACIONES \n');
            }
            if ($email_noti.trim() == '') {
                mensaje.push('-El campo CORREO PARA NOTIFICACIONES esta vacio \n');
            }
            if ($user.trim() == '') {
                mensaje.push('-El campo USERNAME esta vacio \n');
            }
            if ($prefix.trim() == '') {
                mensaje.push('-El campo ORDER PREFIX esta vacio \n');
            }
            if($pass != $confirm_pass)
            {
                mensaje.push('-Las contraseñas no son iguales \n');
            }
            if (mensaje.length != mens.length) {
                event.preventDefault()
                alert(mensaje);
            }
        })
    });
</script>
