{{ content() }}
<p style="text-align: left; padding-top: 10px;">{{ link_to("partners", "class":"btn btn-primary", "&larr; Regresar") }}</p>

{{ form("partners/savepartner", "id":"form") }}
   <fieldset>
       <div width="100%" style="display: block; overflow: auto;">
           <table align="center" class="detalle table-bordered table-striped">
               <tr>
                   <th><label for="name">Nombre:</label></th>
                   <td>{{ text_field("name", "id":"name") }}</td>
               </tr>
               <tr>
                   <th><label for="email">Corre de apoyo:</label></th>
                   <td>{{ text_field("email_support", "id":"email_supo") }}</td>
               </tr>
               <tr>
                   <th><label for="email">Correo para <br>notificaciones:</label></th>
                   <td>{{ text_field("email_notification", "id":"email_noti") }}</td>
               </tr>
               <tr>
                   <th><label for="user">Username:</label></th>
                   <td>{{ text_field("user", "id":"user") }}</td>
               </tr>
               <tr>
                   <th><label for="pass">Contraseña:</label></th>
                   <td>{{ password_field("pass", "id":"pass") }}</td>
               </tr>
               <tr>
                   <th><label for="pass">Confirmar contraseña:&nbsp;&nbsp;</label></th>
                   <td>{{ password_field("password", "id":"password") }}</td>
               </tr>
               <tr>
                   <th><label for="pass">Orden del prefijo:&nbsp;&nbsp;</label></th>
                   <td>{{ text_field("prefix", "id":"prefix") }}</td>
               </tr>
           </table>
       </div>
   </fieldset>

    <ul class="pager">
        <li class="pull-center">
            {{ submit_button("Guardar", "class": "btn btn-primary", "id":"ok") }}
        </li>
    </ul>
</form>

{#------------------------------INICIA JAVASCRIPT--------------------------------------------------#}
<script type="text/javascript">
    $(document).ready(function(){
        $('#ok').on('click', function(){
           // event.preventDefault();

            var mensaje = [];
            var mens = [];
            $email = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+/;
            $contra =/(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$/;

            mens.push('Los siguientes campos son incorrectos o estan vacios:\n');
            mensaje.push('Los siguientes campos son incorrectos o estan vacios:\n');

            $name=$('#name').val();
            $email_supo=$('#email_supo').val();
            $email_noti=$('#email_noti').val();
            $user=$('#user').val();
            $pass=$('#pass').val();
            $password=$('#password').val();
            $prefix=$('#prefix').val();

            if($name.trim()=='')
            {
                mensaje.push('-El campo NOMBRE esta vacio \n');
            }
            if(!($email.test($email_supo)))
            {
                mensaje.push('-El email es invalido en el campo CORREO DE APOYO \n');
            }
            if($email_supo.trim()=='')
            {
                mensaje.push('-El campo CORREO DE APOYO esta vacio \n');
            }
            if(!($email.test($email_noti)))
            {
                mensaje.push('-El email es invalido en el campo CORREO PARA NOTIFICACIONES \n');
            }
            if($email_noti.trim()=='')
            {
                mensaje.push('-El campo CORREO PARA NOTIFICACIONES esta vacio \n');
            }
            if($user.trim()=='')
            {
                mensaje.push('-El campo USERNAME esta vacio \n');
            }

            if(!($contra.test($pass)))
            {
                mensaje.push('-La contraseña debe contener:\n' +
                             '      al menos una letra mayúscula\n' +
                             '      al menos una letra minúscula\n' +
                             '      al menos un número o caracter especial\n' +
                             '      longitud mínima de 7 caracteres\n' +
                             '      longitud máxima de 21 caracteres\n');
            }
            if($password!=$pass)
            {
                mensaje.push('-Las contraseñas no coinciden');
            }
            if($prefix.trim()=='')
            {
                mensaje.push('-El campo ORDER PREFIX esta vacio \n');
            }

            /*
            if($pass.trim()=='')
            {
                mensaje.push('-El campo PASSWORD esta vacio \n');
            }
            if($pass.length < 6)
            {
                mensaje.push('-El campo PASSWORD debe contener minimo 6 caracteres');
            }
            */

            if(mensaje.length != mens.length)
            {
                event.preventDefault()
                alert(mensaje);
            }
        })
    });
</script>