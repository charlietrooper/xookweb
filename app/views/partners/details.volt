{{ content() }}
{{ content() }}
<script type="text/javascript">
    window.onload = function () {
        var atras = document.getElementById('irAtras');
        atras.addEventListener("click", irAtras);
        function irAtras() { this.style.color = 'red'; window.history.back();}
    }
</script>
<p style="text-align: left; padding-top: 10px;">{{ link_to("", "class":"btn btn-primary", "&larr; Regresar", "id":"irAtras") }}</p>

{{ form("partners/update") }}
<div width="100%" style="display: block; overflow: auto;">
    <table align="center"  class="detalle table-bordered table-striped">
    {% for b in d%}
        <tr>
            <td>
                <label>ID:</label>
            </td>
            <td>
                {{b.partner_id}}
            </td>
        </tr>
        <tr>
            <td>
                <label>Nombre:</label>
            </td>
            <td>
                {{b.name}}
            </td>
        </tr>
        <tr>
            <td>
                <label>Correo de apoyo:&nbsp;&nbsp;</label>
            </td>
            <td>
                {{b.email_support}}
            </td>
        </tr>
        <tr>
            <td>
                <label>Correo para<br>notificaciones:</label>
            </td>
            <td>
                <br/>{{b.email_notification}}
            </td>
        </tr>
        <tr>
            <td>
                <label>Api username:</label>
            </td>
            <td>
                {{b.api_username}}
            </td>
        <tr>
            <td>
                <label>Api pass</label>
            </td>
            <td>
                <div class="emergente">
                    <div class="ocultar"><?php echo substr($b->api_pass,0,5).'...'; ?></div>
                    <div class="mostrar mostrarAPI">{{ b.api_pass }}</div>
                </div>
            </td>
        </tr>
        <tr>
            <td>
                <label>Orden del prefijo:</label>
            </td>
            <td>
                <table width="150px" style="table-layout: fixed;"><tr><td style="overflow: auto; white-space: nowrap;">{{b.order_prefix}}</td></tr></table>
            </td>
        </tr>
    {% endfor %}
    </table>

</div>
<br>
<br>
<div align="center">
    {{ link_to("sales-reports/index/" ~ b.partner_id,"Ventas generales", "class":"btn btn-primary btn-lg") }}
    &nbsp;&nbsp;&nbsp;&nbsp;
    {{ link_to("notification-catalogues/index/" ~ b.partner_id, "Catalogos de notification", "class":"btn btn-primary btn-lg") }}
    &nbsp;&nbsp;&nbsp;&nbsp;
      {{ link_to("customers/index/" ~ b.partner_id,"Clientes", "class":"btn btn-primary btn-lg") }}
  {#  {{ link_to("customers/index/" ~ b.partner_id,"Clientes", "class":"btn btn-primary btn-lg") }}#}
</div>
</form>

