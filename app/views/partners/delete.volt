{{ content() }}
<p style="text-align: left; padding-top: 10px;">{{ link_to("partners", "class":"btn btn-primary", "&larr; Regresar") }}</p>

{{ form("partners/eliminar") }}
  <h3 style="text-align: center;">¿Está seguro que desea eliminar los datos?</h3>
<br>
<br>
<fieldset>
    <div width="100%" style="display: block; overflow: auto;">
        <table align="center" class="detalle table-bordered table-striped">
        {% for b in i%}
        <tr>
            <td>
                <label>ID:</label>
            </td>
            <td>
                {{ text_field('partner_id', 'value':b.partner_id, 'readonly':'true') }}
            </td>
        </tr>
        <tr>
            <td>
                <label>Nombre:</label>
            </td>
            <td>
                {{ setDefault('name', b.name) }}
                {{ text_field('name', 'readonly':'true') }}
            </td>
        </tr>
        <tr>
            <td>
                <label>Correo de apoyo:&nbsp;&nbsp;</label>
            </td>
            <td>
                {{ setDefault('email_support', b.email_support) }}
                {{ text_field('email_support', 'readonly':'true') }}
            </td>
        </tr>
        <tr>
            <td>
                <label>Correo para<br>notificaciones:</label>
            </td>
            <td>
                <br>{{ setDefault('email_notification', b.email_notification) }}
                {{ text_field('email_notification', 'readonly':'true') }}
            </td>
        </tr>
        <tr>
            <td>
                <label>Api username:</label>
            </td>
            <td>
                {{ setDefault('username', b.api_username) }}
                {{ text_field('username', 'readonly':'true') }}
            </td>
        </tr>
            <tr>
                <td>
                    <label>Orden del prefijo:</label>
                </td>
                <td>
                    {{ setDefault('prefix', b.order_prefix) }}
                    {{ text_field('prefix', 'readonly':'true') }}
                </td>
            </tr>

        {% endfor %}
        </table>
    </div>
</fieldset>
<ul class="pager">
    <li class="pull-right">
        {{ submit_button("Delete", "class": "btn btn-primary") }}
    </li>
</ul>
</form>

