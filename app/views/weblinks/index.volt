{{ form ('weblinks/buscalinks', 'method':'post') }}
<fieldset>
    <div width="100%" style="display: block; overflow: auto;">
        <table align="center" class="detalle table-bordered table-striped">
            <tr><th colspan="4" style="text-align: center">Busqueda masiva de IBSN's:</th></tr>
            <tr>
                <td><label>Ingresar ISBN's:</label><br>
                    <div class="emergente">
                        {# <div align="center" class="ocultar"><label>?</label></div>#}
                        <div>(Separados por comas, sin espacios)</div>
                    </div>
                </td>
                <td>{{ text_area('isbn', 'cols': 50, 'rows': 10, 'id':'isbn', 'style':'margin-top:5px;') }}</td>
            </tr>
            <tr></tr>
        </table>
        <table align="center">
            <tr>
                <td colspan="3" style="text-align: right">
                    <ul class="pager">
                        <li class="pull-right">
                            {{ submit_button("Obtener reporte", "class": "btn btn-primary", "id":"ok") }}
                        </li>
                    </ul>
                </td>
            </tr>
        </table>
    </div>
</fieldset>
</form>

<script type="text/javascript">
    $(document).ready(function(){
        $('#ok').on('click', function(){
            $isbn = $('#isbn').val();

            if($isbn.trim() == '')
            {
                event.preventDefault();
                alert("Requiere ingresar al menos un ISBN");
            }

            if(($isbn.length) > 500)
            {
                event.preventDefault();
                alert("Demasiados ISBNs ingresados");
            }
        });

    });

</script>