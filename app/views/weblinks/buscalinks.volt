{{ content() }}

<div style="padding:10px;">
    <td colspan="4"><a href="/weblinks/excel" class="btn btn-primary">Descargar reporte en Archivo Excel</a></td>
</div>
<div width="100%" style="display: block; overflow: auto; margin-top:20px;">
    <table class="table table-bordered table-striped" align="center" id="tablita">
        <tr>
            <th>ISBN</th>
            <th>Link Porrua</th>
            <th>Link Gandhi</th>
        </tr>
        <?php $arrayBusqueda=[];?>
        {% for i in 0..(isbnsArray|length-1) %}
                <tr>
                    <td>
                        {% if isbnsArray[i] == null  %}
                            <strong style="color:red;">ISBN no encontrado</strong>
                        {% else %}
                            {{  isbnsArray[i] }}
                        {% endif %}

                    </td>

                    <td>
                        {% if listaPorrua[i]==null %}
                            <strong style="color:red;">Link no encontrado</strong>
                        {% else %}
                            {{ listaPorrua[i]['print'] }}
                        {% endif %}
                    </td>
                    <td>
                        {% if listaGandhi[i]==null %}
                            <strong style="color:red;">Link no encontrado</strong>
                        {% else %}
                            {{ listaGandhi[i]['print'] }}
                        {% endif %}
                    </td>
                </tr>
        {% endfor %}

    </table>
</div>
