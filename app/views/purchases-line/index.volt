{{ content() }}

<div class="row" style="padding-bottom: 30px">
    <div class="col-md-12">
        <label style="text-align: center;">Buscar por:</label>
    </div>

    {% if role=='1' or role == '4' %}

    <div class="col-md-3" style="margin-top: 12px;"style="margin-top: 12px;">
        <label>ID del socio:</label><br/>
        <select id="slc_partner">
            <option value="">Todos los partners</option>
            {% for p in partners %}
                <option value="{{ p.partner_id }}">{{ p.name }}</option>
            {% endfor %}
        </select>
    </div>
    <div class="col-md-3" style="margin-top: 9px;">
        <label>Compras:</label><br/>
        <select id="slc_venta">
            <option value="">Todas las ventas</option>
            <option value="1">Ventas reales</option>
            <option value="2">Ventas gratuitas</option>
        </select>
    </div>
    <div class="col-md-3">
        <label>Fecha de inicio:</label><br>
        {{ date_field('date_uno', 'id':'date_uno') }}

    </div>
    <div class="col-sm-3">
        <label>Fecha final:</label><br>
        {{ date_field('date_dos', 'id':'date_dos') }}
    </div>
    </div>
</div>
        {#---------------------------------------------------------------------------------------------#}
        {#---------------------------------------------------------------------------------------------#}
        {#---------------------------------------------------------------------------------------------#}
        {#---------------------------TABLA PARA ADMINISTRADOR------------------------------------------#}


<div width="100%" style="display: block; overflow: auto;">
    <table class="table table-bordered table-striped" align="center" id="answer-admin">
        <tr><th colspan="12">Registros por página/Total de registros: {{ page.total_items/ page.total_items * page.limit }}/{{ page.total_items }} </th></tr>
        <tr>
            <th>ID detalle compra</th>
            <th>ID de la compra</th>
            {#<th>ID del socio</th>#}
            <th>Fecha de la venta</th>
            <th>ID compra socio</th>
            <th>Resultado</th>
            <th>ID del producto</th>
            <th>Precio</th>
            <th>Cogs</th>
            <th>Código de moneda</th>
            <th>Url de descarga</th>
            <th>ISBN</th>
            <th>Título</th>
        </tr>


        {% for obj in page.items  %}

            <tr>

                <td>{{ obj.line_id }}</td>
                <td>{{ link_to("sales-reports/details/" ~ obj.purchase, obj.purchase,'target': '_blank') }}</td>
                {# <td>{{ link_to("partners/details/" ~ obj.partner_id, obj.partner_id) }}</td>#}

                <td>
                    <div class="emergente">
                        <div class="ocultar"><?php echo substr($obj->date,0,3).'...'; ?></div>
                        <div class="mostrar mostrarDate">{{ obj.date }}</div>
                    </div>
                </td>
                <td>{{ link_to("sales-reports/index/?idSocio=" ~ obj.purchase_partner_id, obj.purchase_partner_id,'target': '_blank') }}</td>
                <td>{{ obj.result }}</td>
                <td>
                <div class="emergente">
                 <div class="ocultar" style="color: #428bca;"><?php echo substr($obj->product_id,0,5).'...'; ?></div>
                 <div class="mostrar mostrarPID">{{ link_to("/library/getbookrev/" ~ obj.product_id, obj.product_id) }}</div>
                </div>
                </td>
                <td style="white-space:nowrap;">
                {% if obj.precio!=="GRATUITO" %}
                 {{ "$ "~ number_format(obj.precio,2) }}
                {% else %}
                 {{ "$ 0.00" }}
                {% endif %}
                </td>
                <td style="white-space: nowrap;">{{ "$ "~ number_format(obj.cogs, 2) }}</td>
                <td>{{ obj.cogs_currency_code }}</td>
                <td>
                <div class="emergente">
                 <div class="ocultar"><?php echo substr($obj->download_url,0,7).'...'; ?></div>
                 <div class="mostrar mostrarURL">{{  obj.download_url }}</div>
                </div>
                </td>
                <td>{{ obj.isbn }}</td>
                <td>
                    <div class="emergente">
                        <div class="ocultar"><?php echo substr($obj->titulo,0,5).'...'; ?></div>
                        <div class="mostrar mostrarTitulo">{{ obj.titulo }}
                        </div>
                    </div>
                </td>
             </tr>

         {% endfor %}
         <tr>
             <td colspan="12" align="left">
                 <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                 <div class="btn-group">
                     {{ link_to("purchases-line/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                     {{ link_to("purchases-line/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                     {{ link_to("purchases-line/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                     {{ link_to("purchases-line/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}

                 </div>
             </td>
         </tr>
     </table>
 </div>
 {#---------------------------------------------------------------------------------------------#}
        {#---------------------------------------------------------------------------------------------#}
        {#---------------------------------------------------------------------------------------------#}
{#-------------------------------------------PARTE PARA PARTNERS-----------------------------------------------------------------#}
{% else %}

    <div class="row" style="padding-bottom: 30px">
        <div class="col-md-3" style="margin-top: 12px;"style="margin-top: 12px;">
            <label>COMPRAS:</label><br/>
            <select id="slc_venta_p">
                <option value="0">Todas las ventas</option>
                <option value="1">Ventas reales</option>
                <option value="2">Ventas gratis</option>
            </select>
        </div>
        <div class="col-md-3" style="margin-top: 9px;">
            <label>FECHA INICIO:</label><br>
            {{ date_field('date_uno', 'id':'date_uno_p') }}
        </div>
        <div class="col-md-3">
            <label>FECHA FIN:</label><br>
            {{ date_field('date_dos', 'id':'date_dos_p') }}
        </div>
        </div>
    </div>

        {#---------------------------------TABLA PARA PARTNERS------------------------------------------------------------#}
    <div width="100%" style="display: block; overflow: auto;">
        <table class="table table-bordered table-striped" align="center" id="answer-partner">
            <tr><th colspan="12">Registros por página/Total de registros: {{ page.total_items/ page.total_items * page.limit }}/{{ page.total_items }} </th></tr>
            <tr>
                <th>ID detalle compra</th>
                <th>ID compra</th>
                <th>Fecha venta</th>
                <th>ID compra socio</th>
                <th>Resultado</th>
                <th>ID producto</th>
                <th>Precio</th>
                <th>Cogs</th>
                <th>Código de moneda</th>
                <th>Url de descarga</th>
                <th>ISBN</th>
                <th>Título</th>
            </tr>

            {% for ob in page.items %}
                <tr>
                    <td>{{ ob.line_id }}</td>
                    <td>{{ link_to("sales-reports/details/" ~ ob.purchase, ob.purchase,'target': '_blank') }}</td>
                    <td>
                        <div class="emergente">
                            <div class="ocultar"><?php echo substr($ob->date,0,3).'...'; ?></div>
                            <div class="mostrar mostrarDate">{{ ob.date }}</div>
                        </div>
                    </td>
                    <td>{{ link_to("sales-reports/index/", ob.purchase_partner_id,'target': '_blank') }}</td>
                    <td>{{ ob.result }}</td>
                    <td>
                        <div class="emergente">
                            <div class="ocultar" style="color: #428bca;"><?php echo substr($ob->product_id,0,5).'...'; ?></div>
                            <div class="mostrar mostrarPID">{{ link_to("/library/getbookrev/" ~ ob.product_id, ob.product_id) }}</div>
                        </div>
                    </td>
                    <td style="white-space:nowrap;">
                        {% if ob.precio!=="GRATUITO" %}
                            {{ "$ "~ number_format(ob.precio,2) }}
                        {% else %}
                            {{ "$ 0.00" }}
                        {% endif %}
                    </td>
                    <td style="white-space: nowrap;">{{ "$ "~ number_format(ob.cogs,2) }}</td>
                    <td>{{ ob.cogs_currency_code }}</td>
                    <td>
                        <div class="emergente">
                            <div class="ocultar"><?php echo substr($obj->download_url,0,7).'...'; ?></div>
                            <div class="mostrar mostrarURL">{{  ob.download_url }}</div>
                        </div>
                    </td>
                    <td>{{ ob.isbn }}</td>
                    <td>
                        <div class="emergente">
                            <div class="ocultar"><?php echo substr($ob->titulo,0,5).'...'; ?></div>
                            <div class="mostrar mostrarTitulo">{{ ob.titulo }}
                            </div>
                        </div>
                    </td>
                </tr>
            {% endfor %}

            <tr>
                <td colspan="12" align="left">
                    <span class="help-inline">{{ page.current }}/{{ page.total_pages }}</span>
                    <div class="btn-group">
                        {{ link_to("purchases-line/index", '<i class="icon-fast-backward"></i> Primera', "class": "btn") }}
                        {{ link_to("purchases-line/index?page=" ~ page.before, '<i class="icon-step-backward"></i> Anterior', "class": "btn") }}
                        {{ link_to("purchases-line/index?page=" ~ page.next, '<i class="icon-step-forward"></i> Siguiente', "class": "btn") }}
                        {{ link_to("purchases-line/index?page=" ~ page.last, '<i class="icon-fast-forward"></i> Última', "class": "btn") }}
                    </div>
                </td>
            </tr>
        </table>
    </div>

{% endif %}

<div align="right" id="link">

</div>


    {#---------------------------------------------------------------------------------------------#}
    {#---------------------------------------------------------------------------------------------#}
    {#---------------------------------------------------------------------------------------------#}
{#-------------------------------------------PARTE JAVASCRIPT-----------------------------------------------------------------#}

<script type="text/javascript">
    $(document).ready(function(){

        $('#date_uno').on('change', function () {
                $ajax_page = '';
                SearchAdmin($ajax_page);
        });


        $('#date_dos').on('change', function () {

                $ajax_page = '';
                SearchAdmin($ajax_page);
        });


        $('#slc_partner').on('change', function(){
            $ajax_page = '';
            SearchAdmin($ajax_page);
        });

        $('#slc_venta').on('change', function(){
            $ajax_page = '';
            SearchAdmin($ajax_page);
        });

        function comas(x)
        {
            x = x.toString();
            var pattern = /(-?\d+)(\d{3})/;
            while (pattern.test(x))
                x = x.replace(pattern, "$1,$2");
            return x;
        }


        function SearchAdmin($ajax_page)
        {
            $page = $ajax_page;
            $slc_partner = $('#slc_partner').val();
            $slc_venta = $('#slc_venta').val();
            $date_uno = $('#date_uno').val();
            $date_dos = $('#date_dos').val();

            $.get("<?php echo $this->url->get('purchases-line/searchadmin')?>",{'partner':$slc_partner, 'venta':$slc_venta, 'date_uno':$date_uno, 'date_dos':$date_dos, 'page':$page},function($data){

                var line = $data;
                var pur =  "<tr><th colspan='12'>Registros por página/Total de registros: " + ~~(line.total_items/line.total_items*line.limit) +"/" + line.total_items+ "</th></tr>"+
                        "<tr>"+
                             "<th>ID Detalle compra</th>"+
                             "<th>ID de compra</th>"+
                            // "<th>ID de socio</th>"+
                             "<th>Fecha venta</th>"+
                             "<th>ID compra socio</td>"+
                             "<th>Resultado</th>"+
                             "<th>ID del producto</th>"+
                             "<th>Precio</th>"+
                             "<th>Cogs</th>"+
                             "<th>Código de moneda</th>"+
                             "<th>URL de descarga</th>"+
                             "<th>ISBN</th>"+
                             "<th>Título</th>"+
                           "</tr>";


                var ob = '';
                $.each(line.items, function(index, ob){
                    if (ob.precio!=="GRATUITO")
                    {
                        var pr1 = 0.00;
                        pr1 += parseFloat(ob.precio);
                        precioDB = comas(pr1.toFixed(2));
                    }

                    else{
                        precioDB="0.00";
                    }


                    var pr2 = 0.00;
                    pr2 += parseFloat(ob.price_before_tax);
                    precioTax = comas(pr2.toFixed(2));

                    if(ob.cogs != null){
                        var cog = 0.00;
                        cog += parseFloat(ob.cogs);
                        cogs = comas(cog.toFixed(2));
                    }else{cogs = "0.00";}

                    pur +=  '<tr>'+
                            '<td>'+ob.line_id+'</td>'+
                            '<td><a href="details/'+ob.purchase+'">'+ob.purchase+'</a></td>'+
                           // '<td><a href="/partners/details/'+ob.partner_id+'">'+ob.partner_id+'</td>'+
                            '<td><div class="emergente"><div class="ocultar">' + (ob.date || " ").substring(0,3)+'...</div><div class="mostrar mostrarDate">'+ ob.date + '</div></div>'+
                            '<td><a href="details/">'+ob.purchase_partner_id+'</a></td>'+
                            '<td>'+ob.result+'</td>'+
                            '<td><div class="emergente"><div class="ocultar" style="color: #428bca;">' + ob.product_id.substring(0,3)+'...</div><div class="mostrar mostrarPID"><a href="/library/getbookrev/'+ob.product_id+'">'+ob.product_id+'</div></div></td>'+
                            '<td style="white-space:nowrap;">$ '+precioDB+'</td>'+
                            '<td style="white-space:nowrap;">$' + cogs + '</td>'+
                            '<td>'+ob.cogs_currency_code+'</td>'+
                            '<td><div class="emergente"><div class="ocultar">'+ (ob.download_url || " ").substring(0,3)+'...</div><div class="mostrar mostrarURL">'+ob.download_url+'</div></div></td>' +
                            '<td>' + ob.isbn + '</td>'+
                            '<td><div class="emergente"><div class="ocultar">' +(ob.titulo || " ").substring(0,5)+'...</div>'+
                            '<div class="mostrar mostrarTitulo">' + ob.titulo + '</div></div></td>';
                           pur += '</tr>';
                });

                pur += "<tr>"+
                            "<td colspan='12' align='left'>"+
                            "<span class='help-inline'>"+line.current+"/"+line.total_pages+"</span>"+
                            "<div class='btn-group'>"+
                            "<a data-first='1' id='first' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                            "<a data-before='"+line.before+"' id='before' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                            "<a data-next='"+line.next+"' id='next' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                            "<a data-last='"+line.last+"' id='last' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                            "</div>"+
                            "</td>"+
                       "</tr>";

                $('table#answer-admin').html(pur);

                $('#link').html(' <a href="/purchases-line/reporteexcel" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');
            });

        }


        {#------------------------------------------------ACCION DE PAGINACION AJAX----------------------------------------------#}

        {#---------------------------------------------------------------------------------------------------------------------------------#}
        $(document).on('click', '#next', function(){
            $ajax_page=$('#next').data('next');

            SearchAdmin($ajax_page);
        });
        $(document).on('click', '#first', function(){
            $ajax_page=$('#first').data('first');

            SearchAdmin($ajax_page);
        });
        $(document).on('click', '#last', function(){
            $ajax_page=$('#last').data('last');

            SearchAdmin($ajax_page);
        });
        $(document).on('click', '#before', function(){
            $ajax_page=$('#before').data('before');

            SearchAdmin($ajax_page);
        });


        {#------------------------------------------------------------------------------------------------------------------#}
        {#------------------------------------------------------------------------------------------------------------------#}
        {#------------------------------------------------------------------------------------------------------------------#}
        {#------------------------------------------------------------------------------------------------------------------#}
        {#------------------------------------------------------------------------------------------------------------------#}
        {#------------------------------------------------------------------------------------------------------------------#}
        {#-------------------------------------------------PARTE PARA PARTNER-----------------------------------------------------------------#}


        $('#slc_venta_p').on('change', function(){
            $ajax_page = '';
            SearchPartner($ajax_page);
        });
        $('#date_uno_p').on('change', function () {
            $ajax_page = '';
            SearchPartner($ajax_page);
        });


        $('#date_dos_p').on('change', function () {

            $ajax_page = '';
            SearchPartner($ajax_page);
        });


        function SearchPartner($ajax_page)
        {
            $page = $ajax_page;
            $slc_venta = $('#slc_venta_p').val();
            $date_uno = $('#date_uno_p').val();
            $date_dos = $('#date_dos_p').val();

            $.get("<?php echo $this->url->get('purchases-line/searchpartner')?>",{'venta':$slc_venta, 'page':$page, 'date_uno':$date_uno, 'date_dos':$date_dos },function($data){
                var line = $data;
                var pur =  "<tr><th colspan='12'>Registros por página/Total de registros: " + ~~(line.total_items/line.total_items*line.limit) +"/" + line.total_items+ "</th></tr>"+
                        "<tr>"+
                        "<th>ID detalle compra</th>"+
                        "<th>ID de compra</th>"+
                        "<th>Fecha venta</th>"+
                        "<th>ID compra socio</td>"+
                        "<th>Resultado</th>"+
                        "<th>ID del producto</th>"+
                        "<th>Precio</th>"+
                        "<th>Cogs</th>"+
                        "<th>Código de moneda</th>"+
                        "<th>URL de descarga</th>"+
                        "<th>ISBN</th>"+
                        "<th>Título</th>"+
                        "</tr>";
                var line = $data;


                $.each(line.items, function(index, ob){
                    if (ob.precio!=="GRATUITO")
                    {
                        var pr1 = 0.00;
                        pr1 += parseFloat(ob.precio);
                        precioDB = comas(pr1.toFixed(2));
                    }

                    else{
                        precioDB="0.00";
                    }

                    var pr2 = 0.00;
                    pr2 += parseFloat(ob.price_before_tax);
                    precioTax = comas(pr2.toFixed(2));

                    if(ob.cogs != null){
                        var cog = 0.00;
                        cog += parseFloat(ob.cogs);
                        cogs = comas(cog.toFixed(2));
                    }else{cogs = "0.00";}


                    pur +=  '<tr>'+
                    '<td>'+ob.line_id+'</td>'+
                    '<td><a href="details/'+ob.purchase+'">'+ob.purchase+'</a></td>'+
                    '<td><div class="emergente"><div class="ocultar">' + (ob.date || " ").substring(0,3)+'...</div><div class="mostrar mostrarDate">'+ ob.date + '</div></div>'+
                    '<td><a href="details/">'+ob.purchase_partner_id+'</a></td>'+
                    '<td>'+ob.result+'</td>'+
                    '<td><div class="emergente"><div class="ocultar" style="color: #428bca;">' + (ob.product_id || " ").substring(0,3)+'...</div><div class="mostrar mostrarPID"><a href="/library/getbookrev/'+ob.product_id+'">'+ob.product_id+'</div></div></td>'+
                    '<td style="white-space:nowrap;">$ '+precioDB+'</td>' +
                    '<td style="white-space:nowrap;">$' + cogs + '</td>'+
                    '<td>'+ob.cogs_currency_code+'</td>'+
                    '<td><div class="emergente"><div class="ocultar">'+(ob.download_url || " ").substring(0,3)+'...</div><div class="mostrar mostrarURL">'+ob.download_url+'</div></div></td>' +
                    '<td>' + ob.isbn + '</td>'+
                    '<td><div class="emergente"><div class="ocultar">' +(ob.titulo || " ").substring(0,5)+'...</div>'+
                    '<div class="mostrar mostrarTitulo">' + ob.titulo + '</div></div></td>';
                    pur += '</tr>';
                });

                pur += "<tr>"+
                        "<td colspan='12' align='left'>"+
                        "<span class='help-inline'>"+line.current+"/"+line.total_pages+"</span>"+
                        "<div class='btn-group'>"+
                        "<a data-first='1' id='first_p' class='btn'><i class='icon-fast-backward'></i>Primera</a>"+
                        "<a data-before='"+line.before+"' id='before_p' class= 'btn'><i class='icon-step-backward'></i> Anterior</a>"+
                        "<a data-next='"+line.next+"' id='next_p' class= 'btn'><i class='icon-step-forward'></i> Siguiente</a>"+
                        "<a data-last='"+line.last+"' id='last_p' class= 'btn'><i class='icon-fast-forward'></i> Última</a>"+
                        "</div>"+
                        "</td>"+
                        "</tr>";

                $('table#answer-partner').html(pur);

                $('#link').html(' <a href="/purchases-line/reporteexcel" class="btn btn-primary">Descargar reporte en Archivo Excel</a>');

                var cont = line.total_items;
                $('#conteo_p').html(cont +' '+ 'ventas');
            });

        }


        {#------------------------------------------------ACCION DE PAGINACION AJAX----------------------------------------------#}

        {#---------------------------------------------------------------------------------------------------------------------------------#}
        $(document).on('click', '#next_p', function(){
            $ajax_page=$('#next_p').data('next');

            SearchPartner($ajax_page);
        });
        $(document).on('click', '#first_p', function(){
            $ajax_page=$('#first_p').data('first');

            SearchPartner($ajax_page);
        });
        $(document).on('click', '#last_p', function(){
            $ajax_page=$('#last_p').data('last');

            SearchPartner($ajax_page);
        });
        $(document).on('click', '#before_p', function(){
            $ajax_page=$('#before_p').data('before');

            SearchPartner($ajax_page);
        });
    });
</script>
