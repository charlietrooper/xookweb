<?php
/**
 * Created by PhpStorm.
 * User: carlos-silva
 * Date: 19/05/2015
 * Time: 13:41
 */
class Users extends \Phalcon\Mvc\Model
{
    public $user_id;
    public $username;
    public $pass;
    public $name;
    public $email;
    public $created_at;
    public $active;
    public $role_id;
    public $partner_id;
}