<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 18/06/2015
 * Time: 11:30 AM
 */
class CustomersDia extends  \Phalcon\Mvc\Model
{
    public  function  getSource()
    {
        return "customers_dia";
    }
    public $parter_id;
    public $hora;
    public $num_customers;
}