<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 27/10/2015
 * Time: 02:20 PM
 */

class ViewPurchasesLine extends \Phalcon\Mvc\Model
{
    public function getSource()
    {
        return "view_purchases_line";
    }

    public $line_id;
    public $purchase;
    public $partner_id;
    public $date;
    public $result;
    public $product_id;
    public $precio;
    public $cogs_currency_code;
    public $price_currency_code;
    public $price_discount_applied;
    public $price_before_tax;
    public $download_url;
    public $content_format;
    public $idioma;
    public $genero;
    public $is_kids;
    public $is_adult_material;
    public $cogs;
    public $titulo;
    public $isbn;
    public $genero_bic;
    public $genero_bisac;
}