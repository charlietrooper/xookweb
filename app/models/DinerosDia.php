<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 18/06/2015
 * Time: 11:30 AM
 */
class DinerosDia extends  \Phalcon\Mvc\Model
{
    public  function  getSource()
    {
        return "dineros_dia";
    }
    public $partner_id;
    public $hora;
    public $dineros;
}