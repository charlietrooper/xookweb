<?php
/**
 * Created by PhpStorm.
 * User: carlos-silva
 * Date: 12/06/2015
 * Time: 17:35
 */
class NotificationCatalogues extends \Phalcon\Mvc\Model
{
    public function getSource()
    {
        return "notificationcatalogues";
    }
    public $notification_id;
    public $date_notification;
    public $user_email;
    public $partner_id;
    public $catalogue_id;
}