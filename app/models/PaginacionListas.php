<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 29/02/2016
 * Time: 04:11 PM
 */
class PaginacionListas
{
    /*
     * Se le pasará el valor de la fecha y se obtendrá el formato de DD/MM/AAAA HH:MM:SS
     */
    public function obtener_paginacion($listado)
    {
        $listado->paginasTotales = ceil($listado->items['TotalItems']/$listado->items['ItemsPerPage']);
        if($listado->page <= 1){
            $listado->anterior = 1;
        }
        else{
            $listado->anterior = $listado->page - 1;
        }
        if($listado->items['TotalItems'] <= 1){
            $listado->siguiente = 1;
        }elseif($listado->page >= $listado->paginasTotales){
            $listado->siguiente = $listado->page;
        }
        else{
            $listado->siguiente = $listado->page + 1;
        }
        $listado->ultima = ceil($listado->items['TotalItems']/$listado->items['ItemsPerPage']);
        $listado->actual = $listado->page;

        return $listado;
    }
}