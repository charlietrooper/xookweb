<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 07/08/2015
 * Time: 05:38 PM
 */
use Phalcon\Paginator\AdapterInterface;
class OrbilePaginator implements AdapterInterface
{

    private $config;
    private $page;


    /**
     * Adapter constructor.
     *
     * @param array $config Config.
     */
    public function __construct(array $config) {

      $this->config = $config;
      $this->page = $config['Content']['Page'];
    }

    /**
     * Set the current page number
     *
     * @param int $page
     */
    public function setCurrentPage($page)
    {
        // TODO: Implement setCurrentPage() method.
        $this->page = $page;
    }

    /**
     * Returns a slice of the resultset to show in the pagination
     *
     * @return \stdClass
     */
    public function getPaginate()
    {
        // TODO: Implement getPaginate() method.
        $page = new stdClass();

        $page->items = $this->config['Content']['Items']['LibraryInfo'];

        $page->first = 1;

        if($this->page <= 1)
        {
            $page->before = 1;
        }
        else{
            $page->before = $this->page - 1;
        }

        $page->current = $this->page;



        $page->last = ceil($this->config['Content']['TotalResults'] / $this->config['Content']['ItemsPerPage']);

        $page->total_pages = $page->last;

        $page->next = $this->page + 1;
        if($this->page >= $page->total_pages)
        {
            $page->next = $page->total_pages;
        }
        else{
            $page->next = $this->page + 1;
        }

        $page->total_items = $this->config['Content']['TotalResults'];
        //var_dump($this->config['Content']['TotalResults']);

        return $page;

    }

    /**
     * Set current rows limit
     *
     * @param int $limit
     */
    public function setLimit($limit)
    {
        // TODO: Implement setLimit() method.
         $this->limitRows = $limit;
    }

    /**
     * Get current rows limit
     *
     * @return int
     */
    public function getLimit()
    {
        // TODO: Implement getLimit() method.
        return $this->limitRows;
    }
}
