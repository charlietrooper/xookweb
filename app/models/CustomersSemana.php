<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 18/06/2015
 * Time: 11:30 AM
 */
class CustomersSemana extends  \Phalcon\Mvc\Model
{
    public  function  getSource()
    {
        return "customers_semana";
    }
    public $parter_id;
    public $dia_sem;
    public $num_customers;
}