<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 29/02/2016
 * Time: 04:11 PM
 */
class ReformateoDeCadena
{
    /*
     * Se le pasará el valor de la fecha y se obtendrá el formato de DD/MM/AAAA HH:MM:SS
     */

    //función que reemplaza caracteres acentuados por el mismo caracter sin acento
    public function stripAccents($stripAccents){
        $unwanted_array = array(    'Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E',
            'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U',
            'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c',
            'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o',
            'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );
        $str = strtr( $stripAccents, $unwanted_array );
        return($str);
    }

    public function obtener_formato($cadena)
    {
        $encontrar = "\u00";
        $resultado = strpos($cadena, $encontrar);
        if ($resultado !== false)
        {
            $valor = array(
                0 => '\u00e1',
                1 => '\u00e9',
                2 => '\u00ed',
                3 => '\u00f3',
                4 => '\u00fa',
                5 => '\u00c1',
                6 => '\u00c9',
                7 => '\u00cd',
                8 => '\u00d3',
                9 => '\u00da',
                10 => '\u00f1',
                11 => '\u00d1'
            );

            $reemplazo = array(
                0 => 'á',
                1 => 'é',
                2 => 'í',
                3 => 'ó',
                4 => 'ú',
                5 => 'Á',
                6 => 'É',
                7 => 'Í',
                8 => 'Ó',
                9 => 'Ú',
                10 => 'ñ',
                11 => 'Ñ'
            );
            $reemplazado = $cadena;
            //$cadena = 'La revoluci\u00f3n de los 22 d\u00edas';
            for ($i = 0; $i < 12; $i++) {
                $reemplazado = str_replace($valor[$i], $reemplazo[$i], $reemplazado);
                //echo $reemplazado;
            }
        }
        else
        {
            $reemplazado = $cadena;
        }
        return $reemplazado;
    }
}