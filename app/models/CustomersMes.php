<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 18/06/2015
 * Time: 11:30 AM
 */
class CustomersMes extends  \Phalcon\Mvc\Model
{
    public  function  getSource()
    {
        return "customers_mes";
    }
    public $partner_id;
    public $dia;
    public $mes;
    public $anio;
    public $num_customers;
}