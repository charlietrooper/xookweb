<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 18/06/2015
 * Time: 11:30 AM
 */
class DinerosSem extends  \Phalcon\Mvc\Model
{
    public  function  getSource()
    {
        return "dineros_sem";
    }
    public $parter_id;
    public $dia_sem;
    public $dineros;
}