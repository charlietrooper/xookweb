<?php
/**
 * Created by PhpStorm.
 * User: carlos-silva
 * Date: 10/06/2015
 * Time: 13:17
 */
class CatalogueUpdates extends \Phalcon\Mvc\Model
{
    public function getSource(){
        return "catalogueupdates";
    }
    public $catalogue_id;
    public $description;
    public $date_update;
}