<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 18/06/2015
 * Time: 11:30 AM
 */
class CatalogFeedFile extends  \Phalcon\Mvc\Model
{
    public  function  getSource()
    {
        return "catalog_feed_file";
    }
    public $file_id;
    public $file_path;
    public $server_path;
    public $start_time;
    public $end_time;
    public $num_ebooks;
}