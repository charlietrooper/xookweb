<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 26/06/2015
 * Time: 04:05 PM
 */
class PurchasesLine extends \Phalcon\Mvc\Model
{
    public function getSource()
    {
        return "purchases_line";
    }

    public $line_id;
    public $purchase_id;
    public $result;
    public $product_id;
    public $cogs_amount;
    public $cogs_currency_code;
    public $price_currency_code;
    public $price_discount_applied;
    public $price_before_tax;
    public $item_id;
    public $download_url;
    public $content_format;
    public $idioma;
    public $genero;
    public $is_kids;
    public $is_adult_material;
    public $cogs;
    public $titulo;
    public $isbn;
    public $genero_bic;
    public $genero_bisac;
}