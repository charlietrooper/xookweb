<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 18/06/2015
 * Time: 11:30 AM
 */
class PreOrder extends  \Phalcon\Mvc\Model
{
    public  function  getSource()
    {
        return "pre_order";
    }
    public $clv_pre_order;
    public $partner_id;
    public $purchase_id;
    public $product_id;
    public $email_customer;
    public $status;
    public $fch_order;
    public $fch_pedido;
    public $fch_entrega;
    public $title_product;
    public $isbn;
    public $kobo_id;
}