<?php
/**
 * Created by PhpStorm.
 * User: carlos-silva
 * Date: 12/06/2015
 * Time: 17:35
 */
class AppConfig extends \Phalcon\Mvc\Model
{
    public function getSource()
    {
        return "app_config";
    }
    public $id_url;
    public $key;
    public $value;
}