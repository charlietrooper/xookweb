<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 18/06/2015
 * Time: 11:30 AM
 */
class CustomersAnio extends  \Phalcon\Mvc\Model
{
    public  function  getSource()
    {
        return "customers_anio";
    }

    public $partner_id;
    public $mes;
    public $anio;
    public $num_customers;
}