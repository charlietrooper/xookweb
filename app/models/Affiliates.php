<?php
/**
 * Created by PhpStorm.
 * User: Xook
 * Date: 25/06/2015
 * Time: 04:24 PM
 */
class Affiliates extends \Phalcon\Mvc\Model
{
    public $affiliate_id;
    public $name;
    public $email;
    public $big_logo;
    public $small_logo;
    public $domain_url;
    public $layout;
    public $configuration;
}