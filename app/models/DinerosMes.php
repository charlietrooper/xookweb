<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 18/06/2015
 * Time: 11:30 AM
 */
class DinerosMes extends  \Phalcon\Mvc\Model
{
    public  function  getSource()
    {
        return "dineros_mes";
    }
    public $parter_id;
    public $dia;
    public $mes;
    public $anio;
    public $dineros;
}