<?php
/**
 * Created by PhpStorm.
 * User: carlos-silva
 * Date: 04/06/2015
 * Time: 12:50
 */
class Customers extends \Phalcon\Mvc\Model
{
    public $customer_id;
    public $partner_id;
    public $kobo_id;
    public $email;
    public $register_date;
    public $partner_customer_id;
    public $result;
    public $orbile_customer_id;
    public $account_type;
}