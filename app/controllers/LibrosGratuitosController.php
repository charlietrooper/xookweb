<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 09/03/2016
 * Time: 01:53 PM
 */

use Phalcon\Paginator\Adapter\model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Router as Router;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class LibrosGratuitosController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Libros Gratuitos');
        parent::initialize();
    }

    public function indexAction()
    {
        $formato = new ReformateoDeCadena();
        $generos = $this->modelsManager->executeQuery("SELECT DISTINCT genero_esp AS genero FROM Generos ORDER BY genero");
        $this->view->setVar('generos', $generos);
        $arrGenesp2=[];
        $arrVacio=[];
        foreach ($generos as $gen)
        {
            $test2=$formato->obtener_formato($gen['genero']);
            array_push($arrGenesp2,$test2);
            array_push($arrVacio,$gen['genero']);

        }
        $this->view->setVar('generosPrint', $arrGenesp2);
        $this->view->setVar('generosArray', $arrVacio);
    }


    public function busquedaAction()
    {
        $elementos = $this->request->get('elementos');
        $language = $this->request->get('language');
        $genero = $this->request->get('genero');
        $genero=  utf8_decode($genero);
       // $items_per_page = 20;
     /*   if ($elementos<=$items_per_page)
        {
            //se definen varibales a utilizar
            $libgratis = (object)[];
            $page = $this->request->get("page", "int", 1);
            $libgratis->page = $page;
        }

        else{
            $page=1;
        }*/
        $page=1;

        if (!isset($elementos))
        {
            $elementos=0;
        }

        if($genero=="TODOS" && $language=="TODOS")
        {
            $url2="books/freeBooks/$elementos/$page";
        }

        else if($genero!=="TODOS" && $language=="TODOS"){
            $url2="books/freeBooks/$genero/$elementos/$page";
        }

        else if($genero=="TODOS" && $language!=="TODOS"){
            $url2="/books/freeBooks/language/$language/$elementos/$page";
        }
        else{
            $url2="/books/freeBooks/filter/$language/$genero/$elementos/$page";
        }


        //Se define la url a la que se conectará
        $ruta = 'orbile_catalog_url';
        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;

        //se realiza la conexión cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$url/$url2");
       // curl_setopt($ch, CURLOPT_URL, $url2);
        curl_setopt($ch, CURLOPT_TIMEOUT, 400);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $resultado = curl_exec ($ch);
        $result = json_decode($resultado, true);

        if(isset($result['Items'][0]))
        {
            //Se decodifica el Json y se asigna al objeto

            // $resultGen=obtenerGeneros($result);
            $arrListado=$this->obtenerListado($result,$elementos);
            for($i=0;$i<count($arrListado[0]);$i++)
            {
                $idioma=$this->obtenerLenguajeCompleto($arrListado[0][$i]['language']);
                $arrListado[0][$i]['leng']=$idioma;
                $autTemp = $this->obtenerDatos($arrListado[0][$i]['author']['contributor']);
                if($autTemp)
                {
                    $autTemp=substr_replace($autTemp,"",-2);
                    $arrListado[0][$i]['autores']=$autTemp;
                }
            }
            $this->view->setVar('libgratis', $arrListado[0]);
            $this->view->setVar('msg', $arrListado[1]);
            $this->persistent->libros = $arrListado;
            $this->persistent->generoExcel = $genero;
        }

        else if(isset($result['0']['_id']))
        {

        }

        else{
            $this->view->setVar('$libgratis', null);
        }


    }

    public function obtenerListado($json, $numElementos)
    {
        $msg="";
        $arrRegresar=[];
        if(isset($json['Items']))
        {
            $arrUsar=$json['Items'];
            array_push($arrRegresar,$arrUsar);
            if(count($arrUsar) < $numElementos)
            {
                $msg="*Advertencia: La cantidad de elementos buscados rebasa la cantida de libros gratuitos en existencia de ese género.";
                array_push($arrRegresar,$msg);
                return $arrRegresar;
            }

            else{
                array_push($arrRegresar,null);
                return $arrRegresar;
            }

        }

        else {
            return null;
        }


    }

    public function obtenerLenguajeCompleto($leng)
    {
        $language="";
        if (strtoupper($leng)=="EN")
        {
            $language="Inglés";
        }

        else if(strtoupper($leng)=="ES")
        {
            $language="Español";
        }

        else{
            $language="Información No Disponible";
        }

        return $language;

    }


    public function excelAction()
    {
        $libros = $this->persistent->libros;
        $genero = $this->persistent->generoExcel;

        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "LIBROS_GRATUITOS_REPORT_".$genero." ". "-" . date("Y-m-d_his") . ".xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $header = array(
            'TÍTULO'=>'string',
            'ISBN'=>'string',
            'ActiveRevisionID'=>'string',
            'Autores'=>'string',
            'Lenguaje'=>'string',
            'GENERO'=>'string'
        );

        $data1=[];
        $conteo=0;
        foreach($libros[0] as $lib)
        {

            $tempArray=[
                $lib['title'],
                $lib['isbn'],
                $lib['active_revision_id'],
                $lib['autores'],
                $lib['leng'],
                $genero
            ];
            array_push($data1,$tempArray);
            $conteo++;

        }

        $writer = new XLSXWriter();
        $writer->setAuthor('Orbile');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();
        exit(0);
    }

    public function obtenerDatos($array) {
        $arrDatos=[];
        $datos="";
        if(is_array($array))
        {
            foreach ($array as $id => $valor) {
                if (is_array($valor)) {
                    $datos .=  $this->obtenerDatos($valor);
                } else {
                    if(empty($valor)) {
                        $datos .= '';
                    } else if(($id=="@value")) {
                        $datos .= $valor .", ";
                    }
                }
            }
            return $datos;
        } else {
            if(empty($array)) {
                return '';
            } else {
                return $array;
            }
            return $array;
        }

    }
}

