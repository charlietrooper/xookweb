
<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 27/10/2015
 * Time: 10:53 AM
 */
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Model\Query as Query;

class PurchasesLineController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Detalle de ventas');
        parent::initialize();
    }

    public function indexAction()
    {
        //Traemos la session y sacamos el role_id y el partner_id
        $session = $this->session->get('auth');
        $partner_id = $session['partner_id'];
        $role_id = $session['role_id'];
          //Mandamos el valor del role_id a la vista
        //para aparecer u ocultar campos de busqueda segun sea "admin o partner"
        $this->view->setvar('role', $role_id);
         //Revisamos que role tiene en el sistema
        //Si es Administrador
        $condiciones="";
        $variables=array();
        if($role_id == 1 || $role_id == 4) {
            //Buscamos los partners para enviarlos a la vista
            $partners = Partners::find();
            $this->view->setVar('partners', $partners);
        }
        else
        {
            $condiciones="ViewPurchasesLine.partner_id = :partner:";
            $variables=array(
                'partner' => $partner_id
            );
        }

        $purchases_line = $this->modelsManager->createBuilder()
            ->columns('line_id, purchase, partner_id, purchase_partner_id, DATE_FORMAT(date, "%d-%m-%Y %H:%i:%s") as date, result, product_id, precio, cogs_currency_code, price_currency_code, price_discount_applied, price_before_tax, download_url, content_format, idioma, genero, is_kids, is_adult_material, cogs, titulo, isbn, genero_bic, genero_bisac')
            ->from('ViewPurchasesLine')
            ->where($condiciones, $variables)
            ->orderBy('line_id DESC');

        $paginator = new PaginatorQueryBuilder(array(
            "builder" => $purchases_line,
            "limit" => 20,
            "page" => $this->request->getQuery('page', 'int')
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /**
     *
     * ////////////////////////////////////IMPRESION DE ARCHIVO EXCEL
     *
     */

    public function ReporteExcelAction()
    {

        ob_end_clean();
        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "DETALLE_VENTAS" . "-" . date("Y-m-d_his") . ".xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');


        //Traemos la variable de session auth y sacamos el role_id y el partner_id
        $session = $this->session->get('auth');
        $role_id = $session['role_id'];
        $partner_id = $session['partner_id'];

        $busq = $this-> persistent->busq;

        //de la varibale busq se extraen los valores

        $condiciones = $busq ['condiciones'];
        $variables = $busq ['variables'];

        //columnas que se traerán en las consultas
        $purchases_line = ViewPurchasesLine::find(array(
            'columns' => 'line_id, purchase, partner_id, purchase_partner_id, DATE_FORMAT(date, "%d-%m-%Y %H:%i:%s") as date, result, product_id, precio, cogs_currency_code, price_currency_code, price_discount_applied, price_before_tax, download_url, content_format, idioma, genero, is_kids, is_adult_material, cogs, titulo, isbn, genero_bic, genero_bisac',
            $condiciones,
            'bind' => $variables,
            'order' => 'line_id DESC'
        ));

        $header = array(
            'ID DETALLE COMPRA' => 'string',
            'ID COMPRA' => 'string',
            'FECHA VENTA' => 'string',
            'ID COMPRA SOCIO' => 'string',
            'RESULTADO' => 'string',
            'ID PRODUCTO' => 'string',
            'PRECIO' => 'string',
            'COGS' => 'string',
            'CODIGO MONEDA' => 'string',
            'ISBN' => 'string',
            'Título' => 'string'
        );
        $data1 = [];

        foreach ($purchases_line as $pl) {
            $tempArray = [
                $pl->line_id,
                $pl->purchase,
                $pl->date,
                $pl->purchase_partner_id,
                $pl->result,
                $pl->product_id,
                $pl->precio,
                $pl->cogs,
                $pl->cogs_currency_code,
                $pl->isbn,
                $pl->titulo,
            ];
            array_push($data1, $tempArray);
        }

        $writer = new XLSXWriter();
        $writer->setAuthor('Orbile');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();
        exit(0);


    }

    /*
     * ///////////////////////////////////////FUNCIONES PARA USO DE AJAX//////////////////////////////////
     */

    private function getSearchConditions($search, $searchValue)
    {
        switch($search)
        {
            case ('partner'):
            {
                $condiciones = 'ViewPurchasesLine.partner_id = :partner:';
                break;
            }
            case ('venta'):
            {
                if($searchValue==1)
                {
                    $condiciones = 'ViewPurchasesLine.result = "Success" AND ViewPurchasesLine.precio > 0';
                    break;
                }

                else if ($searchValue==2)
                {
                    $condiciones = 'ViewPurchasesLine.precio = "GRATUITO"';
                    break;
                }

            }
            case ('date_uno'):
            {

                $condiciones = 'CAST(ViewPurchasesLine.date AS DATE) >= :date_uno:';
                break;
            }
            case ('date_dos'):
            {
                $condiciones = 'CAST(ViewPurchasesLine.date AS DATE) <= :date_dos:';
                break;
            }


        }
        return $condiciones;
    }

    public function SearchAdminAction()
    {
        $pagina = $this->request->get('page', null, '');
        //traemos los datos via get con la peticion ajax
        $partner = $this->request->get('partner', null, '');
        $venta = $this->request->get('venta', null, '');
        $date_uno = $this->request->get('date_uno', null, '');
        $date_dos = $this->request->get('date_dos', null, '');
        $ajax_page = $this->request->get('page', null, '');

        //se crea una variable para el limite de resultados por pagina
        $rowPerPage = 20;

        //deshabilitamos la vista para las peticiones ajax
        $this->view->disable();

        if ($this->request->isGet() == true) {
            $searchParameters = array('partner' => $partner, 'venta' => $venta, 'date_uno' => $date_uno, 'date_dos' => $date_dos);
            $condiciones = '';
            $variables = array();
            foreach ($searchParameters as $search => $search_value) {
                if ($search_value !== '') {
                    $condiciones .= $this->getSearchConditions($search, $search_value);
                    if ($search !== 'venta') {
                        $variables[$search] = $search_value;
                    }
                    $condiciones .= ' AND ';

                }
            }

            if ($condiciones !== '') {
                $condiciones = substr($condiciones, 0, -5);

            }

            $purchases_line = $this->modelsManager->createBuilder()
                ->columns('line_id, purchase, partner_id, purchase_partner_id, DATE_FORMAT(date, "%d-%m-%Y %H:%i:%s") as date, result, product_id, precio, cogs_currency_code, price_currency_code, price_discount_applied, price_before_tax, download_url, content_format, idioma, genero, is_kids, is_adult_material, cogs, titulo, isbn, genero_bic, genero_bisac')
                ->from('ViewPurchasesLine')
                ->where($condiciones, $variables)
                ->orderBy('line_id DESC');

            $this->persistent->busq = array('condiciones' => $condiciones, 'variables' => $variables);

            //Se agraga la consulta al Paginador
            $paginator = new PaginatorQueryBuilder(array(
                "builder" => $purchases_line,
                "limit" => $rowPerPage,
                "page" => $pagina
            ));

            $page = $paginator->getPaginate();
            $page->items = $page->items->toArray();

            $this->response->setJsonContent($page);
            $this->response->setContentType('application/json', 'UTF-8');

            $this->response->setStatusCode(200, "Ok");
            $this->response->send();

        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->response->send();
        }

    }

    /**
     *
     * /////////////////////////////////////BUSQUEDA DE AJAX PARA PARTNERS////////////////////////////////
     *
     *
     */
    public function SearchPartnerAction()
    {
        //Traemos la session para sacar el partner que acceso a  la pagina
        $session = $this->session->get('auth');
        $partner_id = $session['partner_id'];

        //traemos los datos via get con la peticion ajax
        $venta = $this->request->get('venta', null, '');
        $date_uno = $this->request->get('date_uno', null, '');
        $date_dos = $this->request->get('date_dos', null ,'');
        $pagina = $this->request->get('page', null, '');

    //    $ajax_page = $this->request->get('page', null,'');

        //se crea una variable para el limite de resultados por pagina
        $rowPerPage = 20;

        //deshabilitamos la vista para las peticiones ajax
        $this->view->disable();

        if ($this->request->isGet() == true) {
            $searchParameters = array('partner'=>$partner_id,'venta'=>$venta,'date_uno'=>$date_uno,'date_dos'=>$date_dos);
            $condiciones='';
            $variables=array();
            foreach($searchParameters as $search => $search_value)
            {
                if($search_value!=='')
                {
                    $condiciones.=$this->getSearchConditions($search,$search_value);
                    if($search!=='venta')
                    {
                        $variables[$search] = $search_value;
                    }
                    $condiciones.=' AND ';

                }
            }

            if($condiciones!=='')
            {
                $condiciones=substr($condiciones, 0, -5);

            }

            $purchases_line = $this->modelsManager->createBuilder()
                ->columns('line_id, purchase, partner_id, purchase_partner_id, DATE_FORMAT(date, "%d-%m-%Y %H:%i:%s") as date, result, product_id, precio, cogs_currency_code, price_currency_code, price_discount_applied, price_before_tax, download_url, content_format, idioma, genero, is_kids, is_adult_material, cogs, titulo, isbn, genero_bic, genero_bisac')
                ->from('ViewPurchasesLine')
                ->where($condiciones, $variables)
                ->orderBy('line_id DESC');

            $this-> persistent->busq = array('condiciones' => $condiciones, 'variables' => $variables);

            //Se agraga la consulta al Paginador
            $paginator = new PaginatorQueryBuilder(array(
                "builder" => $purchases_line,
                "limit" => $rowPerPage,
                "page" => $pagina
            ));

            $page = $paginator->getPaginate();
            $page->items = $page->items->toArray();

            $this->response->setJsonContent($page);
            $this->response->setContentType('application/json', 'UTF-8');

            $this->response->setStatusCode(200, "Ok");
            $this->response->send();

        }

        else
        {
            $this->response->setStatusCode(404, "Not Found");
            $this->response->send();
        }

    }

}