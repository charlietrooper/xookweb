<?php

use Phalcon\Paginator\Adapter\model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Router as Router;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class PrecioFuturoController extends ControllerBase
{

    public function initialize()
    {
        $this->tag->setTitle('Precio Futuro');
        parent::initialize();
    }

    /*
     * Función para mostrar el listado de Precio Futuro
     */
    public function indexAction()
    {
        //se definen varibales a utilizar
        $pfuturo = (object)[];
        $page = $this->request->get("page", "int", 1);
        $pfuturo->page = $page;
        $items_per_page = 20;

        //Se define la url a la que se conectará
        $ruta = 'orbile_catalog_url';
        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;
        //$url="http://catalog.orbiletest.com";
        //se realiza la conexión cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$url/futureprices/list/$items_per_page/$page");
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $resultado = curl_exec ($ch);

        //Se decodifica el Json y se asigna al objeto
        $result = json_decode($resultado, true);
        $pfuturo->items = $result;

        //se manda llamar la paginación de listados
        $paginacionListas = new PaginacionListas();
        $listadoPfuturo = $paginacionListas->obtener_paginacion($pfuturo);

        $this->view->setVar('pfuturo', $listadoPfuturo);

    }



    /*
     * Función para la busqueda de Precio Futuro
     */
    public function busquedaAction()
    {

        $pfuturo_busqueda = (object)[];
        $page =  intval($this->request->get('pagina', 'int', 1));
        $pfuturoTitulo = $this->request->get('futuroTitulo');
        $pfuturoISBN= $this->request->get('futuroISBN');
        $pActualLista= $this->request->get('pActualLista');
        $pActualVenta= $this->request->get('pActualVenta');
        $pFuturoLista= $this->request->get('pFuturoLista');
        $pFuturoVenta= $this->request->get('pFuturoVenta');
        $busquedaID= $this->request->get('busquedaID');
        $variacion= $this->request->get('variacion');


        $pfuturo_busqueda->page = $page;
        $items_per_page = 20;
        //Se deshabilita la vista para las peticiones ajax
        $this->view->disable();



        if ($this->request->isGet() == true) {
            //Se define la url a la que se conectará
            $ruta = 'orbile_catalog_url';
            $api = AppConfig::findFirst(array(
                'key = :ruta:',
                'bind' => array('ruta' => $ruta)
            ));
            $url = $api->value;
           // $url="http://catalog.orbiletest.com";

            $cadenaCURL="";

            //BUSQUEDA
            switch($busquedaID){
                case 1://BUSQUEDA POR TITULO
                {
                    $pfuturo=$pfuturoTitulo;
                    $pfuturo = urlencode($pfuturo);
                    $cadenaCURL="$url/futureprices/search/title/$pfuturo/$items_per_page/$page";
                    break;
                }

                case 2://BUSQUEDA POR ISBN
                {
                    $pfuturo=$pfuturoISBN;
                    $pfuturo = urlencode($pfuturo);
                    $cadenaCURL="$url/futureprices/search/isbn/$pfuturo";
                    break;
                }

                case 3://BUSQUEDA POR PRECIO ACTUAL DE LISTA
                {
                    switch($pActualLista)
                    {
                        case 1:
                        {
                            $cadenaCURL="$url/futureprices/today/listprice/50orless/$items_per_page/$page";
                            break;
                        }

                        case 2:
                        {
                            $cadenaCURL="$url/futureprices/today/listprice/50to100/$items_per_page/$page";

                            break;
                        }

                        case 3:
                        {
                            $cadenaCURL="$url/futureprices/today/listprice/100to200/$items_per_page/$page";
                            break;
                        }

                        case 4:
                        {
                            $cadenaCURL="$url/futureprices/today/listprice/200to500/$items_per_page/$page";
                            break;
                        }

                        case 5:
                        {
                            $cadenaCURL="$url/futureprices/today/listprice/500ormore/$items_per_page/$page";
                            break;
                        }

                        default:
                        {
                            $cadenaCURL='';
                        }
                    }

                    break;
                }

                case 4://BUSQUEDA POR PRECIO ACTUAL DE VENTA
                {
                    switch($pActualVenta)
                    {
                        case 1:
                        {
                            $cadenaCURL="$url/futureprices/today/sellingprice/50orless/$items_per_page/$page";
                            break;
                        }

                        case 2:
                        {
                            $cadenaCURL="$url/futureprices/today/sellingprice/50to100/$items_per_page/$page";

                            break;
                        }

                        case 3:
                        {
                            $cadenaCURL="$url/futureprices/today/sellingprice/100to200/$items_per_page/$page";
                            break;
                        }

                        case 4:
                        {
                            $cadenaCURL="$url/futureprices/today/sellingprice/200to500/$items_per_page/$page";
                            break;
                        }

                        case 5:
                        {
                            $cadenaCURL="$url/futureprices/today/sellingprice/500ormore/$items_per_page/$page";
                            break;
                        }

                        default:
                        {
                            $cadenaCURL='';
                        }
                    }
                    break;
                }

                case 5://BUSQUEDA POR PRECIO FUTURO DE LISTA
                {
                     switch($pFuturoLista)
                     {
                         case 1:
                         {
                             $cadenaCURL="$url/futureprices/future/listprice/50orless/$items_per_page/$page";
                             break;
                         }

                         case 2:
                         {
                             $cadenaCURL="$url/futureprices/future/listprice/50to100/$items_per_page/$page";

                             break;
                         }

                         case 3:
                         {
                             $cadenaCURL="$url/futureprices/future/listprice/100to200/$items_per_page/$page";
                             break;
                         }

                         case 4:
                         {
                             $cadenaCURL="$url/futureprices/future/listprice/200to500/$items_per_page/$page";
                             break;
                         }

                         case 5:
                         {
                             $cadenaCURL="$url/futureprices/future/listprice/500ormore/$items_per_page/$page";
                             break;
                         }

                         default:
                         {
                             $cadenaCURL='';
                         }
                     }
                    break;
                }

                case 6: //BUSQUEDA POR PRECIO FUTURO DE VENTA
                {
                    switch($pFuturoVenta)
                    {
                        case 1:
                        {
                            $cadenaCURL="$url/futureprices/future/sellingprice/50orless/$items_per_page/$page";
                            break;
                        }

                        case 2:
                        {
                            $cadenaCURL="$url/futureprices/future/sellingprice/50to100/$items_per_page/$page";

                            break;
                        }

                        case 3:
                        {
                            $cadenaCURL="$url/futureprices/future/sellingprice/100to200/$items_per_page/$page";
                            break;
                        }

                        case 4:
                        {
                            $cadenaCURL="$url/futureprices/future/sellingprice/200to500/$items_per_page/$page";
                            break;
                        }

                        case 5:
                        {
                            $cadenaCURL="$url/futureprices/future/sellingprice/500ormore/$items_per_page/$page";
                            break;
                        }

                        default:
                        {
                            $cadenaCURL='';
                        }
                    }
                    break;
                }

                case 7:
                {
                    switch($variacion)
                    {
                        case 1: //sube
                        {
                            $cadenaCURL="$url/futureprices/future/sellingprice/sube/$items_per_page/$page";
                            break;
                        }

                        case 2: //baja
                        {
                            $cadenaCURL="$url/futureprices/future/sellingprice/baja/$items_per_page/$page";

                            break;
                        }

                        case 3: //igual
                        {
                            $cadenaCURL="$url/futureprices/future/sellingprice/igual/$items_per_page/$page";
                            break;
                        }
                    }
                    break;
                }
                default:
                {
                    $cadenaCURL='';
                }
            }


            //se realiza la conexión cURL
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $cadenaCURL);
            curl_setopt($ch, CURLOPT_TIMEOUT, 80);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $resultado = curl_exec ($ch);

            //Se decodifica el Json y se asigna al objeto
            $result = json_decode($resultado, true);
            $pfuturo_busqueda->items = $result;

            //se manda llamar la paginación de listados
            $paginacionListas = new PaginacionListas();
            $listadoPfuturoBusqueda = $paginacionListas->obtener_paginacion($pfuturo_busqueda);


            $this->response->setJsonContent($listadoPfuturoBusqueda);
            $this->response->setContentType('application/json', 'UTF-8');

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }
    }
}

