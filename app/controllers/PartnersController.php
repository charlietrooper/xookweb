<?php

use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Model\Query as Query;

/**
 *
 * Controlador para administracion de datos de partners
 *
 * Class PartnersController
 */
class PartnersController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Socios');
        parent::initialize();
    }

    public function indexAction()
    {
        //Se le asigna a la variable partner la consulta a la tabla Partners
        $partners = $this->modelsManager->createBuilder()
            ->columns('*')
            ->from('Partners')
            ->orderBy('partner_id DESC');

        $numberPage = $this->request->getQuery("page", "int", "1");

        //se crea el paginador que muestra X hojas por página
        $paginator = new PaginatorQueryBuilder(array(
            "builder" => $partners,
            "limit" => 10,
            "page" => $numberPage
        ));

        //xdebug_var_dump($partner);
        //xdebug_var_dump($paginator);
        //se pasa el objeto a la vista con el nombre de page
        $this->view->page = $paginator->getPaginate();
    }
/*
 *
 */
    public function createAction()
    {

    }

    public function deleteAction($obj)
    {
        $i = Partners::find($obj);
        $this->view->setvar("i", $i);
    }

    /**
     *
     * Edita al partner
     *
     * @param $obj Id del partner
     */
    public function editAction($obj)
    {
        $a = Partners::find($obj);
        $this->view->setvar("a", $a);
    }
/*
 *Guarda el partner
 */
    public function savepartnerAction()
    {
        //Trae datos de la vista y guarda en variables
        $name = $this->request->getPost("name");
        $email = $this->request->getPost("email_support");
        $mail = $this->request->getPost("email_notification");
        $username = $this->request->getPost("user");
        $pass = $this->request->getPost("pass");
        $prefix = $this->request->getPost("prefix");

        //Trae el model partner y guardar
        $partner = new Partners();
        $partner->name = $name;
        $partner->email_support = $email;
        $partner->email_notification = $mail;
        $partner->api_username = $username;
        $partner->api_pass = $this->security->hash($pass);
        $partner->order_prefix = $prefix;
        $success = $partner->save();
        if ($success) {
            $this->flash->success("El partner a sido creado exitosamente!!");
            return $this->forward("partners/index");
        } else {

            foreach ($partner->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('partners/index');
        }
    }
/*
 *Funcion para eliminar un partner que ya existe en la BD
 *
 * Solo con el id
 */
    public function eliminarAction()
    {
        //traemos el id mostrado en la vista
        $id = $this->request->getPost("partner_id");

        //Se realiza la busqueda con el id obtenido
        $partner = Partners::find($id);

        //Se llama al metod delete para proceder con la baja del partner
        $success = $partner->delete();
        if ($success) {
            $this->flash->success("El partner con id $id, a sido eliminado!!");
            return $this->forward("partners/index");
        } else {
            foreach ($partner->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('partners/index');
        }
    }
/*
 *Actualiza los datos del parter
 *
 * @Param $obj $id
 */
    public function updateAction()
    {
        //traemos los datos via post de la vista
        $id = $this->request->getPost("partner_id");
        $name = $this->request->getPost("name");
        $email = $this->request->getPost("email_support");
        $mail = $this->request->getPost("email_notification");
        $user = $this->request->getPost("username");
        $pass = $this->request->getPost("pass");
        $prefix = $this->request->getPost("prefix");

        //hacemos la consulta a la BD con el $id
        $partner = Partners::findFirst($id);

        //Checamos que el password de la vista sea igual al de la DB
        if($this->security->checkHash($pass ,$partner->api_pass))
        {
            //Relacionamos los valores traidos de la vista con la de $partner
            $partner->name = $name;
            $partner->email_support = $email;
            $partner->email_notification = $mail;
            $partner->api_username = $user;
            $partner->order_prefix = $prefix;
        }
        else
        {
            //Relacionamos los valores traidos de la vista con la de $partner
            $partner->name = $name;
            $partner->email_support = $email;
            $partner->email_notification = $mail;
            $partner->api_username = $user;
            $partner->api_pass = $this->security->hash($pass);
            $partner->order_prefix = $prefix;
        }


        //Hacemos un update
        $success = $partner->update();
        if ($success) {
            $this->flash->success("El partner con id: $id, a sido editado!!");
            return $this->forward('partners/index');
        }

        else {
            foreach ($partner->getMessages() as $messages) {
                $this->flash->error($messages);
            }
            return $this->response->redirect('partners/index');
        }
    }

    /*
     *Función para visualizar los detalles de
     *cada partner y los links para sus reportes
     */
    public function detailsAction($obj)
    {
        //se hace la consulta
        $d = Partners::find($obj);

        //se envía a la vista los resultados de la búsqueda
        $this->view->setvar("d", $d);

        //Se manda a la sessión el id del partner
        //$this->session->set('part', array('partner_id' => $obj));
    }
}