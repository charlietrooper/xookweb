<?php

use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Model\Query as Query;

/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 25/06/2015
 * Time: 04:28 PM
 */
//comentando para cambios
class AffiliatesController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Affiliates');
        parent::initialize();
    }

    /*
     *
     */
    public function indexAction()
    {
        //Se le asigna a la variable affiliate la consulta a la tabla Affiliates

        $affiliate = $this->modelsManager->createBuilder()
            ->columns('*')
            ->from('Affiliates')
            ->orderBy('affiliate_id DESC');

        $numberPage = $this->request->getQuery("page", "int");

        //Se crea el paginator que muestra x hojas por pagina
        $paginator = new PaginatorQueryBuilder(array(
            "builder" => $affiliate,
            "limit" => 10,
            "page" => $numberPage
        ));

        //Se pasa el objeto a la vista con el nombre page
        $this->view->page = $paginator->getPaginate();
    }

    /*
     *Función para guardar los datos del affiliate
     */
    public function RegisterAction()
    {
        //se obtienen los datos de la vista
        //asignándolos a sus respectivas variables
        $name = $this->request->getPost('name');
        $email = $this->request->getPost('email');
        $big_logo = $this->request->getPost('big_logo');
        $small_logo = $this->request->getPost('small_logo');
        $domain_url = $this->request->getPost('domain_url');
        $layout = $this->request->getPost('layout');
        $configuration = $this->request->getPost('configuration');

        //Se manda a llamar al modelo Affiliate
        $affiliate = new Affiliates();

        //Se guardan los nuevos datos en la tabla
        $affiliate->name = $name;
        $affiliate->email = $email;
        $affiliate->big_logo = $big_logo;
        $affiliate->small_logo = $small_logo;
        $affiliate->domain_url = $domain_url;
        $affiliate->layout = $layout;
        $affiliate->configuration = $configuration;
        //
        $success = $affiliate->save();
        if ($success) {
            $this->flash->success("¡La filial a sido creada exitosamente!");
            return $this->response->redirect("affiliate/index");
        } else {

            foreach ($affiliate->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->response->redirect('affiliates/index');
        }

    }

    /*
     *Función para redireccionar a la vista create.volt
     */
    public function CreateAction()
    {

    }

    /*
     *
     */
    public function EditAction($a)
    {
        $affi = Affiliates::find($a);
        $this->view->setVar("affi", $affi);
    }

    /*
     *
     */
    public function UpdateAction()
    {
        //Traemos los datos via post de la vista
        $id = $this->request->getPost('id');
        $name = $this->request->getPost('name');
        $email = $this->response->getPost('email');
        $big = $this->request->getPost('big');
        $small = $this->request->getPost('small');
        $url = $this->request->getPost('url');
        $layout = $this->request->getPost('layout');
        $config = $this->request->getPost('config');

        //Hacemos la consulta a la DB con el $id
        $affi = Affiliates::findFirst($id);

        //Relacionamos los valores traidos de la vista con la de $affi
        $affi->name = $name;
        $affi->email = $email;
        $affi->big_logo = $big;
        $affi->small_logo = $small;
        $affi->domain_url = $url;
        $affi->layout = $layout;
        $affi->configuration = $config;

        //Hacemos un update a la tabla affiliates
        $success = $affi->update();
        if ($success) {
            $this->flash->success("El afiliado con id: $id, a sido modificado");
            return $this->forward('affiliates/index');
        } else {
            foreach ($affi->getMessages() as $messages) {
                $this->flash->error($messages);
            }
            $this->response->redirect('affiliates/index');
        }
    }

    /*
     *Manda a la vista del registro que
     * se desea eliminar
     */
    public function DeleteAction($obj)
    {
        $a = Affiliates::find($obj);
        $this->view->setvar("a", $a);
    }

    /*
     * Funcion que elimina el registro seleccionado
     */
    public function EliminarAction()
    {
        //traemos el id mostrado en la vista
        $id = $this->request->getPost("id");
        $name = $this->request->getPost("id");

        //Se realiza la busqueda con el id obtenido
        $partner = Affiliates::find($id);

        //Se llama al metod delete para proceder con la baja del partner
        $success = $partner->delete();
        if ($success) {
            $this->flash->success("La filial $name, a sido eliminada");
            $this->response->redirect("Affiliates/index");
        } else {
            foreach ($partner->getMessages() as $message) {
                $this->flash->error($message);
            }
            $this->response->redirect('Affiliates/index');
        }
    }

}