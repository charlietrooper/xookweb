<?php
//include("../library/xlsxwriter.class.php");
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 03/06/2015
 * Time: 05:27 PM
 */
use Phalcon\Paginator\Adapter\model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Model\Query as Query;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorModel;

class CustomersController extends ControllerBase
{
    //ajushduiahuishd
    public function initialize()
    {
        $this->tag->setTitle('Clientes');
        parent::initialize();
    }

    /*
     *Mostrar datos de la tabla Customers
     */
    public function indexAction($obj = '')
    {
        //Traemos la session y sacamos el role_id y el partner_id
        $session = $this->session->get('auth');
        $partner_id = $session['partner_id'];
        $se = $session['role_id'];

        //Mandamos el role_id a la vista y aparecemos campos en la tabla ya sea admin o partner
        $this->view->setVar('se', $se);

        //se obtiene el número de página actual
        $numberPage = $this->request->getQuery("page", "int");

        //Guardamos el mes, año y dias.volt actual
        $dia = '01';
        $mes = date('m');
        $año = date('Y');

        //Para hacer la consulta
        $fechasql = $año.'-'.$mes.'-'.$dia;

        $this->view->setvar('fe', $fechasql);

        /*
         * Hacemos la consulta a la BD y según sea el partner, mostraremos los datos en la vista
         */
        //Si es admin
        $columnas = 'customer_id, Customers.partner_id, Customers.kobo_id, Customers.email, Customers.result,
        DATE_FORMAT(Customers.register_date, "%d-%m-%Y") as register_date, partner_customer_id,
        Customers.orbile_customer_id, account_type, merge, DATE_FORMAT(MAX(date), "%d-%m-%Y") as lastdate, SUM(purchase_book_num) as libros_total, AVG(purchase_book_num) as promedio_libros_compra, (SUM(purchase_sum)/SUM(purchase_book_num)) as gasto_promedio';
        $condiciones="";
        $variables=array();

        if ($se == 1 || $se == 4) {
            $partners = Partners::Find();
            $this->view->setVar('partners', $partners);
            $this->view->setvar('partnergrafic', null);

            if ($obj !== '') {
                $condiciones = 'Customers.partner_id = :partner:';
                $variables = array(
                    'partner' => $obj
                );

                $this->view->setvar('partnergrafic', null);
                $partners = Partners::Find(array(
                    "partner_id != :partner:",
                    'bind' => array('partner' => $obj)
                ));

                $this->view->setVar('partners', $partners);
                $par = Partners::findFirst($obj);
                $nombre = $par->name;
                $this->view->setvar('nombre', $nombre);
                $this->view->setvar('partnergrafic', $obj);
            }
        }
        else {
            $condiciones = 'Customers.partner_id = :partner:';
            $variables = array(
                'partner' => $partner_id
            );
        }


        $customers = $this->modelsManager->createBuilder()
            ->columns($columnas)
            ->from('Customers')
            ->leftJoin('Purchases', 'Customers.orbile_customer_id = p.orbile_customer_id','p')
            ->where($condiciones, $variables)
            ->groupBy(array('Customers.orbile_customer_id','Customers.orbile_customer_id','Customers.orbile_customer_id'))
            ->orderBy('customer_id DESC');

        $paginator = new PaginatorQueryBuilder(array(
            "builder"   => $customers,
            "limit"     => 20,
            "page"      => $this->request->getQuery('page', 'int')
        ));

        $this->view->page = $paginator->getPaginate();
    }


    public function calcularPromedios($arrayClientes)
    {
        $arrQuery = join("', '", $arrayClientes);
        set_time_limit(0);
        $arrayLibrosProm=[];
        $arrayPromedios=[];
        $arrayTotal=[];

        foreach ($arrayClientes as $cliente)
        {
            $comprados = Purchases::find(
                array(
                    "conditions" => "orbile_customer_id = '$cliente'"
                )
            );

            $conteoCompras=Purchases::count(
                array(
                    "conditions" => "orbile_customer_id = '$cliente'"
                )
            );

            $prueba=0;
            if (empty($comprados[0]))
            {
                //$woo="VACIO";
                array_push($arrayPromedios, "0.00");
                array_push($arrayTotal, "0");
                array_push($arrayLibrosProm, "0");
            }

            else{
                $arrayPurchaseID=[];
                foreach ($comprados as $comprado)
                {
                    //$woo="aa";
                    $prueba=$comprado->purchase_id;
                    array_push($arrayPurchaseID,$prueba);
                }

                $sumaTotal=0;
                $conteoPromedio=0;
                foreach ($arrayPurchaseID as $id)
                {
                    // $prueba=(int)$id;
                    $conteo=PurchasesLine::count(
                        array(
                            "column"     => "cogs_amount",
                            "conditions" => "purchase_id = $id"
                        )
                    );


                    $suma=PurchasesLine::sum(
                        array(
                            "column"     => "cogs_amount",
                            "conditions" => "purchase_id = $id"
                        )
                    );
                    $conteoPromedio+=$conteo;
                    $sumaTotal+=$suma;
                }
                $promLibros=$conteoPromedio/$conteoCompras;
                $promedio=$sumaTotal/$conteoPromedio;
                $formato_prom = number_format($promedio, 2, '.', '');
                array_push($arrayPromedios, $formato_prom);
                array_push($arrayTotal, $conteoPromedio);
                array_push($arrayLibrosProm, round($promLibros,1));

                //seguir copiando aquí el código para probar
            }

        }
        return array($arrayPromedios,$arrayTotal,$arrayLibrosProm);
    }



    public function showCsvAction()
    {
        //Se define $y para el conteo de
        //registros en las celdas de Excel
        $y = 1;

        //Traemos la session y sacamos el role_id y el partner_id
        $session = $this->session->get('auth');
        $role_id = $session['role_id'];

        //se obtiene la variable 'busq' que contiene el array con los
        //resultados de la búsqueda en la función search
     //   $busq = $this->session->get('busq');
        $busq = $this-> persistent->busq;

        //de la varibale busq se extraen los valores

        $condiciones = $busq ['condiciones'];
        $variables = $busq ['variables'];


        $customers = $this->modelsManager->createBuilder()
            ->columns('customer_id as cid, Customers.partner_id as pid, Customers.kobo_id as kid, Customers.email, Customers.result, DATE_FORMAT(Customers.register_date, "%d-%m-%Y") as register_date, partner_customer_id, Customers.orbile_customer_id, account_type, merge, DATE_FORMAT(MAX(date), "%d-%m-%Y") as lastdate, SUM(purchase_book_num) as libros_total, AVG(purchase_book_num) as promedio_libros_compra, (SUM(purchase_sum)/SUM(purchase_book_num)) as gasto_promedio')
            ->from('Customers')
            ->where($condiciones, $variables)
            ->leftJoin('Purchases', 'Customers.orbile_customer_id = p.orbile_customer_id','p')
            ->groupBy(array('Customers.orbile_customer_id','Customers.orbile_customer_id','Customers.orbile_customer_id'))
            ->orderBy('customer_id DESC')
            ->getQuery()->execute();

        //Código para librería XLSXWriter

        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "CUSTOMERS REPORT" . "-" . date("Y-m-d_his") . ".xlsx";
        header('Content-disposition: attachment; filename="' . XLSXWriter::sanitize_filename($filename) . '"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        $count=0;


        //para admin
        if ($role_id == 1 || $role_id == 4) {

            $header = array(
                'CUSTOMER ID'=>'string',
                'PARTNER ID'=>'string',
                'KOBO ID'=>'string',
                'EMAIL'=>'string',
                'REGISTER DATE'=>'string',
                'PARTNER CUSTOMER ID'=>'string',
                   'AVERAGE SPENT'=>'string',
                  'BOOK AVERAGE'=>'string',
                  'BOOKS TOTAL'=>'string',
                'LAST PURCHASE'=>'string',
            );

            $data1=[];

            foreach($customers as $cust)
            {
                $tempArray=[
                    $cust->cid,
                    $cust->pid,
                    $cust->kid,
                    $cust->email,
                    $cust->register_date,
                    $cust->partner_customer_id,
                    number_format($cust->gasto_promedio,2),
                    number_format($cust->promedio_libros_compra,2),
                    $cust->libros_total,
                    $cust->lastdate
                ];
                array_push($data1,$tempArray);
            }
        }
        //para partner
        else{
            $header = array(
                'CUSTOMER ID'=>'string',
                'KOBO ID'=>'string',
                'EMAIL'=>'string',
                'RESULT'=>'string',
                'REGISTER DATE'=>'string',
                'PARTNER CUSTOMER ID'=>'string',
                'AVERAGE SPENT'=>'string',
                'BOOK AVERAGE'=>'string',
                'BOOKS TOTAL'=>'string',
                'LAST PURCHASE'=>'string'
            );

            $data1=[];

            foreach($customers as $cust)
            {
                $tempArray=[
                    $cust->cid,
                    $cust->kid,
                    $cust->email,
                    $cust->result,
                    $cust->register_date,
                    $cust->partner_customer_id,
                    number_format($cust->gasto_promedio,2),
                    number_format($cust->promedio_libros_compra,2),
                    $cust->libros_total,
                   $this->checarVacio($cust->lastdate)
                ];
                array_push($data1,$tempArray);
            }
        }

        $writer = new XLSXWriter();
        $writer->setAuthor('Some Author');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();
        exit(0);
    }

    public function checarVacio($valor){
        $msg='';
        if($valor==null)
        {
            $msg='-';
        }
        else{
            $msg=$valor;
        }
        return $msg;
    }

    /*
     * Búsqueda en la tabla Customers para el Administrador
     */

    public function getSearchConditions($search)
    {
        switch($search)
        {
            case ('id'):
            {
                $condiciones = 'Customers.partner_id=:id:';
                break;
            }
            case ('mail'):
            {
                $condiciones ="Customers.email LIKE CONCAT('%', :mail:, '%')";
                break;
            }
            case ('fecha_ini'):
            {
                $condiciones = 'CAST(Customers.register_date AS DATE) >= :fecha_ini:';
                break;
            }
            case ('fecha_fin'):
            {
                $condiciones = 'CAST(Customers.register_date AS DATE) <= :fecha_fin:';
                break;
            }

        }
        return $condiciones;
    }

    public function searchAction()
    {
        //Se obtienen las variables $id, $fecha_ini y $fecha_fin vía $_GET
        $id = $this->request->get('id', null, '');
        $mail = $this->request->get('mail', null, '');
        $fecha_ini = $this->request->get('fecha_ini', null, '');
        $fecha_fin = $this->request->get('fecha_fin', null, '');
        $pagina = $this->request->get('page_ajax', null, '');

        //Se define el límite de registros por página
        $rowPerPage = 20;
        //deshabilitamos la vista para las peticiones ajax
        $this->view->disable();
        //columnas a obtener en la consulta
        $columnas = 'customer_id, Customers.partner_id, Customers.kobo_id, Customers.email, Customers.result, DATE_FORMAT(Customers.register_date, "%d-%m-%Y") as register_date, partner_customer_id, Customers.orbile_customer_id, account_type, merge, DATE_FORMAT(MAX(date), "%d-%m-%Y") as lastdate, SUM(purchase_book_num) as libros_total, AVG(purchase_book_num) as promedio_libros_compra, (SUM(purchase_sum)/SUM(purchase_book_num)) as gasto_promedio';

        if ($this->request->isGet() == true) {
            $searchParameters = array('id' => $id, 'mail' => $mail, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin);
            $condiciones = '';
            $variables = array();
            foreach ($searchParameters as $search => $search_value) {
                if ($search_value !== '') {
                    $condiciones .= $this->getSearchConditions($search);
                    $variables[$search] = $search_value;
                    $condiciones .= ' AND ';

                }
            }

            if ($condiciones !== '') {
                $condiciones = substr($condiciones, 0, -5);

            }


            $customers = $this->modelsManager->createBuilder()
                ->columns('customer_id, Customers.partner_id, Customers.kobo_id, Customers.email, Customers.result, DATE_FORMAT(Customers.register_date, "%d-%m-%Y") as register_date, partner_customer_id, Customers.orbile_customer_id, account_type, merge, DATE_FORMAT(MAX(date), "%d-%m-%Y") as lastdate, SUM(purchase_book_num) as libros_total, AVG(purchase_book_num) as promedio_libros_compra, (SUM(purchase_sum)/SUM(purchase_book_num)) as gasto_promedio')
                ->from('Customers')
                ->where($condiciones, $variables)
                ->leftJoin('Purchases', 'Customers.orbile_customer_id = p.orbile_customer_id','p')
                ->groupBy(array('Customers.orbile_customer_id','Customers.orbile_customer_id','Customers.orbile_customer_id'))
                ->orderBy('customer_id DESC');
            $this->persistent->busq = array('condiciones' => $condiciones, 'variables' => $variables);
            //se incluye el paginador
            $paginator = new PaginatorQueryBuilder(array(
                "builder" => $customers,
                "limit" => $rowPerPage,
                "page" => $pagina
            ));
            $page = $paginator->getPaginate();
            $page->items = $page->items->toArray();

            $this->response->setJsonContent($page);
            $this->response->setContentType('application/json', 'UTF-8');

            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }
    }

    /**
     *Búsqueda en la tabla Customers para el Partner
     *
     * @param $id partner_id, $fecha_ini regiter_date,
     * $fecha_fin register_date
     */
    public function SearchForPartnerAction()
    {
        //Se obtienen las variables $mail, $fecha_ini y $fecha_fin vía $_GET
        $mail = $this->request->get('mail', null, '');
        $fecha_ini = $this->request->get('fecha_ini', null, '');
        $fecha_fin = $this->request->get('fecha_fin', null, '');
        $pagina = $this->request->get('page_ajax', null, '');

        //deshabilitamos la vista para las peticiones ajax
        $this->view->disable();

        //Se define el límite de registros por página
        $rowPerPage = 20;

        //Se obtiene la variable de sesión  para hacer la consultas
        //buscando solo los registros de este patrner
        $session = $this->session->get('auth');
        $id = $session['partner_id'];

        if ($this->request->isGet() == true) {
            $searchParameters = array('mail' => $mail, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin);
            $condiciones = '';
            $variables = array();
            foreach ($searchParameters as $search => $search_value) {
                if ($search_value !== '') {
                    $condiciones .= $this->getSearchConditions($search);
                    $variables[$search] = $search_value;
                    $condiciones .= ' AND ';

                }
            }

            if ($condiciones !== '') {
                $condiciones = substr($condiciones, 0, -5);

            }

            $customers = $this->modelsManager->createBuilder()
                ->columns('customer_id, Customers.partner_id, Customers.kobo_id, Customers.email, Customers.result, DATE_FORMAT(Customers.register_date, "%d-%m-%Y") as register_date, partner_customer_id, Customers.orbile_customer_id, account_type, merge, DATE_FORMAT(MAX(date), "%d-%m-%Y") as lastdate, SUM(purchase_book_num) as libros_total, AVG(purchase_book_num) as promedio_libros_compra, (SUM(purchase_sum)/SUM(purchase_book_num)) as gasto_promedio')
                ->from('Customers')
                ->where($condiciones, $variables)
                ->leftJoin('Purchases', 'Customers.orbile_customer_id = p.orbile_customer_id','p')
                ->groupBy(array('Customers.orbile_customer_id','Customers.orbile_customer_id','Customers.orbile_customer_id'))
                ->orderBy('customer_id DESC');

            $this->persistent->busq = array('condiciones' => $condiciones, 'variables' => $variables);
            //se incluye el paginador
            $paginator = new PaginatorQueryBuilder(array(
                "builder" => $customers,
                "limit" => $rowPerPage,
                "page" => $pagina
            ));
            $page = $paginator->getPaginate();
            $page->items = $page->items->toArray();

            $this->response->setJsonContent($page);
            $this->response->setContentType('application/json', 'UTF-8');

            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }
    }


    /*
     * Función para mostrar los detalles de customers
     */
    public function detailsAction($obj = '')
    {
        $c = Customers::find(array(
            'columns'   => 'customer_id, partner_id, kobo_id, email, result, DATE_FORMAT(register_date, "%d-%m-%Y") as register_date, partner_customer_id, orbile_customer_id, account_type, merge',
            "(orbile_customer_id = :obj:) OR (email = :obj:) OR (customer_id = :obj:)",
            'bind' => array('obj' => $obj)
        ));
        //se envía a la vista los resultados de la búsqueda
        $this->view->setvar("c", $c);
        foreach($c as $cust)
        {
            $customer = $cust->customer_id;
        }

        $c = Customers::findFirst($customer);

        $partner_id = $c->partner_id;
        $kobo_id = $c->kobo_id;
        $orbile_customer_id = $c->orbile_customer_id;
        $this->view->setVar('orbile_customer_id', $orbile_customer_id);

        $api = Partners::findFirst(array(
            "partner_id = :partner_id:",
            'bind' => array('partner_id' => $partner_id)
        ));

        $api_username = $api->api_username;
        $api_pass = $api->api_pass;

        $this->session->set('API_AUTH', array(
            'kobo_id' => $kobo_id,
            'api_user' => $api_username,
            'api_key' => $api_pass
        ));


    }

    public function reportecomprasAction($b = '')
    {
        $purchase = Purchases::find(array(
            "orbile_customer_id = :b:",
            'bind' => array('b' => $b),
            "order" => "purchase_id DESC"
        ));

        $numberPage = $this->request->getQuery("page", "int");

        //se crea el paginador que muestra X hojas por página
        $paginator = new Paginator(array(
            "data" => $purchase,
            "limit" => 20,
            "page" => $numberPage
        ));

        //se pasa el objeto a la vista con el nombre de page
        $this->view->page = $paginator->getPaginate();
    }
}
