<?php
ini_set('max_execution_time', 300);
use Phalcon\Paginator\Adapter\model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Router as Router;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class ComparadorController extends ControllerBase
{


    public function initialize()
    {
        $this->tag->setTitle('Comparador');
        parent::initialize();
    }

    /*
     * Función para mostrar el listado de Precio Futuro
     */
    public function indexAction()
    {


    }

    public function busquedaAction()
    {

        //obtnemos array con ISBNs de la búsqueda del usuario
        $isbnsArray = explode(",", $this->request->get('isbn', null, ''));

        //llamamos nuestros métodos y mandamos la lista de isbns a buscar
        $pPorrua= $this->precioPorrua($isbnsArray);
        $this->view->setVar('precioPorrua', $pPorrua);
        $pGandhi= $this->precioGandhi($isbnsArray);
        $this->view->setVar('precioGandhi', $pGandhi);
        $pOrbile=$this->precioOrbile($isbnsArray);
        $this->view->setVar('precioOrbile', $pOrbile);
        $this->view->setVar('arrayISBN', $isbnsArray);

    }

    public function precioOrbile($listadoISBN)
    {

        $listaOrbile=[];
        $listaBusqueda=[];
        $ruta = 'orbile_catalog_url';
        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;
        //$url="http://catalog.orbiletest.com";
       foreach($listadoISBN as $isbn)
       {
           $cadenaCURL="$url/todayprices/search/isbn/$isbn";
           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $cadenaCURL);
           curl_setopt($ch, CURLOPT_TIMEOUT, 80);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $resultado = curl_exec ($ch);
           if (strlen($resultado)<30)
           {
               array_push($listaOrbile, "NO");
           }
           else
           {
               $result = json_decode($resultado, true);
               array_push($listaOrbile, $result);
           }

       }
        return $listaOrbile;

    }

    public function precioPorrua($listadoISBN)
    {
        // require 'simple_html_dom.php';
        $objParser = new simple_html_dom();
        $listaPorrua=[];
        //mandamos un array con todos los isbns, recorremos y buscamos
        foreach ($listadoISBN as $libro) {
            $libroISBN=$libro;
            $url = 'https://porrua.mx/busqueda/ebooks/'.$libroISBN;
            $html=file_get_html($url); //Devuelve un array con contenido de la página

            $posts=$html->find('h5[class=titulo]');
            if (count($posts)!==1 )
            {
                //si no lo encotró...
                array_push($listaPorrua, "----");
            }

            else{
                foreach ($posts as $post) {
                   // $link=$post->find('strong');
                    $pPorrua=$post->innertext;
                  //  $pPorrua=substr($pPorrua, 0, -3);
                    $pPorrua= substr($pPorrua, strpos($pPorrua, "$") );

                    array_push($listaPorrua, array(
                        'url' => $url,
                        'precio'=> $pPorrua
                    ));

                }
            }

        }

        return $listaPorrua;

    }

    public function precioGandhi($listadoISBN)
    {
        // require 'simple_html_dom.php';
        $objParser = new simple_html_dom();
        $listaGandhi=[];


        //mandamos un array con todos los isbns, recorremos y buscamos
        foreach ($listadoISBN as $libro) {
            $libroISBN=$libro;
            $url = 'http://busqueda.gandhi.com.mx/busca?q='.$libroISBN;
            $html=file_get_html($url); //Devuelve un array con contenido de la página

            $posts=$html->find('span[class=price]');
            if (count($posts)!==1 )
            {
                //si no lo encontró...
                array_push($listaGandhi, "----");
            }

            else{
                foreach ($posts as $post) {
                    $pReal=$post->innertext;
                    $pReal=$pReal.".00";

                    array_push($listaGandhi, array(
                        'url' => $url,
                        'precio'=> $pReal
                    ));
                }
            }
        }

        return $listaGandhi;
    }



}

