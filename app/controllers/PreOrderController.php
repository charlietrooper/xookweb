<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 23/11/2015
 * Time: 01:04 PM
 */

class PreOrderController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Pre Ordenes');
        parent::initialize();
    }

    /**
     *
     *
     */
    public function indexAction()
    {
        $partner = Partners::find();

        $this->view->setVar('partner', $partner);

    }

    /**
     *
     *
     */
    public function listAction()
    {
        $email = $this->request->getPost('email');
        $partner = $this->request->getPost('partner');

        $api = Partners::findFirst(array(
            'partner_id = :id:',
            'bind' => array('id' => $partner)
        ));

        $ruta = 'orbile_api_base_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        //Se asignan los campos de la consulta a una respectiva varibale
        $api_username = $api->api_username;
        $api_pass = $api->api_pass;

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, "$url->value/pre_order/list/$email");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_username\r\nx-api-key:$api_pass"));

        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $curl_response = curl_exec($curl);


        if ($curl_response === FALSE) {
            $error = curl_error($curl);
            //$this->logger->log("cURL Error: $error");

            var_dump("error $error");
            //$curl_response = array('Error' => 'Fallo la conexion con el servidor');
        }

        //var_dump("respuesta $curl_response");

        $res = json_decode($curl_response);
        $this->logger->log('respuesta de api->pre-order/list: ' . $res);

        $this->view->setVar('res', $res);

        $this->persistent->email = $email;

        $this->persistent->username = $api_username;

        $this->persistent->pass = $api_pass;
    }

    /**
     *
     *
     */

    public function cancelAction($product = '')
    {

        $email = $this->persistent->email;
        $username = $this->persistent->username;
        $pass = $this->persistent->pass;

        var_dump("estos valores $email $username $pass");

        $ruta = 'orbile_api_base_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, "$url->value/pre_order/cancel/$email/$product");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $username\r\nx-api-key:$pass"));
        $curl_response = curl_exec($curl);


        if ($curl_response === FALSE) {
            $error = curl_error($curl);

            var_dump($error);

            //$this->logger->log("cURL Error: $error");

            //$curl_response = array('Error' => 'Fallo la conexion con el servidor');
        }

        var_dump($curl_response);
    }
}