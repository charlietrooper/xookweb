<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 09/03/2016
 * Time: 01:53 PM
 */
use Phalcon\Paginator\Adapter\model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Router as Router;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class ListasController extends ControllerBase
{

    public function initialize()
    {
        $this->tag->setTitle('Lista de Pre órdenes');
        parent::initialize();
    }

    /*
     * Función para mostrar el listado de titulares de cuenta
     */
    public function indexAction()
    {
        //se definen varibales a utilizar
        $poBooks = (object)[];
        $page = $this->request->get("page", "int", 1);
        $poBooks->page = $page;
        $items_per_page = 20;

        //Se define la url a la que se conectará
        $ruta = 'orbile_catalog_url';
        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;
        //$url="http://catalog.orbiletest.com";
        //se realiza la conexión cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$url/books/preorderBooks/$items_per_page/$page");
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $resultado = curl_exec ($ch);

        //Se decodifica el Json y se asigna al objeto
        $result = json_decode($resultado, true);
        $poBooks->items = $result;
       // $this->persistent->libros = $result;
        //se manda llamar la paginación de listados
        $paginacionListas = new PaginacionListas();
        $listadoPoBooks = $paginacionListas->obtener_paginacion($poBooks);

        $this->view->setVar('preorderBooks', $listadoPoBooks);

    }

    public function  busquedaAction()
    {

        $poBooks_busqueda = (object)[];
        $page = intval($this->request->get('pagina', 'int', 1));
        $poBooksLanguage= $this->request->get('preorderBooksLista');

        $poBooks_busqueda->page = $page;
        $items_per_page = 20;
        //Se deshabilita la vista para las peticiones ajax
        $this->view->disable();



        if ($this->request->isGet() == true) {
            //Se define la url a la que se conectará
            $ruta = 'orbile_catalog_url';
            $api = AppConfig::findFirst(array(
                'key = :ruta:',
                'bind' => array('ruta' => $ruta)
            ));
            $url = $api->value;
            $cadenaCURL="";

            if($poBooksLanguage==1)
            {
                $cadenaCURL="$url"."/books/preorderBooks/language/es/$items_per_page/$page";
            }

            elseif($poBooksLanguage==2)
            {
                $cadenaCURL="$url"."/books/preorderBooks/language/en/$items_per_page/$page";
            }

            else{
                $cadenaCURL="";
            }

            //se realiza la conexión cURL
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $cadenaCURL);
            curl_setopt($ch, CURLOPT_TIMEOUT, 80);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $resultado = curl_exec ($ch);

            //Se decodifica el Json y se asigna al objeto
            $result = json_decode($resultado, true);
            $poBooks_busqueda->items = $result;

            //se manda llamar la paginación de listados
            $paginacionListas = new PaginacionListas();
            $listadoPoBooksBusqueda = $paginacionListas->obtener_paginacion($poBooks_busqueda);

            $this->response->setJsonContent($listadoPoBooksBusqueda);
            $this->response->setContentType('application/json', 'UTF-8');

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }
    }
/*
    public function excelAction()
    {
        $libros = $this->persistent->libros;

        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "LIBROS_EN_PREORDEN -" . date("Y-m-d_hDis") . ".xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $header = array(
            'TÍTULO'=>'string',
            'ISBN'=>'string',
            'ActiveRevisionID'=>'string',
            'Inicio Preorden'=>'string',
            'Fin Preorden'=>'string',
            'Precio de Lista'=>'string',
            'Precio de Venta'=>'string',
            'Idioma'=>'string'
        );

        $data1=[];
        $conteo=0;
        foreach($libros['Items'] as $lib)
        {

            $tempArray=[
                $lib['title'],
                $lib['isbn'],
                $lib['active_revision_id'],
                $lib['preorder_initial_date'],
                $lib['preorder_end_date'],
                $lib['preorder_listprice'],
                $lib['preorder_sellingprice'],
                $this->traducirIdioma($lib['preorder_language'])
            ];
            array_push($data1,$tempArray);
            $conteo++;

        }

        $writer = new XLSXWriter();
        $writer->setAuthor('Orbile');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();
        exit(0);
    }

    public function traducirIdioma($lang)
    {
        $msg="";
        if(strtoupper($lang)=="EN")
        {
            $msg="Ingl&eacute;s";
        }
        elseif(strtoupper($lang)=="ES")
        {
            $msg="Espa&ntilde;ol";
        }
        return $msg;
    }
*/
}

