<?php

class MergeController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Unión de cuentas');
        parent::initialize();
    }

    public function indexAction()
    {

    }

    public function mergeAction()
    {
        $cuenta = $this->request->getPost('cuenta');
        $contra = $this->request->getPost('contra');

        $ruta = 'orbile_api_base_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));


        $user = $this->session->get('API_AUTH');
        $api_username = $user['api_user'];
        $api_pass = $user['api_key'];

        $post_fields = json_encode(array(
            'Password' => $contra
        ));

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();
        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url->value/authservice/merge_existing_user/$cuenta/123");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Especificamos el post curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);

        //LE DECIMOS QUE PARAMETROS ENVIAMOS
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
        //$this->flash->success($post_fields->Password);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_username\r\nx-api-key:$api_pass"));

        //La respuesta de cURL es guardada en la variable $resultado
        $resultado = curl_exec ($ch);
        $this->flash->success($resultado);
    }

}

