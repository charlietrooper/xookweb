<?php
/**
 * SessionController
 *
 * Allows to authenticate users
 */
class SessionController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Sign Up/Sign In');
        parent::initialize();
    }
    public function indexAction()
    {
        if (!$this->request->isPost()) {

        }
    }

    /**
     * Register an authenticated user into session data
     *
     * @param Users $user
     */

    private function _registerSession( Users $user ,Roles $role)
    {
        //Store user and role info in the session
        $this->session->set('auth', array(
            'role_id' => $role->role_id,
            'role_name' => $role->role_name,
            'id' => $user->user_id,
            'name' => $user->username,
            'partner_id' => $user->partner_id
        ));
    }
    /**
     * This action authenticate and logs an user into the application
     *
     */
    public function startAction()
    {
        if ($this->request->isPost()) {

            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');
            //$pass=$this->security->hash($password);
            // echo $pass;
            $user = Users::findFirst(array(
                "(email = :email: OR username = :email:) AND active = 's'",
                'bind' => array('email' => $email)
            ));
            if ($user != false) {
                if($this->security->checkHash($password ,$user->pass))
                {
                    $this->flash->success('Bienvenido ' . $user->name);
                    $role = Roles::findFirst($user->role_id);
                    $this->_registerSession($user, $role);

                    if($user->role_id == 1){
                        return $this->response->redirect('sales-reports/index');
                    }
                    else{
                        return $this->response->redirect('sales-reports/index');
                    }
                }
            }
            $this->flash->error('Wrong email/password');
        }
        //return $this->response->redirect('session/index');
    }
    /**
     * Finishes the active session redirecting to the index
     *
     * @return unknown
     */
    public function endAction()
    {
        $this->session->destroy();
        return $this->response->redirect('index/index');
    }
}