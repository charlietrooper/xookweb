<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 21/01/2016
 * Time: 02:23 PM
 */
use Phalcon\Mvc\Model\Query as Query;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorModel;



class MasVendidosController extends ControllerBase
{
    /*
     * Función para título de la pagina  en la pestaña
     */
    public function initialize()
    {
        $this->tag->setTitle('Libros mas vendidos');
        parent::initialize();
    }

    /*
     * Función para mostrar los más vendidos actuales
     */
    public function actualAction()
    {

        $fecha1 = "'".getdate()['year']."-".date('m')."-01'";
        $fecha2 = "'".date("Y-m-d")."'";
        $fecha_1 = "01-".date('m')."-".getdate()['year'];
        $fecha_2 = date("d-m-Y");
        $this->view->setVars(array(
            "fecha1"    => $fecha_1,
            "fecha2"    => $fecha_2,
            "elementos" => 10
        ));

       $libros = $this->modelsManager->executeQuery("SELECT COUNT(*) as veces_vendido, pl.titulo AS titulo, pl.isbn AS isbn, pl.product_id AS product_id, pl.genero AS gen, pl.idioma as idioma
          FROM Purchases p INNER JOIN PurchasesLine pl ON p.purchase_id = pl.purchase_id
          WHERE p.date BETWEEN $fecha1 AND $fecha2 AND pl.price_before_tax > 0
          GROUP BY titulo, product_id ORDER BY veces_vendido DESC Limit 10");

        $arrTraducir=$this->traducirGeneros($libros,false);
        $arrTraducirIdioma=$this->traducirIdioma($libros);

        $arrExcel=[$libros,$arrTraducir,$arrTraducirIdioma];
        $this->persistent->libros = $arrExcel;
        $conteo=0;
        foreach($libros as $lib)
        {

            $libroArr[] = array(
                'veces_vendido' => $lib->veces_vendido,
                'titulo'        => $lib->titulo,
                'isbn'          => $lib->isbn,
                'product_id'    => $lib->product_id,
                'genero'        => $arrTraducir[$conteo],
                'idioma'        => $arrTraducirIdioma[$conteo]
            );
            $conteo++;
        }
        //se obtiene la pagina
        $currentPage = $this->request->getQuery('page', 'int');
        $this->persistent->librosArr = $libroArr;
        //Se hace la paginación de los resultados de la consulta
        $paginator = new PaginatorModel(
            array(
                "data"  => $libroArr,
                "limit" => 10,
                "page"  => $currentPage
            )
        );

        //Se envía el pagunator a la vista para que se imprima
        $this->view->page = $paginator->getPaginate();
    }

    /*
     * Función que muestra todos los registros de las ventas por unidades
     */
    public function indexAction()
    {
        $formato = new ReformateoDeCadena();
        $generos = $this->modelsManager->executeQuery("SELECT DISTINCT genero_esp AS genero FROM Generos ORDER BY genero");
        $this->view->setVar('generos', $generos);
        $arrGenesp2=[];
        $arrVacio=[];
        foreach ($generos as $gen)
        {
            $test2=$formato->obtener_formato($gen['genero']);
            array_push($arrGenesp2,$test2);
            array_push($arrVacio,$gen['genero']);

        }
        $this->view->setVar('generosPrint', $arrGenesp2);
        $this->view->setVar('generosArray', $arrVacio);


        $idiomas = $this->modelsManager->executeQuery("SELECT DISTINCT idioma FROM PurchasesLine ORDER BY idioma");
        $this->view->setVar('idiomas', $idiomas);
    }

    public function traducirIdioma($libros)
    {
        $arrIdioma=[];
        foreach ($libros as $lib)
        {
            $dep='';
            $temp=$lib->idioma;
            if(empty($temp)||$temp=="No disponible")
            {
                $dep="-";
                array_push($arrIdioma,$dep);
            }
            elseif($temp=='es' || $temp=='ES'){
                $dep="Español";
                array_push($arrIdioma,$dep);
            }
            else if ($temp=='en'|| $temp=='EN')
            {
                $dep='Inglés';
                array_push($arrIdioma,$dep);
            }


        }

        return $arrIdioma;
    }

    //función para traducir géneros

    public function traducirGeneros($libros,$ban)
    {
        if($ban)
        {
            $arrTraducir=[];
            foreach ($libros as $lib)
            {
                $dep='';
                $temp=mb_convert_encoding($lib->gen, "UTF-8");

                if(empty($temp)||$temp=="No disponible")
                {
                    $dep="-";
                    array_push($arrTraducir,$dep);
                }
                else{
                    $genEsp = Generos::find(
                        array(
                            "columns" => "genero_esp as esp",
                            "conditions" => "genero = '$temp'"
                        )
                    );
                    array_push($arrTraducir,$genEsp[0]->esp);
                }


            }
        }

        else{
            $arrTraducir=[];
            foreach ($libros as $lib)
            {
                $temp=$lib->gen;
                if(empty($temp)||$temp=="No disponible")
                {
                    $dep="-";
                    array_push($arrTraducir,$dep);
                }
                else{
                    $genEsp = Generos::find(
                        array(
                            "columns" => "genero_esp as esp",
                            "conditions" => "genero = '$temp'"
                        )
                    );
                    array_push($arrTraducir,$genEsp[0]->esp);
                }


            }
        }

        return $arrTraducir;
    }

    /*
     * Función par mostrar el listado de libros mas vendidos
     */

    public function pieDataAction(){

        $librosArr = $this->persistent->librosArr;
        $this->view->disable();
       // echo ("bbbbbbbbb");
        if ($this->request->isGet() == true) {
            $this->response->setJsonContent($librosArr);
            $this->response->setContentType('application/json', 'UTF-8');

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }

    }

    public  function busquedaAction()
    {
        $formato = new ReformateoDeCadena();

        $elementos = $this->request->get('elementos');
        $fecha_1 = $this->request->get('fecha1');
        $fecha_2 = $this->request->get('fecha2');
        $genero = $this->request->get('genero');
        $idiomas = $this->request->get('idioma');

        //Se agregan comillas para poder pasar los valores a la query
        $fecha1 = "'".$fecha_1."'";
        $fecha2 = "'".$fecha_2."'";

        $idioma = "'".$idiomas."'";

        //tenemos que quitar los acentos del género para poder usarlo en la query
        $tradGen=  utf8_decode($genero);
        $gen = "'".$tradGen."'";

        //Estos valores se usan en la paginación
        $this->view->setVars(array(
            "fecha1"    => $fecha_1,
            "fecha2"    => $fecha_2,
            "elementos" => $elementos,
            "genero"    => $genero,
            "idiomas"    => $idiomas
        ));

        $this->view->setVar('test', $gen);
        //$this->view->setVar('genero', $genero);
        //Con esta query obtenemos los valores en inglés correspondientes al género en español seleccionado
        $espGen= $this->modelsManager->executeQuery("SELECT genero as genre FROM Generos WHERE genero_esp = $gen");

        //De los valores obtenidos en la query, debemos meterlos a un array que se usará en las queries grandes para buscar los libros
        $arrGeneros=[];
        foreach($espGen as $g)
        {

            array_push($arrGeneros,$g['genre']);
        }

        $arrGenQuery = join("', '", $arrGeneros);

        $ban=false;
        //Búsqueda solo por Género
        $query='SELECT COUNT(*) as veces_vendido, pl.titulo AS titulo, pl.isbn AS isbn, pl.product_id AS product_id, pl.genero AS gen, pl.idioma as idioma
        FROM Purchases p INNER JOIN PurchasesLine pl ON p.purchase_id = pl.purchase_id
        WHERE p.date BETWEEN '. $fecha1 .' AND '.$fecha2.' AND pl.price_before_tax > 0 ';
        $grouped='titulo';
        $ban=true;
        if($idiomas!=='TODOS')
        {

            $query.= 'AND pl.idioma = '.$idioma.' ';
        }

        if($genero!=='TODOS')
        {
           // $ban=false;
            $query.="AND pl.genero IN ('".$arrGenQuery."') ";
            $grouped='gen';
        }
        $query.='GROUP BY '.$grouped.', product_id ORDER BY veces_vendido DESC Limit '.$elementos.'';
        $libros = $this->modelsManager->executeQuery($query);
        $arrTraducirIdioma=$this->traducirIdioma($libros);
        $arrTraducir=$this->traducirGeneros($libros,$ban);

        $arrExcel=[$libros,$arrTraducir,$arrTraducirIdioma];
        $this->persistent->libros = $arrExcel;
        $conteo=0;
       // $arrTraducir=[];
        foreach($libros as $lib)
        {
            $libroArr[] = array(
                'veces_vendido' => $lib->veces_vendido,
                'titulo'        => $lib->titulo,
                'isbn'          => $lib->isbn,
                'product_id'    => $lib->product_id,
                'genero'        => $arrTraducir[$conteo],
                'idioma'        => $arrTraducirIdioma[$conteo]
            );
            $conteo++;
        }
        $this->persistent->librosArr = $libroArr;
        //se obtiene la pagina
        $currentPage = $this->request->getQuery('page', 'int');

        //Se hace la paginación de los resultados de la consulta
        $paginator = new PaginatorModel(
            array(
                "data"  => $libroArr,
                "limit" => 20,
                "page"  => $currentPage
            )
        );

        //Se envía el pagunator a la vista para que se imprima
        $this->view->page = $paginator->getPaginate();

    }

    /*
     * Función para imprir el archivo excel
     */
    public function excelAction()
    {
        $libros = $this->persistent->libros;
        $arrAutores=$this->obtenerAutoresArray($libros[0]);
        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "MAS_VENDIDOS_REPORT" . "-" . date("Y-m-d_his") . ".xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $header = array(
            'Veces Vendido'=>'string',
            'Título'=>'string',
            'Autor'=>'string',
            'ISBN'=>'string',
            'Active Revision ID'=>'string',
            'Género'=>'string',
            'Idioma'=>'string'
        );

        $data1=[];
        $conteo=0;
        foreach($libros[0] as $lib)
        {
            if(count($arrAutores[$conteo]['Items'])==1)
            {
                $listaAutores=$arrAutores[$conteo]['Items'][0]['activeRevision']['contibutors'];
            }

            else {
                foreach ($arrAutores[$conteo]['Items'] as $e)
                {
                    if(!empty($e['activeRevision'])){
                        $listaAutores=$e['activeRevision']['contibutors'];
                    }
                }
            }



            $tempArray=[
                $lib->veces_vendido,
               $lib->titulo,
                $this->obtenerAutor($listaAutores),
                $lib->isbn,
                $lib->product_id,
                $libros[1][$conteo],
                //$lib->esp,
                $libros[2][$conteo],
            ];
            array_push($data1,$tempArray);
            $conteo++;

        }

        $writer = new XLSXWriter();
        $writer->setAuthor('Orbile');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();
        exit(0);
    }

    public function obtenerAutoresArray($libros)
    {
        $arriSBN=array();
        foreach($libros as $lib)
        {
            array_push($arriSBN,$lib->isbn);
        }

        if(!empty($arriSBN))
        {
            $isbns = json_encode($arriSBN);
            $ch = curl_init();
            // $url="https://catalog.orbile.com/isbn/getBooksBasic";

            $ruta = 'orbile_catalog_url';
            $url = AppConfig::findFirst(array(
                'key = :ruta:',
                'bind' => array('ruta' => $ruta)
            ));


            //enviamos la url con los parámetros ingresados por el aprtner
            curl_setopt($ch, CURLOPT_URL, "$url->value/isbn/getBooksBasic");
            //Se asigna el tiempo de espera
            curl_setopt($ch, CURLOPT_TIMEOUT, 80);
            //Afirma que se requiere una respuesta
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            //Especificamos el post
            //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POST, 1);
            //LE DECIMOS QUE PARAMETROS ENVIAMOS
            curl_setopt($ch, CURLOPT_POSTFIELDS, $isbns);
            //para solucionar el problema de "https"
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            //La respuesta de cURL es guardada en la variable $resultado
            $resultado = curl_exec($ch);
            if(curl_exec($ch) === false)
            {
                return 0;
            }

            else{
                // var_dump($resultado);
                $res = json_decode($resultado, true);

                if($res[0]['Items'])
                {
                    return $res;
                }
                else{
                    return 0;
                }

            }
        }

        return $arriSBN;

    }

    public function obtenerAutor($infoLibro)
    {
        $aut = '';
        if ($infoLibro) {
            //$aut = ' / ';
            // $what=isset($infoLibro['activeRevision']['contibutors']['contributor']['@value']);
            if (isset($infoLibro['contributor']['@value'])) {
               // if($infoLibro['contributor']['@attributes']['type']=='Author' || $infoLibro['contributor']['@attributes']['type']=='Editor')
               // {
                    $holder = $infoLibro['contributor']['@value'];
             //   }
                //$aut .= $infoLibro['contributor']['@value'];
            } else {
                $autores = $infoLibro['contributor'];
                $count = count($autores);
                $n = 0;
                foreach ($autores as $autor) {
                    if (!($n > 0)) {
                      //  if($autor['@attributes']['type']=='Author' || $autor['@attributes']['type']=='Editor')
                      //  {
                            $holder = $autor['@value'];
                            $n++;
                      //  }
                    }
                }
            }

        }
        if(isset($holder))
        {
            return $holder;
        }

        else{
            return "--------";
        }

    }
}