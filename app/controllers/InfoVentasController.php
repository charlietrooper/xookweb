<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 21/01/2016
 * Time: 02:23 PM
 */
use Phalcon\Mvc\Model\Query as Query;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorModel;



class InfoVentasController extends ControllerBase
{
    /*
     * Función para título de la pagina  en la pestaña
     */
    public function initialize()
    {
        $this->tag->setTitle('Información de Ventas');
        parent::initialize();
    }

    /*
     * Función que muestra todos los registros de las ventas por unidades
     */
    public function indexAction()
    {

    }



    /*
     * Función par mostrar el listado de libros mas vendidos
     */



    public  function buscaventasAction()
    {
        $session = $this->session->get('auth');
        $partner_id = $session['partner_id'];
        $se = $session['role_id'];

        $isbnsRequest = preg_replace('/\s+/', '', $this->request->get('isbn', null, ''));
        $isbnArr = explode(",", $isbnsRequest);
        $fecha_1 = $this->request->get('fecha1');
        $fecha_2 = $this->request->get('fecha2');

        //Estos valores se usan en la paginación
        $this->view->setVars(array(
            "fecha1"    => $fecha_1,
            "fecha2"    => $fecha_2
        ));

        $query="SELECT COUNT(*) as veces_vendido, pl.titulo AS titulo, pl.isbn AS isbn, pl.product_id AS product_id, pl.idioma as idioma
          FROM Purchases p INNER JOIN PurchasesLine pl ON p.purchase_id = pl.purchase_id WHERE";

        if($fecha_1 !== "" && $fecha_2 !=="")
        {
            $fecha1 = "'".$fecha_1."'";
            $fecha2 = "'".$fecha_2."'";
            $query.=" p.date BETWEEN $fecha1 AND $fecha2 AND";
        }

        if($partner_id!=='0')
        {
            $query.=" p.partner_id = '$partner_id' AND";
        }

        foreach($isbnArr AS $isbn)
        {
            $queryExecute="";
            $queryExecute=$query;
   //         $isbnUser = "'".$isbn."'";
            $queryExecute.=" pl.isbn = $isbn GROUP BY titulo, product_id";

            $libros = $this->modelsManager->executeQuery($queryExecute);

            //Si el query no trajo resultamos, insertamos en el array valores vacíos.
            if(!($libros->valid()))
            {
                $libroArr[] = array(
                    'veces_vendido' => 0,
                    'titulo'        => null,
                    'isbn'          => $isbn,
                    'idioma'        => null,
                    'product_id'    => null
                );
            }

            else{
                foreach($libros as $lib)
                {

                    $libroArr[] = array(
                        'veces_vendido' => $lib->veces_vendido,
                        'titulo'        => $lib->titulo,
                        'isbn'          => $lib->isbn,
                        'idioma'        => $this->obtenerLenguajeCompleto($lib->idioma),
                        'product_id'    => $lib->product_id
                    );

                }

            }

        }




        $this->view->setVar('ventasInfo', $libroArr);
        $this->persistent->libros = $libroArr;
        $this->persistent->fecha_1 = $fecha1;
        $this->persistent->fecha_2 = $fecha2;

    }

    public function obtenerLenguajeCompleto($leng)
    {
        $language="";
        if (strtoupper($leng)=="EN")
        {
            $language="Inglés";
        }

        else if(strtoupper($leng)=="ES")
        {
            $language="Español";
        }

        else{
            $language="Información No Disponible";
        }

        return $language;

    }

    /*
     * Función para imprir el archivo excel
     */
    public function excelAction()
    {
        $libros = $this->persistent->libros;
        $fecha1 = $this->persistent->fecha_1;
        $fecha2 = $this->persistent->fecha_2;

        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "VENTAS_REPORT" . "- De ". $fecha1 ." al ". $fecha2 ." / " . date("Y-m-d_his") . ".xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $header = array(
            'TÍTULO'=>'string',
            'ISBN'=>'string',
            'VENTAS'=>'string',
            'IDIOMA'=>'string'
        );

        $data1=[];
        foreach($libros as $lib)
        {

            $tempArray=[
                $lib['titulo'],
                $lib['isbn'],
                $lib['veces_vendido'],
                $lib['idioma'],


            ];
            array_push($data1,$tempArray);


        }

        $writer = new XLSXWriter();
        $writer->setAuthor('Orbile');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();
        exit(0);
    }
}