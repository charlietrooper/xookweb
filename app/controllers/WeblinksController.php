<?php

use Phalcon\Mvc\Router as Router;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;


class WeblinksController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Busqueda Masiva de ISBN en Porrua/Gandhi');
        parent::initialize();
    }

    /*
     *
     */
    public function indexAction()
    {

    }

    /*
     * Función para búsqueda masiva de ISBNs
     */
    public function buscalinksAction()
    {
        //set_time_limit(120);
        $isbnBusqueda = (object)[];
        $isbnsRequest = preg_replace('/\s+/', '', $this->request->get('isbn', null, ''));
        $isbnsArray = explode(",", $isbnsRequest);
        $listaPorrua = $this->listaPorrua($isbnsArray);
        $listaGandhi = $this->listaGandhi($isbnsArray);

            //$rest = '<br/><pre style="text-align: left;"><br/><br/><br/>' . json_encode($result, JSON_PRETTY_PRINT) . '<br/><br/><br/></pre><br/>';
            $this->view->setvar('listaPorrua', $listaPorrua);
            $this->view->setvar('listaGandhi', $listaGandhi);
            $this->view->setvar('isbnsArray', $isbnsArray);
            $this->persistent->listaPorrua = $listaPorrua;
            $this->persistent->listaGandhi = $listaGandhi;
            $this->persistent->isbnsArray = $isbnsArray;




    }

    function listaPorrua($listaISBN)
    {
        $listaPorrua=[];
        foreach ($listaISBN as $libro) {

            $libroISBN=$libro;
            $url = 'https://www.porrua.mx/busqueda/todos/'.$libroISBN;
            $html=file_get_html($url); //Devuelve un array con contenido de la página

            $posts=$html->find('#librosContainer a');


            if (count($posts)!==1 )
            {
                array_push($listaPorrua, "----");
            }

            else{

                foreach ($posts as $post) {
                    $link=$post->href;
                    $linkPorrua='<a href="'.$link.'">'.$link.'</a>';
                    $porrua= array(
                      'link'=>$link, 'print'=>$linkPorrua
                    );
                    array_push($listaPorrua, $porrua);
                }
            }


        }
        return $listaPorrua;
    }

    function listaGandhi($listaISBN)
    {
        $listaGandhi=[];
        foreach ($listaISBN as $libro) {
            $libroISBN=$libro;
            $url = 'http://busqueda.gandhi.com.mx/busca?q='.$libroISBN;
            $html=file_get_html($url); //Devuelve un array con contenido de la página

            $posts=$html->find('.category-products ul li h2 a');


            //  var_dump($posts);
            if (count($posts)!==1 )
            {
                array_push($listaGandhi, "----");
            }

            else{

                foreach ($posts as $post) {
                    $link=$post->href;
                    $link=substr($link, 4);
                    $linkGandhi='<a href="'.$link.'">'.$link.'</a>';
                    $gandhi= array(
                        'link'=>$link, 'print'=>$linkGandhi
                    );
                    array_push($listaGandhi, $gandhi);
                }
            }

        }
        return $listaGandhi;
    }


    public function excelAction(){

        $listaPorrua =   $this->persistent->listaPorrua;
        $listaGandhi = $this->persistent->listaGandhi;
        $isbnsArray=    $this->persistent->isbnsArray;

        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "REPORTE_LinksPorISBN" . "-" . date("Y-m-d_his") . ".xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $header = array(
            'ISBN'=>'string',
            'Link Porrúa'=>'string',
            'Link Gandhi'=>'string',
        );
        $length=count($isbnsArray);
        $data1=[];
        for($i=0;$i<($length);$i++)
        {
            if(($isbnsArray[$i]))
            {
                $tempArray=[
                    $isbnsArray[$i],
                    $listaPorrua[$i]['link'],
                    $listaGandhi[$i]['link']
                ];
                array_push($data1,$tempArray);
            }

        }

        $writer = new XLSXWriter();
        $writer->setAuthor('Orbile');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();
        exit(0);
    }


}

