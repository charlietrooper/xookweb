<?php

use Phalcon\Mvc\Router as Router;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;


class IsbnController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Busqueda Masiva de ISBN\'s');
        parent::initialize();
    }

    /*
     *
     */
    public function indexAction()
    {

    }

    /*
     * Función para búsqueda masiva de ISBNs
     */
    public function buscamasivaisbnAction()
    {
        //set_time_limit(120);
        $isbnBusqueda = (object)[];
        $isbnsRequest = preg_replace('/\s+/', '', $this->request->get('isbn', null, ''));
        $isbnsArray = explode(",", $isbnsRequest);
        $isbns = json_encode($isbnsArray);
        $fecha1 =  "'".$this->request->get('fecha1')."'";
        $fecha2 ="'". $this->request->get('fecha2')."'";
        $fechasArray=[$fecha1,$fecha2];
        //Se asigna el cURL a la variable $ch
        $ch = curl_init();
       // $url="https://catalog.orbile.com/isbn/getBooksBasic";

        $ruta = 'orbile_catalog_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));


        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url->value/isbn/getBooksBasic");
        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        //Especificamos el post
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);
        //LE DECIMOS QUE PARAMETROS ENVIAMOS
        curl_setopt($ch, CURLOPT_POSTFIELDS, $isbns);
        //para solucionar el problema de "https"
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        //La respuesta de cURL es guardada en la variable $resultado
        $resultado = curl_exec($ch);
        if(curl_exec($ch) === false)
        {
            echo "Error: ".curl_error($ch);
        }

        else{
            // var_dump($resultado);
            $res = json_decode($resultado, true);
            $isbnBusqueda->items = $res;
            //var_dump($res);
            $paginacionListas = new PaginacionListas();
            $listadoBusqueda = $paginacionListas->obtener_paginacion($isbnBusqueda);

            $this->view->setVar('busq', $listadoBusqueda);

            // $this->view->setVar('isbnBusq', $res);
            $this->view->setVar('isbnBusq', $res);

            //Imprimir en JSON bonito para pruebas

            $pretty_json = strip_tags(json_encode($res, JSON_PRETTY_PRINT));

            $rest = '<pre>'. $pretty_json .'</pre>';
            //$rest = '<br/><pre style="text-align: left;"><br/><br/><br/>' . json_encode($result, JSON_PRETTY_PRINT) . '<br/><br/><br/></pre><br/>';
            $this->view->setvar('res', $rest);
            $this->view->setvar('isbnsArray', $isbnsArray);
            $this->persistent->isbnBusq = $res;
            $this->persistent->arrayRes = $isbnsArray;
            $this->persistent->fechasArray = $fechasArray;
        }


    }

    function queryConteo($resultado, $fechasArray)
    {

        if($fechasArray[0]!=="''" && $fechasArray[1]!=="''")
        {
            $idProd = "'".$resultado."'";
            $queryConteo = $this->modelsManager->executeQuery("SELECT COUNT(*) as veces_vendido
                                                      FROM Purchases p
                                                      INNER JOIN PurchasesLine pl ON p.purchase_id = pl.purchase_id
                                                      WHERE pl.product_id = $idProd
                                                      AND p.date BETWEEN $fechasArray[0] AND $fechasArray[1]
                                                      ");
        }
        else{
            $idProd = "'".$resultado."'";
            $queryConteo = $this->modelsManager->executeQuery("SELECT COUNT(*) as veces_vendido
                                                      FROM Purchases p
                                                      INNER JOIN PurchasesLine pl ON p.purchase_id = pl.purchase_id
                                                      WHERE pl.product_id = $idProd
                                                      ");
        }

        return $queryConteo;
    }

    public function excelAction(){
        $busqISBN = $this->persistent->isbnBusq;
        $arrayRes = $this->persistent->arrayRes;
        $fechasArray=$this->persistent->fechasArray;

        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "REPORTE_BUSQUEDAISBN" . "-" . date("Y-m-d_his") . ".xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $header = array(
            'Título'=>'string',
            'ISBN'=>'string',
            'Autor'=>'string',
            'ID'=>'string',
            'Veces Vendido'=>'string',
            'Imprint'=>'string'
        );
        $length=count($busqISBN);
        $data1=[];
        for($i=0;$i<($length);$i++)
        {
            $isbnPasado='';
            if(empty($busqISBN[$i]['Items']))
            {
                    $tempArray=[
                        "No encontrado",
                        $arrayRes[$i],
                        "No encontrado",
                        "No encontrado",
                        "No encontrado",
                        "No encontrado"
                    ];
                    array_push($data1,$tempArray);
            }

            else{
                foreach($busqISBN[$i]['Items'] as $index => $e){

                    if(count($busqISBN[$i]['Items'])==1)
                    {
                        $id="";
                        $veces=0;


                        if(empty($e['activeRevision']['@attributes']['id']))
                        {
                            $id="--------";
                            $veces="------";
                        }
                        else{
                            $id=$e['activeRevision']['@attributes']['id'];
                            $contados=$this->queryConteo($id, $fechasArray);
                            $veces=$contados[0]['veces_vendido'];
                        }

                        $tempArray=[
                            $this->foundDataForReports($e['activeRevision']['title']),
                            $this->foundDataForReports($e['isbn']),
                            $this->foundDataForReports($e['activeRevision']['contibutors']['contributor']['@value']),
                            $id,
                            $veces,
                            $this->foundDataForReports($e['activeRevision']['imprint'])
                        ];
                        array_push($data1,$tempArray);
                    }

                    else{
                        if(!empty($e['activeRevision'])&&($e['isbn']!==$isbnPasado))
                        {
                            $isbnPasado=$e['isbn'];
                            $id="";
                            $veces=0;

                            if(empty($e['activeRevision']['@attributes']['id']))
                            {
                                $id="--------";
                                $veces="------";
                            }
                            else{
                                $id=$e['activeRevision']['@attributes']['id'];
                                $contados=$this->queryConteo($id,$fechasArray);
                                $veces=$contados[0]['veces_vendido'];
                            }

                            $tempArray=[
                                $this->foundDataForReports($e['activeRevision']['title']),
                                $this->foundDataForReports($e['isbn']),
                                $this->foundDataForReports($e['activeRevision']['contibutors']['contributor']['@value']),
                                $id,
                                $veces,
                                $this->foundDataForReports($e['activeRevision']['imprint'])
                            ];
                            array_push($data1,$tempArray);
                        }
                        //Si ya se buscó en todos los resultados, y ninguno trae información
                        //entonces imprimimos el isbn de todas manera con No Diponible
                        else if(($isbnPasado=="") && ($index == (count($busqISBN[$i]['Items'])-1)))
                        {
                            $tempArray=[
                                "No encontrado",
                                $arrayRes[$i],
                                "No encontrado",
                                "No encontrado",
                                "No encontrado",
                                "No encontrado"
                            ];
                            array_push($data1,$tempArray);
                        }

                    }


                }
            }

        }

        $writer = new XLSXWriter();
        $writer->setAuthor('Orbile');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();
        exit(0);
    }

    private function foundDataForReports($data)
    {
        if (empty($data))
        {
            return "--------";
        }

        else{
            return $data;
        }
    }

    }

