<?php
/**
 * Created by PhpStorm.
 * User: carlos-silva
 * Date: 12/06/2015
 * Time: 17:42
 */
use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;


class NotificationCataloguesController extends ControllerBase
{
    /*public function initialize()
    {
        $this->tag->setTitle('NotificationUpdates');
        parent::initialize();
    }*/

    /**
     *
     */
    public function indexAction($obj = '')
    {
        //Traemos la session y sacamos el role_id y el partner_id
        $session=$this->session->get('auth');
        $partner_id=$session['partner_id'];
        $se=$session['role_id'];

        //Mandamos el role_id a la vista y aparecemos campos en la tabla ya sea admin o partner
        $this->view->setVar('se',$se);

        //Guardamos el mes, año y dias.volt actual
        $dia = '01';
        $mes = date('m');
        $año = date('Y');

        //Para hacer la consulta
        $fechasql = $año.'-'.$mes.'-'.$dia;


        $this->view->setvar('fe', $fechasql);
        //Hacemos la consulta a la BD y segun sea el partner mostraremos los datos en la vista
        if($se==1 || $se == 4)
        {
            $partners = Partners::Find();
            $this->view->setVar('partners', $partners);

            //si la variable part esta vacia, ejecutamos el listado general de reportes
            if($obj == '')
            {
                $notification = $this->modelsManager->createBuilder()
                    ->columns('*')
                    ->from('NotificationCatalogues')
                    ->orderBy('date_notification DESC');

                $paginator = new PaginatorQueryBuilder(array(
                    "builder"   => $notification,
                    "limit"     => 20,
                    "page"      => $this->request->getQuery('page', 'int')
                ));

                $this->view->page = $paginator->getPaginate();

                $this->view->setVar('partnergraf', null);
            }
            else
            {
                $notification = $this->modelsManager->createBuilder()
                    ->columns('*')
                    ->from('NotificationCatalogues')
                    ->where('NotificationCatalogues.partner_id = :partner: ', array('partner' => $obj))
                    ->orderBy('date_notification DESC');

                $paginator = new PaginatorQueryBuilder(array(
                    "builder"   => $notification,
                    "limit"     => 20,
                    "page"      => $this->request->getQuery('page', 'int')
                ));

                $this->view->page = $paginator->getPaginate();

                $partners = Partners::Find(array(
                    "partner_id != :partner:",
                    'bind' => array('partner' => $obj)
                ));
                $this->view->setVar('partners', $partners);

                $par = Partners::findFirst($obj);
                $nombre = $par->name;
                $this->view->setVar('nombre', $nombre);
                $this->view->setVar('partnergraf', $obj);
            }
        }
        else{
            $notification = $this->modelsManager->createBuilder()
                ->columns('*')
                ->from('NotificationCatalogues')
                ->where('NotificationCatalogues.partner_id = :partner:', array('partner' => $partner_id))
                ->orderBy('date_notification DESC');

            $paginator = new PaginatorQueryBuilder(array(
                "builder"   => $notification,
                "limit"     => 20,
                "page"      => $this->request->getQuery('page', 'int')
            ));

            $this->view->page = $paginator->getPaginate();
        }
    }

    /**
     * @return \Phalcon\Http\Response
     * @throws PHPExcel_Exception
     * @throws PHPExcel_Writer_Exception
     */
    public function excelAction()
    {
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getActiveSheet()->setTitle('NOTIFICATION CATALOGUES REPORT');
        $y = 1;

        //Traemos la session y sacamos el role_id y el partner_id
        $session = $this->session->get('auth');
        $role_id = $session['role_id'];

        //se obtiene la variable 'busq' que contiene los parametros a imprimir
        $busq = $this->session->get('busq');

        //de la varibale busq se extraen los valores
        $id = $busq ['id'];
        $fecha_ini = $busq ['fecha_ini'];
        $fecha_fin = $busq ['fecha_fin'];

        /*
         * Dependiendo de los datos que que vengan en las varibles
         * $id, $fecha_ini y $fecha_fin se va realizar la consulta
         */
        //si el id esta vacio, se hará la consulta por las fechas
        if($id=='')
        {
            //se realiza la consulta
            $notification = NotificationCatalogues::find(array(
                "date_notification >= :fecha_ini: AND date_notification <= :fecha_fin:",
                'bind' => array('fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin),
                'order' => 'date_notification DESC'
            ));
        }
        //si el id trae información,  y la fecha_ini está vacía, se hará la consulta por id
        elseif($id!='' && $fecha_ini=='')
        {
            //se realiza la consulta
            $notification = NotificationCatalogues::find(array(
                "partner_id = :id:",
                'bind' => array('id' => $id),
                'order' => 'date_notification DESC'
            ));
        }
        //si el id trae información, al igual que la fecha_ini, se hará la consulta por ambos
        elseif($id!='' && $fecha_ini!='')
        {
            //se realiza la consulta
            $notification = NotificationCatalogues::find(array(
                "partner_id = :id: AND (date_notification >= :fecha_ini: AND date_notification <= :fecha_fin:)",
                'bind' => array('id' => $id, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin),
                'order' => 'date_notification DESC'
            ));
        }

        //REPORTES PARA ADMINISTRADOR
        if ($role_id == 1 || $role_id == 4) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $y, 'NOTIFICATION ID')
                ->setCellValue('B' . $y, 'DATE NOTIFICATION')
                ->setCellValue('C' . $y, 'USER EMAIL')
                ->setCellValue('D' . $y, 'PARTNER ID')
                ->setCellValue('E' . $y, 'CATALOGUE ID');

            foreach ($notification as $purch) {
                $y++;
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $y, $purch->notification_id)
                    ->setCellValue('B' . $y, $purch->date_notification)
                    ->setCellValue('C' . $y, $purch->user_email)
                    ->setCellValue('D' . $y, $purch->partner_id)
                    ->setCellValue('E' . $y, $purch->catalogue_id);
            }
        }
        //REPORTES PARA PARTNER
        else {
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $y, 'NOTIFICATION ID')
                ->setCellValue('B' . $y, 'DATE NOTIFICATION')
                ->setCellValue('C' . $y, 'USER EMAIL')
                ->setCellValue('D' . $y, 'CATALOGUE ID');

            foreach ($notification as $purch) {
                $y++;
                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $y, $purch->notification_id)
                    ->setCellValue('B' . $y, $purch->date_notification)
                    ->setCellValue('C' . $y, $purch->user_email)
                    ->setCellValue('D' . $y, $purch->catalogue_id);
            }
        }
        // file name to output
        $fname = "NOTIFICATION CATALOGUES REPORT" . ".xlsx";

        // temp file name to save before output
        $temp_file = tempnam(sys_get_temp_dir(), 'phpexcel');

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel);
        $objWriter->save($temp_file);

        $response = new \Phalcon\Http\Response();

        // Redirect output to a client’s web browser (Excel2007)
        $response->setHeader('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->setHeader('Content-Disposition', 'attachment;filename="' . $fname . '"');
        $response->setHeader('Cache-Control', 'max-age=0');

        // If you're serving to IE 9, then the following may be needed
        $response->setHeader('Cache-Control', 'max-age=1');

        //Set the content of the response
        $response->setContent(file_get_contents($temp_file));

        // delete temp file
        unlink($temp_file);

        //Return the response
        return $response;
    }

    /**
     *Busca los datos y se los muestra,
     * parte solo para administrador
     */
    public function SearchAction()
    {
        //traemos los datos via get con la peticion ajax
        $id = $this->request->get('id',null,'');
        $fecha_ini = $this->request->get('fecha_ini',null,'');
        $fecha_fin = $this->request->get('fecha_fin',null,'');

        //creamos una variable para el limite de resultados por pagina
        $rowPerPage = 20;

        //deshabilitamos la vista para las peticiones ajax
        $this->view->disable();

        //si es una peticion get
        if($this->request->isGet()==true)
        {
            //si el id es null, pero las fechas tienen valor hacemos esta consulta
            if($id == '')
            {
				//Se asigna valor a current con la variable que se envia
				//via get, es el numero de la pagina siguiente para mostrar
				//resultados
                if($current=$this->request->get('page_ajax'))
                {
                    /*si GET es true
                     * Asigna valor a offset
                     */
                    $offset=($current - 1) * $rowPerPage;
                }
                else
                {
                    //si no es peticion GET se le asigna valor cero a offset
                    $offset=0;
                }
                $notification= $this->modelsManager->createBuilder()
                    ->columns('*')
                    ->from('NotificationCatalogues')
                    ->where("NotificationCatalogues.date_notification >= :fecha_ini: AND NotificationCatalogues.date_notification <= :fecha_fin:", array('fecha_ini'=>$fecha_ini,'fecha_fin'=>$fecha_fin))
                    ->orderBy('date_notification DESC')
                    ->offset($offset);

                //Se inserta en la variable busq, los resultados de la busqueda
                //para usarse en la funcion excel
                $this->session->set('busq', array('fecha_ini'=> $fecha_ini, 'fecha_fin' => $fecha_fin));

                //se incluye el paginador
                $paginator= new PaginatorQueryBuilder(array(
                    "builder" => $notification,
                    "limit" => $rowPerPage,
                    "page" => $current
                ));

                $page = $paginator->getPaginate();
                $page->items = $page->items->toArray();

                //Se parsean los resultados
                $this->response->setJsonContent($page);
            }
            /*
            *si la variabe id y fecha no son null
            */
            if($id != '' && $fecha_ini != '' && $fecha_fin != '')
            {
                /*
                * Se asigna valor a current con la variable que se envia
                * via get, es el numero de la pagina siguiente para mostrar
                * resultados
                */

                if($current = $this->request->get('page_ajax'))
                {
                    /*si GET es true
                    * Asigna valor a offset
                    */
                    $offset = ($current - 1) * $rowPerPage;
                }
                else
                {
                    //si no es peticion GET se le asigna valor cero a offset
                    $offset=0;
                }

                $notification=$this->modelsManager->createBuilder()
                    ->columns('*')
                    ->from('NotificationCatalogues')
                    ->where("NotificationCatalogues.partner_id=:id: AND (NotificationCatalogues.date_notification >= :fecha_ini: AND NotificationCatalogues.date_notification <= :fecha_fin:)",array('id'=>$id,'fecha_ini'=>$fecha_ini, 'fecha_fin'=>$fecha_fin))
                    ->orderBy('date_notification DESC')
                    ->offset($offset);

                //Se inserta en la variable busq, los resultados de la busqueda
                //para usarse en la funcion excel
                $this->session->set('busq', array('id' => $id, 'fecha_ini'=> $fecha_ini, 'fecha_fin' => $fecha_fin));

                //se incluye el paginador
                $paginator = new PaginatorQueryBuilder(array(
                    "builder" => $notification,
                    "limit" => $rowPerPage,
                    "page" => $current
                ));


                $page = $paginator->getPaginate();
                $page->items = $page->items->toArray();

                //Parseamo los resultados y listos para enviar a la vista
                $this->response->setJsonContent($page);
            }
            /*
            *Si fechas son null y id es diferente a null
            */
            if($id != '' && $fecha_ini == '' && $fecha_fin == '')
            {

                /*
                * Se asigna valor a current con la variable que se envia
                * via get, es el numero de la pagina siguiente para mostrar
                * resultados
                */
                if($current = $this->request->get('page_ajax'))
                {
                    /*si GET es true
                   * Asigna valor a offset
                   */
                    $offset = ($current - 1) * $rowPerPage;
                }
                else
                {
                    //si no es peticion GET se le asigna valor cero a offset
                    $offset = 0;
                }

                //Se realiza la consulta en la tabla
                $notification = $this->modelsManager->createBuilder()
                    ->columns('*')
                    ->from('NotificationCatalogues')
                    ->where('NotificationCatalogues.partner_id = :id:', array('id' => $id))
                    ->orderBy('date_notification DESC')
                    ->offset($offset);

//Se inserta en la variable busq, los resultados de la busqueda
                //para usarse en la funcion excel
                $this->session->set('busq', array('id' => $id));

                $paginator = new PaginatorQueryBuilder(array(
                    "builder" => $notification,
                    "limit" => $rowPerPage,
                    "page" => $current
                ));

                $page = $paginator->getPaginate();
                $page->items = $page->items->toArray();

                //Parseamos los resultados y los enviamos a la vista
                $this->response->setJsonContent($page);

            }
            //obtenemos los datos de la busqueda

            //devolvemos un json
            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200,"Ok");
            $this->response->send();
        }
        else
        {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }

    }
    /*
 *Funcion para partners
 *
 * Utiliza los valores de id_artner, y campos de fecha
 */
    public function SearchForPartnerAction()
    {
        //Recivimos los valores que se envian via ajax a traves del index por el metodo GET
        $fecha_ini = $this->request->get('fecha_ini',null,'');
        $fecha_fin = $this->request->get('fecha_fin',null,'');

        //Desabilitamos la vista para peticiones ajax
        $this->view->disable();

        //creamos una variable para el limite de resultados por pagina
        $rowPerPage = 20;

        //Traemos la variable de session
        $session = $this->session->get('auth');
        $partner_id = $session['partner_id'];

        if($this->request->isGet() == true)
        {
            /*
				 * Se asigna valor a current con la variable que se envia
				 * via get, es el numero de la pagina siguiente para mostrar
				 * resultados
				 */

            if($current = $this->request->get('page_ajax'))
            {
                $offset = ($current - 1) * $rowPerPage;
            }
            else
            {
                $offset = 0;
            }

            //Se realiza la consulta en la tabla
            $notification = $this->modelsManager->createBuilder()
                ->columns('*')
                ->from('NotificationCatalogues')
                ->where('NotificationCatalogues.partner_id = :id: AND (NotificationCatalogues.date_notification >= :fecha_ini: AND NotificationCatalogues.date_notification <= :fecha_fin:)',array('id'=>$partner_id,'fecha_ini'=>$fecha_ini,'fecha_fin'=>$fecha_fin))
                ->orderBy('date_notification DESC')
                ->offset($offset);

            $this->session->set('busq', array('id' => $partner_id, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin));

            //se incluye el paginador
            $paginator = new PaginatorQueryBuilder(array(
                "builder" => $notification,
                "limit" => $rowPerPage,
                "page" => $current
            ));

            $page = $paginator->getPaginate();
            $page->items = $page->items->toArray();

            $this->response->setJsonContent($page);

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        }
        else{
            $this->response->setStatusCode(404,"Not Found");
            $this->response->send();
        }
    }
}