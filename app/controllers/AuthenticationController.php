<?php

/*
 * COMENTARIO PARA QUE MIKE PUEDA DESCARGAR EL CONTROLLER, AL PARECER LO BORRO Y NO SABE COMO JAJAJA
 */
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;

class AuthenticationController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Autentificación');
        parent::initialize();
    }

    /*
     *función para llamar a la vista index
     */
    public function indexAction()
    {
        //Se consulta la tabla partners para ver con exixtentes
        $partner = Partners::find();

        //Se envía la variable con la consulta para
        //que se enlisten los partners en el formulario
        $this->view->setVar('partner', $partner);
    }

    /*
     *Función para autenticarse con la API, retorna de la API
     * el kobo_id y el result
     */
    public function authenticateAction()
    {
        $ruta = 'orbile_api_base_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        $http = new HttpStatusCodeString();

        //Se obtienen los datos introducidos en los campos de la vista
        $partner_id = $this->request->getPost('partner');
        $partner_user_id = $this->request->getPost('partner_user_id');
        $email = $this->request->getPost('email');

        //Se hace la consulta a la tabla Partners para
        //buscar los datos del partner seleccionado
        $api = Partners::findFirst(array(
            'partner_id = :partner_id:',
            'bind' => array('partner_id' => $partner_id)
        ));

        //Se asignan los campos de la consulta a una respectiva varibale
        $api_username = $api->api_username;
        $api_pass = $api->api_pass;

        $this->logger->log('Api user: ' . $api_username);
        $this->logger->log('Api pass: ' . $api_pass);
        $this->logger->log('url: ' . $url->value);

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();
         //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url->value/authservice/authenticate_or_register/$email/$partner_user_id");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_username\r\nx-api-key:$api_pass"));


        //para apuntar a prod:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //La respuesta de cURL es guardada en la variable $resultado
        $resultado = curl_exec ($ch);

        //mandar a log
        $this->logger->log($resultado);

        //para ver errores posibles que contega el resultado:
        //$error = curl_error($ch);
        //$this->flash->success($error);

        //CODIGO DE ERROR MAYOR A 400
        $httpcode = curl_getinfo($ch);

        if($httpcode['http_code'] > 400)
        {
            $this->flash->error($httpcode['http_code'].": ".$http->http_status_code_string('400'). "=>" .$resultado);
            curl_close($ch);
            return $this->response->redirect('authentication/index');
        }
        else {

            //Se decodifica el JSON que envió la respuesta de la API
            $result = json_decode($resultado, true);

            //Los datos del JSON se guardan en sessión
            $this->session->set('API_AUTH', array(
                'kobo_id' => $result['KoboUserId'],
                'result' => $result['Result'],
                'api_user' => $api_username,
                'api_key' => $api_pass
            ));

            //Se imprimen los resultados en un flash,
            //a la vez que se redirecciona  "purchases"

            $json = json_decode($resultado);

            $this->flash->success('<pre>' . json_encode($json, JSON_PRETTY_PRINT) . '</pre>');
            return $this->response->redirect('purchases/index');
            curl_close($ch);
        }


    }

}