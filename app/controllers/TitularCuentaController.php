<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 09/03/2016
 * Time: 01:53 PM
 */

use Phalcon\Paginator\Adapter\model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Router as Router;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class TitularCuentaController extends ControllerBase
{

    public function initialize()
    {
        $this->tag->setTitle('Titular de cuenta');
        parent::initialize();
    }

    /*
     * Función para mostrar el listado de titulares de cuenta
     */
    public function indexAction()
    {
        //se definen varibales a utilizar
        $titulares = (object)[];
        $page = $this->request->get("page", "int", 1);
        $titulares->page = $page;
        $items_per_page = 20;

        //Se define la url a la que se conectará
        $ruta = 'orbile_catalog_url';
        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;

        //se realiza la conexión cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$url/accountHolderID/list/$items_per_page/$page");
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $resultado = curl_exec ($ch);

        //Se decodifica el Json y se asigna al objeto
        $result = json_decode($resultado, true);
        $titulares->items = $result;

        //se manda llamar la paginación de listados
        $paginacionListas = new PaginacionListas();
        $listadoTitulares = $paginacionListas->obtener_paginacion($titulares);

        $this->view->setVar('titulares', $listadoTitulares);
    }

    /*
     * Función para la busqueda de Account Holders
     */
    public function  busquedaAction()
    {
        $titulares_busqueda = (object)[];
        $page =  intval($this->request->get('pagina', 'int', 1));
        $titulares = $this->request->get('titulares');
        $titulares = urlencode($titulares);
        $titulares_busqueda->page = $page;
        $items_per_page = 20;
        //Se dshabilita la vista para las peticiones ajax
        $this->view->disable();

        if ($this->request->isGet() == true) {
            //Se define la url a la que se conectará
            $ruta = 'orbile_catalog_url';
            $api = AppConfig::findFirst(array(
                'key = :ruta:',
                'bind' => array('ruta' => $ruta)
            ));
            $url = $api->value;

            //se realiza la conexión cURL
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "$url/accountHolderID/search/$titulares/$items_per_page/$page");
            curl_setopt($ch, CURLOPT_TIMEOUT, 80);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $resultado = curl_exec ($ch);

            //Se decodifica el Json y se asigna al objeto
            $result = json_decode($resultado, true);
            $titulares_busqueda->items = $result;

            //se manda llamar la paginación de listados
            $paginacionListas = new PaginacionListas();
            $listadoTitularesBusqueda = $paginacionListas->obtener_paginacion($titulares_busqueda);


            $this->response->setJsonContent($listadoTitularesBusqueda);
            $this->response->setContentType('application/json', 'UTF-8');

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }
    }
}

