<?php

use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Model\Query as Query;

class SalesReportsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Reports');
        parent::initialize();
    }

    /*
     *
     */
    public function indexAction($obj = '')
    {
        $idSocio = $this->request->get('idSocio', null, '');
        $this->view->setVar('idSocio', $idSocio);
        //Traemos la session y sacamos el role_id y el partner_id
        $session = $this->session->get('auth');
        $partner_id = $session['partner_id'];
        $se = $session['role_id'];
        //Mandamos el role_id a la vista y aparecemos campos en la tabla ya sea admin o partner
        $this->view->setVars(array(
            'se'                => $se,
            'id_del_partner'    => $partner_id
        ));

        //Guardamos el mes, anio y dias actuales
        $dia = '01';
        $mes = date('m');
        $anio = date('Y');

        //Para hacer la consulta
        $fechasql = $anio . '-' . $mes . '-' . $dia;
        $this->view->setvar('fe', $fechasql);

        //Se obtienen los paises y se envían a la vista
        $paises = $this->modelsManager->executeQuery("SELECT sortname, name FROM Countries");
        $this->view->setVar('paises', $paises);
        $partners = Partners::Find();
        $this->view->setVar('partners', $partners);
        $conditions="";
        $variables=array();
        if($se == 1 || $se == 4)
        {
            if($obj!=='')
            {
                $conditions='Purchases.partner_id = :partner:';
                $variables=array('partner' => $obj);
                $par = Partners::findFirst($obj);
                $nombre = $par->name;
                $this->view->setVar('nombre', $nombre);
                $this->view->setVar('partnergraf', $obj);
            }
        }
        else{
            $conditions='Purchases.partner_id = :partner:';
            $variables=array('partner' => $partner_id);
        }

        $purchase = $this->modelsManager->createBuilder()
            ->columns('purchase_id, partner_id, user_id, DATE_FORMAT(date, "%d-%m-%Y %H:%i:%s") as date, purchase_partner_id, address_city, address_country, address_state_provincy, address_zip_postal_code, payment_method, result, orbile_customer_id')
            ->from('Purchases')
            ->where($conditions, $variables)
            ->orderBy('purchase_id DESC');

        $paginator = new PaginatorQueryBuilder(array(
            "builder" => $purchase,
            "limit" => 20,
            "page" => $this->request->getQuery('page', 'int')
        ));
        $this->view->page = $paginator->getPaginate();
    }

    /*
     * Función para imprimir el reporte en archivo excel
     */
    public function showCsvAction()
    {

        //Traemos la session y sacamos el role_id
        $session = $this->session->get('auth');
        $role_id = $session['role_id'];

        //Se obtiene la variable persistente 'busq' que contiene los parámetros a imprimir
        $busq = $this-> persistent->busq;

        //de la varibale busq se extraen los valores

        $condiciones = $busq ['condiciones'];
        $variables = $busq ['variables'];

        //  Dependiendo de los datos que que vengan en las varibles
        //  $id, $id_cs, $fecha_ini y $fecha_fin se va realizar la consulta
        $purchases = Purchases::find(array(
            'columns' => 'purchase_id, partner_id, user_id, DATE_FORMAT(date, "%d-%m-%Y %H:%i:%s") as date, purchase_partner_id, address_city, address_country, address_state_provincy, address_zip_postal_code, payment_method, result, orbile_customer_id',
            $condiciones,
            'bind' => $variables,
            'order' => 'purchase_id DESC'
        ));

        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "REPORTE_DE_VENTAS". "-" . date("Y-m-d_his") . ".xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        //REPORTES PARA ADMINISTRADOR
        if ($role_id == 1) {
            $header = array(
                'PURCHASE ID'=>'string',
                'PARTNER ID'=>'string',
                'USER ID'=>'string',
                'DATE'=>'string',
                'PURCHASE PARTNER ID'=>'string',
                'ADDRESS CITY'=>'string',
                'ADDRESS COUNTRY'=>'string',
                'ADDRESS STATE PROVINCY'=>'string',
                'ADDRESS ZIP POSTAL CODE'=>'string',
                'ORBILE CUSTOMER ID'=>'string'
            );
            $data1=[];

            foreach($purchases as $purch)
            {
                $tempArray=[
                    $purch->purchase_id,
                    $purch->partner_id,
                    $purch->user_id,
                    $purch->date,
                    $purch->purchase_partner_id,
                    utf8_decode($purch->address_city),
                    $purch->address_country,
                    utf8_decode($purch->address_state_provincy),
                    $purch->address_zip_postal_code,
                    $purch->orbile_customer_id
                ];
                array_push($data1,$tempArray);
            }

        } //REPORTE PARA PARTNERS
        else {
            $header = array(
                'PURCHASE ID'=>'string',
                'USER ID'=>'string',
                'DATE'=>'string',
                'PURCHASE PARTNER ID'=>'string',
                'ADDRESS CITY'=>'string',
                'ADDRESS COUNTRY'=>'string',
                'ADDRESS STATE PROVINCY'=>'string',
                'ADDRESS ZIP POSTAL CODE'=>'string',
                'ORBILE CUSTOMER ID'=>'string'
            );
            $data1=[];

            foreach($purchases as $purch)
            {
                $tempArray=[
                    $purch->purchase_id,
                    $purch->user_id,
                    $purch->date,
                    $purch->purchase_partner_id,
                    utf8_decode($purch->address_city),
                    $purch->address_country,
                    utf8_decode($purch->address_state_provincy),
                    $purch->address_zip_postal_code,
                    $purch->orbile_customer_id
                ];
                array_push($data1,$tempArray);
            }
        }
        $writer = new XLSXWriter();
        $writer->setAuthor('Orbile');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();

        exit(0);


    }

    private function getSearchConditions($search)
    {
       switch($search)
       {
           case ('id'):
           {
               $condiciones = 'Purchases.partner_id = :id:';
               break;
           }
           case ('id_cs'):
           {
               $condiciones = 'Purchases.purchase_partner_id = :id_cs:';
               break;
           }
           case ('fecha_ini'):
           {
               $condiciones = 'Purchases.date >= :fecha_ini:';
               break;
           }
           case ('fecha_fin'):
           {
               $condiciones = 'CAST(Purchases.date AS DATE) <= :fecha_fin:';
               break;
           }
           case ('pais'):
           {
               $condiciones ='Purchases.address_country = :pais:';
               break;
           }
           case ('estado'):
           {
               $condiciones ='Purchases.address_state_provincy = :estado:';
               break;
           }
           case ('ciudad'):
           {

               $condiciones ='Purchases.address_city = :ciudad:';
               break;
           }

       }
        return $condiciones;
    }

    /*
     *
     */
    public function SearchAction()
    {
        //Se traen los datos via get de la petición ajax
        $pagina = $this->request->get('pagina', null, '');
        $id = $this->request->get('id_partner', null, '');
        $id_cs = $this->request->get('id_cs', null, '');
        $fecha_ini = $this->request->get('fecha_ini', null, '');
        $fecha_fin = $this->request->get('fecha_fin', null, '');
        $pais = $this->request->get('pais', null, '');
        $estado = $this->request->get('estado', null, '');
        $ciudad = $this->request->get('ciudad', null, '');

        //Se trae el rol_id y el partner_id de la variable auth de sesión
        $session = $this->session->get('auth');
        $role_id = $session['role_id'];
        $id_partner_sesion = $session['partner_id'];

        //Se define el límite de elementos por página
        $rowPerPage = 20;

        //Se dshabilita la vista para las peticiones ajax
        $this->view->disable();

        if ($this->request->isGet() == true) {
            if($role_id == 2){
                $id = $id_partner_sesion;
            }
            $searchParameters = array('id'=>$id,'id_cs'=>$id_cs,'fecha_ini'=>$fecha_ini,'fecha_fin'=>$fecha_fin,'pais'=>$pais,'estado'=>$estado,'ciudad'=>$ciudad);
            $condiciones='';
            $variables=array();
            foreach($searchParameters as $search => $search_value)
            {
                if($search_value!=='')
                {
                    $condiciones.=$this->getSearchConditions($search);
                    $variables[$search] = $search_value;
                    $condiciones.=' AND ';

                }
            }

            if($condiciones!=='')
            {
                $condiciones=substr($condiciones, 0, -5);

            }

            //Se realiza la consulta en la tabla
            $purchases = $this->modelsManager->createBuilder()
                ->columns('purchase_id, partner_id, user_id, DATE_FORMAT(date, "%d-%m-%Y %H:%i:%s") as date, purchase_partner_id, address_city, address_country, address_state_provincy, address_zip_postal_code, payment_method, result, orbile_customer_id')
                ->from('Purchases')
                ->where($condiciones, $variables)
                ->orderBy('purchase_id DESC');

            //Se inserta en la variable persistente 'busq', los resultados de la busqueda para usarse en la función showCsv
            $this-> persistent->busq = array('condiciones' => $condiciones, 'variables' => $variables);

            //se incluye el paginador
            $paginator = new PaginatorQueryBuilder(array(
                "builder" => $purchases,
                "limit" => $rowPerPage,
                "page" => $pagina
            ));
            $page = $paginator->getPaginate();

            $page->items = $page->items->toArray();
            $this->response->setJsonContent($page);
            $this->response->setContentType('application/json', 'UTF-8');

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();

        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }

    }

    /*
     * Función para hacer dependiente el pais seleccionado con los estados
     */
    public function estadoAction()
    {
        $pais = $this->request->get('pais', null, '');
        //Se dshabilita la vista para las peticiones ajax
        $this->view->disable();

        //Si es una peticion get se realizará la consulta correspondiente
        if ($this->request->isGet() == true) {
            $estados = $this->modelsManager->executeQuery("SELECT s.name AS estados FROM Countries co
                                                               INNER JOIN States s ON co.id = s.country_id
                                                               WHERE co.sortname = '$pais'");

            $i = 0;
            foreach ($estados as $e) {
                $edos[$i] = $e->estados;
                $i++;
            }
            $this->response->setJsonContent($edos);
            $this->response->setContentType('application/json', 'UTF-8');

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }
    }

    /*
     * Función para hacer dependiente el estado seleccionado con las ciudades
     */
    public function ciudadAction()
    {
        $estado = $this->request->get('estado', null, '');
        //Se dshabilita la vista para las peticiones ajax
        $this->view->disable();

        //Si es una peticion get se realizará la consulta correspondiente
        if ($this->request->isGet() == true) {
            $cities = $this->modelsManager->executeQuery("SELECT ci.name AS ciudades FROM Countries co
                                                               INNER JOIN States s ON co.id = s.country_id
                                                               INNER JOIN Cities ci ON ci.state_id = s.id
                                                               WHERE s.name = '$estado'");

            $i = 0;
            foreach ($cities as $c) {
                $cds[$i] = $c->ciudades;
                $i++;
            }
            $this->response->setJsonContent($cds);
            $this->response->setContentType('application/json', 'UTF-8');

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }
    }

    /*
     * Función para la cinsulta a la tabla "purchases_line"
     * para los detalles de las ventas.
     */
    public function detailsAction($obj)
    {
        $purchase_id = $obj;
        //hacemos la consulta para encontrar el registro de la venta
        $v = Purchases::findFirst(array(
            'conditions'    => 'purchase_id = :purchaseid:',
            'columns'       => 'purchase_id, partner_id, user_id, DATE_FORMAT(date, "%d-%m-%Y %H:%i:%s") as date, purchase_partner_id, address_city, address_country, address_state_provincy, address_zip_postal_code, payment_method, result, orbile_customer_id',
            'bind'          => array('purchaseid' => $purchase_id)));


        $orbile = $v->orbile_customer_id;

        $cust = Customers::findFirst(array(
            'orbile_customer_id = :orbile:',
            'bind' => array('orbile' => $orbile)
        ));

        //enviamos las variables a la vista
        $this->view->setVars(array(
            'purchase_id' => $v->purchase_id,
            'partner_id' => $v->partner_id,
            'user_id' => $v->user_id,
            'date' => $v->date,
            'purchase_partner_id' => $v->purchase_partner_id,
            'address_city' => $v->address_city,
            'address_country' => $v->address_country,
            'address_state_provincy' => $v->address_state_provincy,
            'address_zip_postal_code' => $v->address_zip_postal_code,
            'payment_method' => $v->payment_method,
            'result' => $v->result,
            'orbile_customer_id' => $v->orbile_customer_id,
            'email' => $cust->email
        ));

        //se manda llamar de la session la variable auth y sacamos el role_id
        $session = $this->session->get('auth');
        $se = $session['role_id'];
        //la enviamos a la vista
        $this->view->setVar('se', $se);

        //se hace la consulta
        $d = PurchasesLine::find(array(
            "purchase_id = :purchase_id:",
            'bind' => array('purchase_id' => $purchase_id)
        ));

        $numberPage = $this->request->getQuery("page", "int");

        //se crea el paginador que muestra X hojas por página
        $paginator = new Paginator(array(
            "data" => $d,
            "limit" => 10,
            "page" => $numberPage
        ));

        //se pasa el objeto a la vista con el nombre de page
        $this->view->page = $paginator->getPaginate();

    }

    /*
     * Función para visualizar gráfica de ventas generales
     * al Admin y para seleccionar el partner a graficar
     */
    public function selectgraficaAction()
    {
        //Traemos la session y sacamos el role_id y el partner_id
        $session = $this->session->get('auth');
        $se = $session['role_id'];
        $partner_id = $session['partner_id'];
        $partner = Partners::findFirst($partner_id);
        $partner_name = $partner->name;

        $this->view->setVars(array(
            'se' => $se,
            'partner_id' => $partner_id,
            'partner_name' => $partner_name
        ));

        $esteanio = $this->persistent->esteanioesteanio;
        $anios = $this->persistent->anios;
        //Se envían a la vista
        $this->view->setVar('esteanio', $esteanio);
        $this->view->setVar('anios', $anios);
        $this->view->setVar('a', date('Y'));
        $selectAnios = $this->modelsManager->executeQuery("SELECT year(date) as anio
                                                     FROM Purchases
                                                     GROUP BY anio
                                                     ORDER BY anio");
        $this->view->setVar('selectAnios', $selectAnios);
        $thisyear=date("Y");
        $this->view->setVar('thisyear', $thisyear);
        //Se obtiene el año seleccionado
        $anio = (int)$this->request->get('anio', null, date('Y'));

        //Se consulta la tabla partners para ver con existentes
        $partner = Partners::find();

        //Se envía la variable con la consulta para
        //que se enlisten los partners en el formulario
        $this->view->setVar('partner', $partner);

        //BÚSQUEDAS PARA AÑOS ANTERIORES(con javaScript)
        if ($this->request->get('anio') != null) {
            //Se deshabilita la vista
            $this->view->disable();
            //si es get inicia la busqueda
            if ($this->request->isGet() == true) {


               //->Ventas
                //(consulta)
                $ventas_anio = $this->modelsManager->executeQuery("SELECT SUM(IFNULL(e.num_ventas,0)) as ventas, a.numero_mes as numMes
                                                                    FROM Esteanio e LEFT JOIN anio a
                                                                    ON e.mes = a.numero_mes
                                                                    WHERE e.anio = $anio
                                                                    GROUP BY e.mes
                                                                    ORDER BY a.numero_mes");



                $generalArray=array(1,2,3,4,5,6,7,8,9,10,11,12);

                //Vaciar resultados de query en arrays
                foreach ($ventas_anio as $va) {
                    $ventas_por_anio[] = $va->ventas;
                    $meses[] = $va->numMes;
                }
                //este array se devuelve al final
                $arrVentasFinal=array();
                //Este loop va a llenar arrVentasFinal con resultados del query o con 0 si no hay nada devuelto ese mes

                for($i=0;$i<12;$i++) {
                    $ban=false;
                    foreach($meses as $key=>$pp) {
                        if ($pp == $generalArray[$i]) {
                            array_push($arrVentasFinal,$ventas_por_anio[$key]);
                            $ban=true;
                        }
                    }
                    if(!$ban) {
                        array_push($arrVentasFinal, 0);
                    }
                }

                //->Clientes
                //(consulta)
                $customers_anio = $this->modelsManager->executeQuery("SELECT SUM(IFNULL(ca.num_customers,0)) as num_customers, a.numero_mes as numMes
                                                                    FROM CustomersAnio ca
                                                                    LEFT JOIN anio a
                                                                    ON ca.mes = a.numero_mes
                                                                    WHERE ca.anio = $anio
                                                                    GROUP BY ca.mes
                                                                    ORDER BY a.numero_mes");

                //Conversión del objeto en array preparado para graficar
                foreach ($customers_anio as $ca) {
                    $customers_por_anio[] = $ca->num_customers;
                    $mesesC[]=$ca->numMes;
                }
                $arrCustomersFinal=array();
                //Este loop va a llenar arrVentasFinal con resultados del query o con 0 si no hay nada devuelto ese mes
                for($i=0;$i<12;$i++) {
                    $ban=false;
                    foreach($mesesC as $key=>$pp) {
                        if ($pp == $generalArray[$i]) {
                            array_push($arrCustomersFinal,$customers_por_anio[$key]);
                            $ban=true;
                        }
                    }
                    if(!$ban) {
                        array_push($arrCustomersFinal, 0);
                    }
                }

                //->Dinero
                //Consulta
               $dineros_anio = $this->modelsManager->executeQuery("SELECT SUM(IFNULL(da.dineros,0)) as cantidad_dinero, a.numero_mes as numMes
                                                                    FROM DinerosAnio da
                                                                    LEFT JOIN anio a
                                                                    ON da.mes = a.numero_mes
                                                                    WHERE da.anio = $anio
                                                                    GROUP BY da.mes
                                                                    ORDER BY a.numero_mes");

                //Conversión del objeto en array preparado para graficar
                foreach ($dineros_anio as $da) {
                    $dineros_por_anio[] = $da->cantidad_dinero;
                    $mesesD[]=$da->numMes;
                }
                $arrDineroFinal=array();
                //Este loop va a llenar arrVentasFinal con resultados del query o con 0 si no hay nada devuelto ese mes
                for($i=0;$i<12;$i++) {
                    $ban=false;
                    foreach($mesesD as $key=>$pp) {
                        if ($pp == $generalArray[$i]) {
                            array_push($arrDineroFinal,$dineros_por_anio[$key]);
                            $ban=true;
                        }
                    }
                    if(!$ban) {
                        array_push($arrDineroFinal, 0);
                    }
                }


                $otro_anio = array(
                    'ventas_por_anio' => $arrVentasFinal,
                    'customers_por_anio' => $arrCustomersFinal,
                    'dineros_por_anio' => $arrDineroFinal
                );

                $this->response->setJsonContent($otro_anio);

                $this->response->setStatusCode(200, "Ok");
                $this->response->send();
            } else {
                $this->response->setStatusCode(404, "Not Found");
                $this->response->send();
            }
        }

    }

    /*
     * Función para mostrar la gráfica por año
     */
    public function anioventasAction($id_partner)
    {
        //Se obtiene el rol(admin/orbile_admin o partner)
        $role = $this->session->get('auth');
        $rol = $role['role_id'];
        //Se envía a la vista
        $this->view->setVar('rol', $rol);

        //Se obtiene el nombre del partner
        $partners = Partners::findFirst($id_partner);
        $partner = $partners->name;
        //Se envía a la vista el partner y su id
        $this->view->setVar('id_partner', $id_partner);
        $this->view->setVar('partner', $partner);

        //Se obtienen las variables persistentes de los años disponibles
        //en la base de datos para obtener las opciones de año a elegir
        $esteanio = $this->persistent->esteanioesteanio;
        $anios = $this->persistent->anios;
        //Se envían a la vista
        $this->view->setVar('esteanio', $esteanio);
        $this->view->setVar('anios', $anios);
        $this->view->setVar('a', date('Y'));

        //Se obtiene el año seleccionado
        $anio = (int)$this->request->get('anio', null, date('Y'));

        //BÚSQUEDAS PARA AÑOS ANTERIORES(con javaScript)
        if ($this->request->get('anio') != null) {
            //Se deshabilita la vista
            $this->view->disable();
            //si es get inicia la busqueda
            if ($this->request->isGet() == true) {

                $partnerid = (int)$this->request->get('id_partner', null, '');

                //->Ventas
                //(consulta)
                $ventas_anio = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(e.num_ventas,0) as ventas, a.numero_mes
                                                            FROM Esteanio e RIGHT JOIN anio a
                                                            ON e.mes = a.numero_mes
                                                            AND e.partner_id= $partnerid
                                                            AND e.anio = $anio
                                                            ORDER BY a.numero_mes");

                //Conversión del objeto en array preparado para graficar
                foreach ($ventas_anio as $va) {
                    $ventas_por_anio[] = $va->ventas;
                }

                //->Clientes
                //(consulta)
                $customers_anio = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(ca.num_customers,0) as num_customers, a.numero_mes
                                                                FROM CustomersAnio ca
                                                                RIGHT JOIN anio a
                                                                ON ca.mes = a.numero_mes
                                                                AND ca.partner_id= $partnerid
                                                                AND ca.anio = $anio
                                                                ORDER BY a.numero_mes");

                //Conversión del objeto en array preparado para graficar
                foreach ($customers_anio as $ca) {
                    $customers_por_anio[] = $ca->num_customers;
                }

                //->Dinero
                //Consulta
                $dineros_anio = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(da.dineros,0) as cantidad_dinero, a.numero_mes
                                                            FROM DinerosAnio da RIGHT JOIN anio a
                                                            ON da.mes = a.numero_mes
                                                            AND da.partner_id = $partnerid
                                                            AND da.anio = $anio
                                                            ORDER BY a.numero_mes");

                //Conversión del objeto en array preparado para graficar
                foreach ($dineros_anio as $da) {
                    $dineros_por_anio[] = $da->cantidad_dinero;
                }

                $otro_anio = array(
                    'ventas_por_anio' => $ventas_por_anio,
                    'customers_por_anio' => $customers_por_anio,
                    'dineros_por_anio' => $dineros_por_anio
                );

                $this->response->setJsonContent($otro_anio);

                $this->response->setStatusCode(200, "Ok");
                $this->response->send();
            } else {
                $this->response->setStatusCode(404, "Not Found");
                $this->response->send();
            }
        } //BÚSQUEDAS PARA EL AÑO ACTUAL
        else {
            $partnerid = $id_partner;

            //VENTAS
            //consulta
            $ventas_anio = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(e.num_ventas,0) as ventas, a.numero_mes
                                                            FROM Esteanio e RIGHT JOIN anio a
                                                            ON e.mes = a.numero_mes
                                                            AND e.partner_id= $partnerid
                                                            AND e.anio = $anio
                                                            ORDER BY a.numero_mes");

            //Conversión del objeto en array preparado para graficar
            foreach ($ventas_anio as $va) {
                $ventas_por_anio[] = $va->ventas;
            }

            $this->view->setVar('ventas_por_anio', $ventas_por_anio);

            //CUSTOMERS
            $customers_anio = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(ca.num_customers,0) as num_customers, a.numero_mes
                                                                FROM CustomersAnio ca
                                                                RIGHT JOIN anio a
                                                                ON ca.mes = a.numero_mes
                                                                AND ca.partner_id = $partnerid
                                                                AND ca.anio = $anio
                                                                ORDER BY a.numero_mes");
            //Conversión del objeto en array preparado para graficar
            foreach ($customers_anio as $ca) {
                $customers_por_anio[] = $ca->num_customers;
            }

            $this->view->setVar('usuarios_por_anio', $customers_por_anio);

            //DINEROS
            //Consulta
            $dineros_anio = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(da.dineros,0) as cantidad_dinero, a.numero_mes
                                                            FROM DinerosAnio da RIGHT JOIN anio a
                                                            ON da.mes = a.numero_mes
                                                            AND da.partner_id = $partnerid
                                                            AND da.anio = $anio
                                                            ORDER BY a.numero_mes");

            $generalArray=array(1,2,3,4,5,6,7,8,9,10,11,12);
            //Conversión del objeto en array preparado para graficar
            foreach ($dineros_anio as $da) {
                $dineros_por_anio[] = $da->cantidad_dinero;
                $mesesD[]=$da->numero_mes;
            }
            $arrDineroFinal=array();
            //Este loop va a llenar arrVentasFinal con resultados del query o con 0 si no hay nada devuelto ese mes
            for($i=0;$i<12;$i++) {
                $ban=false;
                foreach($mesesD as $key=>$pp) {
                    if ($pp == $generalArray[$i]) {
                        array_push($arrDineroFinal,$dineros_por_anio[$key]);
                        $ban=true;
                    }
                }
                if(!$ban) {
                    array_push($arrDineroFinal, 0);
                }
            }

            //Conversión del objeto en array preparado para graficar

            $this->view->setVar('dineros_por_anio', $arrDineroFinal);
        }
    }

    /*
     * Función para mostrar la gráfica por mes
     */
    public function mesAction($id_partner = '')
    {
        if ($id_partner == '') {
            //Se obtiene el partner a graficar
            $partnerid = $this->request->get('partnerid');

            //Se consulta el registro coincidente con el id
            $partner_id = Partners::findFirst($partnerid);

            $id_partner = $partner_id->partner_id;
            $this->view->setVar('id_partner', $id_partner);

            //Obtenemos el nombre del partner
            $partner = $partner_id->name;

            //Se envía el nombre de partner a la vista
            $this->view->setVar('partner', $partner);
        } else {
            $partners = Partners::findFirst($id_partner);
            $partner = $partners->name;
            $this->view->setVar('id_partner', $id_partner);
            $this->view->setVar('partner', $partner);
        }

        //Se obtiene el rol(admin/orbile_admin o partner)
        $role = $this->session->get('auth');
        $rol = $role['role_id'];
        //Se envía a la vista
        $this->view->setVar('rol', $rol);

        //Se obtienen los años que haya registrados en la base de datos
        $anios = $this->modelsManager->executeQuery("SELECT year(date) as anio
                                                     FROM Purchases
                                                     GROUP BY anio
                                                     ORDER BY anio");

        //Si no esta en la base de datos el año actual, se crea otra variable que lo contenga
        foreach ($anios as $a) {
            if ($a->anio == date('Y')) {
                unset($esteanio);
                break;
            } else {
                $esteanio = date('Y');
            }
        }
        //Se envían a la vista para obtener las opciones de año a elegir
        $this->view->setVar('esteanio', $esteanio);
        $this->view->setVar('anios', $anios);
        $this->view->setVar('a', date('Y'));
        //Se envían como variables persistentes en el controlador
        $this->persistent->esteanio = $esteanio;
        $this->persistent->anios = $anios;

        //Se obtiene el año seleccionado
        $anio = (int)$this->request->get('anio', null, date('Y'));
        $mes = (int)$this->request->get('mes', null, date('n'));

        //Se asigna el número de días del mes en curso a la variable $num_dias
        switch ($mes) {
            case 1:
                $num_dias = 31;
                break;
            case 2:
                if ($anio % 4 == 0) {
                    $num_dias = 29;
                } else {
                    $num_dias = 28;
                }
                break;
            case 3:
                $num_dias = 31;
                break;
            case 4:
                $num_dias = 30;
                break;
            case 5:
                $num_dias = 31;
                break;
            case 6:
                $num_dias = 30;
                break;
            case 7:
                $num_dias = 31;
                break;
            case 8:
                $num_dias = 31;
                break;
            case 9:
                $num_dias = 30;
                break;
            case 10:
                $num_dias = 31;
                break;
            case 11:
                $num_dias = 30;
                break;
            case 12:
                $num_dias = 31;
                break;
        }

        //Se genera el array para los meses a seleccionar los meses
        $meses = array(
            1 => 'Enero',
            2 => 'Febrero',
            3 => 'Marzo',
            4 => 'Abril',
            5 => 'Mayo',
            6 => 'Junio',
            7 => 'Julio',
            8 => 'Agosto',
            9 => 'Septiembre',
            10 => 'Octubre',
            11 => 'Noviembre',
            12 => 'Diciembre'
        );
        $this->view->setVar('meses', $meses);
        $this->view->setVar('estemes', $mes);

        //BÚSQUEDAS PARA AÑOS ANTERIORES(con javaScript)
        if ($this->request->get('anio') != null) {
            //Se deshabilita la vista
            $this->view->disable();
            //si es get inicia la busqueda
            if ($this->request->isGet() == true) {

                $partnerid = (int)$this->request->get('id_partner', null, '');

                //->Ventas
                //(consulta)
                $ventas_mes = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(e.num_ventas,0) as ventas, m.dia
                                                            FROM Estemes e
                                                            RIGHT JOIN mes m
                                                            ON e.dia = m.dia
                                                            AND e.partner_id = $partnerid
                                                            AND e.mes = $mes
                                                            AND e.anio = $anio
                                                            ORDER BY m.dia");

                //Se genera el Array con los resultados según el npumero de días por mes
                $dia = 0;
                $i = 0;

                foreach ($ventas_mes as $vm) {
                    $ventas_por_mes[] = $vm->ventas;
                    $dia = $dia + 1;
                    $dias[] = $dia;
                    if (++$i == $num_dias) break;
                }

                //->Clientes
                //(consulta)
                $customers_mes = $this->modelsManager->executeQuery("SELECT DISTINCT cm.partner_id, IFNULL(cm.num_customers,0) as num_customers, m.dia
                                                                FROM CustomersMes cm
                                                                RIGHT JOIN mes m
                                                                ON cm.dia = m.dia
                                                                AND cm.partner_id = $partnerid
                                                                AND cm.mes = $mes
                                                                AND cm.anio = $anio
                                                                ORDER BY m.dia");

                //Se genera el Array con los resultados según el npumero de días por mes
                $i = 0;
                foreach ($customers_mes as $cm) {
                    $customers_por_mes[] = $cm->num_customers;
                    if (++$i == $num_dias) break;
                }

                //->Dinero
                //Consulta
                $dineros_mes = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(dm.dineros,0) as cantidad_dinero, m.dia
                                                            FROM DinerosMes dm
                                                            RIGHT JOIN mes m
                                                            ON dm.dia = m.dia
                                                            AND dm.partner_id = $partnerid
                                                            AND dm.mes = $mes
                                                            AND dm.anio = $anio
                                                            ORDER BY m.dia");

                //Se genera el Array con los resultados según el npumero de días por mes
                $i = 0;
                foreach ($dineros_mes as $dm) {
                    $dineros_por_mes[] = $dm->cantidad_dinero;
                    $diasD[]=$dm->dia;
                    if (++$i == $num_dias) break;
                }

                $arrDineroFinal=array();
                //Este loop va a llenar arrVentasFinal con resultados del query o con 0 si no hay nada devuelto ese mes
                for($i=1;$i<=($num_dias);$i++) {
                    $ban=false;
                    foreach($diasD as $key=>$pp) {
                        if ($pp == $i) {
                            array_push($arrDineroFinal, $dineros_por_mes[$key]);
                            $ban=true;
                        }
                    }
                    if(!$ban) {
                        array_push($arrDineroFinal, 0);
                    }
                }


                $otro_anio = array(
                    'ventas_por_mes' => $ventas_por_mes,
                    'dias' => $dias,
                    'customers_por_mes' => $customers_por_mes,
                    'dineros_por_mes' => $arrDineroFinal
                );

                $this->response->setJsonContent($otro_anio);

                $this->response->setStatusCode(200, "Ok");
                $this->response->send();
            } else {
                $this->response->setStatusCode(404, "Not Found");
                $this->response->send();
            }
        } //BÚSQUEDAS PARA EL AÑO ACTUAL
        else {
            $partnerid = $id_partner;

            //Ventas
            $ventas_mes = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(e.num_ventas,0) as ventas, m.dia
                                                            FROM Estemes e
                                                            RIGHT JOIN mes m
                                                            ON e.dia = m.dia
                                                            AND e.partner_id = $partnerid
                                                            AND e.mes = $mes
                                                            AND e.anio = $anio
                                                            ORDER BY m.dia");
            $dia = 0;
            $i = 0;

            foreach ($ventas_mes as $vm) {
                $ventas_por_mes[] = $vm->ventas;
                $dia = $dia + 1;
                $dias[] = $dia;
                if (++$i == $num_dias) break;
            }

            $this->view->setVar('ventas_por_mes', $ventas_por_mes);
            $this->view->setVar('dias', $dias);

            //Clientes
            $customers_mes = $this->modelsManager->executeQuery("SELECT DISTINCT cm.partner_id, IFNULL(cm.num_customers,0) as num_customers, m.dia
                                                                FROM CustomersMes cm
                                                                RIGHT JOIN mes m
                                                                ON cm.dia = m.dia
                                                                AND cm.partner_id = $partnerid
                                                                AND cm.anio = $anio
                                                                AND cm.mes = $mes
                                                                ORDER BY m.dia");
            $i = 0;
            foreach ($customers_mes as $cm) {
                $customers_por_mes[] = $cm->num_customers;
                if (++$i == $num_dias) break;
            }

            $this->view->setVar('usuarios_por_mes', $customers_por_mes);

            //Dineros
            $dineros_mes = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(dm.dineros,0) as cantidad_dinero, m.dia
                                                            FROM DinerosMes dm RIGHT JOIN mes m
                                                            ON dm.dia = m.dia
                                                            AND dm.partner_id = $partnerid
                                                            AND dm.anio = $anio
                                                            AND dm.mes = $mes
                                                            ORDER BY m.dia");

            $i = 0;
            foreach ($dineros_mes as $dm) {
                $dineros_por_mes[] = $dm->cantidad_dinero;
                $diasD[]=$dm->dia;
                if (++$i == $num_dias) break;
            }
            $arrDineroFinal=array();
            //Este loop va a llenar arrVentasFinal con resultados del query o con 0 si no hay nada devuelto ese mes
            for($i=1;$i<($num_dias);$i++) {
                $ban=false;
                foreach($diasD as $key=>$pp) {
                    if ($pp == $i) {
                        array_push($arrDineroFinal,$dineros_mes[$key]);
                        $ban=true;
                    }
                }
                if(!$ban) {
                    array_push($arrDineroFinal, 0);
                }
            }

            $this->view->setVar('dineros_por_mes', $dineros_por_mes);
        }
    }

    /*
     * Función para mostrar la gráfica por semana
     */
    public function semanaAction($id_partner = '')
    {
        $role = $this->session->get('auth');
        $rol = $role['role_id'];
        $this->view->setVar('rol', $rol);

        $partners = Partners::findFirst($id_partner);
        $partner = $partners->name;
        $this->view->setVar('id_partner', $id_partner);
        $this->view->setVar('partner', $partner);

        $partnerid = $id_partner;

        //VENTAS
        $ventas_semana = $this->modelsManager->executeQuery("SELECT e.partner_id, IFNULL(e.num_ventas,0) as ventas, s.dia
                                                                FROM Estasem e
                                                                RIGHT JOIN semana s
                                                                ON e.dia_semana = s.dia
                                                                AND e.partner_id=$partnerid
                                                                ORDER BY s.dia");
        foreach ($ventas_semana as $vs) {
            $ventas_por_semana[] = $vs->ventas;
        }
        $this->view->setVar('ventas_por_semana', $ventas_por_semana);

        //CUSTOMERS
        $customers_semana = $this->modelsManager->executeQuery("SELECT cs.partner_id, IFNULL(cs.num_customers,0) as ventas, s.dia
                                                                FROM CustomersSemana cs
                                                                RIGHT JOIN semana s
                                                                ON cs.dia_sem = s.dia
                                                                AND cs.partner_id=$partnerid
                                                                ORDER BY s.dia");
        foreach ($customers_semana as $cs) {
            $customers_por_semana[] = $cs->ventas;
        }

        $this->view->setVar('usuarios_por_semana', $customers_por_semana);

        //DINEROS :)
        $dineros_semana = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(ds.dineros,0) as cantidad_dinero, s.dia
                                                            FROM DinerosSem ds RIGHT JOIN semana s
                                                            ON ds.dia_sem = s.dia AND ds.partner_id = $partnerid
                                                            ORDER BY s.dia");

        foreach ($dineros_semana as $ds) {
            $dineros_por_semana[] = $ds->cantidad_dinero;
        }

        $this->view->setVar('dineros_por_semana', $dineros_por_semana);
    }

    /*
     * Función para mostrar la gráfica por día
     */
    public function diasAction($id_partner)
    {
        $role = $this->session->get('auth');
        $rol = $role['role_id'];
        $this->view->setVar('rol', $rol);

        $partners = Partners::findFirst($id_partner);
        $partner = $partners->name;
        $this->view->setVar('id_partner', $id_partner);
        $this->view->setVar('partner', $partner);

        $partnerid = $id_partner;

        //VENTAS
        $ventas_dia = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(e.num_ventas,0) as ventas, d.hora
                                                            FROM Estedia e
                                                            RIGHT JOIN dia d
                                                            ON e.hora = d.hora
                                                            AND e.partner_id=$partnerid
                                                            ORDER BY d.hora");
        foreach ($ventas_dia as $vd) {
            $ventas_por_dia[] = $vd->ventas;
        }
        $this->view->setVar('ventas_por_dia', $ventas_por_dia);

        //CUSTOMERS
        $customers_dia = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(cd.num_customers,0) as ventas, d.hora
                                                                FROM CustomersDia cd
                                                                RIGHT JOIN dia d
                                                                ON cd.hora = d.hora
                                                                AND cd.partner_id=$partnerid
                                                                ORDER BY d.hora");
        foreach ($customers_dia as $cd) {
            $customers_por_dia[] = $cd->ventas;
        }

        $this->view->setVar('usuarios_por_dia', $customers_por_dia);

        //DINEROS :)
        $dineros_dia = $this->modelsManager->executeQuery("SELECT DISTINCT IFNULL(dd.dineros,0) as cantidad_dinero, d.hora
                                                            FROM DinerosDia dd RIGHT JOIN dia d
                                                            ON dd.hora = d.hora AND dd.partner_id = $partnerid
                                                            ORDER BY d.hora");



        foreach ($dineros_dia as $dd) {
            $dineros_por_dia[] = $dd->cantidad_dinero;
        }

        $this->view->setVar('dineros_por_dia', $dineros_por_dia);
    }

    /*
     * Función para mostrar la gráfica del total
     */
    public function totalAction($id_partner)
    {
        $role = $this->session->get('auth');
        $rol = $role['role_id'];
        $this->view->setVar('rol', $rol);

        $partners = Partners::findFirst($id_partner);
        $partner = $partners->name;
        $this->view->setVar('id_partner', $id_partner);
        $this->view->setVar('partner', $partner);

        $partnerid = $id_partner;

        //VENTAS
        //Conteo de registros totales de ventas según el partner seleccionado
        $ventatotal = Purchases::count("partner_id = $partnerid");

        //se envía la variable con el conteo obtenido a la vista
        $this->view->setVar('ventatotal', $ventatotal);

        //USUARIOS
        //Conteo de registros totales de ventas según el partner seleccionado
        $Customerstotal = Customers::count("partner_id = $partnerid");

        //se envía la variable con el conteo obtenido a la vista
        $this->view->setVar('Usuariostotal', $Customerstotal);

        $dinerosTotal = $this->modelsManager->executeQuery("SELECT (SUM(pl.cogs_amount))/1000 as dineros FROM Purchases p
                                                            INNER JOIN PurchasesLine pl ON pl.purchase_id = p.purchase_id
                                                            WHERE p.partner_id = $partnerid
                                                            AND price_before_tax > 0 AND pl.result ='Success'");

        foreach ($dinerosTotal as $dt) {
            $dineros_total[] = $dt->dineros;
        }

        $this->view->setVar('dineros_total', $dineros_total);
    }
}