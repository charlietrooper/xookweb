<?php

use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Model\Query as Query;
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 17/06/2015
 * Time: 10:51 AM
 *
 *
 * Class que utiliza solo el ADMIN pararegistrar un nuevo user
 */
class UsersController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Users');
        parent::initialize();
    }
    /*
     *
     */
    public function IndexAction()
    {

        $user = $this->modelsManager->createBuilder()
            ->columns('user_id, username, pass, name, email, DATE_FORMAT(created_at, "%d-%m-%Y %H:%i:%s") as created_at, active, role_id, partner_id')
            ->from('Users')
            ->orderBy('user_id DESC');

        $numberPage = $this->request->getQuery("page", "int");

        //Se crea el paginator que muestra x hojas por pagina
        $paginator = new PaginatorQueryBuilder(array(
            "builder" => $user,
            "limit" => 10,
            "page" => $numberPage
        ));

        $this->view->page = $paginator->getPaginate();

    }
    /*
     * VISTA PARA PODER CAPTURAR UN USUARIO
     */
    public function CreateAction()
    {
        $role=Roles::find();
        $this->view->setVar('rol', $role);

        $partner=Partners::find();
        $this->view->setVar('partner', $partner);
    }
    /**
     *FUNCION QUE RECIVE LOS PARAMETROS
     * Y REGISTRARA UN NUEVO USUSARIO
     */
    public function RegisterAction()
    {

        $dia=date('d');
        $mes=date('m');
        $año=date('Y');
        $username=$this->request->getPost('username');
        $pass=$this->request->getPost('pass');
        $name=$this->request->getPost('name');
        $email=$this->request->getPost('email');
        $created_at=$año."-".$mes."-".$dia;
        $active=$this->request->getPost('active');
        $role_id=$this->request->getPost('role');
        $partner_id=$this->request->getPost('partner');


        $users= new Users();
        $users->username=$username;
        $users->pass=$this->security->hash($pass);
        $users->name=$name;
        $users->email=$email;
        $users->created_at=$created_at;
        $users->active=$active;
        $users->role_id=$role_id;
        $users->partner_id=$partner_id;

        $success=$users->save();
        if($success)
        {
            $this->flash->success("El usuario a sido registrado exitosamente!!");
            return $this->response->redirect('users/index');
        }
        else
        {
            foreach($users->getMessages() as $message)
            {
                $this->flash->error($message);
            }
            return $this->response->redirect('index/index');
        }

    }
    /*
     *VISTA PARA PODER MODIFICAR DATOS USERS
     */
    public function EditAction($u = '')
    {
        $user = Users::find($u);
        $this->view->setvar("user", $user);

        $role = Roles::find();
        $this->view->setvar("rol", $role);

        $partner = Partners::find();
        $this->view->setvar('partner', $partner);
    }

    /*
     *
     *RECIBE PARAMETROS DE EDIT PARA MODIFICAR DATOS DE USER
     *
     */

    public function UpdateAction()
    {
        //Traemos los datos de la vista
        $id=$this->request->getPost('id');
        $username=$this->request->getPost('username');

        $name=$this->request->getPost('name');
        $email=$this->request->getPost('email');
        $rol=$this->request->getPost('role');
        $partner=$this->request->getPost('partner');

        $active=$this->request->getPost('active');
        $pass=$this->request->getPost('pass');

        //Hacemos la consulta a la BD con el $id
        $users = Users::findFirst($id);

        //Checamos si el pass de la vista es igual a la guardada en la DB
        if($this->security->checkHash($pass ,$users->pass))
        {
            //Relacionamos los valores traidos de la interfaz con la de Users
            $users->username=$username;
            $users->name=$name;
            $users->email=$email;
            $users->role_id=$rol;
            $users->partner_id=$partner;
            $users->active=$active;

        }

        //Si no son iguales los pass se hashea el pass de la vista
        else
        {
            //Relacionamos los valores traidos de la interfaz con la de Users y
            //hasheamos el pass nuevo traido de la vista
            $users->username=$username;
            $users->pass=$this->security->hash($pass);
            $users->name=$name;
            $users->email=$email;
            $users->role_id=$rol;
            $users->partner_id=$partner;
            $users->active=$active;

        }

        //Hacemos update
        $success = $users->update();
        if($success)
        {
            $this->flash->success("El user con id: $id, ha sido modificado");
            return $this->response->redirect("users/index");
        }
        else{
            foreach ($users->getMessages() as $messages)
            {
                $this->flash->error($messages);
            }
            return $this->response->redirect("users/index");
        }

    }

    function DeleteAction($u = '')
    {
        $user = Users::find($u);
        $this->view->setvar("u", $user);
    }

    function EliminarAction()
    {
        //traemos el id mostrado en la vista
        $id = $this->request->getPost("user_id");

        //Se realiza la busqueda con el id obtenido
        $user = Users::find($id);

        //Se llama al metod delete para proceder con la baja del user
        $success = $user->delete();
        if ($success) {
            $this->flash->success("El user con id $id, a sido eliminado!!");
            return $this->forward("users/index");
        } else {
            foreach ($user->getMessages() as $message) {
                $this->flash->error($message);
            }
            return $this->forward('users/index');
        }

    }


}