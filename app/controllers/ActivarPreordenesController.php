<?php
/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 04/04/2016
 * Time: 08:34 PM
 */

class ActivarPreordenesController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Activar pre ordenes');
        parent::initialize();
    }

    public function actAction()
    {

    }

    /*
     * Función para activar las preordenes al día
     */

    public function activarAction()
    {
        $ruta = 'orbile_api_base_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        $headers = array();
        $headers[] = 'x-api-user:orbile';
        $headers[] = 'x-api-key:$2a$08$AXVOnD9Q3f56TyG7ujCra.kn6J4FqRlJWW9HGcI8Hn6v/8eX5g3W2';

        $curl = curl_init();

        curl_setopt($curl, CURLOPT_URL, "$url->value/pre_order/active");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

        $curl_response = curl_exec($curl);

        $respuesta = json_decode($curl_response);
        $this->flash->success('<pre>'.json_encode($respuesta, JSON_PRETTY_PRINT). '</pre>');
    }
}