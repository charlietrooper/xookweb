<?php

class ConfigController extends ControllerBase
{
    /*
     * Función para nombre
     * de página en pestaña.
     */
    public function initialize()
    {
        //se envía el tag para el nombre que
        //aparace en las pestañas del navegador
        $this->tag->setTitle('Config');
        parent::initialize();
    }

    /*
     *Función que mandará los datos
     * en la vista del index.
     */
    public function indexAction()
    {
        //Se hace la consulta
        $url = AppConfig::find();

        //se envían los datos de la consulta a la vista.
        $this->view->setVar('url', $url);
    }

    /*
     *Función que listará los datos del
     * registro seleccionado a editar.
     */
    public function editAction($obj)
    {
        //Se busca la variable seleccionada a editar
        $conf = AppConfig::find($obj);

        //Se envía la varibale a la vista
        $this->view->setvar("conf", $conf);
    }

    /*
     * Función que actualizará los
     * datos en la tabla de la BD.
     */
    public function configuraAction($obj)
    {

        //Se obtienes los datos seleccionados
        //en la vista y el id
        $id_url = $this->request->getPost('id_url');
        $key = $this->request->getPost('key');
        $value = $this->request->getPost('value');

        //Se busca el registro a modificar
        $url_consulta = AppConfig::findFirst($id_url);

        //Se insertan los valores
        $url_consulta->value = $value;

        //Los cargamos
        $update = $url_consulta->update();

        if($update)
        {
            $this->flash->success("$key ha sido modificado");
            return $this->response->redirect('config/index');
        }
        else
        {
            foreach ($url_consulta->getMessages() as $messages) {
                $this->flash->error($messages);
            }
            return $this->response->redirect('config/index');
        }
    }
}