<?php

use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;

class PurchasesController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Compras');
        parent::initialize();
    }

    /*
     *
     */
    public function indexAction()
    {
        $api_auth = $this->session->get('API_AUTH');

        $k = $api_auth['kobo_id'];
        $r = $api_auth['result'];

        $this->view->setvar('k', $k);
    }

    /*
     * Función para visualizar los titulos, isbns, y autores de los libros
     * que se comprarán mediante el Active Revision ID
     */
    public function detallesAction()
    {
        $productos = $this->request->get('productos', null, '');
        $num_elementos = $this->request->get('num_elementos', null, '');

        $this->view->disable();

        $ruta = 'orbile_catalog_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        if ($this->request->isGet() == true) {
            for ($i = 0; $i < $num_elementos; $i++) {
                //Se asigna el cURL a la variable $ch
                $ch = curl_init();

                //enviamos la url con los parámetros ingresados por el aprtner
                curl_setopt($ch, CURLOPT_URL, "$url->value/books/getBook/rev/$productos[$i]");

                //Se asigna el tiempo de espera
                curl_setopt($ch, CURLOPT_TIMEOUT, 80);

                //Afirma que se requiere una respuesta
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);


                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

                $resultado = curl_exec($ch);

                $result = json_decode($resultado, true);

                //Id(active revision id), isbn, titulo, autor
                $isbn = $result['isbn'];
                $titulo = $result['activeRevision']['title'];
                $autor = $result['activeRevision']['contibutors']['contributor']['@value'];


                $libros[] = array(
                    'id_actrev' => $productos[$i],
                    'isbn' => $isbn,
                    'titulo' => $titulo,
                    'autor' => $autor
                );
            }
            $this->response->setJsonContent($libros);

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        }
        else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }

        //$this->response->setJsonContent($libros);
        //$this->view->setVar('num_elementos', $num_elementos);
        //$this->persistent->num_elementos = $num_elementos;
        //$this->view->setvar('libros', $libros);
    }


    /*
     *LLAMAMOS A LA API verify_purchase PARA VER SI LOS
     * LIBROS ENVIADOS ESTAN DISONIBLES
     */
    public function verificationAction()
    {
        $http = new HttpStatusCodeString();

        $ruta = 'orbile_api_base_url';

        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        $url = $api->value;

        $user = $this->session->get('API_AUTH');
        $api_username = $user['api_user'];
        $api_pass = $user['api_key'];
        $kobo_id = $user['kobo_id'];

        $address = array();
        $address['City'] = $this->request->getPost('city');
        $address['Country'] = $this->request->getPost('country');
        $address['StateProvince'] = $this->request->getPost('state');
        $address['ZipPostalCode'] = $this->request->getPost('postal');
        //var_dump($api_pass);
        $productIdsDirty = preg_replace('/\s+/', '', $this->request->getPost('product_id'));
        $product_ids = explode(",", $productIdsDirty);

        $post_fields = json_encode(array('user_id' => $this->request->getPost('user_id'),
            'product_ids' => $product_ids,
            'address' => $address
        ));


        //var_dump($post_fields);

        //iniciamos el curl
        $ch = curl_init();
        //var_dump($fields);
        //pasamos la url
        curl_setopt($ch, CURLOPT_URL, "$url/purchase_service/verify_purchase");

        //TIEMPO DE RESPUESTA
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //LE DECIMOS QUE QUEREMOS RECOGER UNA RESPUESTA
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Especificamos el post
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);

        //LE DECIMOS QUE PARAMETROS ENVIAMOS
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);

        //Lo que hace la funcion http_build_query es convertir un array(a=>1,b=>2) en una cadena de texto tipo &a=1&b=2
        //curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query("user_id = $user_id"));

        $headers = array();
        $headers[] = "x-api-user:$api_username";
        $headers[] = "x-api-key:$api_pass";


        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        //RECOGEMOS LA RESPUESTA
        $resultado = curl_exec($ch);

        $httpcode = curl_getinfo($ch);

        if($httpcode['http_code'] > 400)
        {
            $this->flash->error($httpcode['http_code'].": ".$http->http_status_code_string('400'). "=>" .$resultado);
            curl_close($ch);
            return $this->forward('purchases/index');
        }

        else{
            //y finalmente cerramos curl
            curl_close($ch);

            $json = json_decode($resultado);

            $this->flash->success('<pre>'.json_encode($json, JSON_PRETTY_PRINT). '</pre>');
            //print_r($resultado);

            $this->view->setvar('kobo', $kobo_id);

            $this->view->setvar('city',$address['City']);
            $this->view->setvar('country',$address['Country']);
            $this->view->setvar('state',$address['StateProvince']);
            $this->view->setvar('zip',$address['ZipPostalCode']);
        }

        //Se cuenta el número de elementos que contiene el array
        $num_elementos = count($product_ids);
        //Se envía a la variable persistan para utilizarse en tod0 en controlador
        $this->persistent->num_elementos = $num_elementos;

        //var_dump($product_ids);
        //echo $product_ids[0];

        $ruta = 'orbile_catalog_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        for($i = 0; $i < $num_elementos; $i++)
        {
            //Se asigna el cURL a la variable $ch
            $ch = curl_init();

            //enviamos la url con los parámetros ingresados por el aprtner
            curl_setopt($ch, CURLOPT_URL, "$url->value/books/getBook/rev/$product_ids[$i]");

            //Se asigna el tiempo de espera
            curl_setopt($ch, CURLOPT_TIMEOUT, 80);

            //Afirma que se requiere una respuesta
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);


            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $resultado = curl_exec ($ch);

            $result = json_decode($resultado, true);

            //Id(active revision id), isbn, titulo, autor
            $isbn = $result['isbn'];
            $titulo = $result['activeRevision']['title'];
            $autor = $result['activeRevision']['contibutors']['contributor']['@value'];

            $descuento = $result['activeRevision']['prices']['price']['discount'];
            $precio = BookInfoHelper::getCurrentPrice($result);

            $tax = 0.00;




            $libros[] = array(
                'id_actrev' => $product_ids[$i],
                'isbn'      => $isbn,
                'titulo'    => $titulo,
                'autor'     => $autor,

                'descuento' =>$descuento,
                'precio'    => $precio,
                'tax'       => $tax
            );
        }

        $this->view->setVar('num_elementos', $num_elementos);
        $this->persistent->num_elementos = $num_elementos;
        $this->view->setvar('libros', $libros);

    }

    /*
     * Funcion para realizar la compra de libro,
     * ya sea de un solo libro o masivamente.
     */
    public function purchaseAction()
    {
        //Se inicia el http para obtener los mensajes de respuesta
        $http = new HttpStatusCodeString();

        //Se define el nombre del registro que se buscará en
        //base de datos para mandar llamar a la API
        $ruta = 'orbile_api_base_url';

        //Se busca en la base de datos
        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        //Obtenemos el valor encontrado
        $url = $api->value;

       //Obtenemos los valores de autenticacion de sesión
        $user = $this->session->get('API_AUTH');
        $api_username = $user['api_user'];
        $api_pass = $user['api_key'];

        //se obtienen variables de la vista
        $purchase_id = $this->request->getPost('purchase');
        $kobo_id = $this->request->getPost('user_id');

        //Se guardan en la variable de array los valores de direccion
        //obtenidos del formulario
        $address = array(
        'City' => $this->request->getPost('city'),
        'Country' => $this->request->getPost('country'),
        'StateProvince' => $this->request->getPost('state'),
        'ZipPostalCode' => $this->request->getPost('zip'));

        //Se obtiene el numero de elemento
        $num_elementos = $this->persistent->num_elementos;

        //Se ejecuta el ciclo for para guardar todos los productos que se insertaron,
        //así como sus datos de cada uno
        for($i = 0; $i < $num_elementos; $i++)
        {
            if($this->request->getPost("id$i") == '')
            {
                continue;
            }
            else
            {
                $productos[] = array(
                    'COGS' => array(
                        'Amount' => 0,
                        'CurrencyCode' => $this->request->getPost("currency_code$i")
                    ),
                    'PriceCharged' => array(
                        'CurrencyCode' => $this->request->getPost("currency2_code$i"),
                        'DiscountApplied' => $this->request->getPost("discount_applied$i"),
                        'PriceBeforeTax' => $this->request->getPost("price$i"),
                        'Tax' => array(
                            'Amount' => $this->request->getPost("amount_tax$i"),
                            'Name' => $this->request->getPost("name_tax$i")
                        )
                    ),
                    'ProductId' => $this->request->getPost("id$i"),
                );
            }
        }

        //Se agregan a $purchase_files todos los datos de la compra
        $purchases_files = json_encode(array(
            'PurchaseId' => $purchase_id,
            'user_id' => $kobo_id,
            'Products' => $productos,
            'payment' => array($this->request->getPost('method')),
            'Address' => $address
        ));

       //iniciamos el curl
        $ch = curl_init();


        //pasamos la url
        curl_setopt($ch, CURLOPT_URL, "$url/purchase_service/purchase");

        //TIEMPO DE RESPUESTA
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //LE DECIMOS QUE QUEREMOS RECOGER UNA RESPUESTA
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Especificamos el post
        //curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POST, 1);

        //LE DECIMOS QUE PARAMETROS ENVIAMOS
        curl_setopt($ch, CURLOPT_POSTFIELDS, $purchases_files);

        //Lo que hace la funcion http_build_query es convertir un array(a=>1,b=>2) en una cadena de texto tipo &a=1&b=2

        $headers = array();
        $headers[] = "x-api-user:$api_username";
        $headers[] = "x-api-key:$api_pass";


        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        //RECOGEMOS LA RESPUESTA
        $resultado = curl_exec($ch);

        //CODIGO DE ERROR MAYOR A 400
        $httpcode = curl_getinfo($ch);

        if($httpcode['http_code'] > 400)
        {
            $this->flash->error($httpcode['http_code'].": ".$http->http_status_code_string('400'). "=>" .$resultado);
            curl_close($ch);
            return $this->response->redirect('purchases/verification');
        }
        //$resultado_var=var_export($resultado, true);
        //$this->flash->success($resultado_var);
        else{
            curl_close($ch);

            $json = json_decode($resultado);

            $this->flash->success('<pre>'.json_encode($json, JSON_PRETTY_PRINT). '</pre>');

        }

    }



    /*
     * Función para actualizar los datos de purchasesLine(idioma, genero, genero_bic, genero_bisac, cogs, titulo,
     * isbn, isAdultMaterial, isKids), solo disponible para dministrador, sin vista en el menú.
     */
    public function agrAction()
    {

    }

    public function agregardatosAction()
    {
        set_time_limit(0);
        $headers = array();
        $headers[] = "x-api-user:porrua";
        $headers[] = "x-api-key:$2a$08$2iZRiuBDmK6WrMFbnhyM1.Bq6Mpqj7vWHaOkc6F5OT5F43tguwdem";
        $curl = curl_init();

        $ruta = 'orbile_api_base_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        $post_fields = array(
            'prueba1'   => '1',
            'prueba2'   => '2'
        );
        curl_setopt($curl, CURLOPT_URL, "$url->value/sales/repair_extra_data");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        //Agregamos el header para enviar las credenciales y pueda conectarse

        /*  Solo para cuando la url es POST
                curl_setopt($curl, CURLOPT_POST, 1);

                //LE DECIMOS QUE PARA Solo para cuando la url es POST
                curl_setopt($curl, CURLOPT_POSTFIELDS, $post_fields);
        */
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $error = curl_error($curl);
        $this->flash->success($error);


        $curl_response = curl_exec($curl);

        if($curl_response === false)
        {
            $this->flash->success('Curl error: ' . curl_error($curl));
        }
        else
        {
            $respuesta = json_decode($curl_response);
            $this->flash->success('<pre>'.json_encode($respuesta, JSON_PRETTY_PRINT). '</pre>');
        }
/*datos de 1 solo libro, prueba para la insercion de las compras
        $headers = array();
        $headers[] = "x-api-user:porrua";
        $headers[] = "x-api-key:$2a$08$2iZRiuBDmK6WrMFbnhyM1.Bq6Mpqj7vWHaOkc6F5OT5F43tguwdem";
        $curl = curl_init();

        $ruta = 'orbile_api_base_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        curl_setopt($curl, CURLOPT_URL, "$url->value/sales/data_purchase");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        //Agregamos el header para enviar las credenciales y pueda conectarse

        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $error = curl_error($curl);
        $this->flash->success($error);


        $curl_response = curl_exec($curl);

        if($curl_response === false)
        {
            $this->flash->success('Curl error: ' . curl_error($curl));
        }
        else
        {
            $respuesta = json_decode($curl_response);
            $this->flash->success('<pre>'.json_encode($respuesta, JSON_PRETTY_PRINT). '</pre>');
        }
*/
    }


    /*
     * FORMULARIO DE PRUEBA, AUN NO TERMINADO
     */

    public function pruebaAction()
    {
        $address = array();
        $address['City'] = 'city';
        $address['Country'] = 'country';
        $address['StateProvince'] = 'state';
        $address['ZipPostalCode'] = 'postal';

        $purchases_files = json_encode(array(
            'PurchaseId' => 'VENTA',
            'UserId' => 'USER_ID',
            'Products' => [array(
                'COGS' => array(
                    'Amount' => 'acount',
                    'CurrencyCode' => 'currency'
                ),
                'PriceCharged' => array(
                    'CurrencyCode' => 'code',
                    'DiscountApplied' => 'discount',
                    'PriceBeforeTax' => 'price',
                    'Tax' => array(
                        'Amount' => 'amount_tax',
                        'Name' => 'name_tax'
                    )
                ),
                'ProductId' => 'product',
            )/*,
                array(
                    'COGS' => array(
                        'Amount' => 'acountt',
                        'CurrencyCode' => 'currencyy'
                    ),
                    'PriceCharged' => array(
                        'CurrencyCode' => 'codee',
                        'DiscountApplied' => 'discountt',
                        'PriceBeforeTax' => 'pricee',
                        'Tax' => array(
                            'Amount' => 'amount_taxx',
                            'Name' => 'name_taxx'
                        )
                    ),
                    'ProductId' => 'productt'
                )*/
            ],
            'payment' => 'method',
            'Address' => $address
        ));

        $json= json_decode($purchases_files);
        $this->flash->success('<pre>'.json_encode($json, JSON_PRETTY_PRINT).'</pre>');
    }

}