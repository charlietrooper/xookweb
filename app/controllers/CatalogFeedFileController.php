<?php

use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;

/**
 *
 * Controlador para administración de datos de Catalog Feed File
 *
 * Class CatalogFeedFileController
 */
class CatalogFeedFileController extends ControllerBase
{
    /**
     *
     * Mostrar datos de la tabla calatlogfeedfile
     *
     */

    public function initialize()
    {
        $this->tag->setTitle('Catalog Feed File');
        parent::initialize();
    }

    public function indexAction()
    {
        //Se le asigna a la variable catalogfeed la consulta a la tabla CatalogFeedFile
        //$catalogfeed = CatalogFeedFile::find();
        $catalogFeed = $this->modelsManager->createBuilder()
            ->columns('file_id, file_path, server_path, DATE_FORMAT(start_time, "%d-%m-%Y %H:%i:%s") as start_time, DATE_FORMAT(end_time, "%d-%m-%Y %H:%i:%s") as end_time, num_ebooks, active_ebooks, inactive_ebooks')
            ->from('CatalogFeedFile')
            ->orderBy('file_id DESC');

        $paginator = new PaginatorQueryBuilder(array(
            "builder"   => $catalogFeed,
            "limit"     => 10,
            "page"      => $this->request->getQuery('page', 'int')
        ));

        $this->view->page = $paginator->getPaginate();
    }

    /*
     * Acción para imprimir los resultados de las busquedas en un archivo de Excel
     *
     */
    public function excelAction()
    {
        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "REPORTE_CATALOGFEEDFILE" . "-" . date("Y-m-d_his") . ".xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        //se obtiene la variable 'busq' que contiene los parametros a imprimir
        $busq = $this->session->get('busq');

        //de la varibale busq se extraen los valores
        $id = $busq ['id'];
        $fecha_ini = $busq ['fecha_ini'];
        $fecha_fin = $busq ['fecha_fin'];

        /*
         * Dependiendo de los datos que que vengan en las varibles
         * $id, $fecha_ini y $fecha_fin se va realizar la consulta,
         * la cuál quedará alojada en la variable $catalog
         */

        $campos = 'file_id, file_path, server_path, DATE_FORMAT(start_time, "%d-%m-%Y %H:%i:%s") as start_time, DATE_FORMAT(end_time, "%d-%m-%Y %H:%i:%s") as end_time, num_ebooks, active_ebooks, inactive_ebooks';
        //Si el id esta vacío, se hará la consulta por las Fechas
        if($id=='')
        {
            //Se realiza la consulta
            $catalog = CatalogFeedFile::find(array(
                "columns"   => $campos,
                "CAST(start_time AS DATE) >= :fecha_ini: AND CAST(start_time AS DATE) <= :fecha_fin:",
                'bind' => array('fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin),
                'order' => 'file_id DESC'
            ));
        }
        //Si el id trae información,  y la fecha_ini está vacía, se hará la consulta por id
        elseif($id!='' && $fecha_ini=='')
        {
            //Se realiza la consulta
            $catalog = CatalogFeedFile::find(array(
                "columns"   => $campos,
                "file_id = :id:",
                'bind' => array('id' => $id),
                'order' => 'file_id DESC'
            ));
        }
        //Si el id trae información, al igual que la fecha_ini, se hará la consulta por ambos
        elseif($id!='' && $fecha_ini!='')
        {
            //Se realiza la consulta
            $catalog = CatalogFeedFile::find(array(
                "columns"   => $campos,
                "file_id = :id: AND (CAST(start_time AS DATE) >= :fecha_ini: AND CAST(start_time AS DATE) <= :fecha_fin:)",
                'bind' => array('id' => $id, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin),
                'order' => 'file_id DESC'
            ));
        }

        $header = array(
            'ID'=>'string',
            'FILE PATH'=>'string',
            'SERVER PATH'=>'string',
            'START TIME'=>'string',
            'END TIME'=>'string',
            'NUM EBOOKS'=>'string'
        );

        $data1=[];
        foreach($catalog as $purch)
        {

            $tempArray=[
                $purch->file_id,
                $purch->file_path,
                $purch->server_path,
                $purch->start_time,
                $purch->end_time,
                $purch->num_ebooks,
            ];
            array_push($data1,$tempArray);

        }

        $writer = new XLSXWriter();
        $writer->setAuthor('Orbile');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();
        exit(0);
    }

    /*
     * Búsqueda en la tabla CatalogFeedFile
     *
     * @param $id File Id, $fecha_ini fecha inicial para "start time",
     * $fecha_fin fecha final para "start time"
     */
    public function SearchAction()
    {
		//Se obtienen las variables $id, $fecha_ini y $fecha_fin vía $_GET
        $id = $this->request->get('id', null, '');
        $fecha_ini = $this->request->get('fecha_ini', null, '');
        $fecha_fin = $this->request->get('fecha_fin', null, '');
		//Se define el límite de registros por página
        $rowPerPage = 10;

        //deshabilitamos la vista para las peticiones ajax
        $this->view->disable();

        $campos = 'file_id, file_path, server_path, DATE_FORMAT(start_time, "%d-%m-%Y %H:%i:%s") as start_time, DATE_FORMAT(end_time, "%d-%m-%Y %H:%i:%s") as end_time, num_ebooks, active_ebooks, inactive_ebooks';

        //si es una peticion get y es una peticion ajax
        if ($this->request->isGet() == true) {
            //Búsqueda por fechas
            if ($id == '')
            {
                if($current=$this->request->get('page_ajax'))
                {
                    $offset=($current - 1) * $rowPerPage;
                }
                else
                {
                    $offset=0;
                }
                //Se realiza la consulta en la tabla
                $catalogfeed = $this->modelsManager->createBuilder()
                    ->columns($campos)
                    ->from('CatalogFeedFile')
                    ->where('CAST(CatalogFeedFile.start_time AS DATE) >= :fecha_ini: AND CAST(CatalogFeedFile.start_time AS DATE) <= :fecha_fin:', array('fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin))
                    ->offset($offset)
                    ->orderBy('file_id DESC');

                //Se inserta en la variable busq, los resultados de
				//la búsqueda para usarse en la funcion ImprimeExcel
                $this->session->set('busq', array('fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin));
            }
            //Búsqueda por ID y por Fechas
            if ($id != '' && $fecha_ini != '')
            {
                if($current=$this->modelsManager->createBuilder())
                {
                    $offset=($current - 1) * $rowPerPage;
                }
                else
                {
                    $offset=0;
                }
                $catalogfeed = $this->modelsManager->createBuilder()
                    ->columns($campos)
                    ->from('CatalogFeedFile')
                    ->where('CatalogFeedFile.file_id = :id: AND (CAST(CatalogFeedFile.start_time AS DATE) >= :fecha_ini: AND CAST(CatalogFeedFile.start_time AS DATE) <= :fecha_fin:)', array('id' => $id, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin))
                    ->offset($offset)
                    ->orderBy('file_id DESC');
                //Se inserta en la variable busq, los resultados de la busqueda
                //para usarse en la funcion ImprimeExcel
                $this->session->set('busq', array('id' => $id, 'fecha_ini' => $fecha_ini, 'fecha_fin' => $fecha_fin));
            }
            //Búsqueda por ID
            if ($id != '' && $fecha_ini == '' && $fecha_fin == '')
            {
                if($current=$this->request->get('page_ajax'))
                {
                    $offset=($current - 1) * $rowPerPage;
                }
                else
                {
                    $offset=0;
                }
                //Se realiza la consulta en la tabla 
                $catalogfeed = $this->modelsManager->createBuilder()
                    ->columns($campos)
                    ->from('CatalogFeedFile')
                    ->where('CatalogFeedFile.file_id = :id:', array('id' => $id))
                    ->offset($offset)
                    ->orderBy('file_id DESC');

                //Se inserta en la variable busq, los resultados de la busqueda
                //para usarse en la funcion ImprimeExcel
                $this->session->set('busq', array('id' => $id));



            }

            //se incluye el paginador
            $paginator = new PaginatorQueryBuilder(array(
                "builder" => $catalogfeed,
                "limit" => $rowPerPage,
                "page" => $current
            ));
            $page = $paginator->getPaginate();
            $page->items = $page->items->toArray();
            $this->response->setJsonContent($page);
            $this->response->setContentType('application/json', 'UTF-8');

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }

    }

    /*
     *FUNCION QUE REDIRECCIONA A OTRA PAGINA Y MUESTRA EL
     * XML QUE SE SELECCIONA EN LA PAGINA
     *
     * @param $obj
     */

    public function DescargarXMLAction($obj)
    {
        $key = 'orbile_catalog_url';
        //BUSCAR LA RUTA EN EL SERVIDOR
        $config = AppConfig::findFirst(array(
            'key = :id:',
            'bind' => array('id' => $key)
        ));

        //HACER BUSQUEDA DEL SERVER PATH Y FILE PATH
        $catalog = CatalogFeedFile::findFirst(array(
            'file_id = :id:',
            'bind' => array('id' => $obj)
        ));

        //Hosts: 54.84.81.255
        //User: catalogo
        //Pass: orbile123+
        //puerto: 21

        //$url = "$config->value$catalog->server_path";

        //echo "<script>window.open('$url');</script>";
        header ("Location: $config->value$catalog->server_path");


    }
}