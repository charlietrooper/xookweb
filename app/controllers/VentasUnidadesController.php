<?php

use Phalcon\Mvc\Model\Query as Query;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Paginator\Adapter\Model as PaginatorModel;

class VentasUnidadesController extends ControllerBase
{
    /*
     * Función para título de la pagina  en la pestaña
     */
    public function initialize()
    {
        $this->tag->setTitle('Ventas por unidad');
        parent::initialize();
    }

    /*
     * Función que muestra todos los registros de las ventas por unidades
     */
    public function indexAction()
    {
        //Traemos la session y sacamos el role_id y el partner_id
        $session = $this->session->get('auth');
        $partner_id = $session['partner_id'];
        $se = $session['role_id'];

        $this->view->setVar('se', $se);

        $esteanio = date ("Y");
        $estemes = date("m");

        $queryVentas="SELECT YEAR(p.date) year,
                        MONTHNAME(p.date) month,
                        MONTH (p.date) numonth,
                        DAY(p.date) day,
                        COUNT(*) units,
                        FORMAT(SUM(pl.price_before_tax),2) total_ptax,
                        FORMAT(SUM(pl.cogs_amount),2)  total_cogs
                        FROM PurchasesLine pl
                        JOIN Purchases p on(p.purchase_id = pl.purchase_id)
                        WHERE price_before_tax > 0 AND pl.result ='Success'
                        AND YEAR(p.date) = $esteanio and MONTH(p.date) = $estemes";

        $queryTotal="SELECT YEAR(p.date) year,
                    MONTHNAME(p.date) month,
                    COUNT(*) units,
                    FORMAT(SUM(pl.price_before_tax),2) total_ptax,
                    FORMAT(SUM(pl.cogs_amount),2)  total_cogs
                    FROM PurchasesLine pl
                    JOIN Purchases p on(p.purchase_id = pl.purchase_id)
                    WHERE price_before_tax > 0 AND pl.result ='Success'
                    AND YEAR(p.date) = $esteanio and MONTH(p.date) = $estemes";

        $queryAnio="SELECT YEAR(p.date) year FROM Purchases p";
        if($se== 1 || $se == 4)
        {
            $partners = Partners::find();
        }

        if($se == 2)
        {
            $queryVentas.=' AND p.partner_id = '. $partner_id.'';
            $queryTotal.=' AND p.partner_id = '. $partner_id.'';
            $queryAnio.=' WHERE p.partner_id = '. $partner_id.'';
        }
        $queryVentas.=' GROUP BY YEAR(p.date), MONTH(p.date), DAY(p.date) ORDER BY year DESC, numonth DESC, day DESC';
        $queryTotal.=' GROUP BY YEAR(p.date)';
        $queryAnio.=' GROUP BY YEAR(p.date)';

        $ventas = $this->modelsManager->executeQuery($queryVentas);
        $total = $this->modelsManager->executeQuery($queryTotal);
        $anio = $this->modelsManager->executeQuery($queryAnio);

        foreach($total as $t)
    {
        $t_year = $t->year;
        $t_units = $t->units;
        $t_total_ptax = $t->total_ptax;
        $t_total_cogs = $t->total_cogs;
        $t_month = $t->month;
    }

        $this->view->setVars(array(
            't_year'        => $t_year,
            't_units'       => $t_units,
            't_month'       => $t_month,
            't_total_ptax'  => $t_total_ptax,
            't_total_cogs'  => $t_total_cogs,
            'partners'      => $partners

        ));

        //se obtiene la pagina
        $currentPage = $this->request->getQuery('page', 'int');

        //Se hace la paginación de los resultados de la consulta
        $paginator = new PaginatorModel(
            array(
                "data"  => $ventas,
                "limit" => 31,
                "page"  => $currentPage
            )
        );

        //Se envía el pagunator a la vista para que se imprima
        $this->view->page = $paginator->getPaginate();

        //Se guardan los valores obtenidos de a conuslta en un array
        foreach($anio as $a)
        {
            $anios[] = $a->year;
        }

        //Se envía el array a la  vista
        $this->view->setVar('anios', $anios);
    }

    private function getSearchConditions($search, $searchValue)
    {
        switch($search)
        {
            case ('anio'):
            {
                $condiciones = 'and YEAR(p.date) = '.$searchValue.' ';
                break;
            }
            case ('mes'):
            {
                $condiciones = 'and MONTH(p.date) =  '.$searchValue.' ';
                break;
            }
            case ('partner'):
            {
                $condiciones = 'and p.partner_id = '.$searchValue.' ';
                break;
            }

        }
        return $condiciones;
    }

    /*
     * Función para llevar a cabo la parametrización de años y meses y partner
     */
    public function adminbuscaAction()
    {
        //Traemos la session y sacamos el role_id y el partner_id
        $session = $this->session->get('auth');
        $partner_id = $session['partner_id'];
        $se = $session['role_id'];

        //Se obtienen los datos via get con la peticion ajax
        $anio = $this->request->get('anio', null, '');
        $mes = $this->request->get('mes', null, '');
        $partner = $this->request->get('partner', null, '');
        $ajax_page = $this->request->get('page', null,'');

        //Se deshabilita la vista para las peticiones ajax
        $this->view->disable();

        $currentPage = $ajax_page;

        if($this->request->isGet() == true) {
            $query='SELECT YEAR(p.date) year, MONTHNAME(p.date) month, MONTH (p.date) numonth, DAY(p.date) day,COUNT(*) units,FORMAT(SUM(pl.price_before_tax),2) total_ptax,FORMAT(SUM(pl.cogs_amount),2) total_cogs,ROUND(SUM(pl.price_before_tax),2) r_total_ptax,ROUND(SUM(pl.cogs_amount),2) r_total_cogs FROM PurchasesLine pl JOIN Purchases p on(p.purchase_id = pl.purchase_id) WHERE price_before_tax > 0 and pl.result ="Success" ';
            //Búsqueda para admin
            if($se !== 1 && $se !== 4) {
                if($partner_id != 0)
                {
                    $query.='AND p.partner_id = '. $partner_id.' ';
                }
            }
            $searchParameters = array('anio' => $anio, 'mes' => $mes, 'partner' => $partner);
            foreach ($searchParameters as $search => $search_value) {
                if ($search_value > 0) {
                    $query .= $this->getSearchConditions($search, $search_value);
                }
            }
            $query.='GROUP BY YEAR(p.date), MONTH(p.date), DAY(p.date) ORDER BY year DESC, numonth DESC, day DESC';
            $ventas = $this->modelsManager->executeQuery($query);
            $this->persistent->ventas = $ventas;

            $paginator = new PaginatorModel(
                array(
                    "data" => $ventas,
                    "limit" => 31,
                    "page" => $currentPage
                )
            );

            $page = $paginator->getPaginate();
            $this->response->setJsonContent($page);
            $this->response->setContentType('application/json', 'UTF-8');

            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        }

        else
        {
            $this->response->setStatusCode(404, "Not Found");
            $this->response->send();
        }
    }

    /*
     * Funciónpara imprimier el reporte  en Hoja de calculo Excel
     */

    public function excelAction()
    {

        $ventas = $this->persistent->ventas;
        ini_set('display_errors', 0);
        ini_set('log_errors', 1);
        error_reporting(E_ALL & ~E_NOTICE);

        $filename = "REPORTE_VENTA_POR_UNIDADES" . "-" . date("Y-m-d_his") . ".xlsx";
        header('Content-disposition: attachment; filename="'.XLSXWriter::sanitize_filename($filename).'"');
        header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');

        $header = array(
            'Año'=>'string',
            'Mes'=>'string',
            'Día'=>'string',
            'Unidades'=>'string',
            'Precio antes de Tax'=>'string',
            'Precio final'=>'string'
        );

        $data1=[];
        foreach($ventas as $v)
        {

            $tempArray=[
                $v->year,
                $this->traducirMes($v->month),
                $v->day,
                $v->units,
                $v->total_ptax,
                $v->total_cogs,
            ];
            array_push($data1,$tempArray);

        }

        $writer = new XLSXWriter();
        $writer->setAuthor('Orbile');
        $writer->writeSheet($data1,'Sheet1',$header);
        $writer->writeToStdOut();
        exit(0);
    }

    function traducirMes($mesIngles) //Función para traducir meses
        {
            $mesTraducido='';
            switch($mesIngles)
            {
                case ('January'):$mesTraducido='Enero';
                    break;
                case ('February'):$mesTraducido='Febrero';
                    break;
                case ('March'):$mesTraducido='Marzo';
                    break;
                case ('April'):$mesTraducido='Abril';
                    break;
                case ('May'):$mesTraducido='Mayo';
                    break;
                case ('June'):$mesTraducido='Junio';
                    break;
                case ('July'):$mesTraducido='Julio';
                    break;
                case ('August'):$mesTraducido='Agosto';
                    break;
                case ('September'):$mesTraducido='Septiembre';
                    break;
                case ('October'):$mesTraducido='Octubre';
                    break;
                case ('November'):$mesTraducido='Noviembre';
                    break;
                case ('December'):$mesTraducido='Diciembre';
                    break;
                default:$mesTraducido='';
            }
            return $mesTraducido;
        }

}