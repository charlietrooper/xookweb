<?php
/**
 * Created by PhpStorm.
 * User: carlos-silva
 * Date: 19/05/2015
 * Time: 13:09
 */
class SignupController extends \Phalcon\Mvc\Controller
{
    public function indexAction()
    {

    }

    /**
     * Register action
     */
    public function registerAction()
    {
        $user = new users();

        //Almacenar y vereficar errores de validacion
        $success=$user->save($this->request->getPost(),array('name_user','email_user'));

        if($success){
            echo "Gracias por registrarte!";
        }
        else{
            echo "Lo sentimos, los siguientes errores ocurrieron mientras te dabamos de alta: ";
            foreach($user->getMessages() as $message){
                echo $message->getMessage(), "<br/>";
            }
        }

    }
}