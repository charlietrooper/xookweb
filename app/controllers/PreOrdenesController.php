<?php

use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;

/**
 * Created by PhpStorm.
 * User: Miguel Soto
 * Date: 30/06/2016
 * Time: 10:00 AM
 */

class PreOrdenesController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Listar pre órdenes');
        parent::initialize();
    }

    /*
     * Función para listar las pre-órdenes
     */
    public function indexAction()
    {
        //Se realiza la consulta
        $pre_ordenes = $this->modelsManager->createBuilder()
            ->columns('*')
            ->from('PreOrder')
            ->orderBy('fch_order DESC');

        //Se genera el paginator
        $paginator = new PaginatorQueryBuilder(array(
            "builder" => $pre_ordenes,
            "limit" => 20,
            "page" => $this->request->getQuery('page', 'int')
        ));

        //Se manda a la vista
        $this->view->page = $paginator->getPaginate();
    }

    /*
     * Función para la búsqueda
     */
    public function buscarAction() {
        $pagina = $this->request->get('pagina', null, '');
        $estatus = $this->request->get('estatus', null, '');
        $mail = $this->request->get('mail', null, '');
        $fecha_ini = $this->request->get('fecha_ini', null, '');
        $fecha_fin = $this->request->get('fecha_fin', null, '');

        //Se define el límite de elementos por página
        $elementosPorPagina = 20;

        //Se dshabilita la vista para las peticiones ajax
        $this->view->disable();
        //Si es una peticion get se realizará la consulta correspondiente
        if ($this->request->isGet() == true) {
            if($estatus != 'todos') {
                $condiciones = 'PreOrder.status = :estatus:';
                $param_estatus = array('estatus' => $estatus);
            } else {
                $est = 'prueba';
                $condiciones = 'PreOrder.status != :estatus:';
                $param_estatus = array('estatus' => $est);
            }

            if($mail != '') {
                $condiciones .= " AND PreOrder.email_customer LIKE CONCAT('%', ?0, '%')";
                array_push($param_estatus,$mail);
                //$param_estatus['mail'] = $mail;
            }

            if($fecha_ini != '') {
                $condiciones .= ' AND PreOrder.fch_entrega >= :fecha_ini:';
                $param_estatus['fecha_ini'] = $fecha_ini;
            }

            if($fecha_fin != '') {
                $condiciones .= ' AND CAST(PreOrder.fch_entrega AS DATE) <= :fecha_fin:';
                $param_estatus['fecha_fin'] = $fecha_fin;
            }

            //Se realiza la consulta en la tabla
            $preOrder = $this->modelsManager->createBuilder()
                ->columns('*')
                ->from('PreOrder')
                ->where($condiciones, $param_estatus)
                ->orderBy('fch_order DESC');

            //se incluye el paginador
            $paginator = new PaginatorQueryBuilder(array(
                "builder" => $preOrder,
                "limit" => $elementosPorPagina,
                "page" => $pagina
            ));
            $page = $paginator->getPaginate();

            $page->items = $page->items->toArray();

            $this->response->setJsonContent($page);
            $this->response->setContentType('application/json', 'UTF-8');
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();

        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }
    }
}