<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 09/03/2016
 * Time: 01:53 PM
 */
use Phalcon\Paginator\Adapter\model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Router as Router;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class PrecioActualController extends ControllerBase
{

    public function initialize()
    {
        $this->tag->setTitle('Precio Actual');
        parent::initialize();
    }

    /*
     * Función para mostrar el listado de titulares de cuenta
     */
    public function indexAction()
    {
        //se definen varibales a utilizar
        $pactual = (object)[];
        $page = $this->request->get("page", "int", 1);
        $pactual->page = $page;
        $items_per_page = 20;

        //Se define la url a la que se conectará
        $ruta = 'orbile_catalog_url';
        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;
        //$url="http://catalog.orbiletest.com";
        //se realiza la conexión cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$url/todayprices/list/$items_per_page/$page");
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $resultado = curl_exec ($ch);

        //Se decodifica el Json y se asigna al objeto
        $result = json_decode($resultado, true);
        $pactual->items = $result;

        //se manda llamar la paginación de listados
        $paginacionListas = new PaginacionListas();
        $listadoPactual = $paginacionListas->obtener_paginacion($pactual);

        $this->view->setVar('pactual', $listadoPactual);

    }

    public function  busquedaAction()
    {

        $pactual_busqueda = (object)[];
        $page = intval($this->request->get('pagina', 'int', 1));
        $pactualTitulo = $this->request->get('actualTitulo');
        $pactualISBN= $this->request->get('actualISBN');
        $pActualLista= $this->request->get('pActualLista');
        $pActualVenta= $this->request->get('pActualVenta');
        $busquedaID= $this->request->get('busquedaID');

        $pactual_busqueda->page = $page;
        $items_per_page = 20;
        //Se deshabilita la vista para las peticiones ajax
        $this->view->disable();



        if ($this->request->isGet() == true) {
            //Se define la url a la que se conectará
            $ruta = 'orbile_catalog_url';
            $api = AppConfig::findFirst(array(
                'key = :ruta:',
                'bind' => array('ruta' => $ruta)
            ));
            $url = $api->value;
            //$url="http://catalog.orbiletest.com";
            $cadenaCURL="";



            //BUSQUEDA
            switch($busquedaID){
                case 1://BUSQUEDA POR TITULO
                {
                    $pactual=$pactualTitulo;
                    $pactual = urlencode($pactual);
                    $cadenaCURL="$url"."todayprices/search/title/$pactual/$items_per_page/$page";
                    break;
                }

                case 2://BUSQUEDA POR ISBN
                {
                    $pactual=$pactualISBN;
                    $pactual = urlencode($pactual);
                    $cadenaCURL="$url"."todayprices/search/isbn/$pactual";
                    break;
                }

                case 3://BUSQUEDA POR PRECIO ACTUAL DE LISTA
                {
                    switch($pActualLista)
                    {
                        case 1:
                        {
                            $cadenaCURL="$url"."todayprices/listprice/50orless/$items_per_page/$page";
                            break;
                        }

                        case 2:
                        {
                            $cadenaCURL="$url"."todayprices/listprice/50to100/$items_per_page/$page";

                            break;
                        }

                        case 3:
                        {
                            $cadenaCURL="$url"."todayprices/listprice/100to200/$items_per_page/$page";
                            break;
                        }

                        case 4:
                        {
                            $cadenaCURL="$url"."todayprices/listprice/200to500/$items_per_page/$page";
                            break;
                        }

                        case 5:
                        {
                            $cadenaCURL="$url"."todayprices/listprice/500ormore/$items_per_page/$page";
                            break;
                        }

                        default:
                        {
                            $cadenaCURL='';
                        }
                    }

                    break;
                }

                case 4://BUSQUEDA POR PRECIO ACTUAL DE VENTA
                {
                    switch($pActualVenta)
                    {
                        case 1:
                        {
                            $cadenaCURL="$url"."todayprices/sellingprice/50orless/$items_per_page/$page";
                            break;
                        }

                        case 2:
                        {
                            $cadenaCURL="$url"."todayprices/sellingprice/50to100/$items_per_page/$page";

                            break;
                        }

                        case 3:
                        {
                            $cadenaCURL="$url"."todayprices/sellingprice/100to200/$items_per_page/$page";
                            break;
                        }

                        case 4:
                        {
                            $cadenaCURL="$url"."todayprices/sellingprice/200to500/$items_per_page/$page";
                            break;
                        }

                        case 5:
                        {
                            $cadenaCURL="$url"."todayprices/sellingprice/500ormore/$items_per_page/$page";
                            break;
                        }

                        default:
                        {
                            $cadenaCURL='';
                        }
                    }
                    break;
                }

                default:
                {
                    $cadenaCURL='';
                }
            }

            //se realiza la conexión cURL
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $cadenaCURL);
            curl_setopt($ch, CURLOPT_TIMEOUT, 80);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $resultado = curl_exec ($ch);

            //Se decodifica el Json y se asigna al objeto
            $result = json_decode($resultado, true);
            $pactual_busqueda->items = $result;

            //se manda llamar la paginación de listados
            $paginacionListas = new PaginacionListas();
            $listadoPactualBusqueda = $paginacionListas->obtener_paginacion($pactual_busqueda);

            $this->response->setJsonContent($listadoPactualBusqueda);
            $this->response->setContentType('application/json', 'UTF-8');

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }
    }
}

