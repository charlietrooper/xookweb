<?php
/**
 * Created by PhpStorm.
 * User: Prueba
 * Date: 09/03/2016
 * Time: 01:53 PM
 */

use Phalcon\Paginator\Adapter\model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;
use Phalcon\Mvc\Router as Router;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;

class EditorialesController extends ControllerBase
{
    //ajushduiahuishd
    public function initialize()
    {
        $this->tag->setTitle('Editoriales');
        parent::initialize();
    }

    /*
     * Función para mostrar el listado de editoriales
     */
    public function indexAction()
    {
        //se definen varibales a utilizar
        $editoriales = (object)[];
        $page = $this->request->get("page", "int", 1);
        $editoriales->page = $page;
        $items_per_page = 20;

        //Se define la url a la que se conectará
        $ruta = 'orbile_catalog_url';
        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;

        //se realiza la conexión cURL
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "$url/imprints/list/$items_per_page/$page");
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $resultado = curl_exec ($ch);

        //Se decodifica el Json y se asigna al objeto
        $result = json_decode($resultado, true);
        $editoriales->items = $result;

        //se manda llamar la paginación de listados
        $paginacionListas = new PaginacionListas();
        $listadoEditoriales = $paginacionListas->obtener_paginacion($editoriales);

        $this->view->setVar('editoriales', $listadoEditoriales);
    }

    /*
     * Función para la busqueda de editorial
     */
    public function  busquedaAction()
    {
        $editorial_busqueda = (object)[];
        $page = intval($this->request->get('pagina', 'int', 1));
        $editorial = $this->request->get('editorial');
        $editorial = urlencode($editorial);
        $editorial_busqueda->page = $page;
        $items_per_page = 20;
        //Se dshabilita la vista para las peticiones ajax
        $this->view->disable();

        if ($this->request->isGet() == true) {
            //Se define la url a la que se conectará
            $ruta = 'orbile_catalog_url';
            $api = AppConfig::findFirst(array(
                'key = :ruta:',
                'bind' => array('ruta' => $ruta)
            ));
            $url = $api->value;

            //se realiza la conexión cURL
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, "$url/imprints/search/$editorial/$items_per_page/$page");
            curl_setopt($ch, CURLOPT_TIMEOUT, 80);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $resultado = curl_exec ($ch);

            //Se decodifica el Json y se asigna al objeto
            $result = json_decode($resultado, true);
            $editorial_busqueda->items = $result;

            //se manda llamar la paginación de listados
            $paginacionListas = new PaginacionListas();
            $listadoEditorialBusqueda = $paginacionListas->obtener_paginacion($editorial_busqueda);


            $this->response->setJsonContent($listadoEditorialBusqueda);
            $this->response->setContentType('application/json', 'UTF-8');

            //devolvemos un 200, a ido bien
            $this->response->setStatusCode(200, "Ok");
            $this->response->send();
        } else {
            $this->response->setStatusCode(404, "Not Found");
            $this->reponse->send();
        }
    }
}

