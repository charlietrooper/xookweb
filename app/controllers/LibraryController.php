<?php

class LibraryController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Biblioteca');
        parent::initialize();

    }

    /*
     *
     */
    public function indexAction()
    {
        //Traemos el valor del kobo_id para el library

        $user = $this->session->get('API_AUTH');
        $user_id = $user['kobo_id'];



        $this->view->setvar('kobo', $user_id);

    }

    /*
     *MUESTRA LA LIBRERIA QUE TIENE CADA USUARIO DE ORBILE, LOS DATOS
     * SON ENVIADOS DESDE REPORTS/CUSTOMERS, AL SELECCIONAR UN CUSTOMER
     * DE LA TABLA DE REPORTES
     */
    public function getlibraryAction($obj)
    {

        $numberPage = $this->request->get("page", "int", 1);


        $ruta = 'orbile_api_base_url';

        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;
        $items_per_page = '10';
        $page = $numberPage;

        $user = $this->session->get('API_AUTH');
        $api_username = $user['api_user'];
        $api_pass = $user['api_key'];
        $user_id = $obj;//$user['kobo_id'];
        $this->view->setVar('user_id', $user_id);

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();
        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url/library_service/get_library/$items_per_page/$page/$user_id");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_username\r\nx-api-key:$api_pass"));

        //para apuntar a prod:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //La respuesta de cURL es guardada en la variable $resultado
        $resultado = curl_exec ($ch);
        //$error = curl_error($ch);
        //$this->flash->success($error);
        $result = json_decode($resultado,true);

        $this->view->setvar('json', $result);


        //se crea el paginador que muestra X hojas por página
        $paginator = new OrbilePaginator($result);


//        xdebug_var_dump($paginator->getPaginate());

        //se pasa el objeto a la vista con el nombre de page
        $this->view->page = $paginator->getPaginate();
       // var_dump($paginator->getPaginate());

        $pretty_json = strip_tags(json_encode($result, JSON_PRETTY_PRINT));

        $rest = '<pre>'. $pretty_json .'</pre>';
        //$rest = '<br/><pre style="text-align: left;"><br/><br/><br/>' . json_encode($result, JSON_PRETTY_PRINT) . '<br/><br/><br/></pre><br/>';
        $this->view->setvar('rest', $rest);

    }

    /*
     *MUESTRA LA LIBRERIA QUE TIENE CADA USUARIO, PASANDO EL KOBO_ID
     * DESDE EL FORMULARIO DE LIBRARY, LOS DATOS SON RECIBIDOS
     * A TRAVES DE METODO GET
     */

    public function setlibraryAction()
    {
        $session = $this->session->get('auth');
        $role_id = $session['role_id'];
        //Mandamos el valor del role_id a la vista
        //para aparecer u ocultar campos de busqueda segun sea "admin o partner"

        $this->view->setvar('role', $role_id);

        if($this->request->isGet() == true)
        {
            $result = $this->request->get("user_id");//'23ff3266-1e8c-4598-8e01-1e0899609796'; //$this->request->get('kobo_id');
            $this->view->setVar('user_id', $result);

            $customer = Customers::findFirst(array(
                'kobo_id = :id:',
                'bind' => array('id' => $result)
            ));

            $partner = Partners::findFirst(array(
                'partner_id = :id:',
                'bind' => array('id' => $customer->partner_id)
            ));

            $this->session->set('API_AUTH', array(
                'api_user' => $partner->api_username,
                'api_key' => $partner->api_pass,
                'kobo_id' => $result
            ));
        }

        $numberPage = $this->request->get("page", "int", 1);

        $perPage = $this->request->get("items_per_page", "int", 10);
        $ruta = 'orbile_api_base_url';

        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;

        $user = $this->session->get('API_AUTH');
        $api_username = $user['api_user'];
        $api_pass = $user['api_key'];
        $user_id = $user['kobo_id'];

        $kobo_id = $this->request->getPost('kobo_id');
        $items_per_page = $this->request->getPost('items_per_page');

        //$kobo_id ='b2faff6c-13ab-424a-bad1-931a9be79cb7';
        $items_per_page = $perPage;

        $page = $numberPage;

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();
        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url/library_service/get_library/$items_per_page/$page/$user_id");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_username\r\nx-api-key:$api_pass"));

        //para apuntar a prod:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        //La respuesta de cURL es guardada en la variable $resultado
        $resultado = curl_exec ($ch);

        $result = json_decode($resultado, true);

        //$this->flash->success('<pre>' . json_encode($result, JSON_PRETTY_PRINT) . '</pre>');
        //se crea el paginador que muestra X hojas por página
        $paginator = new OrbilePaginator($result);


//        xdebug_var_dump($paginator->getPaginate());

        //se pasa el objeto a la vista con el nombre de page
        $this->view->page = $paginator->getPaginate();

        $pretty_json = strip_tags(json_encode($result, JSON_PRETTY_PRINT));

        $rest = '<pre>'. $pretty_json .'</pre>';

       // $rest = '<pre>' . json_encode($result, JSON_PRETTY_PRINT) . '</pre>';
        $this->view->setvar('rest', $rest);


    }

    /*
     *
     */

    function queryConteo($resultados)
    {
        $idProd = "'".$resultados."'";
        $queryConteo = $this->modelsManager->executeQuery("SELECT COUNT(*) as veces_vendido
                                                      FROM Purchases p
                                                      INNER JOIN PurchasesLine pl ON p.purchase_id = pl.purchase_id
                                                      WHERE pl.product_id = $idProd
                                                      ");
        return $queryConteo;
    }

    public function GetBookRevAction($obj)
    {
        //Busca la ruta de catalog.orbile
        $ruta = 'orbile_catalog_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();

        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url->value/books/getBook/rev/$obj");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);


        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resultado = curl_exec ($ch);
        //$this->flash->success($resultado);

        //$error = curl_error($ch);
        //echo $error;

        $this->view->setVar('resultado', $resultado);




       // $this->view->setVar('test3', $queryConteo->veces_vendido);

        $result = json_decode($resultado, true);


         $this->view->setVar('test3', $result['activeRevision']['@attributes']['id']);

         $contados=$this->queryConteo($result['activeRevision']['@attributes']['id']);


       // $this->view->setVar('test', $test);
         $this->view->setVar('veces_vendido', $contados[0]['veces_vendido']);


        //$this->flash->success('<pre>' . json_encode($result, JSON_PRETTY_PRINT) . '</pre>');

        if($resultado=='null')
        {
            $this->flash->success('Libro no disponible');
            //return $this->forward('libros/index');
            $this->dispatcher->forward(array(
                "controller" => "library",
                "action" => "getlibrary"
            ));

        }
        else
        {

            $json_noSirve = '<pre>'. json_encode($result, JSON_PRETTY_PRINT). '</pre>';
            $json_bonito = strip_tags($json_noSirve);
            $rest = '<br/><pre style="text-align: left;"><br/><br/><br/>' . json_encode($json_bonito, JSON_PRETTY_PRINT) . '<br/><br/><br/></pre><br/>';
            $book=$result;
            $pa = BookInfoHelper::getCurrentPrice($book);
            $this->view->setvar('rest', $json_bonito);
            $this->view->setvar('book',$result);
            $this->view->setvar('pActual',$pa);
        }

    }

    /*
     *ELIMINAMOS EL LIBRO QUE APARECE EN NUESTRA LISTA
     */

    public function DeleteBookAction($obj)
    {
        //BUSCANDO RUTA PARA LLAMADAS A LA API

        $ruta = 'orbile_api_base_url';

        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;

        $user = $this->session->get('API_AUTH');
        $api_username = $user['api_user'];
        $api_pass = $user['api_key'];
        $user_id = $user['kobo_id'];

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();

        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url/library_service/remove_from_library/$obj/$user_id");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_username\r\nx-api-key:$api_pass"));

        //para apuntar a prod:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resultado = curl_exec ($ch);

//      $error = curl_error($ch);
//      $this->flash->success($error);

        $result = json_decode($resultado);

        //$this->flash->success('<pre>' . json_encode($result, JSON_PRETTY_PRINT) . '</pre>');
        if($result == 'Success')
        {
            $this->flash->success("El libro a sido removido con exito");
            $this->forward("library/getlibrary/$user_id");
        }
        else{
            $this->flash->error("El libro no a sido removido");
        }

        $this->view->setvar('book',$result);

    }

    /*
     *FUNCION PARA DESCARGAR LIBRO SELECCIONADO
     *
     * @param product_id
     * @param user_id
     */

    public function DownloadBookAction($obj)
    {
        //BUSCANDO RUTA PARA LLAMADAS A LA API

        $ruta = 'orbile_api_base_url';

        $api = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));
        $url = $api->value;

        $user = $this->session->get('API_AUTH');
        $api_username = $user['api_user'];
        $api_pass = $user['api_key'];
        $user_id = $user['kobo_id'];

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();

        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url/library_service/get_download_urls/$obj/$user_id");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        //Agregamos el header para enviar las credenciales y pueda conectarse
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('header' => "x-api-user: $api_username\r\nx-api-key:$api_pass"));

        //para apuntar a prod:
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resultado = curl_exec ($ch);

        $result = json_decode($resultado);

        $url = $result->DownloadUrls->DownloadUrl->Url;

        header ("Location: $url");
    }
}