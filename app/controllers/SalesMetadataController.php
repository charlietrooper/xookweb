<?php

use Phalcon\Paginator\Adapter\Model as Paginator;
use Phalcon\Paginator\Adapter\QueryBuilder as PaginatorQueryBuilder;

class SalesMetadataController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Sales Metadata Update');
        parent::initialize();
    }

    /*
     *
     */
    public function indexAction()
    {
        $api_auth = $this->session->get('API_AUTH');

        $k = $api_auth['kobo_id'];
        $r = $api_auth['result'];

        $this->view->setvar('k', $k);
    }

    public function updateMetadata()
    {
        set_time_limit(0);
        $headers = array();
        $headers[] = 'x-api-user:orbile';
        $headers[] = 'x-api-key:$2a$08$AXVOnD9Q3f56TyG7ujCra.kn6J4FqRlJWW9HGcI8Hn6v/8eX5g3W2';
        $curl = curl_init();

        $ruta = 'orbile_api_base_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        $post_fields = array(
            'prueba1'   => '1',
            'prueba2'   => '2'
        );
        curl_setopt($curl, CURLOPT_URL, "$url->value/sales/updateMetadata");
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $error = curl_error($curl);
        $this->flash->success($error);


        $curl_response = curl_exec($curl);

        if($curl_response === false)
        {
            $this->flash->success('Curl error: ' . curl_error($curl));
        }
        else
        {
            $respuesta = json_decode($curl_response);
            $this->flash->success('<pre>'.json_encode($respuesta, JSON_PRETTY_PRINT). '</pre>');
        }
    }
}
