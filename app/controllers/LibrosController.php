<?php

use Phalcon\Mvc\Router as Router;
use Phalcon\Paginator\Adapter\NativeArray as PaginatorArray;


class LibrosController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Detalle libros');
        parent::initialize();
    }

    /*
     *
     */
    public function indexAction()
    {

    }

    /**
     *
     *
     */

    public function enviarlibroAction()
    {
        $isbn = $this->request->get('isbn');
        $id_libro = $this->request->get('id_libro');
        if($isbn != '')
        {
            $this->dispatcher->forward(array(
                "action" => "searchbookisbn",
                "params" => array($isbn)
            ));
        }

        if($id_libro != '')
        {
            $this->dispatcher->forward(array(
                "controller" => "library",
                "action" => "getbookrev",
                "params" => array($id_libro)
            ));
        }
    }

    /**
     *
     *
     */

    public function SearchBookIsbnAction($obj)
    {
        if($obj == '')
        {
            $isbn = $this->request->getPost('isbn');
        }
        else
        {
            $isbn = $obj;
        }


        $ruta = 'orbile_catalog_url';
        $url = AppConfig::findFirst(array(
            'key = :ruta:',
            'bind' => array('ruta' => $ruta)
        ));

        //Se asigna el cURL a la variable $ch
        $ch = curl_init();

        //enviamos la url con los parámetros ingresados por el aprtner
        curl_setopt($ch, CURLOPT_URL, "$url->value/books/getBook/isbn/$isbn");

        //Se asigna el tiempo de espera
        curl_setopt($ch, CURLOPT_TIMEOUT, 80);

        //Afirma que se requiere una respuesta
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);


        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $resultado_isbn = curl_exec ($ch);

        $this->view->setVar('resultado', $resultado_isbn);
        $result = json_decode($resultado_isbn,true);
        $json = $result['Items']['Item'];
        $this->view->setvar('json', $json);

        $paginator = new PaginatorArray(
            array(
                "data" => $json,
                "limit" => 10,
                "page" => $this->request->getQuery('page','int')
            )
        );

        $this->view->page = $paginator->getPaginate();
    }

    /*
     * Función para búsqueda masiva de ISBNs
     */
    public function buscamasivaisbnAction()
    {
        $product_ids = explode(",", $this->request->getPost('product_id'));
    }

}

