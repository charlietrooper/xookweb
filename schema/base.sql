/*----
base de datos
----*/

CREATE TABLE partners(
partner_id int not null PRIMARY KEY,
name VARCHAR (20),
email_support VARCHAR (20),
email_notification VARCHAR (20),
api_username VARCHAR (20),
api_pass VARCHAR (65)
);

INSERT INTO partners VALUES (26,'pedro','pedro@mx.com','pedro@mx.com','pedro','$08$JiqdkYEJHy4QMYPHqClJquOuuEFg4wXCImYNxfjU4RvN5q.WpYciW');
INSERT INTO partners VALUES (27,'jose','jose@mx.com','jose@mx.com','jose','$08$UKryexUPJUqtCPsVq0X4WOXwZzoFftrp3ENZvQqmvqrKBHt211aPm');
/*con esta tabla se loguean los usuarios ya sean GHANDI, PORRUA o ADMIN,
  estos campos los agregamos nosotros directamente de la base de datos
*/


CREATE TABLE roles(
role_id int not null PRIMARY KEY,
role_name VARCHAR (20)
);


insert INTO roles VALUES (1,'admin');
INSERT INTO roles VALUES (2,'partner');


CREATE TABLE users(
user_id int not null primary key,
username VARCHAR (20) not null,
pass VARCHAR (65) not null,
name VARCHAR (120) not null,
email VARCHAR (70) not null,
create_at datetime not null,
active char(1) not null,
role_id int not NULL ,
FOREIGN KEY (role_id) REFERENCES roles(role_id)
);

INSERT INTO users VALUES (1,'admin','4dm1n','admin','admin@mx.com',0000-00-00,'s',1);
INSERT INTO users VALUES (2,'ricardo','r1c4rd0','ricardo','ricardo@mx.com',0000-00-00,'s',2);


/*
ES LA TABLA PARA LOS CLIENTES, BUENO PARA SABER QUE CLIENTES PERTENECEN A QUE LIBRERIA
CREO QUE KOBO PUEDE REGRESAR MAS CAMPOS COMO DIRECCIONES Y TELEFONOS, PERO NO SE QUE SEA LO
QUE LES INTERESE A LOS PARTNERS SABER DE LOS CLIENTES O QUE QUIERAN VER EN EL REPORTE, ASI QUE SOLO METI
ASTA EL MOMENTO ESOS CAMPOS
 */
CREATE TABLE customers(
customer_id int NOT NULL PRIMARY KEY ,
partner_id int not null,
kobo_id varchar(32),
email varchar(30) not null,
name varchar(60) not null,
registreDate datetime not null
);

/*
ES LA TABLA PARA GUARDAR LAS ACTUALIZACIONES DEL CATALOGO,
EN EL CAMPO DE DESCRIPCION LO PUSE POR SI SE PUEDE LLENAR CON ALGO, ALGUN DATO O NOMBRE DEL CATALOGO
 */
CREATE TABLE catalogue_updates(
catalogue_id int not null primary key,
description VARCHAR (80) not null,
date_update datetime
);

/*
BUENO ME DIJISTE QUE SE GUARDARA A LOS DATOS DE CUANDO SE HIZO LA NOTIFICACION, A QUIEN SE LE HIZO Y DONDE,
EL DONDE LO ENTENDI COMO EL PARTNER_ID
 */
CREATE TABLE notification_catalogues(
notification_id int not null primary key,
date_notification datetime,
partner_id int,
user_email varchar (70)
);