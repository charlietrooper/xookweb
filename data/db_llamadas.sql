CREATE TABLE PARTNER(
	partner_id serial PRIMARY KEY,
	parner_name varchar(20),
	partner_email_support varchar(20),
	partner_email_notification varchar(20)
);

CREATE TABLE AUTHENTICATE(
	id serial primary key,
	partner_id int,
	user_email varchar(20),
	result varchar(20)
);

CREATE TABLE PURCHASE(
	id serial primary key,
	partner_id int,
	user_email varchar(20),
	product varchar(50),
	purchased_id BINARY(16),
	items varchar(50),
	sale_successfull boolean,
	result varchar(20)
);

CREATE TABLE api_log(
	id serial primary key,
	call_name varchar(20),
	descrip varchar(50),
	date TIMESTAMP
);