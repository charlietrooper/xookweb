-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: orbile-production.cnizc1bg1mgv.us-east-1.rds.amazonaws.com    Database: orbile_prod
-- ------------------------------------------------------
-- Server version	5.6.23-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary view structure for view `changestatusbook`
--

DROP TABLE IF EXISTS `changestatusbook`;
/*!50001 DROP VIEW IF EXISTS `changestatusbook`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `changestatusbook` AS SELECT 
 1 AS `line_id`,
 1 AS `purchase_id`,
 1 AS `purchase_line_id`,
 1 AS `user_id`,
 1 AS `product_id`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `customers_anio`
--

DROP TABLE IF EXISTS `customers_anio`;
/*!50001 DROP VIEW IF EXISTS `customers_anio`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `customers_anio` AS SELECT 
 1 AS `partner_id`,
 1 AS `mes`,
 1 AS `anio`,
 1 AS `num_customers`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `customers_dia`
--

DROP TABLE IF EXISTS `customers_dia`;
/*!50001 DROP VIEW IF EXISTS `customers_dia`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `customers_dia` AS SELECT 
 1 AS `partner_id`,
 1 AS `hora`,
 1 AS `num_customers`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `customers_mes`
--

DROP TABLE IF EXISTS `customers_mes`;
/*!50001 DROP VIEW IF EXISTS `customers_mes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `customers_mes` AS SELECT 
 1 AS `partner_id`,
 1 AS `dia`,
 1 AS `mes`,
 1 AS `anio`,
 1 AS `num_customers`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `customers_semana`
--

DROP TABLE IF EXISTS `customers_semana`;
/*!50001 DROP VIEW IF EXISTS `customers_semana`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `customers_semana` AS SELECT 
 1 AS `partner_id`,
 1 AS `dia_sem`,
 1 AS `num_customers`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `dinerosAnioTotal`
--

DROP TABLE IF EXISTS `dinerosAnioTotal`;
/*!50001 DROP VIEW IF EXISTS `dinerosAnioTotal`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `dinerosAnioTotal` AS SELECT 
 1 AS `cantidad_dinero`,
 1 AS `numero_mes`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `dinerosTotales`
--

DROP TABLE IF EXISTS `dinerosTotales`;
/*!50001 DROP VIEW IF EXISTS `dinerosTotales`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `dinerosTotales` AS SELECT 
 1 AS `mes`,
 1 AS `dineros`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `dineros_anio`
--

DROP TABLE IF EXISTS `dineros_anio`;
/*!50001 DROP VIEW IF EXISTS `dineros_anio`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `dineros_anio` AS SELECT 
 1 AS `partner_id`,
 1 AS `mes`,
 1 AS `anio`,
 1 AS `dineros`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `dineros_dia`
--

DROP TABLE IF EXISTS `dineros_dia`;
/*!50001 DROP VIEW IF EXISTS `dineros_dia`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `dineros_dia` AS SELECT 
 1 AS `partner_id`,
 1 AS `hora`,
 1 AS `dineros`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `dineros_mes`
--

DROP TABLE IF EXISTS `dineros_mes`;
/*!50001 DROP VIEW IF EXISTS `dineros_mes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `dineros_mes` AS SELECT 
 1 AS `partner_id`,
 1 AS `dia`,
 1 AS `mes`,
 1 AS `anio`,
 1 AS `dineros`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `dineros_sem`
--

DROP TABLE IF EXISTS `dineros_sem`;
/*!50001 DROP VIEW IF EXISTS `dineros_sem`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `dineros_sem` AS SELECT 
 1 AS `partner_id`,
 1 AS `dia_sem`,
 1 AS `dineros`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `estasem`
--

DROP TABLE IF EXISTS `estasem`;
/*!50001 DROP VIEW IF EXISTS `estasem`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `estasem` AS SELECT 
 1 AS `partner_id`,
 1 AS `dia_semana`,
 1 AS `num_ventas`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `esteanio`
--

DROP TABLE IF EXISTS `esteanio`;
/*!50001 DROP VIEW IF EXISTS `esteanio`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `esteanio` AS SELECT 
 1 AS `partner_id`,
 1 AS `mes`,
 1 AS `anio`,
 1 AS `num_ventas`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `estedia`
--

DROP TABLE IF EXISTS `estedia`;
/*!50001 DROP VIEW IF EXISTS `estedia`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `estedia` AS SELECT 
 1 AS `partner_id`,
 1 AS `hora`,
 1 AS `num_ventas`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `estemes`
--

DROP TABLE IF EXISTS `estemes`;
/*!50001 DROP VIEW IF EXISTS `estemes`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `estemes` AS SELECT 
 1 AS `partner_id`,
 1 AS `dia`,
 1 AS `mes`,
 1 AS `anio`,
 1 AS `num_ventas`*/;
SET character_set_client = @saved_cs_client;

--
-- Temporary view structure for view `view_purchases_line`
--

DROP TABLE IF EXISTS `view_purchases_line`;
/*!50001 DROP VIEW IF EXISTS `view_purchases_line`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `view_purchases_line` AS SELECT 
 1 AS `line_id`,
 1 AS `purchase`,
 1 AS `partner_id`,
 1 AS `purchase_partner_id`,
 1 AS `date`,
 1 AS `result`,
 1 AS `product_id`,
 1 AS `precio`,
 1 AS `cogs_currency_code`,
 1 AS `price_currency_code`,
 1 AS `price_discount_applied`,
 1 AS `price_before_tax`,
 1 AS `download_url`,
 1 AS `content_format`,
 1 AS `idioma`,
 1 AS `genero`,
 1 AS `is_kids`,
 1 AS `is_adult_material`,
 1 AS `cogs`,
 1 AS `titulo`,
 1 AS `isbn`,
 1 AS `genero_bic`,
 1 AS `genero_bisac`*/;
SET character_set_client = @saved_cs_client;

--
-- Dumping events for database 'orbile_prod'
--

--
-- Dumping routines for database 'orbile_prod'
--

--
-- Final view structure for view `changestatusbook`
--

/*!50001 DROP VIEW IF EXISTS `changestatusbook`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `changestatusbook` AS select `pl`.`line_id` AS `line_id`,`p`.`purchase_id` AS `purchase_id`,`pl`.`purchase_id` AS `purchase_line_id`,`p`.`user_id` AS `user_id`,`pl`.`product_id` AS `product_id` from (`purchases` `p` join `purchases_line` `pl` on((`p`.`purchase_id` = `pl`.`purchase_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `customers_anio`
--

/*!50001 DROP VIEW IF EXISTS `customers_anio`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `customers_anio` AS select `customers`.`partner_id` AS `partner_id`,month(`customers`.`register_date`) AS `mes`,year(`customers`.`register_date`) AS `anio`,count(0) AS `num_customers` from `customers` group by `mes`,`customers`.`partner_id`,`anio` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `customers_dia`
--

/*!50001 DROP VIEW IF EXISTS `customers_dia`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `customers_dia` AS select `customers`.`partner_id` AS `partner_id`,hour(`customers`.`register_date`) AS `hora`,count(0) AS `num_customers` from `customers` where ((year(`customers`.`register_date`) = (select year(now()))) and (month(`customers`.`register_date`) = (select month(now()))) and (dayofmonth(`customers`.`register_date`) = (select dayofmonth(now())))) group by `hora`,`customers`.`partner_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `customers_mes`
--

/*!50001 DROP VIEW IF EXISTS `customers_mes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `customers_mes` AS select `customers`.`partner_id` AS `partner_id`,dayofmonth(`customers`.`register_date`) AS `dia`,month(`customers`.`register_date`) AS `mes`,year(`customers`.`register_date`) AS `anio`,count(0) AS `num_customers` from `customers` group by `dia`,`customers`.`partner_id`,`mes`,`anio` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `customers_semana`
--

/*!50001 DROP VIEW IF EXISTS `customers_semana`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `customers_semana` AS select `customers`.`partner_id` AS `partner_id`,dayofweek(`customers`.`register_date`) AS `dia_sem`,count(0) AS `num_customers` from `customers` where ((year(`customers`.`register_date`) = (select year(now()))) and (week(`customers`.`register_date`,0) = (select week(now(),0)))) group by `dia_sem`,`customers`.`partner_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `dinerosAnioTotal`
--

/*!50001 DROP VIEW IF EXISTS `dinerosAnioTotal`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `dinerosAnioTotal` AS select distinct ifnull(`s`.`dineros`,0) AS `cantidad_dinero`,`a`.`numero_mes` AS `numero_mes` from (`anio` `a` left join `dinerosTotales` `s` on((`a`.`numero_mes` = `s`.`mes`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `dinerosTotales`
--

/*!50001 DROP VIEW IF EXISTS `dinerosTotales`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `dinerosTotales` AS select month(`p`.`date`) AS `mes`,(sum(`pl`.`cogs_amount`) / 1000) AS `dineros` from (`purchases` `p` join `purchases_line` `pl` on((`pl`.`purchase_id` = `p`.`purchase_id`))) where (year(`p`.`date`) = (select year(now()))) AND pl.result = "Success" AND (`pl`.`price_before_tax`) > 0 group by month(`p`.`date`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `dineros_anio`
--

/*!50001 DROP VIEW IF EXISTS `dineros_anio`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `dineros_anio` AS select `p`.`partner_id` AS `partner_id`,month(`p`.`date`) AS `mes`,year(`p`.`date`) AS `anio`,(sum(`pl`.`cogs_amount`) / 1000) AS `dineros` from (`purchases` `p` join `purchases_line` `pl` on((`pl`.`purchase_id` = `p`.`purchase_id`))) WHERE pl.result = "Success" AND (`pl`.`price_before_tax`) > 0 group by month(`p`.`date`),`p`.`partner_id`,year(`p`.`date`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `dineros_dia`
--

/*!50001 DROP VIEW IF EXISTS `dineros_dia`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `dineros_dia` AS select `p`.`partner_id` AS `partner_id`,hour(`p`.`date`) AS `hora`,(sum(`pl`.`cogs_amount`) / 1000) AS `dineros` from (`purchases` `p` join `purchases_line` `pl` on((`pl`.`purchase_id` = `p`.`purchase_id`))) where ((year(`p`.`date`) = (select year(now()))) and (month(`p`.`date`) = (select month(now()))) and (dayofmonth(`p`.`date`) = (select dayofmonth(now())))) AND pl.result = "Success" AND (`pl`.`price_before_tax`) > 0 group by hour(`p`.`date`),`p`.`partner_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `dineros_mes`
--

/*!50001 DROP VIEW IF EXISTS `dineros_mes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `dineros_mes` AS select `p`.`partner_id` AS `partner_id`,dayofmonth(`p`.`date`) AS `dia`,month(`p`.`date`) AS `mes`,year(`p`.`date`) AS `anio`,(sum(`pl`.`cogs_amount`) / 1000) AS `dineros` from (`purchases` `p` join `purchases_line` `pl` on((`pl`.`purchase_id` = `p`.`purchase_id`))) WHERE pl.result = "Success" AND (`pl`.`price_before_tax`) > 0 group by dayofmonth(`p`.`date`),`p`.`partner_id`,month(`p`.`date`),year(`p`.`date`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `dineros_sem`
--

/*!50001 DROP VIEW IF EXISTS `dineros_sem`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `dineros_sem` AS select `p`.`partner_id` AS `partner_id`,dayofweek(`p`.`date`) AS `dia_sem`,(sum(`pl`.`cogs_amount`) / 1000) AS `dineros` from (`purchases` `p` join `purchases_line` `pl` on((`pl`.`purchase_id` = `p`.`purchase_id`))) where ((year(`p`.`date`) = (select year(now()))) and (week(`p`.`date`,0) = (select week(now(),0)))) AND pl.result = "Success" AND (`pl`.`price_before_tax`) > 0 group by dayofweek(`p`.`date`),`p`.`partner_id` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estasem`
--

/*!50001 DROP VIEW IF EXISTS `estasem`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `estasem` AS select `purchases`.`partner_id` AS `partner_id`,dayofweek(`purchases`.`date`) AS `dia_semana`,count(0) AS `num_ventas` from `purchases` where ((year(`purchases`.`date`) = (select year(now()))) and (week(`purchases`.`date`,0) = (select week(now(),0)))) group by `purchases`.`partner_id`,dayofweek(`purchases`.`date`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `esteanio`
--

/*!50001 DROP VIEW IF EXISTS `esteanio`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `esteanio` AS select `purchases`.`partner_id` AS `partner_id`,month(`purchases`.`date`) AS `mes`,year(`purchases`.`date`) AS `anio`,count(0) AS `num_ventas` from `purchases` group by month(`purchases`.`date`),`purchases`.`partner_id`,year(`purchases`.`date`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estedia`
--

/*!50001 DROP VIEW IF EXISTS `estedia`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `estedia` AS select `purchases`.`partner_id` AS `partner_id`,hour(`purchases`.`date`) AS `hora`,count(0) AS `num_ventas` from `purchases` where ((year(`purchases`.`date`) = (select year(now()))) and (dayofmonth(`purchases`.`date`) = (select dayofmonth(now()))) and (month(`purchases`.`date`) = (select month(now())))) group by `purchases`.`partner_id`,hour(`purchases`.`date`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `estemes`
--

/*!50001 DROP VIEW IF EXISTS `estemes`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `estemes` AS select `purchases`.`partner_id` AS `partner_id`,dayofmonth(`purchases`.`date`) AS `dia`,month(`purchases`.`date`) AS `mes`,year(`purchases`.`date`) AS `anio`,count(0) AS `num_ventas` from `purchases` group by `purchases`.`partner_id`,dayofmonth(`purchases`.`date`),month(`purchases`.`date`),year(`purchases`.`date`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `view_purchases_line`
--

/*!50001 DROP VIEW IF EXISTS `view_purchases_line`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`orbile`@`%` SQL SECURITY DEFINER */
/*!50001 VIEW `view_purchases_line` AS select `pl`.`line_id` AS `line_id`,`p`.`purchase_id` AS `purchase`,`p`.`partner_id` AS `partner_id`,`p`.`purchase_partner_id` AS `purchase_partner_id`,`p`.`date` AS `date`,`pl`.`result` AS `result`,`pl`.`product_id` AS `product_id`,(case `pl`.`price_before_tax` when 0 then 'GRATUITO' else `pl`.`price_before_tax` end) AS `precio`,`pl`.`cogs_currency_code` AS `cogs_currency_code`,`pl`.`price_currency_code` AS `price_currency_code`,`pl`.`price_discount_applied` AS `price_discount_applied`,`pl`.`price_before_tax` AS `price_before_tax`,`pl`.`download_url` AS `download_url`,`pl`.`content_format` AS `content_format`,`pl`.`idioma` AS `idioma`,`pl`.`genero` AS `genero`,`pl`.`is_kids` AS `is_kids`,`pl`.`is_adult_material` AS `is_adult_material`,`pl`.`cogs` AS `cogs`,`pl`.`titulo` AS `titulo`,`pl`.`isbn` AS `isbn`,`pl`.`genero_bic` AS `genero_bic`,`pl`.`genero_bisac` AS `genero_bisac` from (`purchases_line` `pl` join `purchases` `p` on((`pl`.`purchase_id` = `p`.`purchase_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-10-17 20:50:39
