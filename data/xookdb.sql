/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50624
Source Host           : localhost:3306
Source Database       : xookdb

Target Server Type    : MYSQL
Target Server Version : 50624
File Encoding         : 65001

Date: 2015-09-23 17:07:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for affiliates
-- ----------------------------
DROP TABLE IF EXISTS `affiliates`;
CREATE TABLE `affiliates` (
  `affiliate_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(30) DEFAULT NULL,
  `big_logo` varchar(30) DEFAULT NULL,
  `small_logo` varchar(30) DEFAULT NULL,
  `domain_url` varchar(30) DEFAULT NULL,
  `layout` varchar(30) DEFAULT NULL,
  `configuration` varchar(80) DEFAULT NULL,
  PRIMARY KEY (`affiliate_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of affiliates
-- ----------------------------
INSERT INTO `affiliates` VALUES ('4', 'Filial 2', 'filial2@gmail.com', 'C:\\docs\\img\\logos\\logo2-1.jpg', 'C:\\docs\\img\\logos\\logo2-2.jpg', 'http:\\\\filial_2.com', 'layout_filial2', 'configuracion_filial2');

-- ----------------------------
-- Table structure for anio
-- ----------------------------
DROP TABLE IF EXISTS `anio`;
CREATE TABLE `anio` (
  `numero_mes` int(11) NOT NULL AUTO_INCREMENT,
  `mes` varchar(10) NOT NULL,
  PRIMARY KEY (`numero_mes`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of anio
-- ----------------------------
INSERT INTO `anio` VALUES ('1', 'Enero');
INSERT INTO `anio` VALUES ('2', 'Febrero');
INSERT INTO `anio` VALUES ('3', 'Marzo');
INSERT INTO `anio` VALUES ('4', 'Abril');
INSERT INTO `anio` VALUES ('5', 'Mayo');
INSERT INTO `anio` VALUES ('6', 'Junio');
INSERT INTO `anio` VALUES ('7', 'Julio');
INSERT INTO `anio` VALUES ('8', 'Agosto');
INSERT INTO `anio` VALUES ('9', 'Septiembre');
INSERT INTO `anio` VALUES ('10', 'Octubre');
INSERT INTO `anio` VALUES ('11', 'Noviembre');
INSERT INTO `anio` VALUES ('12', 'Diciembre');

-- ----------------------------
-- Table structure for api_log
-- ----------------------------
DROP TABLE IF EXISTS `api_log`;
CREATE TABLE `api_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `call_me` varchar(20) DEFAULT NULL,
  `descrip` varchar(50) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of api_log
-- ----------------------------

-- ----------------------------
-- Table structure for app_config
-- ----------------------------
DROP TABLE IF EXISTS `app_config`;
CREATE TABLE `app_config` (
  `id_url` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(120) NOT NULL,
  `value` varchar(120) NOT NULL,
  PRIMARY KEY (`id_url`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of app_config
-- ----------------------------
INSERT INTO `app_config` VALUES ('1', 'orbile_api_base_url', 'http://api.orbiletest.com/');
INSERT INTO `app_config` VALUES ('2', 'orbile_catalog_url', 'http://catalog.orbiletest.com/');

-- ----------------------------
-- Table structure for authenticate
-- ----------------------------
DROP TABLE IF EXISTS `authenticate`;
CREATE TABLE `authenticate` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) DEFAULT NULL,
  `user_email` varchar(20) DEFAULT NULL,
  `result` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of authenticate
-- ----------------------------

-- ----------------------------
-- Table structure for catalogueupdates
-- ----------------------------
DROP TABLE IF EXISTS `catalogueupdates`;
CREATE TABLE `catalogueupdates` (
  `catalogue_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(70) DEFAULT NULL,
  `date_update` date DEFAULT NULL,
  PRIMARY KEY (`catalogue_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of catalogueupdates
-- ----------------------------
INSERT INTO `catalogueupdates` VALUES ('1', 'primer entregable', '2015-06-04');
INSERT INTO `catalogueupdates` VALUES ('2', 'segundi entregable', '2015-06-05');
INSERT INTO `catalogueupdates` VALUES ('3', 'tercer entregable', '2015-06-06');

-- ----------------------------
-- Table structure for catalog_feed_file
-- ----------------------------
DROP TABLE IF EXISTS `catalog_feed_file`;
CREATE TABLE `catalog_feed_file` (
  `file_id` int(11) NOT NULL AUTO_INCREMENT,
  `file_path` varchar(50) NOT NULL,
  `server_path` varchar(50) NOT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `num_ebooks` varchar(25) DEFAULT NULL,
  `active_ebooks` varchar(20) DEFAULT NULL,
  `inactive_ebooks` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=411 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of catalog_feed_file
-- ----------------------------
INSERT INTO `catalog_feed_file` VALUES ('1', '20150624_Orbile_Catalog.xml', 'data/pending/20150624_Orbile_Catalog.xml', '2015-07-30 18:24:55', '2015-07-30 19:33:03', '1746416', null, null);
INSERT INTO `catalog_feed_file` VALUES ('2', '20150624_1730_Orbile_Daily.xml', 'data/pending/20150624_1730_Orbile_Daily.xml', '2015-07-30 19:37:12', '2015-07-30 19:37:17', '2213', null, null);
INSERT INTO `catalog_feed_file` VALUES ('3', '20150624_2130_Orbile_Daily.xml', 'data/pending/20150624_2130_Orbile_Daily.xml', '2015-07-30 19:37:17', '2015-07-30 19:37:21', '1615', null, null);
INSERT INTO `catalog_feed_file` VALUES ('4', '20150625_0130_Orbile_Daily.xml', 'data/pending/20150625_0130_Orbile_Daily.xml', '2015-07-30 19:37:21', '2015-07-30 19:37:22', '789', null, null);
INSERT INTO `catalog_feed_file` VALUES ('5', '20150625_0530_Orbile_Daily.xml', 'data/pending/20150625_0530_Orbile_Daily.xml', '2015-07-30 19:37:22', '2015-07-30 19:37:57', '12195', null, null);
INSERT INTO `catalog_feed_file` VALUES ('6', '20150625_0930_Orbile_Daily.xml', 'data/pending/20150625_0930_Orbile_Daily.xml', '2015-07-30 19:37:57', '2015-07-30 19:39:34', '33850', null, null);
INSERT INTO `catalog_feed_file` VALUES ('7', '20150625_1330_Orbile_Daily.xml', 'data/pending/20150625_1330_Orbile_Daily.xml', '2015-07-30 19:39:34', '2015-07-30 19:39:45', '4675', null, null);
INSERT INTO `catalog_feed_file` VALUES ('8', '20150625_1730_Orbile_Daily.xml', 'data/pending/20150625_1730_Orbile_Daily.xml', '2015-07-30 19:39:45', '2015-07-30 19:39:50', '2140', null, null);
INSERT INTO `catalog_feed_file` VALUES ('9', '20150625_2130_Orbile_Daily.xml', 'data/pending/20150625_2130_Orbile_Daily.xml', '2015-07-30 19:39:50', '2015-07-30 19:39:56', '2775', null, null);
INSERT INTO `catalog_feed_file` VALUES ('10', '20150626_0130_Orbile_Daily.xml', 'data/pending/20150626_0130_Orbile_Daily.xml', '2015-07-30 19:39:56', '2015-07-30 19:39:59', '1073', null, null);
INSERT INTO `catalog_feed_file` VALUES ('11', '20150626_0530_Orbile_Daily.xml', 'data/pending/20150626_0530_Orbile_Daily.xml', '2015-07-30 19:39:59', '2015-07-30 19:40:08', '2145', null, null);
INSERT INTO `catalog_feed_file` VALUES ('12', '20150626_0930_Orbile_Daily.xml', 'data/pending/20150626_0930_Orbile_Daily.xml', '2015-07-30 19:40:08', '2015-07-30 19:40:12', '1700', null, null);
INSERT INTO `catalog_feed_file` VALUES ('13', '20150626_1330_Orbile_Daily.xml', 'data/pending/20150626_1330_Orbile_Daily.xml', '2015-07-30 19:40:12', '2015-07-30 19:40:22', '3820', null, null);
INSERT INTO `catalog_feed_file` VALUES ('14', '20150626_1730_Orbile_Daily.xml', 'data/pending/20150626_1730_Orbile_Daily.xml', '2015-07-30 19:40:22', '2015-07-30 19:40:26', '1380', null, null);
INSERT INTO `catalog_feed_file` VALUES ('15', '20150626_2130_Orbile_Daily.xml', 'data/pending/20150626_2130_Orbile_Daily.xml', '2015-07-30 19:40:26', '2015-07-30 19:40:29', '1326', null, null);
INSERT INTO `catalog_feed_file` VALUES ('16', '20150627_0130_Orbile_Daily.xml', 'data/pending/20150627_0130_Orbile_Daily.xml', '2015-07-30 19:40:29', '2015-07-30 19:40:31', '888', null, null);
INSERT INTO `catalog_feed_file` VALUES ('17', '20150627_0530_Orbile_Daily.xml', 'data/pending/20150627_0530_Orbile_Daily.xml', '2015-07-30 19:40:31', '2015-07-30 19:40:34', '1398', null, null);
INSERT INTO `catalog_feed_file` VALUES ('18', '20150627_0930_Orbile_Daily.xml', 'data/pending/20150627_0930_Orbile_Daily.xml', '2015-07-30 19:40:34', '2015-07-30 19:40:41', '3057', null, null);
INSERT INTO `catalog_feed_file` VALUES ('19', '20150627_1330_Orbile_Daily.xml', 'data/pending/20150627_1330_Orbile_Daily.xml', '2015-07-30 19:40:41', '2015-07-30 19:41:30', '15981', null, null);
INSERT INTO `catalog_feed_file` VALUES ('20', '20150627_1730_Orbile_Daily.xml', 'data/pending/20150627_1730_Orbile_Daily.xml', '2015-07-30 19:41:30', '2015-07-30 19:41:37', '2541', null, null);
INSERT INTO `catalog_feed_file` VALUES ('21', '20150627_2130_Orbile_Daily.xml', 'data/pending/20150627_2130_Orbile_Daily.xml', '2015-07-30 19:41:37', '2015-07-30 19:41:37', '234', null, null);
INSERT INTO `catalog_feed_file` VALUES ('22', '20150628_0130_Orbile_Daily.xml', 'data/pending/20150628_0130_Orbile_Daily.xml', '2015-07-30 19:41:37', '2015-07-30 19:41:38', '430', null, null);
INSERT INTO `catalog_feed_file` VALUES ('23', '20150628_0530_Orbile_Daily.xml', 'data/pending/20150628_0530_Orbile_Daily.xml', '2015-07-30 19:41:38', '2015-07-30 19:41:41', '1708', null, null);
INSERT INTO `catalog_feed_file` VALUES ('24', '20150628_0930_Orbile_Daily.xml', 'data/pending/20150628_0930_Orbile_Daily.xml', '2015-07-30 19:41:41', '2015-07-30 19:41:41', '253', null, null);
INSERT INTO `catalog_feed_file` VALUES ('25', '20150628_1330_Orbile_Daily.xml', 'data/pending/20150628_1330_Orbile_Daily.xml', '2015-07-30 19:41:41', '2015-07-30 19:41:44', '1243', null, null);
INSERT INTO `catalog_feed_file` VALUES ('26', '20150628_1730_Orbile_Daily.xml', 'data/pending/20150628_1730_Orbile_Daily.xml', '2015-07-30 19:41:44', '2015-07-30 19:41:45', '175', null, null);
INSERT INTO `catalog_feed_file` VALUES ('27', '20150628_2130_Orbile_Daily.xml', 'data/pending/20150628_2130_Orbile_Daily.xml', '2015-07-30 19:41:45', '2015-07-30 19:41:45', '325', null, null);
INSERT INTO `catalog_feed_file` VALUES ('28', '20150629_0130_Orbile_Daily.xml', 'data/pending/20150629_0130_Orbile_Daily.xml', '2015-07-30 19:41:45', '2015-07-30 19:41:54', '2902', null, null);
INSERT INTO `catalog_feed_file` VALUES ('29', '20150629_0530_Orbile_Daily.xml', 'data/pending/20150629_0530_Orbile_Daily.xml', '2015-07-30 19:41:54', '2015-07-30 19:41:59', '2512', null, null);
INSERT INTO `catalog_feed_file` VALUES ('30', '20150629_0930_Orbile_Daily.xml', 'data/pending/20150629_0930_Orbile_Daily.xml', '2015-07-30 19:41:59', '2015-07-30 19:42:10', '3113', null, null);
INSERT INTO `catalog_feed_file` VALUES ('31', '20150629_1330_Orbile_Daily.xml', 'data/pending/20150629_1330_Orbile_Daily.xml', '2015-07-30 19:42:10', '2015-07-30 19:42:37', '14274', null, null);
INSERT INTO `catalog_feed_file` VALUES ('32', '20150629_1730_Orbile_Daily.xml', 'data/pending/20150629_1730_Orbile_Daily.xml', '2015-07-30 19:42:37', '2015-07-30 19:42:45', '3698', null, null);
INSERT INTO `catalog_feed_file` VALUES ('33', '20150629_2130_Orbile_Daily.xml', 'data/pending/20150629_2130_Orbile_Daily.xml', '2015-07-30 19:42:45', '2015-07-30 19:42:52', '2770', null, null);
INSERT INTO `catalog_feed_file` VALUES ('34', '20150630_0130_Orbile_Daily.xml', 'data/pending/20150630_0130_Orbile_Daily.xml', '2015-07-30 19:42:52', '2015-07-30 19:42:58', '2665', null, null);
INSERT INTO `catalog_feed_file` VALUES ('35', '20150630_0530_Orbile_Daily.xml', 'data/pending/20150630_0530_Orbile_Daily.xml', '2015-07-30 19:42:58', '2015-07-30 19:43:20', '7969', null, null);
INSERT INTO `catalog_feed_file` VALUES ('36', '20150630_0930_Orbile_Daily.xml', 'data/pending/20150630_0930_Orbile_Daily.xml', '2015-07-30 19:43:20', '2015-07-30 19:43:35', '5862', null, null);
INSERT INTO `catalog_feed_file` VALUES ('37', '20150630_1330_Orbile_Daily.xml', 'data/pending/20150630_1330_Orbile_Daily.xml', '2015-07-30 19:43:35', '2015-07-30 19:43:38', '1104', null, null);
INSERT INTO `catalog_feed_file` VALUES ('38', '20150630_1730_Orbile_Daily.xml', 'data/pending/20150630_1730_Orbile_Daily.xml', '2015-07-30 19:43:38', '2015-07-30 19:43:41', '1733', null, null);
INSERT INTO `catalog_feed_file` VALUES ('39', '20150630_2130_Orbile_Daily.xml', 'data/pending/20150630_2130_Orbile_Daily.xml', '2015-07-30 19:43:41', '2015-07-30 19:43:46', '1773', null, null);
INSERT INTO `catalog_feed_file` VALUES ('40', '20150701_0130_Orbile_Daily.xml', 'data/pending/20150701_0130_Orbile_Daily.xml', '2015-07-30 19:43:46', '2015-07-30 19:43:53', '2760', null, null);
INSERT INTO `catalog_feed_file` VALUES ('41', '20150701_0530_Orbile_Daily.xml', 'data/pending/20150701_0530_Orbile_Daily.xml', '2015-07-30 19:43:53', '2015-07-30 19:44:05', '6510', null, null);
INSERT INTO `catalog_feed_file` VALUES ('42', '20150701_0930_Orbile_Daily.xml', 'data/pending/20150701_0930_Orbile_Daily.xml', '2015-07-30 19:44:05', '2015-07-30 19:44:15', '4771', null, null);
INSERT INTO `catalog_feed_file` VALUES ('43', '20150701_1330_Orbile_Daily.xml', 'data/pending/20150701_1330_Orbile_Daily.xml', '2015-07-30 19:44:15', '2015-07-30 19:44:37', '7366', null, null);
INSERT INTO `catalog_feed_file` VALUES ('44', '20150701_1730_Orbile_Daily.xml', 'data/pending/20150701_1730_Orbile_Daily.xml', '2015-07-30 19:44:37', '2015-07-30 19:44:40', '1395', null, null);
INSERT INTO `catalog_feed_file` VALUES ('45', '20150701_2130_Orbile_Daily.xml', 'data/pending/20150701_2130_Orbile_Daily.xml', '2015-07-30 19:44:40', '2015-07-30 19:44:45', '2058', null, null);
INSERT INTO `catalog_feed_file` VALUES ('46', '20150702_0130_Orbile_Daily.xml', 'data/pending/20150702_0130_Orbile_Daily.xml', '2015-07-30 19:44:45', '2015-07-30 19:44:48', '1440', null, null);
INSERT INTO `catalog_feed_file` VALUES ('47', '20150702_0530_Orbile_Daily.xml', 'data/pending/20150702_0530_Orbile_Daily.xml', '2015-07-30 19:44:48', '2015-07-30 19:44:50', '1258', null, null);
INSERT INTO `catalog_feed_file` VALUES ('48', '20150702_0930_Orbile_Daily.xml', 'data/pending/20150702_0930_Orbile_Daily.xml', '2015-07-30 19:44:50', '2015-07-30 19:44:54', '1598', null, null);
INSERT INTO `catalog_feed_file` VALUES ('49', '20150702_1330_Orbile_Daily.xml', 'data/pending/20150702_1330_Orbile_Daily.xml', '2015-07-30 19:44:54', '2015-07-30 19:44:55', '629', null, null);
INSERT INTO `catalog_feed_file` VALUES ('50', '20150702_1730_Orbile_Daily.xml', 'data/pending/20150702_1730_Orbile_Daily.xml', '2015-07-30 19:44:55', '2015-07-30 19:45:05', '3345', null, null);
INSERT INTO `catalog_feed_file` VALUES ('51', '20150702_2130_Orbile_Daily.xml', 'data/pending/20150702_2130_Orbile_Daily.xml', '2015-07-30 19:45:05', '2015-07-30 19:45:17', '5011', null, null);
INSERT INTO `catalog_feed_file` VALUES ('52', '20150703_0130_Orbile_Daily.xml', 'data/pending/20150703_0130_Orbile_Daily.xml', '2015-07-30 19:45:17', '2015-07-30 19:45:20', '1082', null, null);
INSERT INTO `catalog_feed_file` VALUES ('53', '20150703_0530_Orbile_Daily.xml', 'data/pending/20150703_0530_Orbile_Daily.xml', '2015-07-30 19:45:20', '2015-07-30 19:45:37', '5338', null, null);
INSERT INTO `catalog_feed_file` VALUES ('54', '20150703_0930_Orbile_Daily.xml', 'data/pending/20150703_0930_Orbile_Daily.xml', '2015-07-30 19:45:37', '2015-07-30 19:45:38', '552', null, null);
INSERT INTO `catalog_feed_file` VALUES ('55', '20150703_1330_Orbile_Daily.xml', 'data/pending/20150703_1330_Orbile_Daily.xml', '2015-07-30 19:45:38', '2015-07-30 19:45:43', '2117', null, null);
INSERT INTO `catalog_feed_file` VALUES ('56', '20150703_1730_Orbile_Daily.xml', 'data/pending/20150703_1730_Orbile_Daily.xml', '2015-07-30 19:45:43', '2015-07-30 19:45:46', '1626', null, null);
INSERT INTO `catalog_feed_file` VALUES ('57', '20150703_2130_Orbile_Daily.xml', 'data/pending/20150703_2130_Orbile_Daily.xml', '2015-07-30 19:45:46', '2015-07-30 19:45:48', '1022', null, null);
INSERT INTO `catalog_feed_file` VALUES ('58', '20150704_0130_Orbile_Daily.xml', 'data/pending/20150704_0130_Orbile_Daily.xml', '2015-07-30 19:45:48', '2015-07-30 19:45:50', '709', null, null);
INSERT INTO `catalog_feed_file` VALUES ('59', '20150704_0530_Orbile_Daily.xml', 'data/pending/20150704_0530_Orbile_Daily.xml', '2015-07-30 19:45:50', '2015-07-30 19:45:52', '1026', null, null);
INSERT INTO `catalog_feed_file` VALUES ('60', '20150704_0930_Orbile_Daily.xml', 'data/pending/20150704_0930_Orbile_Daily.xml', '2015-07-30 19:45:52', '2015-07-30 19:45:58', '2460', null, null);
INSERT INTO `catalog_feed_file` VALUES ('61', '20150704_1330_Orbile_Daily.xml', 'data/pending/20150704_1330_Orbile_Daily.xml', '2015-07-30 19:45:58', '2015-07-30 19:45:58', '107', null, null);
INSERT INTO `catalog_feed_file` VALUES ('62', '20150704_1730_Orbile_Daily.xml', 'data/pending/20150704_1730_Orbile_Daily.xml', '2015-07-30 19:45:58', '2015-07-30 19:45:59', '418', null, null);
INSERT INTO `catalog_feed_file` VALUES ('63', '20150704_2130_Orbile_Daily.xml', 'data/pending/20150704_2130_Orbile_Daily.xml', '2015-07-30 19:45:59', '2015-07-30 19:46:03', '565', null, null);
INSERT INTO `catalog_feed_file` VALUES ('64', '20150705_0130_Orbile_Daily.xml', 'data/pending/20150705_0130_Orbile_Daily.xml', '2015-07-30 19:46:03', '2015-07-30 19:46:03', '270', null, null);
INSERT INTO `catalog_feed_file` VALUES ('65', '20150705_0530_Orbile_Daily.xml', 'data/pending/20150705_0530_Orbile_Daily.xml', '2015-07-30 19:46:03', '2015-07-30 19:46:05', '936', null, null);
INSERT INTO `catalog_feed_file` VALUES ('66', '20150705_0930_Orbile_Daily.xml', 'data/pending/20150705_0930_Orbile_Daily.xml', '2015-07-30 19:46:05', '2015-07-30 19:46:05', '138', null, null);
INSERT INTO `catalog_feed_file` VALUES ('67', '20150705_1330_Orbile_Daily.xml', 'data/pending/20150705_1330_Orbile_Daily.xml', '2015-07-30 19:46:05', '2015-07-30 19:46:07', '808', null, null);
INSERT INTO `catalog_feed_file` VALUES ('68', '20150705_1730_Orbile_Daily.xml', 'data/pending/20150705_1730_Orbile_Daily.xml', '2015-07-30 19:46:07', '2015-07-30 19:46:08', '223', null, null);
INSERT INTO `catalog_feed_file` VALUES ('69', '20150705_2130_Orbile_Daily.xml', 'data/pending/20150705_2130_Orbile_Daily.xml', '2015-07-30 19:46:08', '2015-07-30 19:46:11', '975', null, null);
INSERT INTO `catalog_feed_file` VALUES ('70', '20150706_0130_Orbile_Daily.xml', 'data/pending/20150706_0130_Orbile_Daily.xml', '2015-07-30 19:46:11', '2015-07-30 19:46:12', '597', null, null);
INSERT INTO `catalog_feed_file` VALUES ('71', '20150706_0530_Orbile_Daily.xml', 'data/pending/20150706_0530_Orbile_Daily.xml', '2015-07-30 19:46:12', '2015-07-30 19:46:18', '2617', null, null);
INSERT INTO `catalog_feed_file` VALUES ('72', '20150706_0930_Orbile_Daily.xml', 'data/pending/20150706_0930_Orbile_Daily.xml', '2015-07-30 19:46:18', '2015-07-30 19:46:25', '2801', null, null);
INSERT INTO `catalog_feed_file` VALUES ('73', '20150706_1330_Orbile_Daily.xml', 'data/pending/20150706_1330_Orbile_Daily.xml', '2015-07-30 19:46:25', '2015-07-30 19:46:31', '6250', null, null);
INSERT INTO `catalog_feed_file` VALUES ('74', '20150706_1730_Orbile_Daily.xml', 'data/pending/20150706_1730_Orbile_Daily.xml', '2015-07-30 19:46:31', '2015-07-30 19:46:38', '3159', null, null);
INSERT INTO `catalog_feed_file` VALUES ('75', '20150706_2130_Orbile_Daily.xml', 'data/pending/20150706_2130_Orbile_Daily.xml', '2015-07-30 19:46:38', '2015-07-30 19:46:44', '2997', null, null);
INSERT INTO `catalog_feed_file` VALUES ('76', '20150707_0130_Orbile_Daily.xml', 'data/pending/20150707_0130_Orbile_Daily.xml', '2015-07-30 19:46:44', '2015-07-30 19:46:54', '3226', null, null);
INSERT INTO `catalog_feed_file` VALUES ('77', '20150707_0530_Orbile_Daily.xml', 'data/pending/20150707_0530_Orbile_Daily.xml', '2015-07-30 19:46:54', '2015-07-30 19:46:56', '1089', null, null);
INSERT INTO `catalog_feed_file` VALUES ('78', '20150707_0930_Orbile_Daily.xml', 'data/pending/20150707_0930_Orbile_Daily.xml', '2015-07-30 19:46:56', '2015-07-30 19:47:10', '5046', null, null);
INSERT INTO `catalog_feed_file` VALUES ('79', '20150707_1330_Orbile_Daily.xml', 'data/pending/20150707_1330_Orbile_Daily.xml', '2015-07-30 19:47:10', '2015-07-30 19:47:28', '5776', null, null);
INSERT INTO `catalog_feed_file` VALUES ('80', '20150707_1730_Orbile_Daily.xml', 'data/pending/20150707_1730_Orbile_Daily.xml', '2015-07-30 19:47:28', '2015-07-30 19:48:01', '10352', null, null);
INSERT INTO `catalog_feed_file` VALUES ('81', '20150707_2130_Orbile_Daily.xml', 'data/pending/20150707_2130_Orbile_Daily.xml', '2015-07-30 19:48:01', '2015-07-30 19:48:25', '9127', null, null);
INSERT INTO `catalog_feed_file` VALUES ('82', '20150708_0130_Orbile_Daily.xml', 'data/pending/20150708_0130_Orbile_Daily.xml', '2015-07-30 19:48:25', '2015-07-30 19:48:33', '2332', null, null);
INSERT INTO `catalog_feed_file` VALUES ('83', '20150708_0530_Orbile_Daily.xml', 'data/pending/20150708_0530_Orbile_Daily.xml', '2015-07-30 19:48:33', '2015-07-30 19:48:39', '4359', null, null);
INSERT INTO `catalog_feed_file` VALUES ('84', '20150708_0930_Orbile_Daily.xml', 'data/pending/20150708_0930_Orbile_Daily.xml', '2015-07-30 19:48:39', '2015-07-30 19:48:56', '5660', null, null);
INSERT INTO `catalog_feed_file` VALUES ('85', '20150708_1330_Orbile_Daily.xml', 'data/pending/20150708_1330_Orbile_Daily.xml', '2015-07-30 19:48:56', '2015-07-30 19:49:32', '17667', null, null);
INSERT INTO `catalog_feed_file` VALUES ('86', '20150708_1730_Orbile_Daily.xml', 'data/pending/20150708_1730_Orbile_Daily.xml', '2015-07-30 19:49:32', '2015-07-30 19:50:00', '9852', null, null);
INSERT INTO `catalog_feed_file` VALUES ('87', '20150708_2130_Orbile_Daily.xml', 'data/pending/20150708_2130_Orbile_Daily.xml', '2015-07-30 19:50:00', '2015-07-30 19:50:16', '7425', null, null);
INSERT INTO `catalog_feed_file` VALUES ('88', '20150709_0130_Orbile_Daily.xml', 'data/pending/20150709_0130_Orbile_Daily.xml', '2015-07-30 19:50:16', '2015-07-30 19:50:56', '17485', null, null);
INSERT INTO `catalog_feed_file` VALUES ('89', '20150709_0530_Orbile_Daily.xml', 'data/pending/20150709_0530_Orbile_Daily.xml', '2015-07-30 19:50:56', '2015-07-30 19:51:23', '12559', null, null);
INSERT INTO `catalog_feed_file` VALUES ('90', '20150709_0930_Orbile_Daily.xml', 'data/pending/20150709_0930_Orbile_Daily.xml', '2015-07-30 19:51:23', '2015-07-30 19:51:36', '5895', null, null);
INSERT INTO `catalog_feed_file` VALUES ('91', '20150709_1330_Orbile_Daily.xml', 'data/pending/20150709_1330_Orbile_Daily.xml', '2015-07-30 19:51:36', '2015-07-30 19:51:40', '1774', null, null);
INSERT INTO `catalog_feed_file` VALUES ('92', '20150709_1730_Orbile_Daily.xml', 'data/pending/20150709_1730_Orbile_Daily.xml', '2015-07-30 19:51:40', '2015-07-30 19:51:47', '2783', null, null);
INSERT INTO `catalog_feed_file` VALUES ('93', '20150709_2130_Orbile_Daily.xml', 'data/pending/20150709_2130_Orbile_Daily.xml', '2015-07-30 19:51:47', '2015-07-30 19:52:09', '10965', null, null);
INSERT INTO `catalog_feed_file` VALUES ('94', '20150710_0130_Orbile_Daily.xml', 'data/pending/20150710_0130_Orbile_Daily.xml', '2015-07-30 19:52:09', '2015-07-30 19:52:33', '14082', null, null);
INSERT INTO `catalog_feed_file` VALUES ('95', '20150710_0530_Orbile_Daily.xml', 'data/pending/20150710_0530_Orbile_Daily.xml', '2015-07-30 19:52:33', '2015-07-30 19:52:36', '1555', null, null);
INSERT INTO `catalog_feed_file` VALUES ('96', '20150710_0930_Orbile_Daily.xml', 'data/pending/20150710_0930_Orbile_Daily.xml', '2015-07-30 19:52:36', '2015-07-30 19:52:38', '883', null, null);
INSERT INTO `catalog_feed_file` VALUES ('97', '20150710_1330_Orbile_Daily.xml', 'data/pending/20150710_1330_Orbile_Daily.xml', '2015-07-30 19:52:38', '2015-07-30 19:52:40', '697', null, null);
INSERT INTO `catalog_feed_file` VALUES ('98', '20150710_1730_Orbile_Daily.xml', 'data/pending/20150710_1730_Orbile_Daily.xml', '2015-07-30 19:52:40', '2015-07-30 19:52:42', '1091', null, null);
INSERT INTO `catalog_feed_file` VALUES ('99', '20150710_2130_Orbile_Daily.xml', 'data/pending/20150710_2130_Orbile_Daily.xml', '2015-07-30 19:52:42', '2015-07-30 19:54:55', '39223', null, null);
INSERT INTO `catalog_feed_file` VALUES ('100', '20150711_0130_Orbile_Daily.xml', 'data/pending/20150711_0130_Orbile_Daily.xml', '2015-07-30 19:54:55', '2015-07-30 19:55:04', '3635', null, null);
INSERT INTO `catalog_feed_file` VALUES ('101', '20150711_0530_Orbile_Daily.xml', 'data/pending/20150711_0530_Orbile_Daily.xml', '2015-07-30 19:55:04', '2015-07-30 19:55:11', '2545', null, null);
INSERT INTO `catalog_feed_file` VALUES ('102', '20150711_0930_Orbile_Daily.xml', 'data/pending/20150711_0930_Orbile_Daily.xml', '2015-07-30 19:55:11', '2015-07-30 19:55:14', '1750', null, null);
INSERT INTO `catalog_feed_file` VALUES ('103', '20150711_1330_Orbile_Daily.xml', 'data/pending/20150711_1330_Orbile_Daily.xml', '2015-07-30 19:55:14', '2015-07-30 19:55:21', '2522', null, null);
INSERT INTO `catalog_feed_file` VALUES ('104', '20150711_1730_Orbile_Daily.xml', 'data/pending/20150711_1730_Orbile_Daily.xml', '2015-07-30 19:55:21', '2015-07-30 19:55:26', '2383', null, null);
INSERT INTO `catalog_feed_file` VALUES ('105', '20150711_2130_Orbile_Daily.xml', 'data/pending/20150711_2130_Orbile_Daily.xml', '2015-07-30 19:55:26', '2015-07-30 19:55:27', '847', null, null);
INSERT INTO `catalog_feed_file` VALUES ('106', '20150712_0130_Orbile_Daily.xml', 'data/pending/20150712_0130_Orbile_Daily.xml', '2015-07-30 19:55:27', '2015-07-30 19:55:27', '248', null, null);
INSERT INTO `catalog_feed_file` VALUES ('107', '20150712_0530_Orbile_Daily.xml', 'data/pending/20150712_0530_Orbile_Daily.xml', '2015-07-30 19:55:27', '2015-07-30 19:55:29', '769', null, null);
INSERT INTO `catalog_feed_file` VALUES ('108', '20150712_0930_Orbile_Daily.xml', 'data/pending/20150712_0930_Orbile_Daily.xml', '2015-07-30 19:55:29', '2015-07-30 19:55:30', '273', null, null);
INSERT INTO `catalog_feed_file` VALUES ('109', '20150712_1330_Orbile_Daily.xml', 'data/pending/20150712_1330_Orbile_Daily.xml', '2015-07-30 19:55:30', '2015-07-30 19:55:32', '702', null, null);
INSERT INTO `catalog_feed_file` VALUES ('110', '20150712_1730_Orbile_Daily.xml', 'data/pending/20150712_1730_Orbile_Daily.xml', '2015-07-30 19:55:32', '2015-07-30 19:55:33', '216', null, null);
INSERT INTO `catalog_feed_file` VALUES ('111', '20150712_2130_Orbile_Daily.xml', 'data/pending/20150712_2130_Orbile_Daily.xml', '2015-07-30 19:55:33', '2015-07-30 19:55:33', '308', null, null);
INSERT INTO `catalog_feed_file` VALUES ('112', '20150713_0130_Orbile_Daily.xml', 'data/pending/20150713_0130_Orbile_Daily.xml', '2015-07-30 19:55:33', '2015-07-30 19:55:36', '1093', null, null);
INSERT INTO `catalog_feed_file` VALUES ('113', '20150713_0530_Orbile_Daily.xml', 'data/pending/20150713_0530_Orbile_Daily.xml', '2015-07-30 19:55:36', '2015-07-30 19:55:37', '756', null, null);
INSERT INTO `catalog_feed_file` VALUES ('114', '20150713_0930_Orbile_Daily.xml', 'data/pending/20150713_0930_Orbile_Daily.xml', '2015-07-30 19:55:37', '2015-07-30 19:55:43', '2066', null, null);
INSERT INTO `catalog_feed_file` VALUES ('115', '20150713_1330_Orbile_Daily.xml', 'data/pending/20150713_1330_Orbile_Daily.xml', '2015-07-30 19:55:43', '2015-07-30 19:55:50', '6570', null, null);
INSERT INTO `catalog_feed_file` VALUES ('116', '20150713_1730_Orbile_Daily.xml', 'data/pending/20150713_1730_Orbile_Daily.xml', '2015-07-30 19:55:50', '2015-07-30 19:55:56', '2640', null, null);
INSERT INTO `catalog_feed_file` VALUES ('117', '20150713_2130_Orbile_Daily.xml', 'data/pending/20150713_2130_Orbile_Daily.xml', '2015-07-30 19:55:56', '2015-07-30 19:56:07', '4636', null, null);
INSERT INTO `catalog_feed_file` VALUES ('118', '20150714_0130_Orbile_Daily.xml', 'data/pending/20150714_0130_Orbile_Daily.xml', '2015-07-30 19:56:07', '2015-07-30 19:56:11', '1127', null, null);
INSERT INTO `catalog_feed_file` VALUES ('119', '20150714_0530_Orbile_Daily.xml', 'data/pending/20150714_0530_Orbile_Daily.xml', '2015-07-30 19:56:11', '2015-07-30 19:56:12', '1000', null, null);
INSERT INTO `catalog_feed_file` VALUES ('120', '20150714_0930_Orbile_Daily.xml', 'data/pending/20150714_0930_Orbile_Daily.xml', '2015-07-30 19:56:12', '2015-07-30 19:56:14', '968', null, null);
INSERT INTO `catalog_feed_file` VALUES ('121', '20150714_1330_Orbile_Daily.xml', 'data/pending/20150714_1330_Orbile_Daily.xml', '2015-07-30 19:56:14', '2015-07-30 19:56:21', '3142', null, null);
INSERT INTO `catalog_feed_file` VALUES ('122', '20150714_1730_Orbile_Daily.xml', 'data/pending/20150714_1730_Orbile_Daily.xml', '2015-07-30 19:56:21', '2015-07-30 19:56:31', '3576', null, null);
INSERT INTO `catalog_feed_file` VALUES ('123', '20150714_2130_Orbile_Daily.xml', 'data/pending/20150714_2130_Orbile_Daily.xml', '2015-07-30 19:56:31', '2015-07-30 19:56:35', '1471', null, null);
INSERT INTO `catalog_feed_file` VALUES ('124', '20150715_0130_Orbile_Daily.xml', 'data/pending/20150715_0130_Orbile_Daily.xml', '2015-07-30 19:56:35', '2015-07-30 19:56:38', '1232', null, null);
INSERT INTO `catalog_feed_file` VALUES ('125', '20150715_0530_Orbile_Daily.xml', 'data/pending/20150715_0530_Orbile_Daily.xml', '2015-07-30 19:56:38', '2015-07-30 19:56:41', '1499', null, null);
INSERT INTO `catalog_feed_file` VALUES ('126', '20150715_0930_Orbile_Daily.xml', 'data/pending/20150715_0930_Orbile_Daily.xml', '2015-07-30 19:56:41', '2015-07-30 19:56:43', '1577', null, null);
INSERT INTO `catalog_feed_file` VALUES ('127', '20150715_1330_Orbile_Daily.xml', 'data/pending/20150715_1330_Orbile_Daily.xml', '2015-07-30 19:56:43', '2015-07-30 19:56:52', '6983', null, null);
INSERT INTO `catalog_feed_file` VALUES ('128', '20150715_1730_Orbile_Daily.xml', 'data/pending/20150715_1730_Orbile_Daily.xml', '2015-07-30 19:56:52', '2015-07-30 19:56:55', '1218', null, null);
INSERT INTO `catalog_feed_file` VALUES ('129', '20150715_2130_Orbile_Daily.xml', 'data/pending/20150715_2130_Orbile_Daily.xml', '2015-07-30 19:56:55', '2015-07-30 19:57:11', '5006', null, null);
INSERT INTO `catalog_feed_file` VALUES ('130', '20150716_0130_Orbile_Daily.xml', 'data/pending/20150716_0130_Orbile_Daily.xml', '2015-07-30 19:57:11', '2015-07-30 19:57:15', '1792', null, null);
INSERT INTO `catalog_feed_file` VALUES ('131', '20150716_0530_Orbile_Daily.xml', 'data/pending/20150716_0530_Orbile_Daily.xml', '2015-07-30 19:57:15', '2015-07-30 19:57:17', '978', null, null);
INSERT INTO `catalog_feed_file` VALUES ('132', '20150716_0930_Orbile_Daily.xml', 'data/pending/20150716_0930_Orbile_Daily.xml', '2015-07-30 19:57:17', '2015-07-30 19:57:24', '2858', null, null);
INSERT INTO `catalog_feed_file` VALUES ('133', '20150716_1330_Orbile_Daily.xml', 'data/pending/20150716_1330_Orbile_Daily.xml', '2015-07-30 19:57:24', '2015-07-30 19:57:26', '697', null, null);
INSERT INTO `catalog_feed_file` VALUES ('134', '20150716_1730_Orbile_Daily.xml', 'data/pending/20150716_1730_Orbile_Daily.xml', '2015-07-30 19:57:26', '2015-07-30 19:57:35', '3285', null, null);
INSERT INTO `catalog_feed_file` VALUES ('135', '20150716_2130_Orbile_Daily.xml', 'data/pending/20150716_2130_Orbile_Daily.xml', '2015-07-30 19:57:35', '2015-07-30 19:57:38', '1661', null, null);
INSERT INTO `catalog_feed_file` VALUES ('136', '20150717_0130_Orbile_Daily.xml', 'data/pending/20150717_0130_Orbile_Daily.xml', '2015-07-30 19:57:38', '2015-07-30 19:57:40', '815', null, null);
INSERT INTO `catalog_feed_file` VALUES ('137', '20150717_0530_Orbile_Daily.xml', 'data/pending/20150717_0530_Orbile_Daily.xml', '2015-07-30 19:57:40', '2015-07-30 19:57:47', '2723', null, null);
INSERT INTO `catalog_feed_file` VALUES ('138', '20150717_0930_Orbile_Daily.xml', 'data/pending/20150717_0930_Orbile_Daily.xml', '2015-07-30 19:57:47', '2015-07-30 19:57:55', '1632', null, null);
INSERT INTO `catalog_feed_file` VALUES ('139', '20150717_1330_Orbile_Daily.xml', 'data/pending/20150717_1330_Orbile_Daily.xml', '2015-07-30 19:57:55', '2015-07-30 19:58:10', '6703', null, null);
INSERT INTO `catalog_feed_file` VALUES ('140', '20150717_1730_Orbile_Daily.xml', 'data/pending/20150717_1730_Orbile_Daily.xml', '2015-07-30 19:58:10', '2015-07-30 19:58:12', '1356', null, null);
INSERT INTO `catalog_feed_file` VALUES ('141', '20150717_2130_Orbile_Daily.xml', 'data/pending/20150717_2130_Orbile_Daily.xml', '2015-07-30 19:58:12', '2015-07-30 19:58:17', '2113', null, null);
INSERT INTO `catalog_feed_file` VALUES ('142', '20150718_0130_Orbile_Daily.xml', 'data/pending/20150718_0130_Orbile_Daily.xml', '2015-07-30 19:58:17', '2015-07-30 19:58:18', '1132', null, null);
INSERT INTO `catalog_feed_file` VALUES ('143', '20150718_0530_Orbile_Daily.xml', 'data/pending/20150718_0530_Orbile_Daily.xml', '2015-07-30 19:58:18', '2015-07-30 19:58:24', '3239', null, null);
INSERT INTO `catalog_feed_file` VALUES ('144', '20150718_0930_Orbile_Daily.xml', 'data/pending/20150718_0930_Orbile_Daily.xml', '2015-07-30 19:58:24', '2015-07-30 19:58:25', '542', null, null);
INSERT INTO `catalog_feed_file` VALUES ('145', '20150718_1330_Orbile_Daily.xml', 'data/pending/20150718_1330_Orbile_Daily.xml', '2015-07-30 19:58:25', '2015-07-30 19:58:27', '707', null, null);
INSERT INTO `catalog_feed_file` VALUES ('146', '20150718_1730_Orbile_Daily.xml', 'data/pending/20150718_1730_Orbile_Daily.xml', '2015-07-30 19:58:27', '2015-07-30 19:58:28', '241', null, null);
INSERT INTO `catalog_feed_file` VALUES ('147', '20150718_2130_Orbile_Daily.xml', 'data/pending/20150718_2130_Orbile_Daily.xml', '2015-07-30 19:58:28', '2015-07-30 19:58:30', '330', null, null);
INSERT INTO `catalog_feed_file` VALUES ('148', '20150719_0130_Orbile_Daily.xml', 'data/pending/20150719_0130_Orbile_Daily.xml', '2015-07-30 19:58:30', '2015-07-30 19:58:30', '220', null, null);
INSERT INTO `catalog_feed_file` VALUES ('149', '20150719_0530_Orbile_Daily.xml', 'data/pending/20150719_0530_Orbile_Daily.xml', '2015-07-30 19:58:30', '2015-07-30 19:58:31', '524', null, null);
INSERT INTO `catalog_feed_file` VALUES ('150', '20150719_0930_Orbile_Daily.xml', 'data/pending/20150719_0930_Orbile_Daily.xml', '2015-07-30 19:58:31', '2015-07-30 19:58:32', '223', null, null);
INSERT INTO `catalog_feed_file` VALUES ('151', '20150719_1330_Orbile_Daily.xml', 'data/pending/20150719_1330_Orbile_Daily.xml', '2015-07-30 19:58:32', '2015-07-30 19:58:36', '815', null, null);
INSERT INTO `catalog_feed_file` VALUES ('152', '20150719_1730_Orbile_Daily.xml', 'data/pending/20150719_1730_Orbile_Daily.xml', '2015-07-30 19:58:36', '2015-07-30 19:58:37', '182', null, null);
INSERT INTO `catalog_feed_file` VALUES ('153', '20150719_2130_Orbile_Daily.xml', 'data/pending/20150719_2130_Orbile_Daily.xml', '2015-07-30 19:58:37', '2015-07-30 19:58:37', '216', null, null);
INSERT INTO `catalog_feed_file` VALUES ('154', '20150720_0130_Orbile_Daily.xml', 'data/pending/20150720_0130_Orbile_Daily.xml', '2015-07-30 19:58:37', '2015-07-30 19:58:38', '262', null, null);
INSERT INTO `catalog_feed_file` VALUES ('155', '20150720_0530_Orbile_Daily.xml', 'data/pending/20150720_0530_Orbile_Daily.xml', '2015-07-30 19:58:38', '2015-07-30 19:58:41', '1813', null, null);
INSERT INTO `catalog_feed_file` VALUES ('156', '20150720_0930_Orbile_Daily.xml', 'data/pending/20150720_0930_Orbile_Daily.xml', '2015-07-30 19:58:41', '2015-07-30 19:58:51', '2895', null, null);
INSERT INTO `catalog_feed_file` VALUES ('157', '20150720_1330_Orbile_Daily.xml', 'data/pending/20150720_1330_Orbile_Daily.xml', '2015-07-30 19:58:51', '2015-07-30 19:58:54', '2820', null, null);
INSERT INTO `catalog_feed_file` VALUES ('158', '20150720_1730_Orbile_Daily.xml', 'data/pending/20150720_1730_Orbile_Daily.xml', '2015-07-30 19:58:54', '2015-07-30 19:59:02', '6677', null, null);
INSERT INTO `catalog_feed_file` VALUES ('159', '20150720_2130_Orbile_Daily.xml', 'data/pending/20150720_2130_Orbile_Daily.xml', '2015-07-30 19:59:02', '2015-07-30 19:59:07', '1895', null, null);
INSERT INTO `catalog_feed_file` VALUES ('160', '20150721_0130_Orbile_Daily.xml', 'data/pending/20150721_0130_Orbile_Daily.xml', '2015-07-30 19:59:07', '2015-07-30 19:59:12', '2035', null, null);
INSERT INTO `catalog_feed_file` VALUES ('161', '20150721_0530_Orbile_Daily.xml', 'data/pending/20150721_0530_Orbile_Daily.xml', '2015-07-30 19:59:12', '2015-07-30 19:59:18', '2306', null, null);
INSERT INTO `catalog_feed_file` VALUES ('162', '20150721_0930_Orbile_Daily.xml', 'data/pending/20150721_0930_Orbile_Daily.xml', '2015-07-30 19:59:18', '2015-07-30 19:59:19', '952', null, null);
INSERT INTO `catalog_feed_file` VALUES ('163', '20150721_1330_Orbile_Daily.xml', 'data/pending/20150721_1330_Orbile_Daily.xml', '2015-07-30 19:59:19', '2015-07-30 19:59:21', '738', null, null);
INSERT INTO `catalog_feed_file` VALUES ('164', '20150721_1730_Orbile_Daily.xml', 'data/pending/20150721_1730_Orbile_Daily.xml', '2015-07-30 19:59:21', '2015-07-30 19:59:27', '2586', null, null);
INSERT INTO `catalog_feed_file` VALUES ('165', '20150721_2130_Orbile_Daily.xml', 'data/pending/20150721_2130_Orbile_Daily.xml', '2015-07-30 19:59:27', '2015-07-30 19:59:30', '1648', null, null);
INSERT INTO `catalog_feed_file` VALUES ('166', '20150722_0130_Orbile_Daily.xml', 'data/pending/20150722_0130_Orbile_Daily.xml', '2015-07-30 19:59:30', '2015-07-30 19:59:32', '985', null, null);
INSERT INTO `catalog_feed_file` VALUES ('167', '20150722_0530_Orbile_Daily.xml', 'data/pending/20150722_0530_Orbile_Daily.xml', '2015-07-30 19:59:32', '2015-07-30 19:59:34', '561', null, null);
INSERT INTO `catalog_feed_file` VALUES ('168', '20150722_0930_Orbile_Daily.xml', 'data/pending/20150722_0930_Orbile_Daily.xml', '2015-07-30 19:59:34', '2015-07-30 19:59:37', '1758', null, null);
INSERT INTO `catalog_feed_file` VALUES ('169', '20150722_1330_Orbile_Daily.xml', 'data/pending/20150722_1330_Orbile_Daily.xml', '2015-07-30 19:59:37', '2015-07-30 19:59:38', '697', null, null);
INSERT INTO `catalog_feed_file` VALUES ('170', '20150722_1730_Orbile_Daily.xml', 'data/pending/20150722_1730_Orbile_Daily.xml', '2015-07-30 19:59:38', '2015-07-30 19:59:46', '3070', null, null);
INSERT INTO `catalog_feed_file` VALUES ('171', '20150722_2130_Orbile_Daily.xml', 'data/pending/20150722_2130_Orbile_Daily.xml', '2015-07-30 19:59:46', '2015-07-30 19:59:54', '3889', null, null);
INSERT INTO `catalog_feed_file` VALUES ('172', '20150723_0130_Orbile_Daily.xml', 'data/pending/20150723_0130_Orbile_Daily.xml', '2015-07-30 19:59:54', '2015-07-30 19:59:57', '1096', null, null);
INSERT INTO `catalog_feed_file` VALUES ('173', '20150723_0530_Orbile_Daily.xml', 'data/pending/20150723_0530_Orbile_Daily.xml', '2015-07-30 19:59:57', '2015-07-30 19:59:58', '1074', null, null);
INSERT INTO `catalog_feed_file` VALUES ('174', '20150723_0930_Orbile_Daily.xml', 'data/pending/20150723_0930_Orbile_Daily.xml', '2015-07-30 19:59:58', '2015-07-30 20:00:02', '1149', null, null);
INSERT INTO `catalog_feed_file` VALUES ('175', '20150723_1330_Orbile_Daily.xml', 'data/pending/20150723_1330_Orbile_Daily.xml', '2015-07-30 20:00:02', '2015-07-30 20:00:06', '1498', null, null);
INSERT INTO `catalog_feed_file` VALUES ('176', '20150723_1730_Orbile_Daily.xml', 'data/pending/20150723_1730_Orbile_Daily.xml', '2015-07-30 20:00:06', '2015-07-30 20:00:09', '1753', null, null);
INSERT INTO `catalog_feed_file` VALUES ('177', '20150723_2130_Orbile_Daily.xml', 'data/pending/20150723_2130_Orbile_Daily.xml', '2015-07-30 20:00:09', '2015-07-30 20:00:13', '1728', null, null);
INSERT INTO `catalog_feed_file` VALUES ('178', '20150724_0130_Orbile_Daily.xml', 'data/pending/20150724_0130_Orbile_Daily.xml', '2015-07-30 20:00:13', '2015-07-30 20:00:16', '1407', null, null);
INSERT INTO `catalog_feed_file` VALUES ('179', '20150724_0530_Orbile_Daily.xml', 'data/pending/20150724_0530_Orbile_Daily.xml', '2015-07-30 20:00:16', '2015-07-30 20:00:21', '1707', null, null);
INSERT INTO `catalog_feed_file` VALUES ('180', '20150724_0930_Orbile_Daily.xml', 'data/pending/20150724_0930_Orbile_Daily.xml', '2015-07-30 20:00:21', '2015-07-30 20:00:26', '1928', null, null);
INSERT INTO `catalog_feed_file` VALUES ('181', '20150724_1330_Orbile_Daily.xml', 'data/pending/20150724_1330_Orbile_Daily.xml', '2015-07-30 20:00:26', '2015-07-30 20:00:33', '2547', null, null);
INSERT INTO `catalog_feed_file` VALUES ('182', '20150724_1730_Orbile_Daily.xml', 'data/pending/20150724_1730_Orbile_Daily.xml', '2015-07-30 20:00:33', '2015-07-30 20:00:42', '4218', null, null);
INSERT INTO `catalog_feed_file` VALUES ('183', '20150724_2130_Orbile_Daily.xml', 'data/pending/20150724_2130_Orbile_Daily.xml', '2015-07-30 20:00:42', '2015-07-30 20:00:46', '2515', null, null);
INSERT INTO `catalog_feed_file` VALUES ('184', '20150725_0130_Orbile_Daily.xml', 'data/pending/20150725_0130_Orbile_Daily.xml', '2015-07-30 20:00:46', '2015-07-30 20:00:48', '872', null, null);
INSERT INTO `catalog_feed_file` VALUES ('185', '20150725_0530_Orbile_Daily.xml', 'data/pending/20150725_0530_Orbile_Daily.xml', '2015-07-30 20:00:48', '2015-07-30 20:00:53', '1752', null, null);
INSERT INTO `catalog_feed_file` VALUES ('186', '20150725_0930_Orbile_Daily.xml', 'data/pending/20150725_0930_Orbile_Daily.xml', '2015-07-30 20:00:53', '2015-07-30 20:00:56', '1312', null, null);
INSERT INTO `catalog_feed_file` VALUES ('187', '20150725_1330_Orbile_Daily.xml', 'data/pending/20150725_1330_Orbile_Daily.xml', '2015-07-30 20:00:56', '2015-07-30 20:00:58', '1605', null, null);
INSERT INTO `catalog_feed_file` VALUES ('188', '20150725_1730_Orbile_Daily.xml', 'data/pending/20150725_1730_Orbile_Daily.xml', '2015-07-30 20:00:58', '2015-07-30 20:01:06', '3956', null, null);
INSERT INTO `catalog_feed_file` VALUES ('189', '20150725_2130_Orbile_Daily.xml', 'data/pending/20150725_2130_Orbile_Daily.xml', '2015-07-30 20:01:06', '2015-07-30 20:01:07', '283', null, null);
INSERT INTO `catalog_feed_file` VALUES ('190', '20150726_0130_Orbile_Daily.xml', 'data/pending/20150726_0130_Orbile_Daily.xml', '2015-07-30 20:01:07', '2015-07-30 20:01:07', '243', null, null);
INSERT INTO `catalog_feed_file` VALUES ('191', '20150726_0530_Orbile_Daily.xml', 'data/pending/20150726_0530_Orbile_Daily.xml', '2015-07-30 20:01:07', '2015-07-30 20:01:08', '733', null, null);
INSERT INTO `catalog_feed_file` VALUES ('192', '20150726_0930_Orbile_Daily.xml', 'data/pending/20150726_0930_Orbile_Daily.xml', '2015-07-30 20:01:08', '2015-07-30 20:01:09', '655', null, null);
INSERT INTO `catalog_feed_file` VALUES ('193', '20150726_1330_Orbile_Daily.xml', 'data/pending/20150726_1330_Orbile_Daily.xml', '2015-07-30 20:01:09', '2015-07-30 20:01:12', '899', null, null);
INSERT INTO `catalog_feed_file` VALUES ('194', '20150726_1730_Orbile_Daily.xml', 'data/pending/20150726_1730_Orbile_Daily.xml', '2015-07-30 20:01:12', '2015-07-30 20:01:12', '156', null, null);
INSERT INTO `catalog_feed_file` VALUES ('195', '20150726_2130_Orbile_Daily.xml', 'data/pending/20150726_2130_Orbile_Daily.xml', '2015-07-30 20:01:12', '2015-07-30 20:01:13', '237', null, null);
INSERT INTO `catalog_feed_file` VALUES ('196', '20150727_0130_Orbile_Daily.xml', 'data/pending/20150727_0130_Orbile_Daily.xml', '2015-07-30 20:01:13', '2015-07-30 20:01:15', '989', null, null);
INSERT INTO `catalog_feed_file` VALUES ('197', '20150727_0530_Orbile_Daily.xml', 'data/pending/20150727_0530_Orbile_Daily.xml', '2015-07-30 20:01:15', '2015-07-30 20:01:17', '1217', null, null);
INSERT INTO `catalog_feed_file` VALUES ('198', '20150727_0930_Orbile_Daily.xml', 'data/pending/20150727_0930_Orbile_Daily.xml', '2015-07-30 20:01:17', '2015-07-30 20:01:21', '850', null, null);
INSERT INTO `catalog_feed_file` VALUES ('199', '20150727_1330_Orbile_Daily.xml', 'data/pending/20150727_1330_Orbile_Daily.xml', '2015-07-30 20:01:21', '2015-07-30 20:01:24', '4093', null, null);
INSERT INTO `catalog_feed_file` VALUES ('200', '20150727_1730_Orbile_Daily.xml', 'data/pending/20150727_1730_Orbile_Daily.xml', '2015-07-30 20:01:24', '2015-07-30 20:01:29', '3036', null, null);
INSERT INTO `catalog_feed_file` VALUES ('201', '20150727_2130_Orbile_Daily.xml', 'data/pending/20150727_2130_Orbile_Daily.xml', '2015-07-30 20:01:29', '2015-07-30 20:01:33', '2090', null, null);
INSERT INTO `catalog_feed_file` VALUES ('202', '20150728_0130_Orbile_Daily.xml', 'data/pending/20150728_0130_Orbile_Daily.xml', '2015-07-30 20:01:33', '2015-07-30 20:01:39', '2117', null, null);
INSERT INTO `catalog_feed_file` VALUES ('203', '20150728_0530_Orbile_Daily.xml', 'data/pending/20150728_0530_Orbile_Daily.xml', '2015-07-30 20:01:39', '2015-07-30 20:01:45', '2064', null, null);
INSERT INTO `catalog_feed_file` VALUES ('204', '20150728_0930_Orbile_Daily.xml', 'data/pending/20150728_0930_Orbile_Daily.xml', '2015-07-30 20:01:45', '2015-07-30 20:01:49', '1575', null, null);
INSERT INTO `catalog_feed_file` VALUES ('205', '20150728_1330_Orbile_Daily.xml', 'data/pending/20150728_1330_Orbile_Daily.xml', '2015-07-30 20:01:49', '2015-07-30 20:01:51', '884', null, null);
INSERT INTO `catalog_feed_file` VALUES ('206', '20150728_1730_Orbile_Daily.xml', 'data/pending/20150728_1730_Orbile_Daily.xml', '2015-07-30 20:01:51', '2015-07-30 20:01:55', '1480', null, null);
INSERT INTO `catalog_feed_file` VALUES ('207', '20150728_2130_Orbile_Daily.xml', 'data/pending/20150728_2130_Orbile_Daily.xml', '2015-07-30 20:01:55', '2015-07-30 20:01:59', '1388', null, null);
INSERT INTO `catalog_feed_file` VALUES ('208', '20150729_0130_Orbile_Daily.xml', 'data/pending/20150729_0130_Orbile_Daily.xml', '2015-07-30 20:01:59', '2015-07-30 20:02:02', '1155', null, null);
INSERT INTO `catalog_feed_file` VALUES ('209', '20150729_0530_Orbile_Daily.xml', 'data/pending/20150729_0530_Orbile_Daily.xml', '2015-07-30 20:02:02', '2015-07-30 20:02:04', '1202', null, null);
INSERT INTO `catalog_feed_file` VALUES ('210', '20150729_0930_Orbile_Daily.xml', 'data/pending/20150729_0930_Orbile_Daily.xml', '2015-07-30 20:02:04', '2015-07-30 20:02:06', '687', null, null);
INSERT INTO `catalog_feed_file` VALUES ('211', '20150729_1330_Orbile_Daily.xml', 'data/pending/20150729_1330_Orbile_Daily.xml', '2015-07-30 20:02:06', '2015-07-30 20:02:07', '722', null, null);
INSERT INTO `catalog_feed_file` VALUES ('212', '20150729_1730_Orbile_Daily.xml', 'data/pending/20150729_1730_Orbile_Daily.xml', '2015-07-30 20:02:07', '2015-07-30 20:02:14', '2808', null, null);
INSERT INTO `catalog_feed_file` VALUES ('213', '20150729_2130_Orbile_Daily.xml', 'data/pending/20150729_2130_Orbile_Daily.xml', '2015-07-30 20:02:14', '2015-07-30 20:02:18', '1430', null, null);
INSERT INTO `catalog_feed_file` VALUES ('214', '20150730_0130_Orbile_Daily.xml', 'data/pending/20150730_0130_Orbile_Daily.xml', '2015-07-30 20:02:18', '2015-07-30 20:02:23', '1960', null, null);
INSERT INTO `catalog_feed_file` VALUES ('215', '20150730_0530_Orbile_Daily.xml', 'data/pending/20150730_0530_Orbile_Daily.xml', '2015-07-30 20:02:23', '2015-07-30 20:02:26', '1480', null, null);
INSERT INTO `catalog_feed_file` VALUES ('216', '20150730_0930_Orbile_Daily.xml', 'data/pending/20150730_0930_Orbile_Daily.xml', '2015-07-30 20:02:26', '2015-07-30 20:02:27', '726', null, null);
INSERT INTO `catalog_feed_file` VALUES ('217', '20150730_1330_Orbile_Daily.xml', 'data/pending/20150730_1330_Orbile_Daily.xml', '2015-07-30 20:02:27', '2015-07-30 20:02:32', '1615', null, null);
INSERT INTO `catalog_feed_file` VALUES ('218', '20150730_1730_Orbile_Daily.xml', 'data/pending/20150730_1730_Orbile_Daily.xml', '2015-07-30 20:02:32', '2015-07-30 20:02:49', '5578', null, null);
INSERT INTO `catalog_feed_file` VALUES ('219', '20150730_2130_Orbile_Daily.xml', 'data/pending/20150730_2130_Orbile_Daily.xml', '2015-07-30 20:02:49', '2015-07-30 20:03:04', '5914', null, null);
INSERT INTO `catalog_feed_file` VALUES ('220', '20150731_0130_Orbile_Daily.xml', 'data/pending/20150731_0130_Orbile_Daily.xml', '2015-08-14 09:28:18', '2015-08-14 09:28:25', '2013', null, null);
INSERT INTO `catalog_feed_file` VALUES ('221', '20150731_0530_Orbile_Daily.xml', 'data/pending/20150731_0530_Orbile_Daily.xml', '2015-08-14 09:28:25', '2015-08-14 09:28:30', '1666', null, null);
INSERT INTO `catalog_feed_file` VALUES ('222', '20150731_0666_Orbile_Daily.xml', 'data/pending/20150731_0666_Orbile_Daily.xml', '2015-08-14 09:28:30', '2015-08-14 09:28:30', '20', null, null);
INSERT INTO `catalog_feed_file` VALUES ('223', '20150731_0930_Orbile_Daily.xml', 'data/pending/20150731_0930_Orbile_Daily.xml', '2015-08-14 09:28:30', '2015-08-14 09:28:35', '1431', null, null);
INSERT INTO `catalog_feed_file` VALUES ('224', '20150731_1330_Orbile_Daily.xml', 'data/pending/20150731_1330_Orbile_Daily.xml', '2015-08-14 09:28:35', '2015-08-14 09:28:45', '2716', null, null);
INSERT INTO `catalog_feed_file` VALUES ('225', '20150731_1730_Orbile_Daily.xml', 'data/pending/20150731_1730_Orbile_Daily.xml', '2015-08-14 09:28:45', '2015-08-14 09:28:48', '1453', null, null);
INSERT INTO `catalog_feed_file` VALUES ('226', '20150731_2130_Orbile_Daily.xml', 'data/pending/20150731_2130_Orbile_Daily.xml', '2015-08-14 09:28:48', '2015-08-14 09:28:49', '719', null, null);
INSERT INTO `catalog_feed_file` VALUES ('227', '20150801_0130_Orbile_Daily.xml', 'data/pending/20150801_0130_Orbile_Daily.xml', '2015-08-14 09:28:49', '2015-08-14 09:28:51', '667', null, null);
INSERT INTO `catalog_feed_file` VALUES ('228', '20150801_0530_Orbile_Daily.xml', 'data/pending/20150801_0530_Orbile_Daily.xml', '2015-08-14 09:28:51', '2015-08-14 09:28:55', '1934', null, null);
INSERT INTO `catalog_feed_file` VALUES ('229', '20150801_0930_Orbile_Daily.xml', 'data/pending/20150801_0930_Orbile_Daily.xml', '2015-08-14 09:28:55', '2015-08-14 09:29:00', '1036', null, null);
INSERT INTO `catalog_feed_file` VALUES ('230', '20150801_1330_Orbile_Daily.xml', 'data/pending/20150801_1330_Orbile_Daily.xml', '2015-08-14 09:29:00', '2015-08-14 09:29:00', '110', null, null);
INSERT INTO `catalog_feed_file` VALUES ('231', '20150801_1730_Orbile_Daily.xml', 'data/pending/20150801_1730_Orbile_Daily.xml', '2015-08-14 09:29:00', '2015-08-14 09:29:07', '1495', null, null);
INSERT INTO `catalog_feed_file` VALUES ('232', '20150801_2130_Orbile_Daily.xml', 'data/pending/20150801_2130_Orbile_Daily.xml', '2015-08-14 09:29:07', '2015-08-14 09:29:07', '179', null, null);
INSERT INTO `catalog_feed_file` VALUES ('233', '20150802_0130_Orbile_Daily.xml', 'data/pending/20150802_0130_Orbile_Daily.xml', '2015-08-14 09:29:07', '2015-08-14 09:29:08', '156', null, null);
INSERT INTO `catalog_feed_file` VALUES ('234', '20150802_0530_Orbile_Daily.xml', 'data/pending/20150802_0530_Orbile_Daily.xml', '2015-08-14 09:29:08', '2015-08-14 09:29:08', '555', null, null);
INSERT INTO `catalog_feed_file` VALUES ('235', '20150802_0930_Orbile_Daily.xml', 'data/pending/20150802_0930_Orbile_Daily.xml', '2015-08-14 09:29:08', '2015-08-14 09:29:10', '853', null, null);
INSERT INTO `catalog_feed_file` VALUES ('236', '20150802_1330_Orbile_Daily.xml', 'data/pending/20150802_1330_Orbile_Daily.xml', '2015-08-14 09:29:10', '2015-08-14 09:29:14', '1766', null, null);
INSERT INTO `catalog_feed_file` VALUES ('237', '20150802_1730_Orbile_Daily.xml', 'data/pending/20150802_1730_Orbile_Daily.xml', '2015-08-14 09:29:14', '2015-08-14 09:29:15', '160', null, null);
INSERT INTO `catalog_feed_file` VALUES ('238', '20150802_2130_Orbile_Daily.xml', 'data/pending/20150802_2130_Orbile_Daily.xml', '2015-08-14 09:29:15', '2015-08-14 09:29:18', '274', null, null);
INSERT INTO `catalog_feed_file` VALUES ('239', '20150803_0130_Orbile_Daily.xml', 'data/pending/20150803_0130_Orbile_Daily.xml', '2015-08-14 09:29:18', '2015-08-14 09:29:19', '438', null, null);
INSERT INTO `catalog_feed_file` VALUES ('240', '20150803_0530_Orbile_Daily.xml', 'data/pending/20150803_0530_Orbile_Daily.xml', '2015-08-14 09:29:19', '2015-08-14 09:29:27', '2845', null, null);
INSERT INTO `catalog_feed_file` VALUES ('241', '20150803_0930_Orbile_Daily.xml', 'data/pending/20150803_0930_Orbile_Daily.xml', '2015-08-14 09:29:27', '2015-08-14 09:29:58', '8169', null, null);
INSERT INTO `catalog_feed_file` VALUES ('242', '20150803_1330_Orbile_Daily.xml', 'data/pending/20150803_1330_Orbile_Daily.xml', '2015-08-14 09:29:58', '2015-08-14 09:30:05', '5339', null, null);
INSERT INTO `catalog_feed_file` VALUES ('243', '20150803_1730_Orbile_Daily.xml', 'data/pending/20150803_1730_Orbile_Daily.xml', '2015-08-14 09:30:05', '2015-08-14 09:30:14', '3741', null, null);
INSERT INTO `catalog_feed_file` VALUES ('244', '20150803_2130_Orbile_Daily.xml', 'data/pending/20150803_2130_Orbile_Daily.xml', '2015-08-14 09:30:14', '2015-08-14 09:30:22', '1503', null, null);
INSERT INTO `catalog_feed_file` VALUES ('245', '20150804_0130_Orbile_Daily.xml', 'data/pending/20150804_0130_Orbile_Daily.xml', '2015-08-14 09:30:22', '2015-08-14 09:30:25', '766', null, null);
INSERT INTO `catalog_feed_file` VALUES ('246', '20150804_0530_Orbile_Daily.xml', 'data/pending/20150804_0530_Orbile_Daily.xml', '2015-08-14 09:30:25', '2015-08-14 09:30:34', '3154', null, null);
INSERT INTO `catalog_feed_file` VALUES ('247', '20150804_0930_Orbile_Daily.xml', 'data/pending/20150804_0930_Orbile_Daily.xml', '2015-08-14 09:30:34', '2015-08-14 09:30:39', '931', null, null);
INSERT INTO `catalog_feed_file` VALUES ('248', '20150804_1330_Orbile_Daily.xml', 'data/pending/20150804_1330_Orbile_Daily.xml', '2015-08-14 09:30:39', '2015-08-14 09:30:42', '798', null, null);
INSERT INTO `catalog_feed_file` VALUES ('249', '20150804_1730_Orbile_Daily.xml', 'data/pending/20150804_1730_Orbile_Daily.xml', '2015-08-14 09:30:42', '2015-08-14 09:30:51', '3545', null, null);
INSERT INTO `catalog_feed_file` VALUES ('250', '20150804_2130_Orbile_Daily.xml', 'data/pending/20150804_2130_Orbile_Daily.xml', '2015-08-14 09:30:51', '2015-08-14 09:31:00', '2207', null, null);
INSERT INTO `catalog_feed_file` VALUES ('251', '20150805_0130_Orbile_Daily.xml', 'data/pending/20150805_0130_Orbile_Daily.xml', '2015-08-14 09:31:00', '2015-08-14 09:31:03', '1111', null, null);
INSERT INTO `catalog_feed_file` VALUES ('252', '20150805_0530_Orbile_Daily.xml', 'data/pending/20150805_0530_Orbile_Daily.xml', '2015-08-14 09:31:03', '2015-08-14 09:31:04', '1017', null, null);
INSERT INTO `catalog_feed_file` VALUES ('253', '20150805_0930_Orbile_Daily.xml', 'data/pending/20150805_0930_Orbile_Daily.xml', '2015-08-14 09:31:04', '2015-08-14 09:31:24', '5400', null, null);
INSERT INTO `catalog_feed_file` VALUES ('254', '20150805_1330_Orbile_Daily.xml', 'data/pending/20150805_1330_Orbile_Daily.xml', '2015-08-14 09:31:24', '2015-08-14 09:31:26', '866', null, null);
INSERT INTO `catalog_feed_file` VALUES ('255', '20150805_1730_Orbile_Daily.xml', 'data/pending/20150805_1730_Orbile_Daily.xml', '2015-08-14 09:31:26', '2015-08-14 09:31:32', '2210', null, null);
INSERT INTO `catalog_feed_file` VALUES ('256', '20150805_2130_Orbile_Daily.xml', 'data/pending/20150805_2130_Orbile_Daily.xml', '2015-08-14 09:31:32', '2015-08-14 09:31:41', '2840', null, null);
INSERT INTO `catalog_feed_file` VALUES ('257', '20150806_0130_Orbile_Daily.xml', 'data/pending/20150806_0130_Orbile_Daily.xml', '2015-08-14 09:31:41', '2015-08-14 09:31:50', '5838', null, null);
INSERT INTO `catalog_feed_file` VALUES ('258', '20150806_0530_Orbile_Daily.xml', 'data/pending/20150806_0530_Orbile_Daily.xml', '2015-08-14 09:31:50', '2015-08-14 09:31:51', '955', null, null);
INSERT INTO `catalog_feed_file` VALUES ('259', '20150806_0930_Orbile_Daily.xml', 'data/pending/20150806_0930_Orbile_Daily.xml', '2015-08-14 09:31:51', '2015-08-14 09:31:55', '1545', null, null);
INSERT INTO `catalog_feed_file` VALUES ('260', '20150806_1330_Orbile_Daily.xml', 'data/pending/20150806_1330_Orbile_Daily.xml', '2015-08-14 09:31:55', '2015-08-14 09:32:02', '686', null, null);
INSERT INTO `catalog_feed_file` VALUES ('261', '20150806_1730_Orbile_Daily.xml', 'data/pending/20150806_1730_Orbile_Daily.xml', '2015-08-14 09:32:02', '2015-08-14 09:32:11', '2905', null, null);
INSERT INTO `catalog_feed_file` VALUES ('262', '20150806_2130_Orbile_Daily.xml', 'data/pending/20150806_2130_Orbile_Daily.xml', '2015-08-14 09:32:11', '2015-08-14 09:32:22', '2350', null, null);
INSERT INTO `catalog_feed_file` VALUES ('263', '20150807_0130_Orbile_Daily.xml', 'data/pending/20150807_0130_Orbile_Daily.xml', '2015-08-14 09:32:22', '2015-08-14 09:32:25', '1177', null, null);
INSERT INTO `catalog_feed_file` VALUES ('264', '20150807_0530_Orbile_Daily.xml', 'data/pending/20150807_0530_Orbile_Daily.xml', '2015-08-14 09:32:25', '2015-08-14 09:32:29', '1513', null, null);
INSERT INTO `catalog_feed_file` VALUES ('265', '20150807_0930_Orbile_Daily.xml', 'data/pending/20150807_0930_Orbile_Daily.xml', '2015-08-14 09:32:29', '2015-08-14 09:32:40', '2308', null, null);
INSERT INTO `catalog_feed_file` VALUES ('266', '20150807_1330_Orbile_Daily.xml', 'data/pending/20150807_1330_Orbile_Daily.xml', '2015-08-14 09:32:40', '2015-08-14 09:32:42', '697', null, null);
INSERT INTO `catalog_feed_file` VALUES ('267', '20150807_1730_Orbile_Daily.xml', 'data/pending/20150807_1730_Orbile_Daily.xml', '2015-08-14 09:32:42', '2015-08-14 09:33:04', '6895', null, null);
INSERT INTO `catalog_feed_file` VALUES ('268', '20150807_2130_Orbile_Daily.xml', 'data/pending/20150807_2130_Orbile_Daily.xml', '2015-08-14 09:33:04', '2015-08-14 09:33:24', '6114', null, null);
INSERT INTO `catalog_feed_file` VALUES ('269', '20150808_0130_Orbile_Daily.xml', 'data/pending/20150808_0130_Orbile_Daily.xml', '2015-08-14 09:33:24', '2015-08-14 09:33:29', '2259', null, null);
INSERT INTO `catalog_feed_file` VALUES ('270', '20150808_0530_Orbile_Daily.xml', 'data/pending/20150808_0530_Orbile_Daily.xml', '2015-08-14 09:33:29', '2015-08-14 09:33:30', '729', null, null);
INSERT INTO `catalog_feed_file` VALUES ('271', '20150808_0930_Orbile_Daily.xml', 'data/pending/20150808_0930_Orbile_Daily.xml', '2015-08-14 09:33:30', '2015-08-14 09:33:36', '1324', null, null);
INSERT INTO `catalog_feed_file` VALUES ('272', '20150808_1330_Orbile_Daily.xml', 'data/pending/20150808_1330_Orbile_Daily.xml', '2015-08-14 09:33:36', '2015-08-14 09:33:40', '1339', null, null);
INSERT INTO `catalog_feed_file` VALUES ('273', '20150808_1730_Orbile_Daily.xml', 'data/pending/20150808_1730_Orbile_Daily.xml', '2015-08-14 09:33:40', '2015-08-14 09:33:43', '350', null, null);
INSERT INTO `catalog_feed_file` VALUES ('274', '20150808_2130_Orbile_Daily.xml', 'data/pending/20150808_2130_Orbile_Daily.xml', '2015-08-14 09:33:43', '2015-08-14 09:33:44', '359', null, null);
INSERT INTO `catalog_feed_file` VALUES ('275', '20150809_0130_Orbile_Daily.xml', 'data/pending/20150809_0130_Orbile_Daily.xml', '2015-08-14 09:33:44', '2015-08-14 09:33:45', '520', null, null);
INSERT INTO `catalog_feed_file` VALUES ('276', '20150809_0530_Orbile_Daily.xml', 'data/pending/20150809_0530_Orbile_Daily.xml', '2015-08-14 09:33:45', '2015-08-14 09:33:47', '916', null, null);
INSERT INTO `catalog_feed_file` VALUES ('277', '20150809_0930_Orbile_Daily.xml', 'data/pending/20150809_0930_Orbile_Daily.xml', '2015-08-14 09:33:47', '2015-08-14 09:33:47', '173', null, null);
INSERT INTO `catalog_feed_file` VALUES ('278', '20150809_1330_Orbile_Daily.xml', 'data/pending/20150809_1330_Orbile_Daily.xml', '2015-08-14 09:33:47', '2015-08-14 09:33:51', '1143', null, null);
INSERT INTO `catalog_feed_file` VALUES ('279', '20150809_1730_Orbile_Daily.xml', 'data/pending/20150809_1730_Orbile_Daily.xml', '2015-08-14 09:33:51', '2015-08-14 09:33:51', '211', null, null);
INSERT INTO `catalog_feed_file` VALUES ('280', '20150809_2130_Orbile_Daily.xml', 'data/pending/20150809_2130_Orbile_Daily.xml', '2015-08-14 09:33:51', '2015-08-14 09:33:52', '227', null, null);
INSERT INTO `catalog_feed_file` VALUES ('281', '20150810_0130_Orbile_Daily.xml', 'data/pending/20150810_0130_Orbile_Daily.xml', '2015-08-14 09:33:52', '2015-08-14 09:33:56', '1315', null, null);
INSERT INTO `catalog_feed_file` VALUES ('282', '20150810_0530_Orbile_Daily.xml', 'data/pending/20150810_0530_Orbile_Daily.xml', '2015-08-14 09:33:56', '2015-08-14 09:34:20', '6771', null, null);
INSERT INTO `catalog_feed_file` VALUES ('283', '20150810_0930_Orbile_Daily.xml', 'data/pending/20150810_0930_Orbile_Daily.xml', '2015-08-14 09:34:20', '2015-08-14 09:34:30', '3110', null, null);
INSERT INTO `catalog_feed_file` VALUES ('284', '20150810_1330_Orbile_Daily.xml', 'data/pending/20150810_1330_Orbile_Daily.xml', '2015-08-14 09:34:30', '2015-08-14 09:34:51', '8010', null, null);
INSERT INTO `catalog_feed_file` VALUES ('285', '20150810_1730_Orbile_Daily.xml', 'data/pending/20150810_1730_Orbile_Daily.xml', '2015-08-14 09:34:51', '2015-08-14 09:35:01', '4538', null, null);
INSERT INTO `catalog_feed_file` VALUES ('286', '20150810_2130_Orbile_Daily.xml', 'data/pending/20150810_2130_Orbile_Daily.xml', '2015-08-14 09:35:01', '2015-08-14 09:35:13', '4255', null, null);
INSERT INTO `catalog_feed_file` VALUES ('287', '20150811_0130_Orbile_Daily.xml', 'data/pending/20150811_0130_Orbile_Daily.xml', '2015-08-14 09:35:13', '2015-08-14 09:35:27', '2821', null, null);
INSERT INTO `catalog_feed_file` VALUES ('288', '20150811_0530_Orbile_Daily.xml', 'data/pending/20150811_0530_Orbile_Daily.xml', '2015-08-14 09:35:27', '2015-08-14 09:35:31', '1837', null, null);
INSERT INTO `catalog_feed_file` VALUES ('289', '20150811_0930_Orbile_Daily.xml', 'data/pending/20150811_0930_Orbile_Daily.xml', '2015-08-14 09:35:31', '2015-08-14 09:36:04', '9097', null, null);
INSERT INTO `catalog_feed_file` VALUES ('290', '20150811_1330_Orbile_Daily.xml', 'data/pending/20150811_1330_Orbile_Daily.xml', '2015-08-14 09:36:04', '2015-08-14 09:36:36', '9356', null, null);
INSERT INTO `catalog_feed_file` VALUES ('291', '20150811_1730_Orbile_Daily.xml', 'data/pending/20150811_1730_Orbile_Daily.xml', '2015-08-14 09:36:36', '2015-08-14 09:39:19', '48614', null, null);
INSERT INTO `catalog_feed_file` VALUES ('292', '20150811_2130_Orbile_Daily.xml', 'data/pending/20150811_2130_Orbile_Daily.xml', '2015-08-14 09:39:19', '2015-08-14 09:41:48', '46679', null, null);
INSERT INTO `catalog_feed_file` VALUES ('293', '20150812_0130_Orbile_Daily.xml', 'data/pending/20150812_0130_Orbile_Daily.xml', '2015-08-14 09:41:48', '2015-08-14 09:42:59', '20922', null, null);
INSERT INTO `catalog_feed_file` VALUES ('294', '20150812_0530_Orbile_Daily.xml', 'data/pending/20150812_0530_Orbile_Daily.xml', '2015-08-14 09:42:59', '2015-08-14 09:43:53', '15517', null, null);
INSERT INTO `catalog_feed_file` VALUES ('295', '20150812_0930_Orbile_Daily.xml', 'data/pending/20150812_0930_Orbile_Daily.xml', '2015-08-14 09:43:53', '2015-08-14 09:43:59', '2025', null, null);
INSERT INTO `catalog_feed_file` VALUES ('296', '20150812_1330_Orbile_Daily.xml', 'data/pending/20150812_1330_Orbile_Daily.xml', '2015-08-14 09:43:59', '2015-08-14 09:44:10', '3431', null, null);
INSERT INTO `catalog_feed_file` VALUES ('297', '20150812_1730_Orbile_Daily.xml', 'data/pending/20150812_1730_Orbile_Daily.xml', '2015-08-14 09:44:10', '2015-08-14 09:44:14', '1371', null, null);
INSERT INTO `catalog_feed_file` VALUES ('298', '20150812_2130_Orbile_Daily.xml', 'data/pending/20150812_2130_Orbile_Daily.xml', '2015-08-14 09:44:14', '2015-08-14 09:44:20', '2124', null, null);
INSERT INTO `catalog_feed_file` VALUES ('299', '20150813_0130_Orbile_Daily.xml', 'data/pending/20150813_0130_Orbile_Daily.xml', '2015-08-14 09:44:20', '2015-08-14 09:44:24', '930', null, null);
INSERT INTO `catalog_feed_file` VALUES ('300', '20150813_0530_Orbile_Daily.xml', 'data/pending/20150813_0530_Orbile_Daily.xml', '2015-08-14 09:44:24', '2015-08-14 09:44:33', '1884', null, null);
INSERT INTO `catalog_feed_file` VALUES ('301', '20150813_0930_Orbile_Daily.xml', 'data/pending/20150813_0930_Orbile_Daily.xml', '2015-08-14 09:44:33', '2015-08-14 09:44:37', '779', null, null);
INSERT INTO `catalog_feed_file` VALUES ('302', '20150813_1330_Orbile_Daily.xml', 'data/pending/20150813_1330_Orbile_Daily.xml', '2015-08-14 09:44:37', '2015-08-14 09:44:39', '860', null, null);
INSERT INTO `catalog_feed_file` VALUES ('303', '20150813_1730_Orbile_Daily.xml', 'data/pending/20150813_1730_Orbile_Daily.xml', '2015-08-14 09:44:39', '2015-08-14 09:45:02', '8078', null, null);
INSERT INTO `catalog_feed_file` VALUES ('304', '20150813_2130_Orbile_Daily.xml', 'data/pending/20150813_2130_Orbile_Daily.xml', '2015-08-14 09:45:03', '2015-08-14 09:46:57', '34129', null, null);
INSERT INTO `catalog_feed_file` VALUES ('305', '20150814_0130_Orbile_Daily.xml', 'data/pending/20150814_0130_Orbile_Daily.xml', '2015-08-14 09:46:57', '2015-08-14 09:49:41', '35595', null, null);
INSERT INTO `catalog_feed_file` VALUES ('306', '20150814_0530_Orbile_Daily.xml', 'data/pending/20150814_0530_Orbile_Daily.xml', '2015-08-14 09:49:41', '2015-08-14 09:52:20', '35561', null, null);
INSERT INTO `catalog_feed_file` VALUES ('307', '20150814_0930_Orbile_Daily.xml', 'data/pending/20150814_0930_Orbile_Daily.xml', '2015-08-14 09:52:20', '2015-08-14 09:55:46', '44682', null, null);
INSERT INTO `catalog_feed_file` VALUES ('308', '20150814_1330_Orbile_Daily.xml', 'data/pending/20150814_1330_Orbile_Daily.xml', '2015-08-14 09:55:46', '2015-08-14 09:58:40', '36847', null, null);
INSERT INTO `catalog_feed_file` VALUES ('309', '20150814_1730_Orbile_Daily.xml', 'data/pending/20150814_1730_Orbile_Daily.xml', '2015-08-14 13:01:27', '2015-08-14 13:02:46', '17173', null, null);
INSERT INTO `catalog_feed_file` VALUES ('310', '20150814_2130_Orbile_Daily.xml', 'data/pending/20150814_2130_Orbile_Daily.xml', '2015-08-14 17:01:28', '2015-08-14 17:02:44', '18300', null, null);
INSERT INTO `catalog_feed_file` VALUES ('311', '20150815_0130_Orbile_Daily.xml', 'data/pending/20150815_0130_Orbile_Daily.xml', '2015-08-14 21:01:30', '2015-08-14 21:03:18', '22903', null, null);
INSERT INTO `catalog_feed_file` VALUES ('312', '20150815_0530_Orbile_Daily.xml', 'data/pending/20150815_0530_Orbile_Daily.xml', '2015-08-15 01:01:33', '2015-08-15 01:03:28', '25832', null, null);
INSERT INTO `catalog_feed_file` VALUES ('313', '20150815_0930_Orbile_Daily.xml', 'data/pending/20150815_0930_Orbile_Daily.xml', '2015-08-15 05:01:35', '2015-08-15 05:03:49', '28604', null, null);
INSERT INTO `catalog_feed_file` VALUES ('314', '20150815_1330_Orbile_Daily.xml', 'data/pending/20150815_1330_Orbile_Daily.xml', '2015-08-15 09:01:37', '2015-08-15 09:03:45', '25344', null, null);
INSERT INTO `catalog_feed_file` VALUES ('315', '20150815_1730_Orbile_Daily.xml', 'data/pending/20150815_1730_Orbile_Daily.xml', '2015-08-15 13:01:39', '2015-08-15 13:03:12', '19234', null, null);
INSERT INTO `catalog_feed_file` VALUES ('316', '20150815_2130_Orbile_Daily.xml', 'data/pending/20150815_2130_Orbile_Daily.xml', '2015-08-15 17:01:42', '2015-08-15 17:03:50', '26859', null, null);
INSERT INTO `catalog_feed_file` VALUES ('317', '20150816_0130_Orbile_Daily.xml', 'data/pending/20150816_0130_Orbile_Daily.xml', '2015-08-15 21:01:44', '2015-08-15 21:03:18', '21008', null, null);
INSERT INTO `catalog_feed_file` VALUES ('318', '20150816_0530_Orbile_Daily.xml', 'data/pending/20150816_0530_Orbile_Daily.xml', '2015-08-16 01:01:45', '2015-08-16 01:03:16', '19110', null, null);
INSERT INTO `catalog_feed_file` VALUES ('319', '20150816_0930_Orbile_Daily.xml', 'data/pending/20150816_0930_Orbile_Daily.xml', '2015-08-16 05:01:48', '2015-08-16 05:03:10', '17911', null, null);
INSERT INTO `catalog_feed_file` VALUES ('320', '20150816_1330_Orbile_Daily.xml', 'data/pending/20150816_1330_Orbile_Daily.xml', '2015-08-16 09:01:50', '2015-08-16 09:03:26', '19837', null, null);
INSERT INTO `catalog_feed_file` VALUES ('321', '20150816_1730_Orbile_Daily.xml', 'data/pending/20150816_1730_Orbile_Daily.xml', '2015-08-16 13:01:50', '2015-08-16 13:03:14', '18277', null, null);
INSERT INTO `catalog_feed_file` VALUES ('322', '20150816_2130_Orbile_Daily.xml', 'data/pending/20150816_2130_Orbile_Daily.xml', '2015-08-16 17:01:53', '2015-08-16 17:02:56', '12958', null, null);
INSERT INTO `catalog_feed_file` VALUES ('323', '20150817_0130_Orbile_Daily.xml', 'data/pending/20150817_0130_Orbile_Daily.xml', '2015-08-16 21:01:54', '2015-08-16 21:02:44', '10965', null, null);
INSERT INTO `catalog_feed_file` VALUES ('324', '20150817_0530_Orbile_Daily.xml', 'data/pending/20150817_0530_Orbile_Daily.xml', '2015-08-17 01:01:55', '2015-08-17 01:02:56', '13783', null, null);
INSERT INTO `catalog_feed_file` VALUES ('325', '20150817_0930_Orbile_Daily.xml', 'data/pending/20150817_0930_Orbile_Daily.xml', '2015-08-17 05:01:56', '2015-08-17 05:02:57', '13482', null, null);
INSERT INTO `catalog_feed_file` VALUES ('326', '20150817_1330_Orbile_Daily.xml', 'data/pending/20150817_1330_Orbile_Daily.xml', '2015-08-17 10:02:28', '2015-08-17 10:19:43', '359427', null, null);
INSERT INTO `catalog_feed_file` VALUES ('327', '20150817_1730_Orbile_Daily.xml', 'data/pending/20150817_1730_Orbile_Daily.xml', '2015-08-17 13:02:25', '2015-08-17 13:03:12', '12773', '10714', '2059');
INSERT INTO `catalog_feed_file` VALUES ('328', '20150817_2130_Orbile_Daily.xml', 'data/pending/20150817_2130_Orbile_Daily.xml', '2015-08-17 17:02:25', '2015-08-17 17:03:17', '14560', '0', null);
INSERT INTO `catalog_feed_file` VALUES ('329', '20150818_0130_Orbile_Daily.xml', 'data/pending/20150818_0130_Orbile_Daily.xml', '2015-08-17 21:02:27', '2015-08-17 21:04:22', '33449', null, null);
INSERT INTO `catalog_feed_file` VALUES ('330', '20150818_0530_Orbile_Daily.xml', 'data/pending/20150818_0530_Orbile_Daily.xml', '2015-08-18 01:02:29', '2015-08-18 01:04:09', '28211', null, null);
INSERT INTO `catalog_feed_file` VALUES ('331', '20150818_0930_Orbile_Daily.xml', 'data/pending/20150818_0930_Orbile_Daily.xml', '2015-08-18 05:02:31', '2015-08-18 05:04:15', '29920', null, null);
INSERT INTO `catalog_feed_file` VALUES ('332', '20150818_1330_Orbile_Daily.xml', 'data/pending/20150818_1330_Orbile_Daily.xml', '2015-08-18 09:02:34', '2015-08-18 09:04:18', '29310', null, null);
INSERT INTO `catalog_feed_file` VALUES ('333', '20150818_1730_Orbile_Daily.xml', 'data/pending/20150818_1730_Orbile_Daily.xml', '2015-08-18 13:02:36', '2015-08-18 13:03:41', '19007', '19007', null);
INSERT INTO `catalog_feed_file` VALUES ('334', '20150818_2130_Orbile_Daily.xml', 'data/pending/20150818_2130_Orbile_Daily.xml', '2015-08-18 17:02:37', '2015-08-18 17:03:24', '13487', '13487', null);
INSERT INTO `catalog_feed_file` VALUES ('335', '20150819_0130_Orbile_Daily.xml', 'data/pending/20150819_0130_Orbile_Daily.xml', '2015-08-18 21:02:38', '2015-08-18 21:03:49', '19632', '19632', null);
INSERT INTO `catalog_feed_file` VALUES ('336', '20150819_0530_Orbile_Daily.xml', 'data/pending/20150819_0530_Orbile_Daily.xml', '2015-08-19 01:02:39', '2015-08-19 01:03:41', '17777', '17777', null);
INSERT INTO `catalog_feed_file` VALUES ('337', '20150819_0930_Orbile_Daily.xml', 'data/pending/20150819_0930_Orbile_Daily.xml', '2015-08-19 05:02:40', '2015-08-19 05:04:19', '28443', '28443', null);
INSERT INTO `catalog_feed_file` VALUES ('338', '20150819_1330_Orbile_Daily.xml', 'data/pending/20150819_1330_Orbile_Daily.xml', '2015-08-19 09:02:42', '2015-08-19 09:03:52', '21055', '21055', null);
INSERT INTO `catalog_feed_file` VALUES ('339', '20150819_1730_Orbile_Daily.xml', 'data/pending/20150819_1730_Orbile_Daily.xml', '2015-08-19 13:02:44', '2015-08-19 13:03:55', '18997', '18997', null);
INSERT INTO `catalog_feed_file` VALUES ('340', '20150819_2130_Orbile_Daily.xml', 'data/pending/20150819_2130_Orbile_Daily.xml', '2015-08-19 17:02:45', '2015-08-19 17:03:57', '20966', '20966', null);
INSERT INTO `catalog_feed_file` VALUES ('341', '20150820_0130_Orbile_Daily.xml', 'data/pending/20150820_0130_Orbile_Daily.xml', '2015-08-19 21:02:48', '2015-08-19 21:04:03', '21297', '21297', null);
INSERT INTO `catalog_feed_file` VALUES ('342', '20150820_0530_Orbile_Daily.xml', 'data/pending/20150820_0530_Orbile_Daily.xml', '2015-08-20 01:02:49', '2015-08-20 01:03:38', '13883', '13883', null);
INSERT INTO `catalog_feed_file` VALUES ('343', '20150820_0930_Orbile_Daily.xml', 'data/pending/20150820_0930_Orbile_Daily.xml', '2015-08-20 05:02:50', '2015-08-20 05:03:33', '12191', '12191', null);
INSERT INTO `catalog_feed_file` VALUES ('344', '20150820_1330_Orbile_Daily.xml', 'data/pending/20150820_1330_Orbile_Daily.xml', '2015-08-20 09:02:51', '2015-08-20 09:03:55', '18556', '18556', null);
INSERT INTO `catalog_feed_file` VALUES ('345', '20150820_1730_Orbile_Daily.xml', 'data/pending/20150820_1730_Orbile_Daily.xml', '2015-08-20 13:02:53', '2015-08-20 13:04:47', '30657', '30657', null);
INSERT INTO `catalog_feed_file` VALUES ('346', '20150820_2130_Orbile_Daily.xml', 'data/pending/20150820_2130_Orbile_Daily.xml', '2015-08-20 17:02:55', '2015-08-20 17:05:11', '39176', '39176', null);
INSERT INTO `catalog_feed_file` VALUES ('347', '20150821_0130_Orbile_Daily.xml', 'data/pending/20150821_0130_Orbile_Daily.xml', '2015-08-20 21:03:05', '2015-08-20 21:08:54', '106472', '106472', null);
INSERT INTO `catalog_feed_file` VALUES ('348', '20150821_0530_Orbile_Daily.xml', 'data/pending/20150821_0530_Orbile_Daily.xml', '2015-08-21 01:03:08', '2015-08-21 01:08:07', '89190', '89190', null);
INSERT INTO `catalog_feed_file` VALUES ('349', '20150821_0930_Orbile_Daily.xml', 'data/pending/20150821_0930_Orbile_Daily.xml', '2015-08-21 05:03:20', '2015-08-21 05:08:23', '91877', '91877', null);
INSERT INTO `catalog_feed_file` VALUES ('350', '20150821_1330_Orbile_Daily.xml', 'data/pending/20150821_1330_Orbile_Daily.xml', '2015-08-21 09:03:23', '2015-08-21 09:08:01', '81956', '81956', null);
INSERT INTO `catalog_feed_file` VALUES ('351', '20150821_1730_Orbile_Daily.xml', 'data/pending/20150821_1730_Orbile_Daily.xml', '2015-08-21 13:03:31', '2015-08-21 13:06:59', '61580', '61580', null);
INSERT INTO `catalog_feed_file` VALUES ('352', '20150821_2130_Orbile_Daily.xml', 'data/pending/20150821_2130_Orbile_Daily.xml', '2015-08-21 17:03:35', '2015-08-21 17:08:50', '96698', '96698', null);
INSERT INTO `catalog_feed_file` VALUES ('353', '20150822_0130_Orbile_Daily.xml', 'data/pending/20150822_0130_Orbile_Daily.xml', '2015-08-21 21:03:41', '2015-08-21 21:09:32', '107541', '107541', null);
INSERT INTO `catalog_feed_file` VALUES ('354', '20150822_0530_Orbile_Daily.xml', 'data/pending/20150822_0530_Orbile_Daily.xml', '2015-08-22 01:03:54', '2015-08-22 01:08:35', '86653', '86653', null);
INSERT INTO `catalog_feed_file` VALUES ('355', '20150822_0930_Orbile_Daily.xml', 'data/pending/20150822_0930_Orbile_Daily.xml', '2015-08-22 05:04:00', '2015-08-22 05:09:27', '101859', '101859', null);
INSERT INTO `catalog_feed_file` VALUES ('356', '20150822_1330_Orbile_Daily.xml', 'data/pending/20150822_1330_Orbile_Daily.xml', '2015-08-22 09:04:02', '2015-08-22 09:06:52', '50451', '50451', null);
INSERT INTO `catalog_feed_file` VALUES ('357', '20150822_1730_Orbile_Daily.xml', 'data/pending/20150822_1730_Orbile_Daily.xml', '2015-08-22 13:04:03', '2015-08-22 13:04:05', '374', '374', null);
INSERT INTO `catalog_feed_file` VALUES ('358', '20150822_2130_Orbile_Daily.xml', 'data/pending/20150822_2130_Orbile_Daily.xml', '2015-08-22 17:04:04', '2015-08-22 17:04:12', '2755', '2755', null);
INSERT INTO `catalog_feed_file` VALUES ('359', '20150823_0130_Orbile_Daily.xml', 'data/pending/20150823_0130_Orbile_Daily.xml', '2015-08-22 21:04:04', '2015-08-22 21:04:04', '233', '233', null);
INSERT INTO `catalog_feed_file` VALUES ('360', '20150823_0530_Orbile_Daily.xml', 'data/pending/20150823_0530_Orbile_Daily.xml', '2015-08-23 01:04:04', '2015-08-23 01:04:13', '2778', '2778', null);
INSERT INTO `catalog_feed_file` VALUES ('361', '20150823_0930_Orbile_Daily.xml', 'data/pending/20150823_0930_Orbile_Daily.xml', '2015-08-23 05:04:01', '2015-08-23 05:04:02', '210', '210', null);
INSERT INTO `catalog_feed_file` VALUES ('362', '20150823_1330_Orbile_Daily.xml', 'data/pending/20150823_1330_Orbile_Daily.xml', '2015-08-23 09:04:04', '2015-08-23 09:04:11', '2271', '2271', null);
INSERT INTO `catalog_feed_file` VALUES ('363', '20150823_1730_Orbile_Daily.xml', 'data/pending/20150823_1730_Orbile_Daily.xml', '2015-08-23 13:04:05', '2015-08-23 13:04:12', '2381', '2381', null);
INSERT INTO `catalog_feed_file` VALUES ('364', '20150823_2130_Orbile_Daily.xml', 'data/pending/20150823_2130_Orbile_Daily.xml', '2015-08-23 17:04:05', '2015-08-23 17:04:07', '1782', '1782', null);
INSERT INTO `catalog_feed_file` VALUES ('365', '20150824_0130_Orbile_Daily.xml', 'data/pending/20150824_0130_Orbile_Daily.xml', '2015-08-23 21:04:05', '2015-08-23 21:04:08', '1046', '1046', null);
INSERT INTO `catalog_feed_file` VALUES ('366', '20150824_0530_Orbile_Daily.xml', 'data/pending/20150824_0530_Orbile_Daily.xml', '2015-08-24 01:04:05', '2015-08-24 01:04:09', '1110', '1110', null);
INSERT INTO `catalog_feed_file` VALUES ('367', '20150824_0930_Orbile_Daily.xml', 'data/pending/20150824_0930_Orbile_Daily.xml', '2015-08-24 05:04:05', '2015-08-24 05:04:12', '1828', '1828', null);
INSERT INTO `catalog_feed_file` VALUES ('368', '20150824_1330_Orbile_Daily.xml', 'data/pending/20150824_1330_Orbile_Daily.xml', '2015-08-24 09:04:05', '2015-08-24 09:04:19', '4472', '4472', null);
INSERT INTO `catalog_feed_file` VALUES ('369', '20150824_1730_Orbile_Daily.xml', 'data/pending/20150824_1730_Orbile_Daily.xml', '2015-08-24 13:04:07', '2015-08-24 13:04:21', '5661', '5661', null);
INSERT INTO `catalog_feed_file` VALUES ('370', '20150824_2130_Orbile_Daily.xml', 'data/pending/20150824_2130_Orbile_Daily.xml', '2015-08-24 17:04:06', '2015-08-24 17:04:18', '3912', '3912', null);
INSERT INTO `catalog_feed_file` VALUES ('371', '20150825_0130_Orbile_Daily.xml', 'data/pending/20150825_0130_Orbile_Daily.xml', '2015-08-24 21:04:05', '2015-08-24 21:04:10', '1557', '1557', null);
INSERT INTO `catalog_feed_file` VALUES ('372', '20150825_0530_Orbile_Daily.xml', 'data/pending/20150825_0530_Orbile_Daily.xml', '2015-08-25 01:04:06', '2015-08-25 01:04:14', '1986', '1986', null);
INSERT INTO `catalog_feed_file` VALUES ('373', '20150825_0930_Orbile_Daily.xml', 'data/pending/20150825_0930_Orbile_Daily.xml', '2015-08-25 05:04:06', '2015-08-25 05:04:12', '2026', '2026', null);
INSERT INTO `catalog_feed_file` VALUES ('374', '20150825_1330_Orbile_Daily.xml', 'data/pending/20150825_1330_Orbile_Daily.xml', '2015-08-25 09:04:05', '2015-08-25 09:04:09', '1022', '1022', null);
INSERT INTO `catalog_feed_file` VALUES ('375', '20150825_1730_Orbile_Daily.xml', 'data/pending/20150825_1730_Orbile_Daily.xml', '2015-08-25 13:04:06', '2015-08-25 13:04:12', '1609', '1609', null);
INSERT INTO `catalog_feed_file` VALUES ('376', '20150825_2130_Orbile_Daily.xml', 'data/pending/20150825_2130_Orbile_Daily.xml', '2015-08-25 17:04:06', '2015-08-25 17:04:11', '1612', '1612', null);
INSERT INTO `catalog_feed_file` VALUES ('377', '20150826_0130_Orbile_Daily.xml', 'data/pending/20150826_0130_Orbile_Daily.xml', '2015-08-25 21:04:06', '2015-08-25 21:04:18', '2849', '2849', null);
INSERT INTO `catalog_feed_file` VALUES ('378', '20150826_0530_Orbile_Daily.xml', 'data/pending/20150826_0530_Orbile_Daily.xml', '2015-08-26 01:04:06', '2015-08-26 01:04:09', '1045', '1045', null);
INSERT INTO `catalog_feed_file` VALUES ('379', '20150826_0930_Orbile_Daily.xml', 'data/pending/20150826_0930_Orbile_Daily.xml', '2015-08-26 05:04:07', '2015-08-26 05:04:13', '1659', '1659', null);
INSERT INTO `catalog_feed_file` VALUES ('380', '20150826_1330_Orbile_Daily.xml', 'data/pending/20150826_1330_Orbile_Daily.xml', '2015-08-26 09:04:06', '2015-08-26 09:04:08', '446', '446', null);
INSERT INTO `catalog_feed_file` VALUES ('381', '20150826_1730_Orbile_Daily.xml', 'data/pending/20150826_1730_Orbile_Daily.xml', '2015-08-26 13:04:06', '2015-08-26 13:04:19', '3252', '3252', null);
INSERT INTO `catalog_feed_file` VALUES ('382', '20150826_2130_Orbile_Daily.xml', 'data/pending/20150826_2130_Orbile_Daily.xml', '2015-08-26 17:04:08', '2015-08-26 17:04:23', '4496', '4496', null);
INSERT INTO `catalog_feed_file` VALUES ('383', '20150827_0130_Orbile_Daily.xml', 'data/pending/20150827_0130_Orbile_Daily.xml', '2015-08-26 21:04:07', '2015-08-26 21:04:10', '1605', '1605', null);
INSERT INTO `catalog_feed_file` VALUES ('384', '20150827_0530_Orbile_Daily.xml', 'data/pending/20150827_0530_Orbile_Daily.xml', '2015-08-27 01:04:07', '2015-08-27 01:04:11', '1501', '1501', null);
INSERT INTO `catalog_feed_file` VALUES ('385', '20150827_0930_Orbile_Daily.xml', 'data/pending/20150827_0930_Orbile_Daily.xml', '2015-08-27 05:04:07', '2015-08-27 05:04:13', '2627', '2627', null);
INSERT INTO `catalog_feed_file` VALUES ('386', '20150827_1330_Orbile_Daily.xml', 'data/pending/20150827_1330_Orbile_Daily.xml', '2015-08-27 09:04:08', '2015-08-27 09:04:12', '1779', '1779', null);
INSERT INTO `catalog_feed_file` VALUES ('387', '20150827_1730_Orbile_Daily.xml', 'data/pending/20150827_1730_Orbile_Daily.xml', '2015-08-27 13:04:08', '2015-08-27 13:04:27', '5930', '5930', null);
INSERT INTO `catalog_feed_file` VALUES ('388', '20150827_2130_Orbile_Daily.xml', 'data/pending/20150827_2130_Orbile_Daily.xml', '2015-08-27 17:04:07', '2015-08-27 17:04:12', '2679', '2679', null);
INSERT INTO `catalog_feed_file` VALUES ('389', '20150828_0130_Orbile_Daily.xml', 'data/pending/20150828_0130_Orbile_Daily.xml', '2015-08-27 21:04:08', '2015-08-27 21:04:18', '4265', '4265', null);
INSERT INTO `catalog_feed_file` VALUES ('390', '20150828_0530_Orbile_Daily.xml', 'data/pending/20150828_0530_Orbile_Daily.xml', '2015-08-28 01:04:08', '2015-08-28 01:04:21', '3517', '3517', null);
INSERT INTO `catalog_feed_file` VALUES ('391', '20150828_0930_Orbile_Daily.xml', 'data/pending/20150828_0930_Orbile_Daily.xml', '2015-08-28 05:04:09', '2015-08-28 05:04:36', '7383', '7383', null);
INSERT INTO `catalog_feed_file` VALUES ('392', '20150828_1330_Orbile_Daily.xml', 'data/pending/20150828_1330_Orbile_Daily.xml', '2015-08-28 09:04:10', '2015-08-28 09:04:54', '15778', '15778', null);
INSERT INTO `catalog_feed_file` VALUES ('393', '20150828_1730_Orbile_Daily.xml', 'data/pending/20150828_1730_Orbile_Daily.xml', '2015-08-28 13:04:10', '2015-08-28 13:04:53', '11198', '11198', null);
INSERT INTO `catalog_feed_file` VALUES ('394', '20150828_2130_Orbile_Daily.xml', 'data/pending/20150828_2130_Orbile_Daily.xml', '2015-08-28 17:04:11', '2015-08-28 17:04:22', '4319', '4319', null);
INSERT INTO `catalog_feed_file` VALUES ('395', '20150829_0130_Orbile_Daily.xml', 'data/pending/20150829_0130_Orbile_Daily.xml', '2015-08-28 21:04:10', '2015-08-28 21:04:18', '2069', '2069', null);
INSERT INTO `catalog_feed_file` VALUES ('396', '20150829_0530_Orbile_Daily.xml', 'data/pending/20150829_0530_Orbile_Daily.xml', '2015-08-29 01:04:11', '2015-08-29 01:04:15', '2167', '2167', null);
INSERT INTO `catalog_feed_file` VALUES ('397', '20150829_0930_Orbile_Daily.xml', 'data/pending/20150829_0930_Orbile_Daily.xml', '2015-08-29 05:04:10', '2015-08-29 05:04:20', '3912', '3912', null);
INSERT INTO `catalog_feed_file` VALUES ('398', '20150829_1330_Orbile_Daily.xml', 'data/pending/20150829_1330_Orbile_Daily.xml', '2015-08-29 09:04:11', '2015-08-29 09:04:16', '2049', '2049', null);
INSERT INTO `catalog_feed_file` VALUES ('399', '20150829_1730_Orbile_Daily.xml', 'data/pending/20150829_1730_Orbile_Daily.xml', '2015-08-29 13:04:11', '2015-08-29 13:04:12', '543', '543', null);
INSERT INTO `catalog_feed_file` VALUES ('400', '20150829_2130_Orbile_Daily.xml', 'data/pending/20150829_2130_Orbile_Daily.xml', '2015-08-29 17:04:11', '2015-08-29 17:04:25', '5797', '5797', null);
INSERT INTO `catalog_feed_file` VALUES ('401', '20150830_0130_Orbile_Daily.xml', 'data/pending/20150830_0130_Orbile_Daily.xml', '2015-08-29 21:04:12', '2015-08-29 21:04:25', '4839', '4839', null);
INSERT INTO `catalog_feed_file` VALUES ('402', '20150830_0530_Orbile_Daily.xml', 'data/pending/20150830_0530_Orbile_Daily.xml', '2015-08-30 01:04:12', '2015-08-30 01:04:37', '7727', '7727', null);
INSERT INTO `catalog_feed_file` VALUES ('403', '20150830_0930_Orbile_Daily.xml', 'data/pending/20150830_0930_Orbile_Daily.xml', '2015-08-30 05:04:10', '2015-08-30 05:04:11', '173', '173', null);
INSERT INTO `catalog_feed_file` VALUES ('404', '20150830_1330_Orbile_Daily.xml', 'data/pending/20150830_1330_Orbile_Daily.xml', '2015-08-30 09:04:13', '2015-08-30 09:04:24', '3722', '3722', null);
INSERT INTO `catalog_feed_file` VALUES ('405', '20150830_1730_Orbile_Daily.xml', 'data/pending/20150830_1730_Orbile_Daily.xml', '2015-08-30 13:04:12', '2015-08-30 13:04:13', '301', '301', null);
INSERT INTO `catalog_feed_file` VALUES ('406', '20150830_2130_Orbile_Daily.xml', 'data/pending/20150830_2130_Orbile_Daily.xml', '2015-08-30 17:04:12', '2015-08-30 17:04:13', '313', '313', null);
INSERT INTO `catalog_feed_file` VALUES ('407', '20150831_0130_Orbile_Daily.xml', 'data/pending/20150831_0130_Orbile_Daily.xml', '2015-08-30 21:04:13', '2015-08-30 21:04:33', '6937', '6937', null);
INSERT INTO `catalog_feed_file` VALUES ('408', '20150831_0530_Orbile_Daily.xml', 'data/pending/20150831_0530_Orbile_Daily.xml', '2015-08-31 01:04:13', '2015-08-31 01:04:18', '2012', '2012', null);
INSERT INTO `catalog_feed_file` VALUES ('409', '20150831_0930_Orbile_Daily.xml', 'data/pending/20150831_0930_Orbile_Daily.xml', '2015-08-31 05:04:12', '2015-08-31 05:04:50', '10430', '10430', null);
INSERT INTO `catalog_feed_file` VALUES ('410', '20150831_1330_Orbile_Daily.xml', 'data/pending/20150831_1330_Orbile_Daily.xml', '2015-08-31 09:04:15', '2015-08-31 09:04:50', '11301', '11301', null);

-- ----------------------------
-- Table structure for customers
-- ----------------------------
DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `customer_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` int(11) DEFAULT NULL,
  `kobo_id` varchar(40) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `result` varchar(20) DEFAULT NULL,
  `register_date` date DEFAULT NULL,
  `partner_customer_id` varchar(70) DEFAULT NULL,
  `orbile_customer_id` varchar(40) DEFAULT NULL,
  `account_type` char(1) DEFAULT NULL,
  PRIMARY KEY (`customer_id`)
) ENGINE=InnoDB AUTO_INCREMENT=514 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of customers
-- ----------------------------
INSERT INTO `customers` VALUES ('429', '26', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', 'aserrano@porrua.com', 'Success', '2015-08-26', '1234533d534das', 'c1408222-4048-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('430', '26', '2de7cc26-e969-47f5-ba5d-d21b8693c272', 'rrendon@gmail.com', 'Success', '2015-08-26', '12345', '5294ba55-4c01-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('431', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', 'goku@orbile.com', 'Success', '2015-08-27', '12345', 'c86d5c81-4c05-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('432', '26', '00000000-0000-0000-0000-000000000000', 'valv29@gmail.com', 'DuplicateEmailCannot', '2015-08-27', '12345', '0be6a979-40cd-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('433', '30', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', 'goku@orbile.com', 'Success', '2015-08-27', 'ORB123', 'c86d5c81-4c05-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('434', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', 'vegeta@orbile.com', 'Success', '2015-08-26', 'dragonballsuper', 'e5545f8e-4c0d-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('435', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', 'aserrano@porrua.com', 'Success', '2015-08-28', '12345', 'c1408222-4048-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('436', '26', '00000000-0000-0000-0000-000000000000', 'jorgeesteban@pengostores.com', 'DuplicateEmailCannot', '2015-08-26', '123', '717da323-4c20-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('437', '26', '0ee484be-5511-4943-a682-3881b844b04c', 'jorgeesteban 1@pengostores.com', 'Success', '2015-08-27', '437039', '9d8ecd38-4c30-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('438', '28', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', 'goku@orbile.com', 'Success', '2015-08-27', 'other', 'c86d5c81-4c05-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('439', '27', '8f6ffef3-7280-4d75-a374-ba38075b3776', 'somtestvictor@otro.com', 'Success', '2015-08-27', 'testvictor3', '652ccb25-4cc5-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('440', '26', '7b3f3fd1-81a3-4f66-be7c-616f7e6448ba', 'rebe_rs2@hotmail.com', 'Success', '2015-08-28', '12345', 'e8e02117-405e-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('441', '26', '97e559e6-f8b6-4bbe-97fc-7b79aae0f823', 'rebe_rs@hotmail.com', 'Success', '2015-08-27', '12345', '2a0d52af-4cc9-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('442', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', 'alberto_asr@hotmail.com', 'Success', '2015-08-27', '1c69d944d5f622857b9ae350df4a7f3a', '672266c3-4051-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('443', '26', '00000000-0000-0000-0000-000000000000', 'rebecarsk6@gmail.com', 'DuplicateEmailCannot', '2015-08-27', '12345', '2fff0908-4cdd-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('444', '26', 'a006e83f-171a-43e1-b58f-bd8c02fe3ef8', 'rebecarsk62@gmail.com', 'Success', '2015-08-27', '12345', '5f43d719-4cdd-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('445', '26', 'fb0ece0c-8ae2-4daf-b77d-51fcb8b76af4', 'lexcastest@gmail.com', 'Success', '2015-08-28', '437027', '765c3bc9-4d98-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('446', '27', '7b3f3fd1-81a3-4f66-be7c-616f7e6448ba', 'rebe_rs2@hotmail.com', 'Success', '2015-08-28', '12345', 'e8e02117-405e-11e5-840a-12778e705135', 'S');
INSERT INTO `customers` VALUES ('447', '26', '00000000-0000-0000-0000-000000000000', 'eduardogarcia@pengostores.com', 'DuplicateEmailCannot', '2015-08-28', '336464', '76716e7c-4ddd-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('448', '26', '00000000-0000-0000-0000-000000000000', 'montiel.daniel88@gmail.com', 'DuplicateEmailCannot', '2015-08-28', '336492', '45bbd0b0-4de1-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('449', '26', '2271f24b-bf61-4509-b67b-8bff41f7fc2d', 'dmontiel@pengostores.com', 'Success', '2015-08-28', '336493', '1f6e206f-4de2-11e5-840a-12778e705135', 'P');
INSERT INTO `customers` VALUES ('513', '27', '4635b877-bc1e-4b3e-a67e-5345d9fbf361', 'links@gmail.com', 'Success', '2015-09-23', 'links', '29b9f992-6220-11e5-840a-12778e705135', 'P');

-- ----------------------------
-- Table structure for dia
-- ----------------------------
DROP TABLE IF EXISTS `dia`;
CREATE TABLE `dia` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hora` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dia
-- ----------------------------
INSERT INTO `dia` VALUES ('1', '0');
INSERT INTO `dia` VALUES ('2', '1');
INSERT INTO `dia` VALUES ('3', '2');
INSERT INTO `dia` VALUES ('4', '3');
INSERT INTO `dia` VALUES ('5', '4');
INSERT INTO `dia` VALUES ('6', '5');
INSERT INTO `dia` VALUES ('7', '6');
INSERT INTO `dia` VALUES ('8', '7');
INSERT INTO `dia` VALUES ('9', '8');
INSERT INTO `dia` VALUES ('10', '9');
INSERT INTO `dia` VALUES ('11', '10');
INSERT INTO `dia` VALUES ('12', '11');
INSERT INTO `dia` VALUES ('13', '12');
INSERT INTO `dia` VALUES ('14', '13');
INSERT INTO `dia` VALUES ('15', '14');
INSERT INTO `dia` VALUES ('16', '15');
INSERT INTO `dia` VALUES ('17', '16');
INSERT INTO `dia` VALUES ('18', '17');
INSERT INTO `dia` VALUES ('19', '18');
INSERT INTO `dia` VALUES ('20', '19');
INSERT INTO `dia` VALUES ('21', '20');
INSERT INTO `dia` VALUES ('22', '21');
INSERT INTO `dia` VALUES ('23', '22');
INSERT INTO `dia` VALUES ('24', '23');

-- ----------------------------
-- Table structure for mes
-- ----------------------------
DROP TABLE IF EXISTS `mes`;
CREATE TABLE `mes` (
  `dia` int(11) NOT NULL AUTO_INCREMENT,
  `mes` varchar(2) NOT NULL,
  PRIMARY KEY (`dia`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mes
-- ----------------------------
INSERT INTO `mes` VALUES ('1', '8');
INSERT INTO `mes` VALUES ('2', '8');
INSERT INTO `mes` VALUES ('3', '8');
INSERT INTO `mes` VALUES ('4', '8');
INSERT INTO `mes` VALUES ('5', '8');
INSERT INTO `mes` VALUES ('6', '8');
INSERT INTO `mes` VALUES ('7', '8');
INSERT INTO `mes` VALUES ('8', '8');
INSERT INTO `mes` VALUES ('9', '8');
INSERT INTO `mes` VALUES ('10', '8');
INSERT INTO `mes` VALUES ('11', '8');
INSERT INTO `mes` VALUES ('12', '8');
INSERT INTO `mes` VALUES ('13', '8');
INSERT INTO `mes` VALUES ('14', '8');
INSERT INTO `mes` VALUES ('15', '8');
INSERT INTO `mes` VALUES ('16', '8');
INSERT INTO `mes` VALUES ('17', '8');
INSERT INTO `mes` VALUES ('18', '8');
INSERT INTO `mes` VALUES ('19', '8');
INSERT INTO `mes` VALUES ('20', '8');
INSERT INTO `mes` VALUES ('21', '8');
INSERT INTO `mes` VALUES ('22', '8');
INSERT INTO `mes` VALUES ('23', '8');
INSERT INTO `mes` VALUES ('24', '8');
INSERT INTO `mes` VALUES ('25', '8');
INSERT INTO `mes` VALUES ('26', '8');
INSERT INTO `mes` VALUES ('27', '8');
INSERT INTO `mes` VALUES ('28', '8');
INSERT INTO `mes` VALUES ('29', '8');
INSERT INTO `mes` VALUES ('30', '8');
INSERT INTO `mes` VALUES ('31', '8');

-- ----------------------------
-- Table structure for notificationcatalogues
-- ----------------------------
DROP TABLE IF EXISTS `notificationcatalogues`;
CREATE TABLE `notificationcatalogues` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT,
  `date_notification` date NOT NULL,
  `user_email` varchar(50) NOT NULL,
  `partner_id` int(11) NOT NULL,
  `catalogue_id` int(11) NOT NULL,
  PRIMARY KEY (`notification_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of notificationcatalogues
-- ----------------------------
INSERT INTO `notificationcatalogues` VALUES ('1', '2015-06-04', 'ricardo@gandhi.com', '27', '1');
INSERT INTO `notificationcatalogues` VALUES ('2', '2015-06-04', 'franco@porrua.com', '28', '1');
INSERT INTO `notificationcatalogues` VALUES ('3', '2015-06-05', 'ricardo@gandhi.com', '27', '2');
INSERT INTO `notificationcatalogues` VALUES ('4', '2015-06-06', 'franco@porrua.com', '28', '3');

-- ----------------------------
-- Table structure for partners
-- ----------------------------
DROP TABLE IF EXISTS `partners`;
CREATE TABLE `partners` (
  `partner_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `email_support` varchar(20) DEFAULT NULL,
  `email_notification` varchar(20) DEFAULT NULL,
  `api_username` varchar(30) DEFAULT NULL,
  `api_pass` varchar(65) CHARACTER SET latin1 COLLATE latin1_bin DEFAULT NULL,
  `order_prefix` varchar(3) DEFAULT NULL,
  PRIMARY KEY (`partner_id`),
  UNIQUE KEY `partner_id` (`partner_id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of partners
-- ----------------------------
INSERT INTO `partners` VALUES ('26', 'Librerias Gandhi SA ', 'gandhi@mx.com', 'gandhi@mx.com', 'gandhi', '$2a$08$JiqdkYEJHy4QMYPHqClJquOuuEFg4wXCImYNxfjU4RvN5q.WpYciW', null);
INSERT INTO `partners` VALUES ('27', 'LibrerÃ­a de PorrÃºa', 'porrua@mx.com', 'porrua@mx.com', 'porrua', '$2a$08$2iZRiuBDmK6WrMFbnhyM1.Bq6Mpqj7vWHaOkc6F5OT5F43tguwdem', null);
INSERT INTO `partners` VALUES ('28', 'calabozo', 'calabozo@mx.com', 'calabozo@mx.com', 'calabozo', '$2a$08$Bj4XXDzIKJOZ8a0xLsvgjeOIHq6wRVe24Gqu4y4mO/zHBZ9drlJDG', null);
INSERT INTO `partners` VALUES ('30', 'kobo', 'kobo@gmail.com', 'kobo@gmail.com', 'kobo', '$2a$08$S77d79cslxrBLYz1f1arIecNnKjnyIgXKEbzF7fKnqgZG4YJz3wQ6', null);

-- ----------------------------
-- Table structure for purchases
-- ----------------------------
DROP TABLE IF EXISTS `purchases`;
CREATE TABLE `purchases` (
  `purchase_id` int(11) NOT NULL AUTO_INCREMENT,
  `partner_id` varchar(20) DEFAULT NULL,
  `user_id` varchar(40) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `purchase_partner_id` varchar(50) DEFAULT NULL,
  `address_city` varchar(50) DEFAULT NULL,
  `address_country` varchar(50) DEFAULT NULL,
  `address_state_provincy` varchar(50) DEFAULT NULL,
  `address_zip_postal_code` varchar(20) DEFAULT NULL,
  `payment_method` varchar(20) DEFAULT NULL,
  `result` varchar(20) DEFAULT NULL,
  `orbile_customer_id` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`purchase_id`)
) ENGINE=InnoDB AUTO_INCREMENT=314 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchases
-- ----------------------------
INSERT INTO `purchases` VALUES ('229', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-26 10:26:00', '2385uyt6', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('230', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-26 10:35:00', '266', 'MX', 'MX', 'MX', '12345', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('231', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-26 10:42:00', '', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'InternalError', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('232', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-26 10:45:00', '268', 'MX', 'MX', 'MX', '12345', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('233', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-26 10:45:00', 'fjkhsdf23042', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('234', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-26 10:48:00', '8794739738', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('235', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-26 10:49:00', '938602y78273', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('236', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-26 10:51:00', '86r75ytft5', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('237', '30', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-26 11:06:00', 'picolo1', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('238', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-26 11:10:00', 'tatatatatata', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'InternalError', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('239', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 11:18:00', 'hioj', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('240', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-26 02:25:00', 'IAA141646', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', null);
INSERT INTO `purchases` VALUES ('241', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:30:00', '654', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('242', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:31:00', '1', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('243', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:32:00', '12', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('244', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:33:00', '13', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'InternalError', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('245', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:34:00', '14', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('246', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:35:00', '15', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('247', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:36:00', '16', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('248', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:36:00', '17', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('249', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:37:00', '18', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('250', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:37:00', '19', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'InternalError', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('251', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:38:00', '20', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('252', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:39:00', '21', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('253', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:39:00', '22', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('254', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:40:00', '24', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'InternalError', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('255', '26', 'ea5940b7-e676-40a7-aed0-efd500c24e02', '2015-08-26 04:40:00', '25', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'InternalError', 'e5545f8e-4c0d-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('256', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 12:59:00', 'IAA141755', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', null);
INSERT INTO `purchases` VALUES ('257', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 01:37:00', 'IAA141773', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', null);
INSERT INTO `purchases` VALUES ('258', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 01:41:00', 'IAA141774', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', null);
INSERT INTO `purchases` VALUES ('259', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-27 09:12:00', '31416', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('260', '28', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-27 09:13:00', 'other', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('261', '26', '6117e5c4-9fde-4e17-ae93-567ad3a7fe3e', '2015-08-27 09:13:00', '3678', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'c86d5c81-4c05-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('262', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 09:58:00', 'IAA141804', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', null);
INSERT INTO `purchases` VALUES ('263', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 12:23:00', 'IAA141829', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('264', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 01:18:00', 'IAA141836', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('265', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 01:59:00', '467775665', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'InternalError', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('266', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 04:00:00', 'IAA141861', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('267', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 04:23:00', 'IAA14186109', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('268', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 04:26:00', 'IAA4186109', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('269', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 04:27:00', 'IA4186109', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('270', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 04:29:00', 'IA418609', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('271', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 04:37:00', 'Ip418609', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('272', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 04:43:00', 'Ip41809', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('273', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 04:44:00', 'Ip4109', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('274', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 04:59:00', 'Ip409', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('275', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 05:04:00', 'I409', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('276', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 05:09:00', 'I4090909', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('277', '26', '7b3f3fd1-81a3-4f66-be7c-616f7e6448ba', '2015-08-27 05:09:00', 'das2e3e3da', 'MX', 'MX', 'MX', '06100', 'CREDITO', 'Success', 'e8e02117-405e-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('278', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 05:13:00', 'I409099', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('279', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 05:14:00', 'I40999', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('280', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 05:53:00', 'o8zfus', 'Mexico', 'MX', 'Mexico', '55200', null, 'InternalError', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('281', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 05:58:00', '5tkjtk', 'Mexico', 'MX', 'Mexico', '55200', null, 'InternalError', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('282', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 05:58:00', '96s7rz', 'Mexico', 'MX', 'Mexico', '55200', null, 'InternalError', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('283', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 06:01:00', 'z512k6', 'Mexico', 'MX', 'Mexico', '55200', null, 'InternalError', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('284', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 06:02:00', '9i0ary', 'Mexico', 'MX', 'Mexico', '55200', null, 'Success', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('285', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 06:14:00', 'IAA141888', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('286', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 06:26:00', 'I4090089', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('287', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 06:29:00', 'I40089', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('288', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 06:36:00', 'I4089', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('289', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 06:43:00', 'f6jvnd', 'Mexico', 'MX', 'Mexico', '55200', null, 'InternalError', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('290', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 06:43:00', 'dg3m5n', 'Mexico', 'MX', 'Mexico', '55200', null, 'InternalError', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('291', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 06:43:00', 'n05i8j', 'Mexico', 'MX', 'Mexico', '55200', null, 'InternalError', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('292', '27', 'b56df979-9967-423d-8a9d-835da2da14d3', '2015-08-27 06:43:00', 'vd3r3n', 'Mexico', 'MX', 'Mexico', '55200', null, 'InternalError', '672266c3-4051-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('293', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-27 06:54:00', 'I408997', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('294', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 07:49:00', 'I4087', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('295', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 07:54:00', 'I400387', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('296', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 08:28:00', 'I40037', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('297', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 08:42:00', 'I90037', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('298', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 08:46:00', 'I9037', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('299', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 08:48:00', 'I937', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('300', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 08:50:00', 'I9937', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('301', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 08:52:00', 'I37', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('302', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 08:55:00', 'I397', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('303', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 09:00:00', 'I399000007', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('304', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 09:08:00', 'I39900007', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('305', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 09:09:00', 'IAA141977', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('306', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 09:14:00', 'IAA141986', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('307', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 09:27:00', 'IAA141991', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('308', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 09:33:00', 'IAA141996', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('309', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 09:42:00', 'I399007', 'Mexico', 'MX', 'Mexico', '55060', null, 'InternalError', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('310', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 09:44:00', 'IAA142000', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('311', '27', '3d04dff4-f8fa-4c2c-9015-86ac7313856c', '2015-08-28 10:32:00', 'IAA142010', 'Mexico', 'MX', 'Mexico', '55060', null, 'Success', 'c1408222-4048-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('312', '27', '7b3f3fd1-81a3-4f66-be7c-616f7e6448ba', '2015-08-28 02:36:00', 'IAA142061', 'Mexico', 'MX', 'Mexico', '09730', null, 'Success', 'e8e02117-405e-11e5-840a-12778e705135');
INSERT INTO `purchases` VALUES ('313', '26', '2271f24b-bf61-4509-b67b-8bff41f7fc2d', '2015-08-28 07:15:00', '000907193', 'WW', 'WW', 'Distrito Federal', '07300', null, 'Success', '1f6e206f-4de2-11e5-840a-12778e705135');

-- ----------------------------
-- Table structure for purchases_line
-- ----------------------------
DROP TABLE IF EXISTS `purchases_line`;
CREATE TABLE `purchases_line` (
  `line_id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_id` varchar(30) DEFAULT NULL,
  `result` varchar(30) DEFAULT NULL,
  `product_id` varchar(50) DEFAULT NULL,
  `cogs_amount` varchar(50) DEFAULT NULL,
  `cogs_currency_code` varchar(50) DEFAULT NULL,
  `price_currency_code` varchar(50) DEFAULT NULL,
  `price_discount_applied` varchar(50) DEFAULT NULL,
  `price_before_tax` varchar(50) DEFAULT NULL,
  `item_id` varchar(50) DEFAULT NULL,
  `download_url` varchar(500) DEFAULT NULL,
  `content_format` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`line_id`)
) ENGINE=InnoDB AUTO_INCREMENT=473 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of purchases_line
-- ----------------------------
INSERT INTO `purchases_line` VALUES ('320', '229', 'Success', '5850a195-213a-4ad5-8cd6-ff084534f7d1', '111', 'MXN', 'MXN', '0', '111', '5850a195-213a-4ad5-8cd6-ff084534f7d1', null, null);
INSERT INTO `purchases_line` VALUES ('321', '230', 'Success', '9b06f1e9-edf3-4dc5-a604-0be9d1921034', '111', 'MXN', 'MXN', '0', '111', '9b06f1e9-edf3-4dc5-a604-0be9d1921034', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=9b06f1e9-edf3-4dc5-a604-0be9d1921034&dlFormat=5&dlExpiry=635762006784019458&dlToken=1291f7594d37d56649d95a08c3085569de607d2d', 'EPUB');
INSERT INTO `purchases_line` VALUES ('322', '231', 'InternalError', '52e49cc9-c95e-46bf-a492-ce3c68460be8', '111', 'MXN', 'MXN', '0', '111', '52e49cc9-c95e-46bf-a492-ce3c68460be8', null, null);
INSERT INTO `purchases_line` VALUES ('323', '231', 'InternalError', '4c922232-3ff5-4964-9580-00fca7d509a6', '222', 'MXN', 'MXN', '0', '222', '4c922232-3ff5-4964-9580-00fca7d509a6', null, null);
INSERT INTO `purchases_line` VALUES ('324', '232', 'Success', '035ea331-896d-41d2-9b68-c39988d82eae', '111', 'MXN', 'MXN', '0', '111', '035ea331-896d-41d2-9b68-c39988d82eae', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=035ea331-896d-41d2-9b68-c39988d82eae&dlFormat=5&dlExpiry=635762012615700920&dlToken=f05f624c46a62be5e2a33a148c5fb7cfcf2eebc1', 'EPUB');
INSERT INTO `purchases_line` VALUES ('325', '233', 'Success', '2f97f5c4-c39a-4731-80b3-0f89c12e0b8d', '111', 'MXN', 'MXN', '0', '111', '2f97f5c4-c39a-4731-80b3-0f89c12e0b8d', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=2f97f5c4-c39a-4731-80b3-0f89c12e0b8d&dlFormat=5&dlExpiry=635762012638824884&dlToken=cd3a89fe0c70308b6b3b5287b0c98f5474d5c083', 'EPUB');
INSERT INTO `purchases_line` VALUES ('326', '233', 'Success', '26d4dce9-bcba-41e5-ad11-0fb9f293a40f', '222', 'MXN', 'MXN', '0', '222', '26d4dce9-bcba-41e5-ad11-0fb9f293a40f', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=26d4dce9-bcba-41e5-ad11-0fb9f293a40f&dlFormat=5&dlExpiry=635762012638824884&dlToken=95bcecb6f2c1f2f54cf90efc16ae737f8b643532', 'EPUB');
INSERT INTO `purchases_line` VALUES ('327', '234', 'Success', '391be90e-f671-4b82-aa00-fe16b85f3a23', '111', 'MXN', 'MXN', '0', '111', '391be90e-f671-4b82-aa00-fe16b85f3a23', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=391be90e-f671-4b82-aa00-fe16b85f3a23&dlFormat=5&dlExpiry=635762014671653528&dlToken=2c01547965cdc3ddb6e7ec6cd03b4fd7e1f2a68b', 'EPUB');
INSERT INTO `purchases_line` VALUES ('328', '234', 'Success', '614f52bb-c051-4df7-86b1-fd0686ec858d', '222', 'MXN', 'MXN', '0', '222', '614f52bb-c051-4df7-86b1-fd0686ec858d', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=614f52bb-c051-4df7-86b1-fd0686ec858d&dlFormat=5&dlExpiry=635762014671653528&dlToken=ed84695553b28d177bd44cd7dcc66f06d8138b8c', 'EPUB');
INSERT INTO `purchases_line` VALUES ('329', '235', 'Success', 'b61b669b-8483-45ea-bc69-fc7c8d3f97a6', '111', 'MXN', 'MXN', '0', '111', 'b61b669b-8483-45ea-bc69-fc7c8d3f97a6', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=b61b669b-8483-45ea-bc69-fc7c8d3f97a6&dlFormat=5&dlExpiry=635762015290970346&dlToken=131a952cc774558c5bb48f4fc6de627700d156f6', 'EPUB');
INSERT INTO `purchases_line` VALUES ('330', '235', 'Success', 'ba30ac9c-5766-42fa-af8d-fc3620dabacd', '222', 'MXN', 'MXN', '0', '222', 'ba30ac9c-5766-42fa-af8d-fc3620dabacd', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=ba30ac9c-5766-42fa-af8d-fc3620dabacd&dlFormat=5&dlExpiry=635762015290970346&dlToken=2802b861212c49dca54393bfacf12c3bab75491f', 'EPUB');
INSERT INTO `purchases_line` VALUES ('331', '236', 'Success', '72cc1a72-df9c-4a54-8440-fbb7e616a760', '111', 'MXN', 'MXN', '0', '111', '72cc1a72-df9c-4a54-8440-fbb7e616a760', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=72cc1a72-df9c-4a54-8440-fbb7e616a760&dlFormat=5&dlExpiry=635762016150076038&dlToken=ef94562e4f81b5e658f364f6d11d910e7f57c77b', 'EPUB');
INSERT INTO `purchases_line` VALUES ('332', '236', 'Success', '09839f19-f94f-46be-a1a0-fba1a5154ebb', '222', 'MXN', 'MXN', '0', '222', '09839f19-f94f-46be-a1a0-fba1a5154ebb', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=09839f19-f94f-46be-a1a0-fba1a5154ebb&dlFormat=5&dlExpiry=635762016150076038&dlToken=59d2f8da836c24fd7b2e0a6a38f489699202e3bf', 'EPUB');
INSERT INTO `purchases_line` VALUES ('333', '237', 'Success', '03d6d735-e999-4529-8293-b7022dfae5db', '111', 'MXN', 'MXN', '0', '111', '03d6d735-e999-4529-8293-b7022dfae5db', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=03d6d735-e999-4529-8293-b7022dfae5db&dlFormat=5&dlExpiry=635762025152576038&dlToken=e91938f9e8fbb21c843b8f93896084699f125892', 'EPUB');
INSERT INTO `purchases_line` VALUES ('334', '238', 'ContentNotFound', '64299cc3-e620-4b9c-a642-1b77b8289552', '111', 'MXN', 'MXN', '0', '111', '64299cc3-e620-4b9c-a642-1b77b8289552', null, null);
INSERT INTO `purchases_line` VALUES ('335', '239', 'Success', '06c2beb8-0eb1-400e-9325-a16cf811bff7', '111', 'MXN', 'MXN', '0', '111', '06c2beb8-0eb1-400e-9325-a16cf811bff7', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=06c2beb8-0eb1-400e-9325-a16cf811bff7&dlFormat=5&dlExpiry=635762032488669788&dlToken=38000872e73ff478ed0ab0d947c9381a5e9c3c02', 'EPUB');
INSERT INTO `purchases_line` VALUES ('336', '240', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635762144954985338&dlToken=c6a52de40bb0b5c5482df79ecee1985fae551210', 'EPUB');
INSERT INTO `purchases_line` VALUES ('337', '240', 'Success', 'ef6fa7a6-a137-40bd-a2e7-6b932c4b8cd3', '105', 'MXN', 'MXN', null, '1', 'ef6fa7a6-a137-40bd-a2e7-6b932c4b8cd3', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=ef6fa7a6-a137-40bd-a2e7-6b932c4b8cd3&dlFormat=5&dlExpiry=635762144954985338&dlToken=53ea8fb69d51ffa43d3657911925bb5f273489cb', 'EPUB');
INSERT INTO `purchases_line` VALUES ('338', '241', 'Success', '7bf492a2-6aa1-4bfd-a600-fef6f1a3708e', '111', 'MXN', 'MXN', '0', '111', '7bf492a2-6aa1-4bfd-a600-fef6f1a3708e', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=7bf492a2-6aa1-4bfd-a600-fef6f1a3708e&dlFormat=5&dlExpiry=635762219923607170&dlToken=270ceb6b1c9cccff0ef8c8715524bd06a523417e', 'EPUB');
INSERT INTO `purchases_line` VALUES ('339', '241', 'Success', 'b0935ed8-4842-403d-8bc1-fe4261f4f9b5', '222', 'MXN', 'MXN', '0', '222', 'b0935ed8-4842-403d-8bc1-fe4261f4f9b5', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=b0935ed8-4842-403d-8bc1-fe4261f4f9b5&dlFormat=5&dlExpiry=635762219923607170&dlToken=7e98d2646199a6d34b8276898cbfdb4b93af1da8', 'EPUB');
INSERT INTO `purchases_line` VALUES ('340', '242', 'Success', '67a0af04-3eee-4a02-bd6a-f745d8a12b48', '111', 'MXN', 'MXN', '0', '111', '67a0af04-3eee-4a02-bd6a-f745d8a12b48', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=67a0af04-3eee-4a02-bd6a-f745d8a12b48&dlFormat=5&dlExpiry=635762220509116888&dlToken=59d59fd885d725f6767908a37ebbd48089b7d8ff', 'EPUB');
INSERT INTO `purchases_line` VALUES ('341', '242', 'Success', '8c41fd24-4f58-4a8c-bf00-f7162bb07d68', '222', 'MXN', 'MXN', '0', '222', '8c41fd24-4f58-4a8c-bf00-f7162bb07d68', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=8c41fd24-4f58-4a8c-bf00-f7162bb07d68&dlFormat=5&dlExpiry=635762220509116888&dlToken=fc67cf81fe67404f8b61fb158ed2585734238ea1', 'EPUB');
INSERT INTO `purchases_line` VALUES ('342', '243', 'Success', '4d665732-5d76-464f-9aac-f713031bcdad', '111', 'MXN', 'MXN', '0', '111', '4d665732-5d76-464f-9aac-f713031bcdad', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=4d665732-5d76-464f-9aac-f713031bcdad&dlFormat=5&dlExpiry=635762220890634838&dlToken=e1803fa057f76c2692c245c75a7e6cbcbe43474a', 'EPUB');
INSERT INTO `purchases_line` VALUES ('343', '243', 'Success', '0db57d47-247c-41c1-8192-f6793bd8cc33', '222', 'MXN', 'MXN', '0', '222', '0db57d47-247c-41c1-8192-f6793bd8cc33', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=0db57d47-247c-41c1-8192-f6793bd8cc33&dlFormat=5&dlExpiry=635762220890634838&dlToken=a882ba8e465bbccb37ade587e91915223f90a281', 'EPUB');
INSERT INTO `purchases_line` VALUES ('344', '244', 'ContentNotFound', 'd9ef9233-dc38-4115-93d2-f668567fe1b2', '111', 'MXN', 'MXN', '0', '111', 'd9ef9233-dc38-4115-93d2-f668567fe1b2', null, null);
INSERT INTO `purchases_line` VALUES ('345', '244', 'Success', '56b87512-0010-4ada-81c8-f63d07c103bf', '222', 'MXN', 'MXN', '0', '222', '56b87512-0010-4ada-81c8-f63d07c103bf', null, null);
INSERT INTO `purchases_line` VALUES ('346', '245', 'Success', '30d9d109-b15b-491f-a7c9-f62647bb5231', '111', 'MXN', 'MXN', '0', '111', '30d9d109-b15b-491f-a7c9-f62647bb5231', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=30d9d109-b15b-491f-a7c9-f62647bb5231&dlFormat=5&dlExpiry=635762222022509838&dlToken=f6f5047364df1107914f8491bbd86289753ea195', 'EPUB');
INSERT INTO `purchases_line` VALUES ('347', '245', 'Success', '6dfd8611-3712-4ffc-9a24-f5e485602d6a', '222', 'MXN', 'MXN', '0', '222', '6dfd8611-3712-4ffc-9a24-f5e485602d6a', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=6dfd8611-3712-4ffc-9a24-f5e485602d6a&dlFormat=5&dlExpiry=635762222022509838&dlToken=abee0668596c31e35304812362c9b312fa4c9514', 'EPUB');
INSERT INTO `purchases_line` VALUES ('348', '246', 'Success', 'e412cce0-cdc0-4eb1-a3ac-f327168f2a8e', '111', 'MXN', 'MXN', '0', '111', 'e412cce0-cdc0-4eb1-a3ac-f327168f2a8e', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=e412cce0-cdc0-4eb1-a3ac-f327168f2a8e&dlFormat=5&dlExpiry=635762222696900946&dlToken=a11eb5a9f3ed75c0d49273dc9db45f701391b77a', 'EPUB');
INSERT INTO `purchases_line` VALUES ('349', '246', 'Success', 'efa05969-dc14-45f5-9bad-f2b457be7ef0', '222', 'MXN', 'MXN', '0', '222', 'efa05969-dc14-45f5-9bad-f2b457be7ef0', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=efa05969-dc14-45f5-9bad-f2b457be7ef0&dlFormat=5&dlExpiry=635762222696900946&dlToken=cf0433d3a6433fedebe58a0966dc65cee9dff407', 'EPUB');
INSERT INTO `purchases_line` VALUES ('350', '247', 'Success', '7ea3f680-c6b0-4d8f-9748-f0d00646214f', '111', 'MXN', 'MXN', '0', '111', '7ea3f680-c6b0-4d8f-9748-f0d00646214f', null, null);
INSERT INTO `purchases_line` VALUES ('351', '247', 'Success', '167a1637-6ddb-4e43-a8d9-f01dda6a0fce', '222', 'MXN', 'MXN', '0', '222', '167a1637-6ddb-4e43-a8d9-f01dda6a0fce', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=167a1637-6ddb-4e43-a8d9-f01dda6a0fce&dlFormat=5&dlExpiry=635762223220009838&dlToken=80af675dc442c1dc4415a7eb56059a9b22eef548', 'EPUB');
INSERT INTO `purchases_line` VALUES ('352', '248', 'Success', '16735c22-8edb-448c-b87c-efab4f80342d', '111', 'MXN', 'MXN', '0', '111', '16735c22-8edb-448c-b87c-efab4f80342d', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=16735c22-8edb-448c-b87c-efab4f80342d&dlFormat=5&dlExpiry=635762223529541088&dlToken=865b66e0bea196009eecfff197650585900aae57', 'EPUB');
INSERT INTO `purchases_line` VALUES ('353', '248', 'Success', '05070aa6-66f2-455f-874a-ef2b9e76c0df', '222', 'MXN', 'MXN', '0', '222', '05070aa6-66f2-455f-874a-ef2b9e76c0df', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=05070aa6-66f2-455f-874a-ef2b9e76c0df&dlFormat=5&dlExpiry=635762223529541088&dlToken=26bbcd76ef2d773ef9a5f013fe126f776bbea36b', 'EPUB');
INSERT INTO `purchases_line` VALUES ('354', '249', 'Success', 'bbfaec42-b300-4a40-a1ec-ef17c6f2a908', '111', 'MXN', 'MXN', '0', '111', 'bbfaec42-b300-4a40-a1ec-ef17c6f2a908', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=bbfaec42-b300-4a40-a1ec-ef17c6f2a908&dlFormat=5&dlExpiry=635762223834541088&dlToken=76a157418d13b1bbf334a0d3b202bc49f7445bad', 'EPUB');
INSERT INTO `purchases_line` VALUES ('355', '249', 'Success', 'b709d9dc-96fd-4c70-bb66-eee42e5b9649', '222', 'MXN', 'MXN', '0', '222', 'b709d9dc-96fd-4c70-bb66-eee42e5b9649', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=b709d9dc-96fd-4c70-bb66-eee42e5b9649&dlFormat=5&dlExpiry=635762223834697338&dlToken=394ccab8ab21fa2374f519b5a246b42fa94d3a8c', 'EPUB');
INSERT INTO `purchases_line` VALUES ('356', '250', 'ContentNotFound', '377b72c6-2354-43d4-ac00-ee7dc5a19069', '111', 'MXN', 'MXN', '0', '111', '377b72c6-2354-43d4-ac00-ee7dc5a19069', null, null);
INSERT INTO `purchases_line` VALUES ('357', '250', 'Success', '7486d668-7aa2-4d09-bb6f-ee630e45a292', '222', 'MXN', 'MXN', '0', '222', '7486d668-7aa2-4d09-bb6f-ee630e45a292', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=7486d668-7aa2-4d09-bb6f-ee630e45a292&dlFormat=5&dlExpiry=635762224145167583&dlToken=01c209bc250466b41fc571a380c2cb1d45223956', 'EPUB');
INSERT INTO `purchases_line` VALUES ('358', '251', 'Success', '016b3681-cb13-48f3-b292-ee50a73a3f57', '111', 'MXN', 'MXN', '0', '111', '016b3681-cb13-48f3-b292-ee50a73a3f57', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=016b3681-cb13-48f3-b292-ee50a73a3f57&dlFormat=5&dlExpiry=635762224469228009&dlToken=08ea99e58e2dab9ce6140713eeb3eea8f8d08378', 'EPUB');
INSERT INTO `purchases_line` VALUES ('359', '251', 'Success', '3fe22525-dc79-4bd8-aa4a-edcaee968903', '222', 'MXN', 'MXN', '0', '222', '3fe22525-dc79-4bd8-aa4a-edcaee968903', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=3fe22525-dc79-4bd8-aa4a-edcaee968903&dlFormat=5&dlExpiry=635762224469228009&dlToken=261a5db2d83f7e834e626d7ee8d201f8ef0ef549', 'EPUB');
INSERT INTO `purchases_line` VALUES ('360', '252', 'Success', '9db2f793-0923-4c1a-9fba-ec07f0b72ce6', '111', 'MXN', 'MXN', '0', '111', '9db2f793-0923-4c1a-9fba-ec07f0b72ce6', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=9db2f793-0923-4c1a-9fba-ec07f0b72ce6&dlFormat=5&dlExpiry=635762224963603588&dlToken=87283427573e6f697920b5e174c32de2c80027f6', 'EPUB');
INSERT INTO `purchases_line` VALUES ('361', '252', 'Success', 'ebc5334b-0884-4abd-b6c8-ebcec6b9a0a3', '222', 'MXN', 'MXN', '0', '222', 'ebc5334b-0884-4abd-b6c8-ebcec6b9a0a3', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=ebc5334b-0884-4abd-b6c8-ebcec6b9a0a3&dlFormat=5&dlExpiry=635762224963603588&dlToken=9615105e0a34ccbf31dc85f2991bb50141d65c07', 'EPUB');
INSERT INTO `purchases_line` VALUES ('362', '253', 'Success', '7e81c01d-819e-4955-9fc1-ebaa9d1c487c', '111', 'MXN', 'MXN', '0', '111', '7e81c01d-819e-4955-9fc1-ebaa9d1c487c', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=7e81c01d-819e-4955-9fc1-ebaa9d1c487c&dlFormat=5&dlExpiry=635762225236879346&dlToken=48d954304ade42262cd22e654040debed4f4bc8e', 'EPUB');
INSERT INTO `purchases_line` VALUES ('363', '253', 'Success', 'dfbd9c89-fe47-49e9-a7e5-e9f54499b048', '222', 'MXN', 'MXN', '0', '222', 'dfbd9c89-fe47-49e9-a7e5-e9f54499b048', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=dfbd9c89-fe47-49e9-a7e5-e9f54499b048&dlFormat=5&dlExpiry=635762225236879346&dlToken=bf0c7bc93bb0a6c3ddc8972f23eb03d65ead5ca2', 'EPUB');
INSERT INTO `purchases_line` VALUES ('364', '254', 'ContentNotAllowedInGeo', 'bbd5363f-32ce-4d20-9eb1-e90cc3eb7984', '111', 'MXN', 'MXN', '0', '111', 'bbd5363f-32ce-4d20-9eb1-e90cc3eb7984', null, null);
INSERT INTO `purchases_line` VALUES ('365', '254', 'Success', '589d8f99-b302-43f4-917d-e9036ede9516', '222', 'MXN', 'MXN', '0', '222', '589d8f99-b302-43f4-917d-e9036ede9516', null, null);
INSERT INTO `purchases_line` VALUES ('366', '255', 'ContentNotFound', 'bcb02386-87a4-472e-bba8-e8d29643cb69', '111', 'MXN', 'MXN', '0', '111', 'bcb02386-87a4-472e-bba8-e8d29643cb69', null, null);
INSERT INTO `purchases_line` VALUES ('367', '255', 'Success', '1d66f8d3-ba2b-42ad-97f2-e8a130cb5877', '222', 'MXN', 'MXN', '0', '222', '1d66f8d3-ba2b-42ad-97f2-e8a130cb5877', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=ea5940b7-e676-40a7-aed0-efd500c24e02&dlVolumeId=1d66f8d3-ba2b-42ad-97f2-e8a130cb5877&dlFormat=5&dlExpiry=635762225805634838&dlToken=f4e4a09d842169ae3fa93220470b3216eb53b1f5', 'EPUB');
INSERT INTO `purchases_line` VALUES ('368', '256', 'Success', '903c1ba5-5a1a-45c2-bc57-220b5c42fe86', '117', 'MXN', 'MXN', null, '1', '903c1ba5-5a1a-45c2-bc57-220b5c42fe86', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=903c1ba5-5a1a-45c2-bc57-220b5c42fe86&dlFormat=5&dlExpiry=635762524883501638&dlToken=157d0a8d726e089597c8654f06e8a3c733911596', 'EPUB');
INSERT INTO `purchases_line` VALUES ('369', '256', 'Success', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('370', '257', 'Success', '06c2beb8-0eb1-400e-9325-a16cf811bff7', '105', 'MXN', 'MXN', null, '1', '06c2beb8-0eb1-400e-9325-a16cf811bff7', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=06c2beb8-0eb1-400e-9325-a16cf811bff7&dlFormat=5&dlExpiry=635762548037381092&dlToken=728fef4ab4ee46f63ac89c9deab004b31ad30d75', 'EPUB');
INSERT INTO `purchases_line` VALUES ('371', '258', 'Success', '00de7118-9526-40d9-8347-108792eeacc4', '105', 'MXN', 'MXN', null, '1', '00de7118-9526-40d9-8347-108792eeacc4', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=00de7118-9526-40d9-8347-108792eeacc4&dlFormat=5&dlExpiry=635762550609567982&dlToken=0c372f2fbad6ff2fa13beba0881a14b4346d0a49', 'EPUB');
INSERT INTO `purchases_line` VALUES ('372', '258', 'Success', 'e00df1d3-fa03-4bba-a702-b181242c5538', '299', 'MXN', 'MXN', null, '1', 'e00df1d3-fa03-4bba-a702-b181242c5538', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=e00df1d3-fa03-4bba-a702-b181242c5538&dlFormat=5&dlExpiry=635762550609567982&dlToken=21599463db1613ddb7eb876d95be581b81954616', 'EPUB');
INSERT INTO `purchases_line` VALUES ('373', '259', 'Success', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', '111', 'MXN', 'MXN', '0', '111', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', null, null);
INSERT INTO `purchases_line` VALUES ('374', '260', 'Success', '187477bb-d0c3-4f98-8715-00058f080dcc', '111', 'MXN', 'MXN', '0', '111', '187477bb-d0c3-4f98-8715-00058f080dcc', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=187477bb-d0c3-4f98-8715-00058f080dcc&dlFormat=5&dlExpiry=635762821552539054&dlToken=744f51f45e0163830b3b63033e8dd0dd141a354e', 'EPUB');
INSERT INTO `purchases_line` VALUES ('375', '261', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '111', 'MXN', 'MXN', '0', '111', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=6117e5c4-9fde-4e17-ae93-567ad3a7fe3e&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635762821562810250&dlToken=bcc16bcb23c588b4141a259152a6b0c9db314e67', 'EPUB');
INSERT INTO `purchases_line` VALUES ('376', '262', 'Success', 'ecdfd37a-7595-411e-9153-93ebfa6dec92', '117', 'MXN', 'MXN', null, '1', 'ecdfd37a-7595-411e-9153-93ebfa6dec92', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=ecdfd37a-7595-411e-9153-93ebfa6dec92&dlFormat=5&dlExpiry=635762848599695799&dlToken=e329aeaafca406574fb22272a9fb381ef184fc65', 'EPUB');
INSERT INTO `purchases_line` VALUES ('377', '262', 'Success', '9c35408a-87aa-42cb-b919-0d89de247fc2', '124', 'MXN', 'MXN', null, '1', '9c35408a-87aa-42cb-b919-0d89de247fc2', null, null);
INSERT INTO `purchases_line` VALUES ('378', '263', 'Success', 'ecdfd37a-7595-411e-9153-93ebfa6dec92', '117', 'MXN', 'MXN', null, '1', 'ecdfd37a-7595-411e-9153-93ebfa6dec92', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=ecdfd37a-7595-411e-9153-93ebfa6dec92&dlFormat=5&dlExpiry=635762935710669100&dlToken=4835bd1da8c78e775071cd0137824bcb106b16db', 'EPUB');
INSERT INTO `purchases_line` VALUES ('379', '263', 'Success', '903c1ba5-5a1a-45c2-bc57-220b5c42fe86', '117', 'MXN', 'MXN', null, '1', '903c1ba5-5a1a-45c2-bc57-220b5c42fe86', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=903c1ba5-5a1a-45c2-bc57-220b5c42fe86&dlFormat=5&dlExpiry=635762935710669100&dlToken=71953f0a94c686ee96158e9e35b966c43435e1a2', 'EPUB');
INSERT INTO `purchases_line` VALUES ('380', '264', 'Success', 'ecdfd37a-7595-411e-9153-93ebfa6dec92', '117', 'MXN', 'MXN', null, '1', 'ecdfd37a-7595-411e-9153-93ebfa6dec92', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=ecdfd37a-7595-411e-9153-93ebfa6dec92&dlFormat=5&dlExpiry=635762968636169355&dlToken=fa33e2c0de1149c959dc6b7f50b142d747d5f8a9', 'EPUB');
INSERT INTO `purchases_line` VALUES ('381', '265', 'Success', '4aeecfdd-cd5b-43d3-9f5f-b201d5eccfd8', '111', 'MXN', 'MXN', '0', '111', '4aeecfdd-cd5b-43d3-9f5f-b201d5eccfd8', null, null);
INSERT INTO `purchases_line` VALUES ('382', '265', 'ContentNotFound', '4aeecfdd-cd5b-43d3-9f5f-b00000000000', '222', 'MXN', 'MXN', '0', '222', '4aeecfdd-cd5b-43d3-9f5f-b00000000000', null, null);
INSERT INTO `purchases_line` VALUES ('383', '266', 'Success', '00de7118-9526-40d9-8347-108792eeacc4', '105', 'MXN', 'MXN', null, '1', '00de7118-9526-40d9-8347-108792eeacc4', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=00de7118-9526-40d9-8347-108792eeacc4&dlFormat=5&dlExpiry=635763065795244050&dlToken=a56ef13c9001560ca64f5dd68457b86e09c0d842', 'EPUB');
INSERT INTO `purchases_line` VALUES ('384', '267', 'ContentAlreadyOwned', '00de7118-9526-40d9-8347-108792eeacc4', '105', 'MXN', 'MXN', null, '1', '00de7118-9526-40d9-8347-108792eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('385', '268', 'ContentAlreadyOwned', '00de7118-9526-40d9-8347-108792eeacc4', '105', 'MXN', 'MXN', null, '1', '00de7118-9526-40d9-8347-108792eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('386', '269', 'ContentAlreadyOwned', '00de7118-9526-40d9-8347-108792eeacc4', '105', 'MXN', 'MXN', null, '1', '00de7118-9526-40d9-8347-108792eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('387', '270', 'ContentAlreadyOwned', '00de7118-9526-40d9-8347-108792eeacc4', '105', 'MXN', 'MXN', null, '1', '00de7118-9526-40d9-8347-108792eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('388', '271', 'ContentNotFound', '00de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '00de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('389', '272', 'ContentNotFound', '00de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '00de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('390', '273', 'ContentNotFound', '00de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '00de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('391', '274', 'Success', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('392', '275', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('393', '275', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('394', '276', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('395', '276', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('396', '277', 'Success', '52e49cc9-c95e-46bf-a492-ce3c68460be8', '111', 'MXN', 'MXN', '0', '111', '52e49cc9-c95e-46bf-a492-ce3c68460be8', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=7b3f3fd1-81a3-4f66-be7c-616f7e6448ba&dlVolumeId=52e49cc9-c95e-46bf-a492-ce3c68460be8&dlFormat=5&dlExpiry=635763107407749350&dlToken=7ea82771176e265e9a13f3dc0b1bd04cc80dc60e', 'EPUB');
INSERT INTO `purchases_line` VALUES ('397', '278', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('398', '278', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763109560874350&dlToken=2ea1310c5c4aa51734ef77d56d36c0a6548d3ee1', 'EPUB');
INSERT INTO `purchases_line` VALUES ('399', '279', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('400', '279', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('401', '279', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763109945249350&dlToken=2ade76e4c8d7207fbaddef92104610eb07566b01', 'EPUB');
INSERT INTO `purchases_line` VALUES ('402', '280', 'ContentNotFound', '12349cc9-c95e-46bf-a492-123c68460123', '1', 'USD', 'USD', '1', '1', '12349cc9-c95e-46bf-a492-123c68460123', null, null);
INSERT INTO `purchases_line` VALUES ('403', '281', 'ContentAlreadyOwned', 'ecdfd37a-7595-411e-9153-93ebfa6dec92', '1', 'USD', 'USD', '1', '1', 'ecdfd37a-7595-411e-9153-93ebfa6dec92', null, null);
INSERT INTO `purchases_line` VALUES ('404', '282', 'ContentAlreadyOwned', 'ecdfd37a-7595-411e-9153-93ebfa6dec92', '1', 'USD', 'USD', '1', '1', 'ecdfd37a-7595-411e-9153-93ebfa6dec92', null, null);
INSERT INTO `purchases_line` VALUES ('405', '283', 'ContentAlreadyOwned', 'ca39b088-b291-48a0-919e-48bb4bba972f', '1', 'USD', 'USD', '1', '1', 'ca39b088-b291-48a0-919e-48bb4bba972f', null, null);
INSERT INTO `purchases_line` VALUES ('406', '284', 'Success', '03d6d735-e999-4529-8293-b7022dfae5db', '1', 'USD', 'USD', '1', '1', '03d6d735-e999-4529-8293-b7022dfae5db', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=03d6d735-e999-4529-8293-b7022dfae5db&dlFormat=5&dlExpiry=635763138681870651&dlToken=086e9e17fbad59f22e22d1e1a02ff59b9c6758ef', 'EPUB');
INSERT INTO `purchases_line` VALUES ('407', '285', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763146173072707&dlToken=169e2bce9cc28c4833a73c6fa825d19e7b81f7d4', 'EPUB');
INSERT INTO `purchases_line` VALUES ('408', '285', 'Success', '29b64971-a833-4ece-be8d-c36415ee0dca', '39', 'MXN', 'MXN', null, '1', '29b64971-a833-4ece-be8d-c36415ee0dca', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=b56df979-9967-423d-8a9d-835da2da14d3&dlVolumeId=29b64971-a833-4ece-be8d-c36415ee0dca&dlFormat=5&dlExpiry=635763146173072707&dlToken=9fd871591ff9482e7aedeb85e9b8f397825bbc82', 'EPUB');
INSERT INTO `purchases_line` VALUES ('409', '285', 'Success', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', '82', 'MXN', 'MXN', null, '1', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', null, null);
INSERT INTO `purchases_line` VALUES ('410', '286', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('411', '286', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('412', '286', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763153226811850&dlToken=a646fa628a866f36e0b8c5ad51472115c35fb70a', 'EPUB');
INSERT INTO `purchases_line` VALUES ('413', '287', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('414', '287', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('415', '287', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763155356138935&dlToken=2b13776ee857ae2c89a0f1e89177ebd4886efc38', 'EPUB');
INSERT INTO `purchases_line` VALUES ('416', '288', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('417', '288', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('418', '288', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763159435833631&dlToken=1d51be4311ab6bbe880394a73c4d7d7da87d40c5', 'EPUB');
INSERT INTO `purchases_line` VALUES ('419', '289', 'ContentAlreadyOwned', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', '1', 'USD', 'USD', '1', '1', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', null, null);
INSERT INTO `purchases_line` VALUES ('420', '290', 'ContentAlreadyOwned', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', '1', 'USD', 'USD', '1', '1', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', null, null);
INSERT INTO `purchases_line` VALUES ('421', '291', 'ContentAlreadyOwned', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', '1', 'USD', 'USD', '1', '1', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', null, null);
INSERT INTO `purchases_line` VALUES ('422', '292', 'ContentAlreadyOwned', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', '1', 'USD', 'USD', '1', '1', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', null, null);
INSERT INTO `purchases_line` VALUES ('423', '293', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('424', '293', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('425', '293', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763170266578950&dlToken=439219c057bec23c4ddb5598f77fe3adaded0e11', 'EPUB');
INSERT INTO `purchases_line` VALUES ('426', '294', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('427', '294', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('428', '294', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763634886318131&dlToken=f492f2fc04efaf3680ae434a3b8bb11a9d7b4e2c', 'EPUB');
INSERT INTO `purchases_line` VALUES ('429', '295', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('430', '295', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('431', '295', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763638192099381&dlToken=0365988ac539000fcac065ecf519b63e8dbcde1d', 'EPUB');
INSERT INTO `purchases_line` VALUES ('432', '296', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('433', '296', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('434', '296', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763658274658853&dlToken=c27cbcf928d57164ac05aac7c1bd1845bd188cee', 'EPUB');
INSERT INTO `purchases_line` VALUES ('435', '297', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('436', '297', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('437', '297', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763667215094803&dlToken=a8a0f543e837e2e371a9b8e56fc886f573746712', 'EPUB');
INSERT INTO `purchases_line` VALUES ('438', '298', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('439', '298', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('440', '298', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763669316811381&dlToken=7cefcbf1b8c01df7c8b2eacf1f20d8e4699d1367', 'EPUB');
INSERT INTO `purchases_line` VALUES ('441', '299', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('442', '299', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('443', '299', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763670486776442&dlToken=ac8c21cc9b82df840c0da51c2ce998cfd7e644a7', 'EPUB');
INSERT INTO `purchases_line` VALUES ('444', '300', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('445', '300', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('446', '300', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763671525825642&dlToken=04dd7a3b431b995718650b0d29cf15c4f8e8f6d0', 'EPUB');
INSERT INTO `purchases_line` VALUES ('447', '301', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('448', '301', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('449', '301', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763672953463618&dlToken=8e5f7c9d86091f9057c9194f62839c919b9f5a96', 'EPUB');
INSERT INTO `purchases_line` VALUES ('450', '302', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('451', '302', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('452', '302', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763674735561381&dlToken=c0a228f1d50222d2e55512679e403afd8ace7bf6', 'EPUB');
INSERT INTO `purchases_line` VALUES ('453', '303', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('454', '303', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('455', '303', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763677715717631&dlToken=1203bf299d2e30b454224f27cdf3adfc705aec51', 'EPUB');
INSERT INTO `purchases_line` VALUES ('456', '304', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('457', '304', 'ContentAlreadyOwned', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('458', '305', 'Success', 'ef8024fe-eda1-4ccb-a64c-c68582866009', '180', 'MXN', 'MXN', null, '1', 'ef8024fe-eda1-4ccb-a64c-c68582866009', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=ef8024fe-eda1-4ccb-a64c-c68582866009&dlFormat=5&dlExpiry=635763683369580290&dlToken=2de11e79724b6fa4d522ada01bc08c77afb8dbda', 'EPUB');
INSERT INTO `purchases_line` VALUES ('459', '306', 'Success', '9f8f63cd-df69-4984-962e-383ef0d0edfc', '105', 'MXN', 'MXN', null, '1', '9f8f63cd-df69-4984-962e-383ef0d0edfc', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=9f8f63cd-df69-4984-962e-383ef0d0edfc&dlFormat=5&dlExpiry=635763686296811381&dlToken=467d31436e25649261f7ca3dc884ef543fed43a9', 'EPUB');
INSERT INTO `purchases_line` VALUES ('460', '306', 'Success', 'ef6fa7a6-a137-40bd-a2e7-6b932c4b8cd3', '105', 'MXN', 'MXN', null, '1', 'ef6fa7a6-a137-40bd-a2e7-6b932c4b8cd3', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=ef6fa7a6-a137-40bd-a2e7-6b932c4b8cd3&dlFormat=5&dlExpiry=635763686297436381&dlToken=743337c5192a054e2e64a0197658759ff3bdf60f', 'EPUB');
INSERT INTO `purchases_line` VALUES ('461', '307', 'Success', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', '82', 'MXN', 'MXN', null, '1', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', null, null);
INSERT INTO `purchases_line` VALUES ('462', '308', 'Success', '9c35408a-87aa-42cb-b919-0d89de247fc2', '124', 'MXN', 'MXN', null, '1', '9c35408a-87aa-42cb-b919-0d89de247fc2', null, null);
INSERT INTO `purchases_line` VALUES ('463', '309', 'ContentNotFound', '09de7118-9526-40d9-8347-108902eeacc4', '105', 'MXN', 'MXN', null, '1', '09de7118-9526-40d9-8347-108902eeacc4', null, null);
INSERT INTO `purchases_line` VALUES ('464', '309', 'Success', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('465', '310', 'Success', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('466', '311', 'Success', '703e392d-5a42-4776-801b-508ec9a7a36f', '177', 'MXN', 'MXN', null, '1', '703e392d-5a42-4776-801b-508ec9a7a36f', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=703e392d-5a42-4776-801b-508ec9a7a36f&dlFormat=5&dlExpiry=635763733109155131&dlToken=0128599d321eac200aff18e89daae92bd500fea9', 'EPUB');
INSERT INTO `purchases_line` VALUES ('467', '311', 'Success', '23d149a5-9f28-4a41-891d-39b2b2030f21', '188', 'MXN', 'MXN', null, '1', '23d149a5-9f28-4a41-891d-39b2b2030f21', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=3d04dff4-f8fa-4c2c-9015-86ac7313856c&dlVolumeId=23d149a5-9f28-4a41-891d-39b2b2030f21&dlFormat=5&dlExpiry=635763733109155131&dlToken=618ec21cba17bdcab3dc57b5a959bc24ca231399', 'EPUB');
INSERT INTO `purchases_line` VALUES ('468', '312', 'Success', '488e5a15-b1b4-4784-a8f3-155594df2e07', '139', 'MXN', 'MXN', null, '1', '488e5a15-b1b4-4784-a8f3-155594df2e07', null, null);
INSERT INTO `purchases_line` VALUES ('469', '312', 'Success', '334df458-9dcd-488f-9ed2-a0a5184b7b3a', '177', 'MXN', 'MXN', null, '1', '334df458-9dcd-488f-9ed2-a0a5184b7b3a', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=7b3f3fd1-81a3-4f66-be7c-616f7e6448ba&dlVolumeId=334df458-9dcd-488f-9ed2-a0a5184b7b3a&dlFormat=5&dlExpiry=635763879630651431&dlToken=aaf6f8c1417b281a53a8197da69f1198d240215e', 'EPUB');
INSERT INTO `purchases_line` VALUES ('470', '312', 'Success', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', '82', 'MXN', 'MXN', null, '1', 'edbc2ccb-4d81-4c16-85e7-1ace927b0989', null, null);
INSERT INTO `purchases_line` VALUES ('471', '312', 'Success', '9c35408a-87aa-42cb-b919-0d89de247fc2', '124', 'MXN', 'MXN', null, '1', '9c35408a-87aa-42cb-b919-0d89de247fc2', null, null);
INSERT INTO `purchases_line` VALUES ('472', '313', 'Success', 'b022094f-50de-44a6-914a-0ef4b5c495a4', '130', 'USD', 'USD', null, '130', 'b022094f-50de-44a6-914a-0ef4b5c495a4', 'http://perftc.kobobooks.com/handlers/ContentDownload.ashx?dlUserId=2271f24b-bf61-4509-b67b-8bff41f7fc2d&dlVolumeId=b022094f-50de-44a6-914a-0ef4b5c495a4&dlFormat=5&dlExpiry=635764046928318281&dlToken=3babc9b2a707a594b195abb71e23ae2b4f168093', 'EPUB');

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `role_id` int(11) NOT NULL,
  `role_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', 'admin');
INSERT INTO `roles` VALUES ('2', 'partner');
INSERT INTO `roles` VALUES ('3', 'affiliate');

-- ----------------------------
-- Table structure for semana
-- ----------------------------
DROP TABLE IF EXISTS `semana`;
CREATE TABLE `semana` (
  `dia` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_dia` varchar(9) NOT NULL,
  PRIMARY KEY (`dia`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of semana
-- ----------------------------
INSERT INTO `semana` VALUES ('1', 'Domingo');
INSERT INTO `semana` VALUES ('2', 'Lunes');
INSERT INTO `semana` VALUES ('3', 'Martes');
INSERT INTO `semana` VALUES ('4', 'Miércoles');
INSERT INTO `semana` VALUES ('5', 'jueves');
INSERT INTO `semana` VALUES ('6', 'Viernes');
INSERT INTO `semana` VALUES ('7', 'Sábado');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(70) NOT NULL,
  `pass` varchar(70) NOT NULL,
  `name` varchar(120) NOT NULL,
  `email` varchar(70) NOT NULL,
  `created_at` date NOT NULL,
  `active` char(1) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `partner_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'admin', '$2a$08$gm33BEa8VdL6iY68SOQlZebUBMubNfr9vT0zrzA95foidrqYgFulq', 'admin', 'admin@gmail.com', '2015-06-17', 's', '1', '0');
INSERT INTO `users` VALUES ('2', 'ricardo', '$2a$08$qwJkv6xLwO2S2Q14sF1AceZOdC2h0iS7mRveqyg2Yl/3MrkSt.jA2', 'ricardo', 'ricardo@gandhi.com', '2015-06-17', 's', '2', '27');
INSERT INTO `users` VALUES ('3', 'franco', '$2a$08$IC7GHAetNQ8Eie3XuwHeuOvYBFAadhYS6LpkGLP.fCwTL5Hjddare', 'franco', 'franco@porrua.com', '2015-06-17', 's', '2', '28');
INSERT INTO `users` VALUES ('4', 'pedro', '$2a$08$UnnPmYg8DaMUET3TwOUDmeECT2NRZqAywSSYFIzFxbQRAT.xU3WUq', 'pedro', 'pedro@porrua.com', '2015-06-25', 's', '2', '27');
INSERT INTO `users` VALUES ('5', 'pepe', '$2a$08$Ez3mQ9RkWdLhYg6U4JisWuZG6W7QqekElFy.c9ZA4xtoRlpd/MTLG', 'pepe', 'pepe@gmail.com', '2015-08-24', 's', '2', '27');
INSERT INTO `users` VALUES ('6', 'toÃ±o', '$2a$08$1ybNtSAiWTKrbzGOqWylb.iPUs3zVZ28XeaSPaY6LcBYO9pNjJ6pu', 'toÃ±o', 'toÃ±o@gmail.com', '2015-08-24', 's', '2', '27');

-- ----------------------------
-- View structure for changestatusbook
-- ----------------------------
DROP VIEW IF EXISTS `changestatusbook`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `changestatusbook` AS select `pl`.`line_id` AS `line_id`,`p`.`purchase_id` AS `purchase_id`,`pl`.`purchase_id` AS `purchase_line_id`,`p`.`user_id` AS `user_id`,`pl`.`product_id` AS `product_id` from (`purchases` `p` join `purchases_line` `pl` on((`p`.`purchase_id` = `pl`.`purchase_id`))) ;

-- ----------------------------
-- View structure for estasem
-- ----------------------------
DROP VIEW IF EXISTS `estasem`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `estasem` AS select `purchases`.`partner_id` AS `partner_id`,dayofweek(`purchases`.`date`) AS `dia_semana`,count(0) AS `num_ventas` from `purchases` where ((year(`purchases`.`date`) = (select year(now()))) and (week(`purchases`.`date`,0) = (select week(now(),0)))) group by `purchases`.`partner_id`,dayofweek(`purchases`.`date`) ;

-- ----------------------------
-- View structure for esteanio
-- ----------------------------
DROP VIEW IF EXISTS `esteanio`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `esteanio` AS select `purchases`.`partner_id` AS `partner_id`,month(`purchases`.`date`) AS `mes`,count(0) AS `num_ventas` from `purchases` where (year(`purchases`.`date`) = (select year(now()))) group by month(`purchases`.`date`),`purchases`.`partner_id` order by `purchases`.`partner_id`,month(`purchases`.`date`) ;

-- ----------------------------
-- View structure for estedia
-- ----------------------------
DROP VIEW IF EXISTS `estedia`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `estedia` AS select `purchases`.`partner_id` AS `partner_id`,hour(`purchases`.`date`) AS `hora`,count(0) AS `num_ventas` from `purchases` where ((year(`purchases`.`date`) = (select year(now()))) and (dayofmonth(`purchases`.`date`) = (select dayofmonth(now()))) and (month(`purchases`.`date`) = (select month(now())))) group by `purchases`.`partner_id`,hour(`purchases`.`date`) ;

-- ----------------------------
-- View structure for estemes
-- ----------------------------
DROP VIEW IF EXISTS `estemes`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `estemes` AS select `purchases`.`partner_id` AS `partner_id`,dayofmonth(`purchases`.`date`) AS `dia`,count(0) AS `num_ventas` from `purchases` where ((year(`purchases`.`date`) = (select year(now()))) and (month(`purchases`.`date`) = (select month(now())))) group by `purchases`.`partner_id`,dayofmonth(`purchases`.`date`) ;

-- ----------------------------
-- View structure for usuariosanio
-- ----------------------------
DROP VIEW IF EXISTS `usuariosanio`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `usuariosanio` AS select `users`.`partner_id` AS `partner_id`,month(`users`.`created_at`) AS `mes`,count(0) AS `num_ventas` from `users` where (year(`users`.`created_at`) = (select year(now()))) group by month(`users`.`created_at`),`users`.`partner_id` order by `users`.`partner_id`,month(`users`.`created_at`) ;

-- ----------------------------
-- View structure for usuariosdia
-- ----------------------------
DROP VIEW IF EXISTS `usuariosdia`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `usuariosdia` AS select `users`.`partner_id` AS `partner_id`,hour(`users`.`created_at`) AS `hora`,count(0) AS `num_ventas` from `users` where ((year(`users`.`created_at`) = (select year(now()))) and (dayofmonth(`users`.`created_at`) = (select dayofmonth(now()))) and (month(`users`.`created_at`) = (select month(now())))) group by `users`.`partner_id`,hour(`users`.`created_at`) ;

-- ----------------------------
-- View structure for usuariosmes
-- ----------------------------
DROP VIEW IF EXISTS `usuariosmes`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `usuariosmes` AS select `users`.`partner_id` AS `partner_id`,dayofmonth(`users`.`created_at`) AS `dia`,count(0) AS `num_ventas` from `users` where ((year(`users`.`created_at`) = (select year(now()))) and (month(`users`.`created_at`) = (select month(now())))) group by `users`.`partner_id`,dayofmonth(`users`.`created_at`) ;

-- ----------------------------
-- View structure for usuariossemana
-- ----------------------------
DROP VIEW IF EXISTS `usuariossemana`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER  VIEW `usuariossemana` AS select `users`.`partner_id` AS `partner_id`,dayofweek(`users`.`created_at`) AS `dia_semana`,count(0) AS `num_ventas` from `users` where ((year(`users`.`created_at`) = (select year(now()))) and (week(`users`.`created_at`,0) = (select week(now(),0)))) group by `users`.`partner_id`,dayofweek(`users`.`created_at`) ;
